package com.trupti.foodnetbuyer

import io.flutter.app.FlutterApplication
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.ContentResolver
import android.content.Context
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build

import androidx.multidex.MultiDex

class Application : FlutterApplication() {
    override fun onCreate() {
        super.onCreate()
        createNotificationChannel()
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val sound: Uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE.toString() + "://" + getPackageName() + "/" + R.raw.mixkit_bell) //Here is FILE_NAME is the name of file that you want to play
            val audioAttributes: AudioAttributes = AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build()
            val channelHigh = NotificationChannel(
                    "FoodNetBuyer_id",
                    "FoodNetBuyerNotificationChannel",
                    NotificationManager.IMPORTANCE_HIGH
            )
            channelHigh.enableLights(true);
            channelHigh.enableVibration(true);
            channelHigh.setSound(sound, audioAttributes);
            val manager = getSystemService(NotificationManager::class.java)
            manager.createNotificationChannel(channelHigh)
        }
    }
}