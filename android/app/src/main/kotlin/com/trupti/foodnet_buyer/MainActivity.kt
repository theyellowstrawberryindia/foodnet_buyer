package com.trupti.foodnetbuyer

import androidx.annotation.NonNull
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant


class MainActivity: FlutterActivity() {

    companion object {
        private const val APP_VERSION_CHANNEL = "com.trupti.foodnetbuyer/appversion"
    }

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        setAppVersionChannel(flutterEngine)
    }

    private fun setAppVersionChannel(@NonNull flutterEngine: FlutterEngine) {
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, APP_VERSION_CHANNEL).setMethodCallHandler {
            call, result ->
            when {
                call.method == "getAppVersionName" -> result.success(BuildConfig.VERSION_NAME)
                call.method == "getAppVersionCode" -> result.success(BuildConfig.VERSION_CODE)
                else -> result.notImplemented()
            }
        }

    }
}
