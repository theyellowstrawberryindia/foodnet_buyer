import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {

    private let APP_VERSION_CHANNEL = "com.trupti.foodnetbuyer/appversion"

  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GeneratedPluginRegistrant.register(with: self)
    
    if #available(iOS 10.0, *) {
      UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
    }

    let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
        registerAppVersionChannel(controller: controller, channelName: APP_VERSION_CHANNEL)

    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}

private func registerAppVersionChannel(controller: FlutterViewController, channelName: String) {
    let appVersionChannel = FlutterMethodChannel(name: channelName, binaryMessenger: controller.binaryMessenger)
    appVersionChannel.setMethodCallHandler({
      (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
        if call.method == "getAppVersionName" {
            result(Bundle.main.buildVersionName)
        } else if call.method == "getAppVersionCode" {
            result(Int(Bundle.main.buildVersionNumber ?? "1"))
        } else {
            result(FlutterMethodNotImplemented)
            return
        }
    })
}

extension Bundle {
    var buildVersionName: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}
