class KeyName {
  static const String MOBILE_NO = 'MOBILE_NO';
  static const String EMAIL = 'EMAIL';
  static const String TOKEN = 'TOKEN';
  static const String FIRST_NAME = 'FIRST_NAME';
  static const String LAST_NAME = 'LAST_NAME';
  static const String OTP = 'OTP';
  static const String IS_LOGIN = 'IS_LOGIN';
  static const String ADDRESS_PICKED = 'addressPicked';
  static const String ACCESS_TOKEN = 'ACCESS_TOKEN';
  static const String REFRESH_TOKEN = 'REFRESH_TOKEN';
  static const String IS_LOGGED_IN = 'loggedIn';
  static const String SHOW_NOTIFICATION_BADGE = 'showNotificationBadge';

  static const String LATITUDE = "latitude";
  static const String LONGITUDE = "longitude";
  static const String ADDRESS = "address";
  static const String LOCATION_DATA = "locationData";
  static const String CART_ID = "cartId";
  static const String PARTY_ID = "partyId";
  static const String BUYER_ID = "buyerId";
  static const String PARTY_LOCATION_ID = "partyLocationId";
  static const String CART_RESPONSE = "cartResponse";
  static const String USER_ID = "userId";
  static const String PARTY_CODE = "partyCode";
  static const String ASSOCIATE_CART = "shouldAssociateCart";
  static const String USER_ADDRESS = "user_address";
  static const String USER_SELECTED_ADDRESS = "user_selected_address";
  static const String USER_PARTY_ID = "userPartyId";
  static const String ADDRESS_ID = "addressId";

  static const String STORE_CODE = "storeCode";
  static const String STORE_ID = "storeId";
}
