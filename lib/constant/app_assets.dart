class AppAssets {
  AppAssets._();

  static String get check {
    return '✓';
  }

  static String get appLogo {
    return 'assets/images/logo.png';
  }

  static String get cart {
    return 'assets/images/cart.png';
  }

  static String get menu {
    return 'assets/images/menu.png';
  }

  static String get location {
    return 'assets/images/location.png';
  }

  static String get notification {
    return 'assets/images/notification.png';
  }

  static String get add {
    return 'assets/images/add.png';
  }

  static String get user {
    return 'assets/images/person.png';
  }

  static String get dashboard {
    return 'assets/images/dashboard.png';
  }

  static String get order {
    return 'assets/images/order.png';
  }

  static String get home {
    return 'assets/images/home.png';
  }

  static String get category {
    return 'assets/images/category.png';
  }

  static String get categoryActive {
    return 'assets/images/category_filled.png';
  }

  static String get microphone {
    return 'assets/images/microphone.png';
  }

  static String get raw {
    return 'assets/images/raw.png';
  }

  static String get packed {
    return 'assets/images/packed.png';
  }

  static String get freshies {
    return 'assets/images/freshies.png';
  }

  static String get groceries {
    return 'assets/images/groceries.png';
  }

  static String get downArrow {
    return 'assets/images/down_arrow.png';
  }

  static String get downArrowFilled {
    return 'assets/images/down_arrow_filled.png';
  }

  static String get upArrow {
    return 'assets/images/up_arrow.png';
  }

  static String get locationDialogImage {
    return 'assets/images/location_img.png';
  }

  static String get incrementCounter {
    return 'assets/images/increment_counter.png';
  }

  static String get decrementCounter {
    return 'assets/images/decrement_counter.png';
  }

  static String get couponCode {
    return 'assets/images/coupon_code.png';
  }

  static String get orderPlaced {
    return 'assets/images/order_placed.png';
  }

  static String get search {
    return 'assets/images/search.png';
  }

  static String get topOffer {
    return 'assets/images/topoffer.png';
  }

  static String get bestSelling {
    return 'assets/images/bestselling.png';
  }

  static String get topPicks {
    return 'assets/images/toppicks.png';
  }

  static String get appLogo2 {
    return 'assets/images/app_logo.png';
  }

  static String get selectedTick {
    return 'assets/images/selected_tick.png';
  }

  static String get checked {
    return 'assets/images/checked.png';
  }

  static String get unchecked {
    return 'assets/images/unchecked.png';
  }

  static String get couponPlaceholder {
    return 'assets/images/coupon_placeholder.png';
  }

  static String get crossIcon {
    return 'assets/images/cross_icon.png';
  }

  static String get deleteIcon {
    return 'assets/images/delete_item.png';
  }

  static String get store {
    return 'assets/images/store.png';
  }

  static String get scanner {
    return 'assets/images/scanner.png';
  }

  static String get seller {
    return 'assets/images/seller_person.png';
  }

  static String get dashboardIcon {
    return 'assets/images/dashboard_icon.png';
  }

  static String get registerPlaceHolder {
    return 'assets/images/registerplaceholder.png';
  }

  static String get item_out {
    return 'assets/images/item_out.png';
  }

  static String get itemin {
    return 'assets/images/itemin.png';
  }

  static String get accept {
    return 'assets/images/acept.png';
  }

  static String get due {
    return 'assets/images/due.png';
  }
}
