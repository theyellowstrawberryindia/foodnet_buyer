class AppFonts {
  AppFonts._();

  static double get textSize8 {
    return 8.0;
  }

  static double get textSize9 {
    return 9.0;
  }

  static double get textSize10 {
    return 10.0;
  }

  static double get textSize12 {
    return 12.0;
  }

  static double get textSize11 {
    return 11.0;
  }

  static double get textSize14 {
    return 14.0;
  }

  static double get textSize16 {
    return 16.0;
  }

  static double get textSize18 {
    return 18.0;
  }

  static double get textSize20 {
    return 20.0;
  }

  static double get textSize30 {
    return 30.0;
  }

  static const double btnTextSize = 14;

  static const double titleTextSize = 16;

  static const double inputTextSize = 14;

  static String fontFamilyLight = "Poppins-Light";

  static String get poppinsLight {
    return 'Poppins-Light';
  }

  static String get poppinsRegular {
    return 'Poppins-Regular';
  }

  static String get poppinsBold {
    return 'Poppins-Bold';
  }

  static String get poppinsExtraBold {
    return 'Poppins-ExtraBold';
  }
}
