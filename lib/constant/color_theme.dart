import 'package:flutter/material.dart';

class ColorTheme {
  ColorTheme._i();

  static MaterialColor get primarySwatch {
    return MaterialColor(0xffF0674C, customColor);
  }

  static Map<int, Color> customColor = {
    50: Color.fromRGBO(240, 103, 76, .1),
    100: Color.fromRGBO(240, 103, 76, .2),
    200: Color.fromRGBO(240, 103, 76, .3),
    300: Color.fromRGBO(240, 103, 76, .4),
    400: Color.fromRGBO(240, 103, 76, .5),
    500: Color.fromRGBO(240, 103, 76, .6),
    600: Color.fromRGBO(240, 103, 76, .7),
    700: Color.fromRGBO(240, 103, 76, .8),
    800: Color.fromRGBO(240, 103, 76, .9),
    900: Color.fromRGBO(240, 103, 76, 1),
  };

  static Color get primaryColor {
    return Color(0xFFF0674C);
  }

  static Color get secondaryColor {
    return Color(0xFFF0674C);
  }

  static Color get darkGrey {
    return Color(0xFF666666);
  }

  static Color get lightGrey {
    return Color(0xFFf5f5f5);
  }

  static Color get white {
    return Colors.white;
  }

  static Color get black {
    return Colors.black;
  }

  static Color get green {
    return Color(0xffADE1C2);
  }

  static Color get cardStatusGreen {
    return Color(0xff19AC53);
  }

  static Color get orderCardGreen {
    return Color(0xff19AC53);
  }

  static Color get green1 {
    return Color(0xff4CAF50);
  }

  static const Color FF00000014 = Color(0X00000014);
  static const Color FF333333 = Color(0XFF333333);
  static const Color FFF97062 = Color(0XFFF97062);
  static const Color FF999999 = Color(0XFF999999);
  static const Color transparent = Color(0X00000000);
  static const Color FF0000001A = Color(0XFF0000001A);
  static const Color FFF8F8F8 = Color(0XFFF8F8F8);
  static const Color FFFDFDFD = Color(0XFFFDFDFD);
  static const Color FFF0674C = Color(0xFFF0674C);
  static const Color FF666666 = Color(0xFF666666);
  static const Color FFE3E3E3 = Color(0xFFE3E3E3);
  static const Color FFE5E5E5 = Color(0xFFE5E5E5);
  static const Color FF050202 = Color(0xFF050202);
  static const Color FFD9D9D9 = Color(0xFFD9D9D9);
  static const Color FFF4F4F4 = Color(0xFFF4F4F4);
  static const Color FF171F2B = Color(0xFF171F2B);
  static const Color FF707070 = Color(0xFF707070);
  static const Color FFBABABA = Color(0xFFBABABA);
  static const Color FFE6E6E6 = Color(0xFFE6E6E6);

  static const List<Color> colors = [
    Color(0XFFFFE3B7),
    Color(0XFFF7DEDC),
    Color(0XFFFED2AC),
    Color(0XFFFDCFCF),
    Color(0XFFF5C8C4),
    Color(0XFFFEBC85),
    Color(0XFFFADAB5),
    Color(0XFFFECDCD),
    Color(0XFFFEA6A6),
  ];
  static const List<Color> categoryBoxColors = [
    Color(0XFFF8E3C4),
    Color(0XFFF8E3E1),
    Color(0XFFFEE1C9),
    Color(0XFFFAD5D5),
    Color(0XFFF5CECA),
    Color(0XFFFED3AF),
    Color(0XFFF6DFC5),
    Color(0XFFFEDEDE),
    Color(0XFFFEC4C4),
  ];

  static Color get red {
    return Colors.redAccent;
  }
}
