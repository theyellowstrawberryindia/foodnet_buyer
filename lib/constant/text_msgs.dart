class TextMsgs {
  static const String UNABLE_TO_PICK_LOCATION =
      "We are uanble to get your current location.";
  static const String SERVICE_NOT_AVAILABLE =
      "Service not avilable in your area.";
  static const String SERVICE_NOT_AVAILABLE_2 =
      "$SERVICE_NOT_AVAILABLE Please set a different address.";
  static const String ENABLE_GPS =
      "Please enable location service from settings";
  static const String ERROR_SAVING_LOCATION =
      "There was some error in saving your location. Please try again.";
  static const String COMMON_ERROR = "Something went wrong. Please try again.";
  static const String ADDED_TO_CART = "Item added to cart successfully";
  static const String REMOVE_PRODUCT_FROM_CART =
      "Remove item from shopping cart?";
  static const String CLEAR_CART =
      "Are you sure you want to clear your shopping cart?";
  static const String OUT_OF_STOCK = "Product out of stock";
  static const String STORE_CLOSED =
      "Store is close for the day. Please order in operational hours";
  static const String FAILED_TO_LOAD_CART = "Failed to load shopping cart";
  static const String NO_ITEM_IN_CART = "No item in cart";
  static const String NO_NOTIFICATION = "No notifications available";
  static const String FAILED_TO_LOAD_SUMMARY = "Failed to load summary";
  static const String FAILED_TO_LOAD_DATA = "Failed to load data";
  static const String UNABLE_TO_ADD_ADDRESS = "Unable to add address";
  static const String UNABLE_TO_DELETE_ADDRESS = "Unable to delete address";
  static const String ADD_ADDRESS_TO_ACCOUNT = "Add address to your account";
  static const String ALREADY_ITEMS_IN_CART =
      "You already have an active cart. Clear it and proceed?";
  static const String ERROR_IN_CART =
      "Kindly check your cart as some of the items are not be available";
  static const String ADDRESS_CANNOT_DELETE =
      "Cannot delete address as it is already associated with the current shopping cart";
  static const String STORE_CHANGE =
      "Selecting this address will clear the existing cart. Do you want to continue?";
  static const String INVALID_PIN_CODE = "Please enter a valid pin code";
  static const String ADDITIONAL_CHARGES =
      "Due to different delivery date and time, additional convinience fee will be charged for this item";
  static const String FREE_COUPON =
      "*Congratulations ! *\nYou have been awarded 5 free delivery coupons !\n\nT&C apply *";

  static const String BTN_CANCEL = "Cancel";
  static const String FAILED_CHECKOUT_API = "Failed checkout api";
  static const String FAILED_PURCHASE_API = "Failed purchase order api";

  static const String RUPEE_SYMBOL = '\u{20B9}';
}
