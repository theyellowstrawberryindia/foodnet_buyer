import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodnet_buyer/data/model/cart_response.dart';
import 'package:foodnet_buyer/data/repository/cart_repository.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';
import 'package:foodnet_buyer/util/shopping_cart_util.dart';

import 'cart_event.dart';
import 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartRepository repository = CartRepository.get();

  CartBloc() : super(FetchingDataState());

  @override
  Stream<CartState> mapEventToState(CartEvent event) async* {
    if (event is GetCartDetailEvent) {
      try {
        yield FetchingDataState();
        var response = await repository.getCart(
            PreferenceManager.getCartId(), PreferenceManager.getUserPartyId());
        if (response != null) {
          ShoppingCartUtil.get().updateCart(response);
        }
        yield CartReceivedSate(response);
      } catch (e) {
        print(e);
        AppUtils.showCustomErrorMsg(e);
        yield ApiFailedState();
      }
    } else if (event is UpdateCartEvent) {
      try {
        yield CartReceivedSate(
            event.response ?? CartResponse(shoppingcartitems: []));
      } catch (e) {
        print(e);
        yield ApiFailedState();
      }
    } else if (event is CheckoutCart) {
      try {
        var response = await repository.checkoutOrder(event.couponCode);
        yield OrderPlacedState(response);
      } catch (e) {
        print(e);
        AppUtils.showCustomErrorMsg(e);
        yield ApiFailedState();
      }
    } else if (event is GetOrderDetailEvent) {
      try {
        var response = await repository.getOrderDetail(event.orderId);
        yield OrderDetailReceivedState(response);
      } catch (e) {
        print(e);
        AppUtils.showCustomErrorMsg(e);
        yield CheckoutApiFailedState();
      }
    } else if (event is GetPurchaseOrderDetailsEvent) {
      try {
        var response = await repository.getPurchaseOrder();
        yield GetPurchaseOrderDetailsState(response);
      } catch (e) {
        print(e);
        AppUtils.showCustomErrorMsg(e);
        yield ApiFailedState();
      }
    } else if (event is GetEditOrderDetailsEvent) {
      try {
        yield FetchingDataState();
        var response = await repository.getPOCart(event.orderId);
        if (response != null) {
          ShoppingCartUtil.get().updateCart(response);
        }
        yield CartReceivedSate(response);
      } catch (e) {
        print(e);
        AppUtils.showCustomErrorMsg(e);
        yield ApiFailedState();
      }
    }
  }
}
