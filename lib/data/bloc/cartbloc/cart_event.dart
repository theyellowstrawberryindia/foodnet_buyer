import 'package:foodnet_buyer/data/model/cart_response.dart';

abstract class CartEvent {}

class LoadingEvent extends CartEvent {}

class GetCartDetailEvent extends CartEvent {
  GetCartDetailEvent();
}

class CheckoutCart extends CartEvent {
  String couponCode;
  CheckoutCart(this.couponCode);
}

class UpdateCartEvent extends CartEvent {
  CartResponse response;
  UpdateCartEvent(this.response);
}

class GetOrderDetailEvent extends CartEvent {
  String orderId;
  GetOrderDetailEvent(this.orderId);
}

class GetPurchaseOrderDetailsEvent extends CartEvent {
  GetPurchaseOrderDetailsEvent();
}

class GetEditOrderDetailsEvent extends CartEvent {
  int orderId;
  GetEditOrderDetailsEvent(this.orderId);
}
