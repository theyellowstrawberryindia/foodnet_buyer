import 'package:foodnet_buyer/data/model/OrderModel.dart';
import 'package:foodnet_buyer/data/model/cart_response.dart';

abstract class CartState {}

class FetchingDataState extends CartState {}

class CartReceivedSate extends CartState {
  CartResponse response;
  CartReceivedSate(this.response);
}

class OrderPlacedState extends CartState {
  List<CartResponse> response;
  OrderPlacedState(this.response);
}

class OrderDetailReceivedState extends CartState {
  dynamic response;
  OrderDetailReceivedState(this.response);
}

class GetPurchaseOrderDetailsState extends CartState {
  OrderModel response;
  GetPurchaseOrderDetailsState(this.response);
}

class ApiFailedState extends CartState {}

class CheckoutApiFailedState extends CartState {}
