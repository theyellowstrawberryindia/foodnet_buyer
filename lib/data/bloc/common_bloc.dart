import 'dart:async';

class CommonBloc {

  static CommonBloc _instance = CommonBloc.internal();

  CommonBloc factory() {
    return _instance;
  }

  CommonBloc.internal();

  static CommonBloc get() {
    return _instance;
  }

  StreamController<dynamic> _badgeController;
  StreamController<dynamic> _notificationBadgeController;
  StreamController<dynamic> _searchController;
  StreamController<dynamic> _homeController;
  StreamController<bool> _updateAddressList;
  List<Function> _badgeListener = [];
  List<Function> _notificationBadgeListener = [];
  List<Function> _searchListener = [];
  List<Function> _homeListener = [];

  init() {
    _badgeController = StreamController<dynamic>();
    _badgeController.stream.listen((event) {
      _badgeListener.forEach((element) {
        element?.call();
      });
    });

    _notificationBadgeController = StreamController<dynamic>();
    _notificationBadgeController.stream.listen((event) {
      _notificationBadgeListener.forEach((element) {
        element?.call();
      });
    });

    _searchController = StreamController<dynamic>();
    _searchController.stream.listen((event) {
      _searchListener.forEach((element) {
        element?.call();
      });
    });

    _homeController = StreamController<dynamic>();
    _homeController.stream.listen((event) {
      _homeListener.forEach((element) {
        element?.call();
      });
    });
  }

  updateBadge() {
    _badgeController.sink.add(true);
  }

  addBadgeListener(Function listener) {
    _badgeListener.add(listener);
  }

  removeBadgeListener(Function listener) {
    _badgeListener.remove(listener);
  }

  updateNotificationBadge() {
    _notificationBadgeController.sink.add(true);
  }

  addNotificationBadgeListener(Function listener) {
    _notificationBadgeListener.add(listener);
  }

  removeNotificationBadgeListener(Function listener) {
    _notificationBadgeListener.remove(listener);
  }

  clearSearchFocus() {
    _searchController.sink.add(true);
  }

  addSearchListener(Function listener) {
    _searchListener.add(listener);
  }

  removeSearchListener(Function listener) {
    _searchListener.remove(listener);
  }

  updateHome() {
    _homeController.sink.add(true);
  }

  addHomeListener(Function listener) {
    _homeListener.add(listener);
  }

  removeHomeListener(Function listener) {
    _homeListener.remove(listener);
  }

  createAndListen(Function(bool) listener) {
    _updateAddressList?.close();
    _updateAddressList = StreamController();
    _updateAddressList.stream.listen((event) {
      listener.call(event);
    });
  }

  refreshAddress() {
    _updateAddressList?.sink?.add(true);
  }

  closeAddressListener() {
    _updateAddressList?.close();
  }

  dispose() {
    _updateAddressList?.close();
    _notificationBadgeController?.close();
    _badgeController?.close();
    _searchController?.close();
    _homeController?.close();
    _badgeListener?.clear();
    _searchListener?.clear();
    _homeListener?.clear();
    _notificationBadgeListener?.clear();
  }
}