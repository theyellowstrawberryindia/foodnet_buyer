import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodnet_buyer/data/repository/main_reporitory.dart';
import 'package:foodnet_buyer/util/app_utils.dart';

import 'coupon_event.dart';
import 'coupon_state.dart';

class CouponBloc extends Bloc<CouponEvent, CouponState> {

  MainRepository repository = MainRepository.get();

  CouponBloc(): super(FetchingDataState());

  @override
  Stream<CouponState> mapEventToState(CouponEvent event) async* {

    if (event is GetAllCouponEvent) {
      try {
        yield FetchingDataState();
        var response = await repository.getCouponListing();
        yield AllCouponReceivedState(response);
      } catch (e) {
        print(e);
        AppUtils.showCustomErrorMsg(e);
        yield ApiFailedState();
      }
    } else if (event is GetCouponDetailEvent) {
      try {
        yield FetchingDataState();
        var response = await repository.getCouponDetails(event.id, event.fromCoupon);
        yield CouponDetailReceivedState(response);
      } catch (e) {
        print(e);
        AppUtils.showCustomErrorMsg(e);
        yield ApiFailedState();
      }
    }
  }
}