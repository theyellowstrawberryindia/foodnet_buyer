abstract class CouponEvent {}

class LoadingEvent extends CouponEvent {}

class GetAllCouponEvent extends CouponEvent {
  GetAllCouponEvent();
}

class GetCouponDetailEvent extends CouponEvent {
  String id;
  bool fromCoupon;
  GetCouponDetailEvent(this.id, this.fromCoupon);
}