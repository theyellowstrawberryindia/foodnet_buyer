import 'package:foodnet_buyer/data/model/coupon_item.dart';

abstract class CouponState {}

class FetchingDataState extends CouponState {}

class AllCouponReceivedState extends CouponState {
  List<CouponItem> items;
  AllCouponReceivedState(this.items);
}

class CouponDetailReceivedState extends CouponState {
  dynamic data;
  CouponDetailReceivedState(this.data);
}

class ApiFailedState extends CouponState {}