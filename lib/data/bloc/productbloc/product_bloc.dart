import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodnet_buyer/data/repository/main_reporitory.dart';
import 'package:foodnet_buyer/util/app_utils.dart';

import 'product_event.dart';
import 'product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {

  MainRepository repository = MainRepository.get();

  ProductBloc(): super(FetchingDataState());

  @override
  Stream<ProductState> mapEventToState(ProductEvent event) async* {

    if (event is GetAllProductOfCategoryEvent) {
      try {
        yield FetchingDataState();
        var response = await repository.getAllProductOfCategory(event.category);
        yield AllProductOfCategoryReceivedState(response);
      } catch (e) {
        print(e);
        AppUtils.showCustomErrorMsg(e);
        yield ApiFailedState();
      }
    } else if (event is GetAllProductOfSubCategoryEvent) {
      try {
        yield FetchingDataState();
        var response = await repository.getAllProductsOfSubCategory(event.categoryId, event.productCatalogId, event.isParentCategory ?? false);
        yield AllProductReceivedState(response);
      } catch (e) {
        print(e);
        AppUtils.showCustomErrorMsg(e);
        yield ApiFailedState();
      }
    } else if (event is GetProductDetailEvent) {
      try {
        yield FetchingDataState();
        var response = await repository.getProductDetails(event.productId, event.productCatalogId);
        yield ProductDetailReceivedState(response);
      } catch (e) {
        print(e);
        AppUtils.showCustomErrorMsg(e);
        yield ApiFailedState();
      }
    }
  }
}