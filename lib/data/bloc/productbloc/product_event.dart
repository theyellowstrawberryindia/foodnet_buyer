import 'package:foodnet_buyer/data/model/category_response.dart';

abstract class ProductEvent {}

class LoadingEvent extends ProductEvent {}

class GetAllProductOfCategoryEvent extends ProductEvent {
  Child category;
  String productCatalogId;
  GetAllProductOfCategoryEvent({this.category, this.productCatalogId});
}

class GetAllProductOfSubCategoryEvent extends ProductEvent {
  String categoryId, productCatalogId;
  bool isParentCategory;
  GetAllProductOfSubCategoryEvent({this.categoryId, this.productCatalogId, this.isParentCategory = false});
}

class GetProductDetailEvent extends ProductEvent {
  String productId, productCatalogId;
  GetProductDetailEvent({this.productId, this.productCatalogId});
}