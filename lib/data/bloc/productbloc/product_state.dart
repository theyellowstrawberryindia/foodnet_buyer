import 'package:foodnet_buyer/data/model/product.dart';

abstract class ProductState {}

class FetchingDataState extends ProductState {}

class AllProductReceivedState extends ProductState {
  List<Product> response;
  AllProductReceivedState(this.response);
}

class AllProductOfCategoryReceivedState extends ProductState {
  dynamic response;
  AllProductOfCategoryReceivedState(this.response);
}

class ProductDetailReceivedState extends ProductState {
  Product product;
  ProductDetailReceivedState(this.product);
}

class ApiFailedState extends ProductState {}