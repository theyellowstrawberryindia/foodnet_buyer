import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodnet_buyer/data/repository/main_reporitory.dart';
import 'package:foodnet_buyer/util/app_utils.dart';

import 'tab_event.dart';
import 'tab_state.dart';

class TabBloc extends Bloc<TabEvent, TabState> {

  MainRepository repository = MainRepository.get();
  String lastSearchedKeyword = "";

  TabBloc(): super(FetchingDataState());

  @override
  Stream<TabState> mapEventToState(TabEvent event) async* {

    if (event is GetTabDateEvent) {
      try {
        yield FetchingDataState();
        var response = await repository.getCategories(lat: event.lat, lng: event.lng);
        yield DataReceivedState(response);
      } catch (e) {
        print(e);
        AppUtils.showCustomErrorMsg(e);
        yield ApiFailedState();
      }
    } else if (event is GetAllHomeSectionEvent) {
      try {
        yield FetchingDataState();
        var response = await repository.getHomeSection();
        yield AllHomeSectionReceivedState(response);
      } catch (e) {
        print(e);
        AppUtils.showCustomErrorMsg(e);
        yield ApiFailedState();
      }
    } else if (event is GetSectionContentEvent) {
      try {
        yield FetchingDataState();
        var response = await repository.getHomeSectionContent(event.sectionId);
        yield SectionContentReceivedState(response);
      } catch (e) {
        print(e);
        AppUtils.showCustomErrorMsg(e);
        yield ApiFailedState();
      }
    } else if (event is GetSearchResultEvent) {
      try {
        yield FetchingDataState();
        lastSearchedKeyword = event.keyword;
        var response = await repository.getSearchResult(event.keyword);
        yield SearchResultState(response);
      } catch (e) {
        print(e);
        AppUtils.showCustomErrorMsg(e);
        yield ApiFailedState();
      }
    } else if (event is CancelSearchEvent) {
      try {
        lastSearchedKeyword = "";
        yield SearchIdleState();
      } catch (e) {
        print(e);
        AppUtils.showCustomErrorMsg(e);
        yield ApiFailedState();
      }
    }
  }
}