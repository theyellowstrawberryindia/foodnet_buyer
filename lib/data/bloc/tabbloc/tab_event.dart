import 'package:foodnet_buyer/data/model/home_section.dart';

abstract class TabEvent {}

class TabLoadingEvent extends TabEvent {}

class GetTabDateEvent extends TabEvent {
  String lat,lng;
  GetTabDateEvent({this.lat, this.lng});
}

class GetAllHomeSectionEvent extends TabEvent {
  GetAllHomeSectionEvent();
}

class GetSectionContentEvent extends TabEvent {
  String sectionId;
  GetSectionContentEvent(this.sectionId);
}

class GetSearchResultEvent extends TabEvent {
  String keyword;
  GetSearchResultEvent(this.keyword);
}

class CancelSearchEvent extends TabEvent {
  CancelSearchEvent();
}
