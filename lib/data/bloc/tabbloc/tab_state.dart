import 'package:foodnet_buyer/data/model/category_response.dart';
import 'package:foodnet_buyer/data/model/home_section.dart';
import 'package:foodnet_buyer/data/model/product.dart';

abstract class TabState {}

class FetchingDataState extends TabState {}

class DataReceivedState extends TabState {
  CategoryResponse pageData;

  DataReceivedState(this.pageData);
}

class AllHomeSectionReceivedState extends TabState {
  List<HomeSection> sections;
  AllHomeSectionReceivedState(this.sections);
}

class SectionContentReceivedState extends TabState {
  List<dynamic> sectionData;
  SectionContentReceivedState(this.sectionData);
}

class SearchResultState extends TabState {
  List<Product> result;
  SearchResultState(this.result);
}

class SearchIdleState extends TabState {}


class ApiFailedState extends TabState {}