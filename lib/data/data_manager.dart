import 'package:foodnet_buyer/data/model/location_data.dart';

class DataManager {
  static DataManager _instance = DataManager.internal();

  DataManager factory() {
    return _instance;
  }

  DataManager.internal();

  static DataManager get() {
    return _instance;
  }

  bool fromCart = false;
  String fcmToken;
  bool refreshAddress = false;
  LocationData _locationData;
  String appVersionName = "1.0.0";
  int appVersionCode = 1;
  dynamic notificationData;

  int cartId;
  String delivery_date;
  String delivery_time;

  LocationData getLocationData() => _locationData;
  setLocationData(LocationData data) {
    _locationData = data;
  }

  List<dynamic> states = [
    {"id": 34, "state_code": "35", "state_name": "ANDAMAN AND NICOBAR ISLANDS"},
    {"id": 27, "state_code": "28", "state_name": "ANDHRA PRADESH"},
    {"id": 36, "state_code": "37", "state_name": "ANDHRA PRADESH"},
    {"id": 12, "state_code": "12", "state_name": "ARUNACHAL PRADESH"},
    {"id": 18, "state_code": "18", "state_name": "ASSAM"},
    {"id": 10, "state_code": "10", "state_name": "BIHAR"},
    {"id": 4, "state_code": "4", "state_name": "CHANDIGARH"},
    {"id": 22, "state_code": "22", "state_name": "CHATTISGARH"},
    {
      "id": 25,
      "state_code": "25",
      "state_name": "DADRA AND NAGAR HAVELI AND DAMAN AND DIU"
    },
    {"id": 7, "state_code": "7", "state_name": "DELHI"},
    {"id": 29, "state_code": "30", "state_name": "GOA"},
    {"id": 24, "state_code": "24", "state_name": "GUJARAT"},
    {"id": 6, "state_code": "6", "state_name": "HARYANA"},
    {"id": 2, "state_code": "2", "state_name": "HIMACHAL PRADESH"},
    {"id": 1, "state_code": "1", "state_name": "JAMMU AND KASHMIR"},
    {"id": 20, "state_code": "20", "state_name": "JHARKHAND"},
    {"id": 28, "state_code": "29", "state_name": "KARNATAKA"},
    {"id": 31, "state_code": "32", "state_name": "KERALA"},
    {"id": 37, "state_code": "38", "state_name": "LADAKH"},
    {"id": 30, "state_code": "31", "state_name": "LAKSHWADEEP"},
    {"id": 23, "state_code": "23", "state_name": "MADHYA PRADESH"},
    {"id": 26, "state_code": "27", "state_name": "MAHARASHTRA"},
    {"id": 14, "state_code": "14", "state_name": "MANIPUR"},
    {"id": 17, "state_code": "17", "state_name": "MEGHLAYA"},
    {"id": 15, "state_code": "15", "state_name": "MIZORAM"},
    {"id": 13, "state_code": "13", "state_name": "NAGALAND"},
    {"id": 21, "state_code": "21", "state_name": "ODISHA"},
    {"id": 33, "state_code": "34", "state_name": "PUDUCHERRY"},
    {"id": 3, "state_code": "3", "state_name": "PUNJAB"},
    {"id": 8, "state_code": "8", "state_name": "RAJASTHAN"},
    {"id": 11, "state_code": "11", "state_name": "SIKKIM"},
    {"id": 32, "state_code": "33", "state_name": "TAMIL NADU"},
    {"id": 35, "state_code": "36", "state_name": "TELANGANA"},
    {"id": 16, "state_code": "16", "state_name": "TRIPURA"},
    {"id": 9, "state_code": "9", "state_name": "UTTAR PRADESH"},
    {"id": 5, "state_code": "5", "state_name": "UTTARAKHAND"},
    {"id": 19, "state_code": "19", "state_name": "WEST BENGAL"}
  ];
}
