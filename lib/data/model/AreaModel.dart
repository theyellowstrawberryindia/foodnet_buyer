// To parse this JSON data, do
//
//     final areaModel = areaModelFromJson(jsonString);

import 'dart:convert';

AreaModel areaModelFromJson(String str) => AreaModel.fromJson(json.decode(str));

String areaModelToJson(AreaModel data) => json.encode(data.toJson());

class AreaModel {
  AreaModel({
    this.status,
    this.message,
    this.areadata,
  });

  bool status;
  String message;
  List<Areadatum> areadata;

  factory AreaModel.fromJson(Map<String, dynamic> json) => AreaModel(
    status: json["status"],
    message: json["message"],
    areadata: List<Areadatum>.from(json["data"].map((x) => Areadatum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": List<dynamic>.from(areadata.map((x) => x.toJson())),
  };
}

class Areadatum {
  Areadatum({
    this.area,
  });

  String area;

  factory Areadatum.fromJson(Map<String, dynamic> json) => Areadatum(
    area: json["area"],
  );

  Map<String, dynamic> toJson() => {
    "area": area,
  };
}
