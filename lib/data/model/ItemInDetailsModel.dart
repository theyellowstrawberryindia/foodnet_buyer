// To parse this JSON data, do
//
//     final itemInDetailsModel = itemInDetailsModelFromJson(jsonString);

import 'dart:convert';

import 'package:foodnet_buyer/ui/add_gross_weight.dart';
import 'package:foodnet_buyer/util/app_utils.dart';

ItemInDetailsModel itemInDetailsModelFromJson(String str) => ItemInDetailsModel.fromJson(json.decode(str));

String itemInDetailsModelToJson(ItemInDetailsModel data) => json.encode(data.toJson());

class ItemInDetailsModel {
  ItemInDetailsModel({
    this.status,
    this.message,
    this.itemInData,
  });

  bool status;
  String message;
  ItemInData itemInData;

  factory ItemInDetailsModel.fromJson(Map<String, dynamic> json) => ItemInDetailsModel(
    status: json["status"],
    message: json["message"],
    itemInData: ItemInData.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": itemInData.toJson(),
  };
}

class ItemInData {
  ItemInData({
    this.id,
    this.lineItems,
    this.finalTotal,
    this.totalBase,
    this.totalTaxes,
    this.convenienceCharges,
    this.discountAmount,
    this.grossWeight,
    this.deliveryDate,
    this.deliveryTime,
    this.storeId,
    this.createdAt,
    this.iteminitems,
    this.party,
    this.status,
    this.itemindocuments,
    this.isAssignedGrossWeight,
  });

  int id;
  String lineItems;
  String finalTotal;
  String totalBase;
  String totalTaxes;
  String convenienceCharges;
  String discountAmount;
  dynamic grossWeight;
  DateTime deliveryDate;
  String deliveryTime;
  int storeId;
  DateTime createdAt;
  List<Iteminitem> iteminitems;
  Party party;
  Status status;
  List<UploadPhotos> itemindocuments=[];
  bool isAssignedGrossWeight;

  factory ItemInData.fromJson(Map<String, dynamic> json) => ItemInData(
    id: json["id"],
    lineItems: json["line_items"],
    finalTotal: json["final_total"],
    totalBase: json["total_base"],
    totalTaxes: json["total_taxes"],
    convenienceCharges: json["convenience_charges"],
    discountAmount: json["discount_amount"],
    grossWeight: json["gross_weight"],
    deliveryDate: DateTime.parse(json["delivery_date"]),
    deliveryTime: json["delivery_time"],
    storeId: json["storeId"],
    createdAt: DateTime.parse(json["createdAt"]),
    iteminitems: List<Iteminitem>.from(json["iteminitems"].map((x) => Iteminitem.fromJson(x))),
    party: Party.fromJson(json["party"]),
    status: Status.fromJson(json["status"]),
    isAssignedGrossWeight: json["isAssignedGrossWeight"] != null ? json["isAssignedGrossWeight"] : false,
    itemindocuments: List<UploadPhotos>.from(json["itemindocuments"].map((x) => UploadPhotos.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "line_items": lineItems,
    "final_total": finalTotal,
    "total_base": totalBase,
    "total_taxes": totalTaxes,
    "convenience_charges": convenienceCharges,
    "discount_amount": discountAmount,
    "gross_weight": grossWeight,
    "delivery_date": "${deliveryDate.year.toString().padLeft(4, '0')}-${deliveryDate.month.toString().padLeft(2, '0')}-${deliveryDate.day.toString().padLeft(2, '0')}",
    "delivery_time": deliveryTime,
    "storeId": storeId,
    "createdAt": createdAt.toIso8601String(),
    "iteminitems": List<dynamic>.from(iteminitems.map((x) => x.toJson())),
    "party": party.toJson(),
    "status": status.toJson(),
    "itemindocuments": List<dynamic>.from(itemindocuments.map((x) => x)),
    "isAssignedGrossWeight": isAssignedGrossWeight,
  };
}

class Iteminitem {
  Iteminitem({
    this.id,
    this.sequence,
    this.quantity,
    this.minQty,
    this.productCatalogId,
    this.basePrice,
    this.appliedTaxPercent,
    this.price,
    this.totalBase,
    this.totalTaxes,
    this.packOf,
    this.packOfUnit,
    this.saleUnit,
    this.packets,
    this.iteminId,
    this.productId,
    this.totalQty,
    this.productName,
    this.subtext,
  });

  int id;
  int sequence;
  String quantity;
  double minQty;
  int productCatalogId;
  String basePrice;
  String appliedTaxPercent;
  String price;
  String totalBase;
  String totalTaxes;
  String packOf;
  String packOfUnit;
  dynamic saleUnit;
  int packets;
  int iteminId;
  int productId;
  String totalQty;
  String productName;
  String subtext;

  set setQty(String data)=>quantity=data;

  set setTotalPrice(String data){
    var total = double.parse(quantity)*double.parse(data);
    totalBase = total.toStringAsFixed(2);
  }

  //{"id":206,"sequence":1,"quantity":"6","productCatalogId":1,
  // "base_price":"150.00","applied_tax_percent":"0.12","price":"168.00",
  // "total_base":"900.00","total_taxes":"108.00","pack_of":"6.00",
  // "pack_of_unit":"pc","sale_unit":null,"packets":1,"discount_amount":"0.00",
  // "orderId":138,"productId":602,"total_qty":"36 pc","product_name":"Punjabi Samosa"}

  factory Iteminitem.fromJson(Map<String, dynamic> json) => Iteminitem(
    id: json["id"],
    minQty: json["min_order_qty"] != null
        ? double.parse(json["min_order_qty"].toString())
        : 1.0,
    sequence: json["sequence"],
    quantity: json["quantity"]?.toString(),
    basePrice: json["base_price"],
    appliedTaxPercent: json["applied_tax_percent"],
    price: json["price"],
    totalBase: json["total_base"],
    totalTaxes: json["total_taxes"],
    packOf: json["pack_of"],
    packOfUnit: json["pack_of_unit"],
    saleUnit: json["sale_unit"],
    packets: json["packets"],
    productId: json["productId"],
    totalQty: json["total_qty"],
    productName: json["product_name"],
    subtext: json["sub_text"] ?? "",
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "sequence": sequence,
    "quantity": quantity,
    "minQty": minQty?.toString(),
    "productCatalogId": productCatalogId,
    "base_price": basePrice,
    "applied_tax_percent": appliedTaxPercent,
    "price": price,
    "total_base": totalBase,
    "total_taxes": totalTaxes,
    "pack_of": packOf,
    "pack_of_unit": packOfUnit,
    "sale_unit": saleUnit,
    "packets": packets,
    "iteminId": iteminId,
    "productId": productId,
    "total_qty": totalQty,
    "product_name": productName,
  };

  bool isKg() => packOfUnit?.toLowerCase() == "kg";


  String getDisplayQty() {
    return AppUtils.getDisplayValue(quantity, decimalUpTo: 3);
  }

  String getDisplayPackOff() {
    return AppUtils.getDisplayValue(packOf);
  }
}

class Party {
  Party({
    this.mobile,
    this.person,
  });

  String mobile;
  dynamic person;

  factory Party.fromJson(Map<String, dynamic> json) => Party(
    mobile: json["mobile"],
    person: json["person"],
  );

  Map<String, dynamic> toJson() => {
    "mobile": mobile,
    "person": person,
  };
}

class Status {
  Status({
    this.id,
    this.statusText,
  });

  int id;
  String statusText;

  factory Status.fromJson(Map<String, dynamic> json) => Status(
    id: json["id"],
    statusText: json["status_text"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "status_text": statusText,
  };
}
