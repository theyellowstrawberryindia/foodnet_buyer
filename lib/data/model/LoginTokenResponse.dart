// To parse this JSON data, do
//
//     final loginTokenResponse = loginTokenResponseFromJson(jsonString);

class LoginTokenResponse {
  LoginTokenResponse({
    this.status,
    this.message,
    this.accessToken,
    this.refreshToken,
    this.userId,
    this.partyCode,
    this.userpartyId,
    this.email,
    this.mobile,
    this.firstName,
    this.lastName,
    this.address,
  });

  bool status;
  String message;
  String accessToken;
  String refreshToken;
  String userId;
  String partyCode;
  int userpartyId;
  String email;
  String mobile;
  String firstName;
  String lastName;
  List<dynamic> address;

  factory LoginTokenResponse.fromJson(Map<String, dynamic> json) {
    var responseData =
        json["responseData"] != null ? json["responseData"]["data"] : null;
    var person = responseData != null ? responseData["person"] : null;
    return LoginTokenResponse(
      status: json["status"],
      message: json["message"],
      accessToken: json["accessToken"],
      refreshToken: json["refreshToken"],
      partyCode: responseData != null ? responseData["party_code"] : null,
      email: responseData != null ? responseData["email"] : null,
      mobile: responseData != null ? responseData["mobile"] : null,
      userpartyId: responseData != null ? responseData["id"] : null,
      userId: person != null ? person["id"].toString() : null,
      firstName: person != null ? person["first_name"] : null,
      lastName: person != null ? person["last_name"] : null,
      address: responseData != null
          ? responseData["addresses"] != null
              ? responseData["addresses"]
              : null
          : null,
    );
  }
}
