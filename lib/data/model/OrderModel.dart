// To parse this JSON data, do
//
//     final orderModel = orderModelFromJson(jsonString);

import 'dart:convert';

OrderModel orderModelFromJson(String str) => OrderModel.fromJson(json.decode(str));

class OrderModel {
  OrderModel({
    this.status,
    this.message,
    this.orderItems,
  });

  bool status;
  String message;
  List<OrderItem> orderItems=[];

  factory OrderModel.fromJson(Map<String, dynamic> json) => OrderModel(
    status: json["status"],
    message: json["message"],
    orderItems: List<OrderItem>.from(json["data"].map((x) => OrderItem.fromJson(x))),
  );
}

class OrderItem {
  OrderItem({
    this.id,
    this.lineItems,
    this.finalTotal,
    this.totalBase,
    this.totalTaxes,
    this.convenienceCharges,
    this.discountAmount,
    this.deliveryDate,
    this.deliveryTime,
    this.addressId,
    this.createdAt,
    this.isSent,
    this.party,
    this.tid,
    this.address,
    this.hasFssai,
    this.sellerLogo,
    this.buyerLogo,
    this.status,
  });

  int id;
  String lineItems;
  String finalTotal;
  String totalBase;
  String totalTaxes;
  String convenienceCharges;
  String discountAmount;
  DateTime deliveryDate;
  String deliveryTime;
  int addressId;
  DateTime createdAt;
  bool isSent;
  Party party;
  Tid tid;
  Address address;
  bool hasFssai;
  String sellerLogo;
  String buyerLogo;
  String status;

  factory OrderItem.fromJson(Map<String, dynamic> json) => OrderItem(
    id: json["id"],
    lineItems: json["line_items"],
    finalTotal: json["final_total"],
    totalBase: json["total_base"],
    totalTaxes: json["total_taxes"],
    convenienceCharges: json["convenience_charges"],
    discountAmount: json["discount_amount"],
    deliveryDate: DateTime.parse(json["delivery_date"]),
    deliveryTime: json["delivery_time"],
    addressId: json["addressId"],
    createdAt: DateTime.parse(json["createdAt"]),
    isSent: json["is_sent"],
    party: Party.fromJson(json["party"]),
    tid: Tid.fromJson(json["tid"]),
    address: json["address"] != null ? Address.fromJson(json["address"]) : null,
    hasFssai: json["has_fssai"],
    sellerLogo: json["seller_logo"],
    buyerLogo: json["buyer_logo"],
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "line_items": lineItems,
    "final_total": finalTotal,
    "total_base": totalBase,
    "total_taxes": totalTaxes,
    "convenience_charges": convenienceCharges,
    "discount_amount": discountAmount,
    "delivery_date": "${deliveryDate.year.toString().padLeft(4, '0')}-${deliveryDate.month.toString().padLeft(2, '0')}-${deliveryDate.day.toString().padLeft(2, '0')}",
    "delivery_time": deliveryTime,
    "addressId": addressId,
    "createdAt": createdAt.toIso8601String(),
    "is_sent": isSent,
    "party": party.toJson(),
    "tid": tid.toJson(),
    "address": address.toJson(),
    "has_fssai": hasFssai,
    "seller_logo": sellerLogo,
    "buyer_logo": buyerLogo,
    "status": status,
  };
  
  bool canEdit() => status?.toLowerCase() == 'new';
}

class Address {
  Address({
    this.addressLine2,
    this.addressLine3,
    this.area,
    this.id,
    this.flatNumber,
    this.building,
    this.society,
    this.addressLine1,
    this.city,
    this.pinCode,
  });

  String addressLine2;
  String addressLine3;
  String area;
  int id;
  String flatNumber;
  String building;
  String society;
  String addressLine1;
  String city;
  String pinCode;

  factory Address.fromJson(dynamic data) {
    try {
      if (data is Map) {
        var json = data;
        return Address(
          addressLine2: json["address_line2"],
          addressLine3: json["address_line3"],
          area: json["area"],
          id: json["id"],
          flatNumber: json["flat_number"],
          building: json["building"],
          society: json["society"],
          addressLine1: json["address_line1"],
          city: json["city"],
          pinCode: json["pin_code"],
        );
      }
    } catch (e) {
    }

    return Address();
  }

  Map<String, dynamic> toJson() => {
    "address_line2": addressLine2,
    "address_line3": addressLine3,
    "area": area,
    "id": id,
    "flat_number": flatNumber,
    "building": building,
    "society": society,
    "address_line1": addressLine1,
    "city": city,
    "pin_code": pinCode,
  };
}

class Party {
  Party({
    this.mobile,
    this.person,
    this.partylocation,
  });

  String mobile;
  dynamic person;
  Partylocation partylocation;

  factory Party.fromJson(Map<String, dynamic> json) => Party(
    mobile: json["mobile"],
    person: json["person"],
    partylocation: Partylocation.fromJson(json["partylocation"]),
  );

  Map<String, dynamic> toJson() => {
    "mobile": mobile,
    "person": person,
    "partylocation": partylocation.toJson(),
  };
}

class Partylocation {
  Partylocation({
    this.id,
    this.fssaiinfo,
  });

  int id;
  Fssaiinfo fssaiinfo;

  factory Partylocation.fromJson(Map<String, dynamic> json) => Partylocation(
    id: json["id"],
    fssaiinfo: Fssaiinfo.fromJson(json["fssaiinfo"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "fssaiinfo": fssaiinfo.toJson(),
  };
}

class Fssaiinfo {
  Fssaiinfo({
    this.fssai,
    this.expiryDate,
  });

  String fssai;
  DateTime expiryDate;

  factory Fssaiinfo.fromJson(Map<String, dynamic> json) => Fssaiinfo(
    fssai: json["fssai"],
    expiryDate: DateTime.parse(json["expiry_date"]),
  );

  Map<String, dynamic> toJson() => {
    "fssai": fssai,
    "expiry_date": "${expiryDate.year.toString().padLeft(4, '0')}-${expiryDate.month.toString().padLeft(2, '0')}-${expiryDate.day.toString().padLeft(2, '0')}",
  };
}

class Tid {
  Tid({
    this.id,
    this.businesstids,
    this.status,
  });

  int id;
  List<Businesstid> businesstids=[];
  List<Status> status=[];

  factory Tid.fromJson(Map<String, dynamic> json) => Tid(
    id: json["id"],
    businesstids: List<Businesstid>.from(json["businesstids"].map((x) => Businesstid.fromJson(x))),
    status: List<Status>.from(json["status"].map((x) => Status.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "businesstids": List<dynamic>.from(businesstids.map((x) => x.toJson())),
    "status": List<dynamic>.from(status.map((x) => x.toJson())),
  };
}

class Businesstid {
  Businesstid({
    this.id,
    this.eventName,
    this.action,
    this.eventDate,
    this.eventTime,
    this.createdBy,
    this.createdAt,
  });

  int id;
  String eventName;
  String action;
  DateTime eventDate;
  String eventTime;
  String createdBy;
  DateTime createdAt;

  factory Businesstid.fromJson(Map<String, dynamic> json) => Businesstid(
    id: json["id"],
    eventName: json["event_name"],
    action: json["action"],
    eventDate: DateTime.parse(json["event_date"]),
    eventTime: json["event_time"],
    createdBy: json["createdBy"],
    createdAt: DateTime.parse(json["createdAt"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "event_name": eventName,
    "action": action,
    "event_date": "${eventDate.year.toString().padLeft(4, '0')}-${eventDate.month.toString().padLeft(2, '0')}-${eventDate.day.toString().padLeft(2, '0')}",
    "event_time": eventTime,
    "createdBy": createdBy,
    "createdAt": createdAt.toIso8601String(),
  };
}

class Status {
  Status({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Status.fromJson(Map<String, dynamic> json) => Status(
    key: json["key"],
    value: json["value"],
  );

  Map<String, dynamic> toJson() => {
    "key": key,
    "value": value,
  };
}
