// To parse this JSON data, do
//
//     final searchModel = searchModelFromJson(jsonString);

import 'dart:convert';

SearchModel searchModelFromJson(String str) => SearchModel.fromJson(json.decode(str));

String searchModelToJson(SearchModel data) => json.encode(data.toJson());

class SearchModel {
  SearchModel({
    this.searchData,
  });

  List<SearchDatum> searchData;

  factory SearchModel.fromJson(Map<String, dynamic> json) => SearchModel(
    searchData: List<SearchDatum>.from(json["data"].map((x) => SearchDatum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": List<dynamic>.from(searchData.map((x) => x.toJson())),
  };
}

class SearchDatum {
  SearchDatum({
    this.id,
    this.minQty,
    this.productCode,
    this.productName,
    this.subText,
    this.saleUomId,
    this.sizeUomId,
    this.isProductInstock,
    this.productcatalogId,
    this.documentUrl,
    this.categoryId,
    this.productBaseRate,
    this.productTaxRate,
    this.productMrp,
    this.size,
    this.saleUnit,
    this.saleUnitName,
    this.sizeUnit,
    this.sizeUnitName,
    this.sizeVariants,
    this.type,
    this.attrJson,
  });

  int id;
  double minQty;
  String productCode;
  String productName;
  String subText;
  int saleUomId;
  int sizeUomId;
  int isProductInstock;
  int productcatalogId;
  String documentUrl;
  int categoryId;
  String productBaseRate;
  String productTaxRate;
  String productMrp;
  String size;
  String saleUnit;
  String saleUnitName;
  String sizeUnit;
  String sizeUnitName;
  List<SizeVariant> sizeVariants=[];
  bool checked=false;
  String type;
  AttrJson attrJson;

  set setChecked(bool data){
    checked = data;
  }

  set setSizeVariants(List<SizeVariant> data){
    sizeVariants = data;
  }

  factory SearchDatum.fromJson(Map<String, dynamic> json) => SearchDatum(
    id: json["id"],
    minQty: json["min_order_qty"] != null
        ? double.parse(json["min_order_qty"].toString())
        : 1.0,
    productCode: json["product_code"],
    productName: json["product_name"],
    subText: json["sub_text"],
    saleUomId: json["saleUomId"],
    sizeUomId: json["sizeUomId"],
    isProductInstock: json["is_product_instock"],
    productcatalogId: json["productcatalogId"],
    documentUrl: json["document_url"],
    categoryId: json["categoryId"],
    productBaseRate: json["product_base_rate"],
    productTaxRate: json["product_tax_rate"],
    productMrp: json["product_mrp"],
    size: json["size"],
    saleUnit: json["saleUnit"],
    saleUnitName: json["saleUnitName"],
    sizeUnit: json["sizeUnit"],
    sizeUnitName: json["sizeUnitName"],
    type: json["type"],
    sizeVariants:json["sizeVariants"]==null?[]: List<SizeVariant>.from(json["sizeVariants"].map((x) => SizeVariant.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "min_order_qty" : minQty,
    "product_code": productCode,
    "product_name": productName,
    "sub_text": subText,
    "saleUomId": saleUomId,
    "sizeUomId": sizeUomId,
    "is_product_instock": isProductInstock,
    "productcatalogId": productcatalogId,
    "document_url": documentUrl,
    "categoryId": categoryId,
    "product_base_rate": productBaseRate,
    "product_tax_rate": productTaxRate,
    "product_mrp": productMrp,
    "size": size,
    "saleUnit": saleUnit,
    "saleUnitName": saleUnitName,
    "sizeUnit": sizeUnit,
    "sizeUnitName": sizeUnitName,
    "sizeVariants": List<dynamic>.from(sizeVariants.map((x) => x.toJson())),
    "type": type,
    "attrJSON": attrJson.toJson(),
  };
}

class AttrJson {
  AttrJson({
    this.foodType,
    this.deliveryTime,
  });

  String foodType;
  String deliveryTime;

  factory AttrJson.fromJson(Map<String, dynamic> json) => AttrJson(
    foodType: json["foodType"],
    deliveryTime: json["deliveryTime"],
  );

  Map<String, dynamic> toJson() => {
    "foodType": foodType,
    "deliveryTime": deliveryTime,
  };
}

class SizeVariant {
  SizeVariant({
    this.id,
    this.minQty,
    this.sizeUnit,
    this.size,
    this.taxRate,
    this.salePrice,
    this.saleUnit,
    this.productPrice,
    this.productDisplayName,
    this.subText,
  });

  double minQty;
  String id;
  String sizeUnit;
  String size;
  String taxRate;
  String salePrice;
  String saleUnit;
  String productPrice;
  String productDisplayName;
  bool checked=false;
  dynamic subText='';

  set setChecked(bool data){
    checked = data;
  }
  set setSubText(String data){
    subText = data;
  }

  set setId(String pid){
    id = pid;
  }

  factory SizeVariant.fromJson(Map<String, dynamic> json) => SizeVariant(
    id: json["Id"],
    minQty: json["min_order_qty"] != null
        ? double.parse(json["min_order_qty"].toString())
        : 1.0,
    sizeUnit: json["baseUOM"],
    size: json["saleQty"],
    taxRate: json["taxRate"],
    salePrice: json["salePrice"],
    saleUnit: json["sellingUOM"],
    productPrice: json["productPrice"],
    productDisplayName: json["productDisplayName"],
  );

  Map<String, dynamic> toJson() => {
    "Id": id,
    "min_order_qty": minQty,
    "baseUOM": sizeUnit,
    "saleQty": size,
    "taxRate": taxRate,
    "salePrice": salePrice,
    "sellingUOM": saleUnit,
    "productPrice": productPrice,
    "productDisplayName": productDisplayName,
  };
}
