import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/date_time_util.dart';
import 'package:intl/intl.dart';

class CartProduct {
  CartProduct({
    this.id,
    this.sequence,
    this.quantity,
    this.minQty,
    this.productcatalogId,
    this.storeId,
    this.storeCode,
    this.basePrice,
    this.appliedTaxPercent,
    this.price,
    this.totalBase,
    this.totalTaxes,
    this.packOf,
    this.packOfUnit,
    this.packets,
    this.deliveryDate,
    this.deliveryTime,
    this.discountAmount,
    this.createdAt,
    this.updatedAt,
    this.tenantId,
    this.productId,
    this.shoppingcartId,
    this.imgUrl,
    this.productName,
    this.saleUnit,
    this.msg,
    this.subText,
  });

  int id;
  int sequence;
  double quantity;
  double minQty;
  int productcatalogId;
  String storeCode;
  int storeId;
  int packets;
  int tenantId;
  int productId;
  int shoppingcartId;
  String basePrice;
  String appliedTaxPercent;
  String price;
  String totalBase;
  String totalTaxes;
  String packOf;
  String packOfUnit;
  DateTime deliveryDate;
  String deliveryTime;
  double discountAmount;
  DateTime createdAt;
  DateTime updatedAt;
  String imgUrl;
  String productName;
  String msg;
  String saleUnit;
  String subText;

  factory CartProduct.fromJson(Map<String, dynamic> json) => CartProduct(
        id: json["id"],
        sequence: json["sequence"],
        quantity: json["quantity"] != null
            ? double.parse(json["quantity"].toString())
            : 1.0,
        minQty: json["min_order_qty"] != null
            ? double.parse(json["min_order_qty"].toString())
            : 1.0,
        productcatalogId: json["productCatalogId"],
        storeId: json.containsKey("storeId") ? json["storeId"] : 1,
        storeCode:
            json.containsKey("storeCode") ? json["storeCode"] : "ripul_cpt",
        basePrice: json["base_price"],
        appliedTaxPercent: json["applied_tax_percent"],
        price: json["price"],
        totalBase: json["total_base"],
        totalTaxes: json["total_taxes"],
        packOf: json["pack_of"],
        packOfUnit: json["pack_of_unit"],
        packets: int.parse(json["packets"].toString()),
        deliveryDate: json["delivery_date"] != null
            ? DateTimeUtil.parseDate(json["delivery_date"])
            : null,
        deliveryTime: json["delivery_time"] != null
            ? DateTimeUtil.convertToHHMM(json["delivery_time"])
            : null,
        discountAmount: double.parse(json["discount_amount"].toString()),
        createdAt: json["createdAt"] != null
            ? DateTime.parse(json["createdAt"])
            : null,
        updatedAt: json["updatedAt"] != null
            ? DateTime.parse(json["updatedAt"])
            : null,
        tenantId: json["tenantId"] != null
            ? int.parse(json["tenantId"].toString())
            : null,
        productId: int.parse(json["productId"].toString()),
        shoppingcartId: json["shoppingcartId"] != null
            ? int.parse(json["shoppingcartId"].toString())
            : null,
        imgUrl: json["product_img"],
        productName: json["product_name"],
        saleUnit: json["sale_unit"]?.toString(),
        subText: json["sub_text"]?.toString(),
        msg: json["message"],
      );

  DateTime getDateTimeToCompare() {
    if (deliveryTime == null || !deliveryTime.contains(":")) {
      return deliveryDate;
    }
    List<String> timePart = deliveryTime.split(":");

    DateTime dateTime = DateTime(deliveryDate.year, deliveryDate.month,
        deliveryDate.day, int.parse(timePart[0]), int.parse(timePart[1]));
    return dateTime;
  }

  String getDateAsString() => deliveryDate != null
      ? DateFormat("dd/MM/yyyy").format(deliveryDate)
      : null;

  bool isSameDate(DateTime dataTime) {
    return getDateAsString() == DateFormat("dd/MM/yyyy").format(dataTime);
  }

  bool isSameDateTime(DateTime dateTime) {
    DateTime newDateWithoutSec = DateTime(dateTime.year, dateTime.month,
        dateTime.day, dateTime.hour, dateTime.minute);
    return getDateTimeToCompare().isAtSameMomentAs(newDateWithoutSec);
  }

  String getTotalPrice() {
    double totalBaseValue = double.parse(totalBase);
    double totalTaxesValue = double.parse(totalTaxes);
    return (totalBaseValue + totalTaxesValue).toStringAsFixed(2);
  }

  void updateDateTime(DateTime newDateTime) {
    deliveryDate = newDateTime;
    deliveryTime = DateFormat("HH:mm").format(newDateTime);
  }

  bool isSameProduct(CartProduct product) {
    int packOffInt = double.parse(packOf).toInt();
    int packOffInt2 = double.parse(product.packOf).toInt();
    return productId == product.productId &&
        packOffInt == packOffInt2 &&
        packOfUnit == product.packOfUnit &&
        getDateAsString() == product.getDateAsString();
  }

  bool isSameProductForQtyChange(CartProduct product) {
    return productId == product.productId &&
        packOf == product.packOf &&
        packOfUnit == product.packOfUnit;
  }

  update(CartProduct product) {
    quantity = quantity + product.quantity;
    packets = quantity.toInt();
    deliveryDate = product.deliveryDate;
    deliveryTime = product.deliveryTime;
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "sequence": sequence,
        "quantity": quantity,
        "min_order_qty": minQty,
        "productCatalogId": productcatalogId,
        "storeId": storeId,
        "storeCode": storeCode,
        "base_price": basePrice,
        "applied_tax_percent": appliedTaxPercent,
        "price": price,
        "total_base": totalBase,
        "total_taxes": totalTaxes,
        "pack_of": packOf,
        "pack_of_unit": packOfUnit,
        "packets": packets,
        "delivery_date": getDateAsString(),
        "delivery_time": deliveryTime,
        "discount_amount": discountAmount,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "tenantId": tenantId,
        "productId": productId,
        "shoppingcartId": shoppingcartId,
        "product_img": imgUrl,
        "sale_unit": saleUnit,
        "sub_text": subText,
        "product_name": productName,
      };

  Map<String, dynamic> toCartJson() => {
        "id": productId,
        "qty": getDisplayQty(),
        "sequence": sequence.toString(),
        "packOf": packOf != null ? double.parse(packOf).toInt().toString() : "",
        "packOfUnit": packOfUnit,
        "saleUnit": saleUnit,
        "packets": packets.toString(),
        "delivery_date": getDateAsString(),
        "delivery_time": deliveryTime != null
            ? DateTimeUtil.convertToHHMM(deliveryTime)
            : null,
//    "discount": discountAmount.toString(),
      };

  String getDisplayQty() {
    return AppUtils.getDisplayValue(quantity, decimalUpTo: 3);
  }

  String getDisplayPackOff() {
    return AppUtils.getDisplayValue(packOfUnit);
  }

  bool isKg() => packOfUnit?.toLowerCase() == "kg";
}
