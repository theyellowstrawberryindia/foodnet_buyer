// To parse this JSON data, do
//
//     final cartResponse = cartResponseFromJson(jsonString);

import 'package:foodnet_buyer/data/model/cart_product.dart';
import 'package:foodnet_buyer/util/date_time_util.dart';

class CartResponse {
  CartResponse({
    this.id,
    this.lineItems,
    this.tenantId,
    this.addressId,
    this.convenienceCharges,
    this.discountAmount,
    this.storeId,
    this.userPartyId,
    this.updatedAt,
    this.createdAt,
    this.totalBase,
    this.totalTaxes,
    this.finalTotal,
    this.partyId,
    this.deliveryDate,
    this.deliveryTime,
    this.shoppingcartitems,
    this.allowed,
    this.couponCode,
  });

  int id;
  String lineItems;
  int tenantId;
  int addressId;
  String convenienceCharges;
  String discountAmount;
  int storeId;
  int userPartyId;
  DateTime updatedAt;
  DateTime createdAt;
  String totalBase;
  String totalTaxes;
  String finalTotal;
  String partyId;
  DateTime deliveryDate;
  String deliveryTime;
  List<CartProduct> shoppingcartitems;
  bool allowed;
  String couponCode;

  factory CartResponse.fromJson(Map<String, dynamic> json) => CartResponse(
      id: json["id"],
      lineItems: json["line_items"].toString(),
      tenantId: json["tenantId"] != null
          ? json["tenantId"] is int
              ? json["tenantId"]
              : int.parse(json["tenantId"])
          : null,
      addressId: json["addressId"],
      convenienceCharges: json["convenience_charges"]?.toString() ?? "0",
      discountAmount: json["discount_amount"] is int
          ? json["discount_amount"].toString()
          : json["discount_amount"],
      storeId: json["storeId"],
      userPartyId: json["userPartyId"],
//    updatedAt: DateTime.parse(json["updatedAt"]),
//    createdAt: DateTime.parse(json["createdAt"]),
      totalBase: json["total_base"] is int
          ? json["total_base"].toString()
          : json["total_base"],
      totalTaxes: json["total_taxes"] is int
          ? json["total_taxes"].toString()
          : json["total_taxes"],
      finalTotal: json["final_total"] != null && json["final_total"] is double
          ? json["final_total"].toString()
          : json["final_total"] is int
              ? json["final_total"].toString()
              : json["final_total"],
      partyId: json["partyId"]?.toString(),
      deliveryDate: json["delivery_date"] != null
          ? DateTimeUtil.parseDate(json["delivery_date"])
          : null,
      deliveryTime: json["delivery_time"] != null
          ? DateTimeUtil.convertToHHMM(json["delivery_time"])
          : null,
      shoppingcartitems: json["shoppingcartitems"] != null
          ? List<CartProduct>.from(
              json["shoppingcartitems"].map((x) => CartProduct.fromJson(x)))
          : json["items"] != null
              ? List<CartProduct>.from(
                  json["items"].map((x) => CartProduct.fromJson(x)))
              : json["orderitems"] != null
                  ? List<CartProduct>.from(
                      json["orderitems"].map((x) => CartProduct.fromJson(x)))
                  : [],
      allowed: json["allowed"] ?? true,
      couponCode: json["couponCode"] != null
          ? json["couponCode"]?.toString()
          : json["coupon_code"] != null
              ? json["coupon_code"]?.toString()
              : null);

  String getDateAsString() => deliveryDate != null
      ? "${deliveryDate?.day.toString()?.padLeft(2, '0')}/${deliveryDate?.month.toString()?.padLeft(2, '0')}/${deliveryDate?.year.toString()?.padLeft(4, '0')}"
      : null;

  bool isNotEmpty() =>
      shoppingcartitems != null && shoppingcartitems.isNotEmpty;

  bool isCouponApplied() => couponCode != null;

  Map<String, dynamic> toJson() => {
        "id": id,
        "line_items": lineItems,
        "tenantId": tenantId,
        "addressId": addressId,
        "convenience_charges": convenienceCharges,
        "discount_amount": discountAmount,
        "storeId": storeId,
        "userPartyId": userPartyId,
//    "updatedAt": updatedAt.toIso8601String(),
//    "createdAt": createdAt.toIso8601String(),
        "total_base": totalBase,
        "total_taxes": totalTaxes,
        "final_total": finalTotal,
        "partyId": partyId,
        "delivery_date": getDateAsString(),
        "delivery_time": deliveryTime,
        "couponCode": couponCode,
        "shoppingcartitems":
            List.from(shoppingcartitems.map((x) => x.toJson())),
      };
}
