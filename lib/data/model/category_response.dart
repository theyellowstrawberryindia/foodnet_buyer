import 'dart:convert';

CategoryResponse categoryResponseFromJson(String str) =>
    CategoryResponse.fromJson(json.decode(str));

String categoryResponseToJson(CategoryResponse data) =>
    json.encode(data.toJson());

class CategoryResponse {
  CategoryResponse({
    this.store,
    this.categories,
    this.partyGroup,
  });

  Store store;
  List<SuperCategory> categories;
  PartyGroup partyGroup;

  factory CategoryResponse.fromJson(Map<String, dynamic> json) =>
      CategoryResponse(
        store: Store.fromJson(json["store"]),
        categories: List<SuperCategory>.from(
            json["categories"].map((x) => SuperCategory.fromJson(x))),
        partyGroup: PartyGroup.fromJson(json["partyGroup"]),
      );

  Map<String, dynamic> toJson() => {
        "store": store.toJson(),
//    "categories": categories.toJson(),
        "partyGroup": partyGroup.toJson(),
      };
}

class SuperCategory {
  SuperCategory({
    this.id,
    this.superCategoryName,
    this.tenantId,
    this.supercategorydocuments,
    this.children,
  });

  int id;
  String superCategoryName;
  int tenantId;
  List<CategoryDocument> supercategorydocuments;
  List<Child> children;

  factory SuperCategory.fromJson(Map<String, dynamic> json) => SuperCategory(
        id: json["id"],
        superCategoryName: json["super_category_name"],
        tenantId: json["tenantId"],
        supercategorydocuments: json["supercategorydocuments"] != null
            ? List<CategoryDocument>.from(json["supercategorydocuments"]
                .map((x) => CategoryDocument.fromJson(x)))
            : [],
        children: json["children"] != null
            ? List<Child>.from(json["children"].map((x) => Child.fromJson(x)))
            : null,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "super_category_name": superCategoryName,
        "tenantId": tenantId,
        "supercategorydocuments":
            List<dynamic>.from(supercategorydocuments.map((x) => x.toJson())),
        "children": List<dynamic>.from(children.map((x) => x.toJson())),
      };
}

class Child {
  Child({
    this.id,
    this.categoryName,
    this.superCategoryId,
    this.parentCategoryId,
    this.tenantId,
    this.categorydocuments,
    this.children,
  });

  int id;
  String categoryName;
  int superCategoryId;
  int parentCategoryId;
  int tenantId;
  List<CategoryDocument> categorydocuments;
  List<Child> children;

  factory Child.fromJson(Map<String, dynamic> json) => Child(
        id: json["id"],
        categoryName: json["category_name"],
        superCategoryId: json["superCategoryId"],
        parentCategoryId:
            json["parentCategoryId"] == null ? null : json["parentCategoryId"],
        tenantId: json["tenantId"],
        categorydocuments: json["categorydocuments"] != null
            ? List<CategoryDocument>.from(json["categorydocuments"]
                .map((x) => CategoryDocument.fromJson(x)))
            : null,
        children: json["children"] == null
            ? null
            : List<Child>.from(json["children"].map((x) => Child.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "category_name": categoryName,
        "superCategoryId": superCategoryId,
        "parentCategoryId": parentCategoryId == null ? null : parentCategoryId,
        "tenantId": tenantId,
        "categorydocuments":
            List<dynamic>.from(categorydocuments.map((x) => x.toJson())),
        "children": children == null
            ? null
            : List<dynamic>.from(children.map((x) => x.toJson())),
      };
}

class CategoryDocument {
  CategoryDocument({
    this.id,
    this.documentId,
    this.documentUrl,
    this.documentUsage,
  });

  int id;
  int documentId;
  String documentUrl;
  DocumentUsage documentUsage;

  factory CategoryDocument.fromJson(Map<String, dynamic> json) =>
      CategoryDocument(
        id: json["id"],
        documentId: json["document_id"],
        documentUrl: json["document_url"],
        documentUsage: documentUsageValues.map[json["document_usage"]],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "document_id": documentId,
        "document_url": documentUrl,
        "document_usage": documentUsageValues.reverse[documentUsage],
      };
}

enum DocumentUsage { ICON }

final documentUsageValues = EnumValues({"icon": DocumentUsage.ICON});

class PartyGroup {
  PartyGroup({
    this.id,
    this.groupName,
    this.shortName,
  });

  int id;
  String groupName;
  String shortName;

  factory PartyGroup.fromJson(Map<String, dynamic> json) => PartyGroup(
        id: json["id"],
        groupName: json["group_name"],
        shortName: json["short_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "group_name": groupName,
        "short_name": shortName,
      };
}

class Store {
  Store({
    this.id,
    this.partylocationId,
    this.partyId,
    this.sellerId,
    this.storeCode,
    this.seller,
    this.productCatalogId,
  });

  int id;
  int partylocationId;
  int partyId;
  int sellerId;
  String storeCode;
  Seller seller;
  int productCatalogId;

  factory Store.fromJson(Map<String, dynamic> json) => Store(
        id: json["id"],
        partylocationId: json["partylocationId"],
        partyId: json["partyId"],
        sellerId: json.containsKey("sellerId") ? json["sellerId"] : 1,
        storeCode:
            json.containsKey("store_code") ? json["store_code"] : "ripul_cpt",
        seller: Seller.fromJson(json["seller"]),
        productCatalogId: json["productCatalogId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "partylocationId": partylocationId,
        "partyId": partyId,
        "sellerId": sellerId,
        "store_code": storeCode,
        "seller": seller.toJson(),
        "productCatalogId": productCatalogId,
      };
}

class Seller {
  Seller({
    this.id,
  });

  int id;

  factory Seller.fromJson(Map<String, dynamic> json) => Seller(
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
      };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
