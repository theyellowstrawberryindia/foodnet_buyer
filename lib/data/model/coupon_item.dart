
class CouponItem {
  CouponItem({
    this.couponId,
    this.discountAmountFlat,
    this.discountAmountPercentage,
    this.couponText,
    this.couponCode,
    this.description,
    this.partyUsageHistory,
    this.documentId,
    this.documentUrl,
    this.usage,
    this.maxUsage,
  });

  int couponId;
  String discountAmountFlat;
  String discountAmountPercentage;
  String couponText;
  String couponCode;
  String description;
  List<PartyUsageHistory> partyUsageHistory;
  int documentId;
  String documentUrl;
  String usage;
  String maxUsage;

  factory CouponItem.fromJson(Map<String, dynamic> json) => CouponItem(
    couponId: json["couponId"],
    discountAmountFlat: json["discount_amount_flat"]?.toString(),
    discountAmountPercentage: json["discount_amount_percentage"]?.toString(),
    couponText: json["coupon_text"],
    couponCode: json["coupon_code"],
    description: json["description"],
    partyUsageHistory: json["partyUsageHistory"] != null ? List<PartyUsageHistory>.from(json["partyUsageHistory"].map((x) => PartyUsageHistory.fromJson(x))) : [],
    documentId: json["document_id"],
    documentUrl: json["document_url"],
    usage: json["used"]?.toString() ?? "0",
    maxUsage: json["max_usage"]?.toString() ?? "5",
  );

  Map<String, dynamic> toJson() => {
    "couponId": couponId,
    "discount_amount_flat": discountAmountFlat,
    "discount_amount_percentage": discountAmountPercentage,
    "coupon_text": couponText,
    "coupon_code": couponCode,
    "description": description,
    "partyUsageHistory": List<dynamic>.from(partyUsageHistory.map((x) => x.toJson())),
    "document_id": documentId,
    "document_url": documentUrl,
    "used": usage,
    "max_usage": maxUsage,
  };
}

class PartyUsageHistory {
  PartyUsageHistory({
    this.couponId,
    this.appliedDate,
    this.discountAmount,
  });

  int couponId;
  DateTime appliedDate;
  String discountAmount;

  factory PartyUsageHistory.fromJson(Map<String, dynamic> json) => PartyUsageHistory(
    couponId: json["couponId"],
    appliedDate: DateTime.parse(json["applied_date"]),
    discountAmount: json["discount_amount"],
  );

  Map<String, dynamic> toJson() => {
    "couponId": couponId,
    "applied_date": appliedDate.toIso8601String(),
    "discount_amount": discountAmount,
  };
}