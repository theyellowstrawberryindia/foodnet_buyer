// To parse this JSON data, do
//
//     final homeSection = homeSectionFromJson(jsonString);

import 'dart:convert';

import 'package:foodnet_buyer/data/model/category_response.dart';
import 'package:foodnet_buyer/data/model/product.dart';

HomeSection homeSectionFromJson(String str) => HomeSection.fromJson(json.decode(str));

String homeSectionToJson(HomeSection data) => json.encode(data.toJson());

class HomeSection {
  HomeSection({
    this.id,
    this.sectionName,
    this.sequenceNumber,
    this.sectionTemplateId,
    this.partyId,
    this.sellerId,
    this.screenId,
    this.storeId,
    this.screen,
    this.sectionTemplate,
    this.items,
    this.documentUrl,
  });

  int id;
  String sectionName;
  int sequenceNumber;
  int sectionTemplateId;
  int partyId;
  dynamic sellerId;
  int screenId;
  dynamic storeId;
  Screen screen;
  SectionTemplate sectionTemplate;
  List<dynamic> items;
  String documentUrl;

  factory HomeSection.fromJson(Map<String, dynamic> json) {

    List<dynamic> items;
    if (json["section_name"].toString() == "supercategories") {
      items = List<SuperCategory>.from(json["sectionChildren"].map((x)=>SuperCategory.fromJson(x)));
    } else if (json["section_name"].toString() == "categories") {
      items = List<Child>.from(json["sectionChildren"].map((x)=>Child.fromJson(x)));
    } else {
      items = List<Product>.from(json["sectionChildren"].map((x)=>Product.fromJson(x)));
    }
    return HomeSection(
      id: json["id"],
      sectionName: json["section_name"],
      sequenceNumber: json["sequence_number"],
      sectionTemplateId: json["sectiontemplateId"],
      partyId: json["partyId"],
      sellerId: json["sellerId"],
      screenId: json["screenId"],
      storeId: json["storeId"],
      documentUrl: json["document_url"],
      screen: Screen.fromJson(json["screen"]),
      sectionTemplate: SectionTemplate.fromJson(json["sectiontemplate"]),
      items: items
    );
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "section_name": sectionName,
    "sequence_number": sequenceNumber,
    "sectiontemplateId": sectionTemplateId,
    "partyId": partyId,
    "sellerId": sellerId,
    "screenId": screenId,
    "storeId": storeId,
    "screen": screen.toJson(),
    "sectiontemplate": sectionTemplate.toJson(),
  };

  setItems(List<Product> items) {
    this.items = items;
  }
}

class Screen {
  Screen({
    this.screenCode,
  });

  String screenCode;

  factory Screen.fromJson(Map<String, dynamic> json) => Screen(
    screenCode: json["screen_code"],
  );

  Map<String, dynamic> toJson() => {
    "screen_code": screenCode,
  };
}

class SectionTemplate {
  SectionTemplate({
    this.scrollDirection,
    this.templateName,
  });

  String scrollDirection;
  String templateName;

  bool isHorizontal() => scrollDirection == "horizontal";

  bool isVertical() => scrollDirection == "vertical";

  bool isAdCampaignType() => templateName == 'banner_adcampaign_section';

  bool isProductType() => templateName == 'products_section';

  bool isPromotionalCampaignType() => templateName == 'banner_promotionalcampaign_section';

  bool isSuperCategoriesType() => templateName == 'supercategories_section';

  bool isCategoriesType() => templateName == 'categories_section';


  factory SectionTemplate.fromJson(Map<String, dynamic> json) => SectionTemplate(
    scrollDirection: json["scroll_direction"],
    templateName: json["template_name"],
  );

  Map<String, dynamic> toJson() => {
    "scroll_direction": scrollDirection,
  };
}
