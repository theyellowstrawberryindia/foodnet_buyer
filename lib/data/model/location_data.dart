// To parse this JSON data, do
//
//     final locationData = locationDataFromJson(jsonString);

import 'dart:convert';

import 'package:foodnet_buyer/util/date_time_util.dart';

LocationData locationDataFromJson(String str) =>
    LocationData.fromJson(json.decode(str));

String locationDataToJson(LocationData data) => json.encode(data.toJson());

class LocationData {
  LocationData({
    this.sellerPartyId,
    this.id,
    this.storeCode,
    this.sellerId,
    this.addressId,
    this.groupName,
    this.sellerurl,
    this.partylocationId,
    this.zone,
    this.storeOpeningTime,
    this.storeClosingTime,
    this.storeOpenTimeString,
    this.storeCloseTimeString,
    this.productCatalogId,
    this.tcUrl,
    this.ppUrl,
    this.faqUrl,
    this.maxAdvanceOrderDate,
    this.store_id,
    this.store_code,
  });

  int maxAdvanceOrderDate;
  int sellerPartyId;
  int id;
  String storeCode;
  int sellerId;
  int addressId;
  String groupName;
  String sellerurl;
  int partylocationId;
  String productCatalogId = "1";
  String tcUrl;
  String ppUrl;
  String faqUrl;
  String storeOpenTimeString;
  String storeCloseTimeString;
  DateTime storeOpeningTime;
  DateTime storeClosingTime;
  Zone zone;
  int store_id;
  String store_code;

  factory LocationData.fromJson(Map<String, dynamic> json) => LocationData(
        sellerPartyId: json["sellerPartyId"],
        id: json["id"],
        storeCode: json["store_code"],
        sellerId: json["sellerId"],
        addressId: json["addressId"],
        groupName: json["group_name"],
        partylocationId: json["partylocationId"],
        sellerurl: json["seller_logo"],
        productCatalogId: json["productCatalogId"] != null
            ? json["productCatalogId"].toString()
            : "1",
        tcUrl: json["tcUrl"] != null ? json["tcUrl"].toString() : null,
        ppUrl: json["ppUrl"] != null ? json["ppUrl"].toString() : null,
        faqUrl: json["faqUrl"] != null ? json["faqUrl"].toString() : null,
        storeOpeningTime:
            DateTimeUtil.getCurrentDayWithTime(json["order_start_time"]),
        storeClosingTime:
            DateTimeUtil.getCurrentDayWithTime(json["order_end_time"]),
        storeOpenTimeString: json["order_start_time"],
        storeCloseTimeString: json["order_end_time"],
        maxAdvanceOrderDate: json["max_days_for_advance_order"] ?? 30,
        zone: json["zone"] != null ? Zone.fromJson(json["zone"]) : null,
        store_id: json["store_id"],
        store_code: json["store_code"],
      );

  bool isInOperationalHours() {
    DateTime dateTime = DateTime.now();
    return dateTime.isAtSameMomentAs(storeOpeningTime) ||
        (dateTime.isAfter(storeOpeningTime) &&
            dateTime.isBefore(storeClosingTime));
  }

  DateTime getMaxOrderDate() {
    DateTime dateTime = DateTime.now().add(Duration(days: maxAdvanceOrderDate));
    return dateTime;
  }

  String getOperationalHoursForDisplay() {
    return "$storeOpenTimeString - $storeCloseTimeString";
  }

  Map<String, dynamic> toJson() => {
        "sellerPartyId": sellerPartyId,
        "id": id,
        "store_code": storeCode,
        "sellerId": sellerId,
        "addressId": addressId,
        "group_name": groupName,
        "partylocationId": partylocationId,
        "seller_logo": sellerurl,
        "productCatalogId": productCatalogId,
        "tcUrl": tcUrl,
        "ppUrl": ppUrl,
        "faqUrl": faqUrl,
        "max_days_for_advance_order": maxAdvanceOrderDate,
        // "order_start_time": DateTimeUtil.getTimeFromIn24FormatDateTime(storeOpeningTime),
        // "order_end_time": DateTimeUtil.getTimeFromIn24FormatDateTime(storeClosingTime),
        "zone": zone.toJson(),
        "store_id": store_id,
        "store_code": store_code,
      };
}

class Zone {
  Zone({
    this.isAcceptOrder,
    this.sameDayDelivery,
    this.deliveryTime,
    this.cutoffTime,
    this.convenienceFee,
  });

  bool isAcceptOrder;
  bool sameDayDelivery;
  String deliveryTime;
  String cutoffTime;
  String convenienceFee;

  factory Zone.fromJson(Map<String, dynamic> json) => Zone(
        isAcceptOrder: json["is_accept_order"],
        sameDayDelivery: json["same_day_delivery"],
        deliveryTime: json["delivery_time"] ?? "12:00",
        cutoffTime: json["same_day_cut_off"] ?? "18:00",
        convenienceFee: json["convenience_fee"]?.toString(),
      );

  Map<String, dynamic> toJson() => {
        "is_accept_order": isAcceptOrder,
        "same_day_delivery": sameDayDelivery,
        "delivery_time": deliveryTime,
        "cutoff_time": cutoffTime,
        "convenience_fee": convenienceFee,
      };
}
