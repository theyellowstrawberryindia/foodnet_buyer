// To parse this JSON data, do
//
//     final orderDetailsModel = orderDetailsModelFromJson(jsonString);

import 'dart:convert';

import 'package:foodnet_buyer/data/model/ItemInDetailsModel.dart';
import 'package:foodnet_buyer/ui/add_gross_weight.dart';

OrderDetailsModel orderDetailsModelFromJson(String str) =>
    OrderDetailsModel.fromJson(json.decode(str));

String orderDetailsModelToJson(OrderDetailsModel data) =>
    json.encode(data.toJson());

class OrderDetailsModel {
  OrderDetailsModel({
    this.status,
    this.message,
    this.orderData,
  });

  bool status;
  String message;
  OrderData orderData;

  factory OrderDetailsModel.fromJson(Map<String, dynamic> json) =>
      OrderDetailsModel(
        status: json["status"],
        message: json["message"],
        orderData: OrderData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": orderData.toJson(),
      };
}

class OrderData {
  OrderData({
    this.id,
    this.lineItems,
    this.finalTotal,
    this.totalBase,
    this.totalTaxes,
    this.convenienceCharges,
    this.discountAmount,
    this.deliveryDate,
    this.deliveryTime,
    this.storeId,
    this.createdAt,
    this.orderitems,
    this.party,
    this.status,
    this.tid,
    this.address,
    this.grnItems,
    this.addressId,
    this.deliverychallanitems,
    this.comment,
    this.grossWeight,
    this.dcGrossWeight,
    this.itemInGrossWeight,
    this.itemindocuments,
    this.invoiceitems,
    this.invoicedocuments,
    this.invoiceAddress,
    this.invoiceNumber,
    this.grndocuments,
    this.sellerLogo,
    this.buyerLogo,
    this.isAssignedGrossWeight,
    this.dueDate,
  });

  int id;
  String lineItems;
  String finalTotal;
  String totalBase;
  String totalTaxes;
  String convenienceCharges;
  String discountAmount;
  DateTime deliveryDate;
  String deliveryTime;
  int storeId;
  DateTime createdAt;
  List<Iteminitem> orderitems = [];
  List<Iteminitem> grnItems = [];
  List<Iteminitem> deliverychallanitems = [];
  List<Iteminitem> invoiceitems = [];
  List<UploadPhotos> itemindocuments=[];
  List<UploadPhotos> invoicedocuments=[];
  List<UploadPhotos> grndocuments=[];
  Party party;
  OrderDataStatus status;
  Tid tid;
  Address address;
  InvoiceAddress invoiceAddress;
  dynamic addressId;
  dynamic grossWeight;
  String comment;
  String dcGrossWeight;
  String itemInGrossWeight;
  dynamic invoiceNumber;
  String dueDate;
  String sellerLogo, buyerLogo;
  bool isAssignedGrossWeight;

  factory OrderData.fromJson(Map<String, dynamic> json) => OrderData(
        id: json["id"],
        lineItems: json["line_items"],
        finalTotal: json["final_total"],
        totalBase: json["total_base"],
        totalTaxes: json["total_taxes"],
        convenienceCharges: json["convenience_charges"],
        discountAmount: json["discount_amount"],
        deliveryDate: DateTime.parse(json["delivery_date"]),
        deliveryTime: json["delivery_time"],
        storeId: json["storeId"],
        addressId: json.containsKey('addressId')
            ? json['addressId']
            : (json.containsKey('invoiceaddress')?InvoiceAddress.fromJson(json["invoiceaddress"]).id:Address.fromJson(json["address"]).id),
        comment: json["comment"] != null ? json["comment"].toString() : "",
        grossWeight: json.containsKey('gross_weight')
            ? json['gross_weight']
            : '',
        dcGrossWeight: json.containsKey('dc_gross_weight') ? json['dc_gross_weight']?.toString()  ?? "" : '',
        itemInGrossWeight: json.containsKey('itemin_gross_weight') ? json['itemin_gross_weight']?.toString() ?? "" : '',
    invoiceNumber: json.containsKey('invoice_number')
            ? json['invoice_number']
            : '',
        createdAt: DateTime.parse(json["createdAt"]),
        orderitems: json.containsKey('orderitems')
            ? List<Iteminitem>.from(
                json["orderitems"].map((x) => Iteminitem.fromJson(x)))
            : [],
        grnItems: json.containsKey('grnitems')
            ? List<Iteminitem>.from(
                json["grnitems"].map((x) => Iteminitem.fromJson(x)))
            : [],
    deliverychallanitems: json.containsKey('deliverychallanitems')
            ? List<Iteminitem>.from(
                json["deliverychallanitems"].map((x) => Iteminitem.fromJson(x)))
            : [],
    invoiceitems: json.containsKey('invoiceitems')
            ? List<Iteminitem>.from(
                json["invoiceitems"].map((x) => Iteminitem.fromJson(x)))
            : [],
    itemindocuments: json.containsKey('dcdocuments')
            ? List<UploadPhotos>.from(
                json["dcdocuments"].map((x) => UploadPhotos.fromJson(x )))
            : [],
    invoicedocuments: json.containsKey('invoicedocuments')
            ? List<UploadPhotos>.from(
                json["invoicedocuments"].map((x) => UploadPhotos.fromJson(x )))
            : [],
    grndocuments: json.containsKey('grndocuments')
            ? List<UploadPhotos>.from(
                json["grndocuments"].map((x) => UploadPhotos.fromJson(x )))
            : [],
        party: Party.fromJson(json["party"]),
        status: OrderDataStatus.fromJson(json["status"]),
        tid: json.containsKey('tid') ? Tid.fromJson(json["tid"]) : null,
        address: json.containsKey('address')
            ? Address.fromJson(json["address"])
            : null,
        invoiceAddress: json.containsKey('invoiceaddress')
            ? InvoiceAddress.fromJson(json["invoiceaddress"])
            : null,
        sellerLogo: json["seller_logo"],
        buyerLogo: json["buyer_logo"],
        isAssignedGrossWeight: json["isAssignedGrossWeight"] != null ? json["isAssignedGrossWeight"] : false,
        dueDate: json["dueDate"] ?? ""
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "line_items": lineItems,
        "final_total": finalTotal,
        "total_base": totalBase,
        "total_taxes": totalTaxes,
        "convenience_charges": convenienceCharges,
        "discount_amount": discountAmount,
        "delivery_date":
            "${deliveryDate.year.toString().padLeft(4, '0')}-${deliveryDate.month.toString().padLeft(2, '0')}-${deliveryDate.day.toString().padLeft(2, '0')}",
        "delivery_time": deliveryTime,
        "storeId": storeId,
        "createdAt": createdAt.toIso8601String(),
        "orderitems": List<dynamic>.from(orderitems.map((x) => x.toJson())),
        "party": party.toJson(),
        "status": status.toJson(),
        "tid": tid.toJson(),
        "address": address.toJson(),
      };
}

class Address {
  Address({
    this.addressLine2,
    this.addressLine3,
    this.area,
    this.id,
    this.flatNumber,
    this.building,
    this.society,
    this.addressLine1,
    this.city,
    this.pinCode,
  });

  String addressLine2;
  String addressLine3;
  String area;
  int id;
  String flatNumber;
  String building;
  String society;
  String addressLine1;
  String city;
  String pinCode;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        addressLine2: json["address_line2"],
        addressLine3: json["address_line3"],
        area: json["area"],
        id: json["id"],
        flatNumber: json["flat_number"],
        building: json["building"],
        society: json["society"],
        addressLine1: json["address_line1"],
        city: json["city"],
        pinCode: json["pin_code"],
      );

  Map<String, dynamic> toJson() => {
        "address_line2": addressLine2,
        "address_line3": addressLine3,
        "area": area,
        "id": id,
        "flat_number": flatNumber,
        "building": building,
        "society": society,
        "address_line1": addressLine1,
        "city": city,
        "pin_code": pinCode,
      };
      // {address_line2: , address_line3: , area: Mumbai, id: 126, 
      //flat_number: 119-123, building: Dani House, society: , address_line1: V.P.Road, city: Mumbai, pin_code: 40004},
  String toString() => '${flatNumber} ${building} \n${addressLine1} ${addressLine2} ${addressLine3}\n ${area} ${city} ${pinCode}.';
}

class InvoiceAddress {
  InvoiceAddress({
    this.addressLine2,
    this.addressLine3,
    this.area,
    this.lat,
    this.lon,
    this.zones,
    this.id,
    this.addressName,
    this.flatNumber,
    this.building,
    this.society,
    this.addressLine1,
    this.city,
    this.pinCode,
    this.addressType,
    this.createdBy,
    this.updatedBy,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.invoiceId,
  });

  String addressLine2;
  String addressLine3;
  String area;
  dynamic lat;
  dynamic lon;
  dynamic zones;
  int id;
  String addressName;
  String flatNumber;
  String building;
  String society;
  String addressLine1;
  String city;
  String pinCode;
  String addressType;
  String createdBy;
  String updatedBy;
  DateTime createdAt;
  String updatedAt;
  dynamic deletedAt;
  int invoiceId;

  factory InvoiceAddress.fromJson(Map<String, dynamic> json) => InvoiceAddress(
    addressLine2: json["address_line2"],
    addressLine3: json["address_line3"],
    area: json["area"],
    lat: json["lat"],
    lon: json["lon"],
    zones: json["zones"],
    id: json["id"],
    addressName: json["address_name"],
    flatNumber: json["flat_number"],
    building: json["building"],
    society: json["society"],
    addressLine1: json["address_line1"],
    city: json["city"],
    pinCode: json["pin_code"],
    addressType: json["address_type"],
    createdBy: json["createdBy"],
    updatedBy: json["updatedBy"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: json["updatedAt"],
    deletedAt: json["deletedAt"],
    invoiceId: json["invoiceId"],
  );

  Map<String, dynamic> toJson() => {
    "address_line2": addressLine2,
    "address_line3": addressLine3,
    "area": area,
    "lat": lat,
    "lon": lon,
    "zones": zones,
    "id": id,
    "address_name": addressName,
    "flat_number": flatNumber,
    "building": building,
    "society": society,
    "address_line1": addressLine1,
    "city": city,
    "pin_code": pinCode,
    "address_type": addressType,
    "createdBy": createdBy,
    "updatedBy": updatedBy,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt,
    "deletedAt": deletedAt,
    "invoiceId": invoiceId,
  };

  String toString()=>'${flatNumber} ${building} ${society} \n${addressLine1} ${addressLine2} ${addressLine3}\n ${area} ${city} ${pinCode} ';
}


class Orderitem {
  Orderitem({
    this.id,
    this.sequence,
    this.quantity,
    this.productCatalogId,
    this.basePrice,
    this.appliedTaxPercent,
    this.price,
    this.totalBase,
    this.totalTaxes,
    this.packOf,
    this.packOfUnit,
    this.saleUnit,
    this.packets,
    this.discountAmount,
    this.orderId,
    this.productId,
    this.totalQty,
    this.productName,
  });

  int id;
  dynamic sequence;
  dynamic quantity;
  int productCatalogId;
  dynamic basePrice;
  String appliedTaxPercent;
  dynamic price;
  String totalBase;
  String totalTaxes;
  String packOf;
  String packOfUnit;
  dynamic saleUnit;
  int packets;
  String discountAmount;
  int orderId;
  int productId;
  String totalQty;
  String productName;

  factory Orderitem.fromJson(Map<String, dynamic> json) => Orderitem(
        id: json["id"],
        sequence: json["sequence"],
        quantity: json["quantity"],
        productCatalogId: json["productCatalogId"],
        basePrice: json["base_price"],
        appliedTaxPercent: json["applied_tax_percent"],
        price: json["price"],
        totalBase: json["total_base"],
        totalTaxes: json["total_taxes"],
        packOf: json["pack_of"],
        packOfUnit: json["pack_of_unit"],
        saleUnit: json["sale_unit"],
        packets: json["packets"],
        discountAmount: json["discount_amount"],
        orderId: json["orderId"],
        productId: json["productId"],
        totalQty: json["total_qty"],
        productName: json["product_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "sequence": sequence,
        "quantity": quantity,
        "productCatalogId": productCatalogId,
        "base_price": basePrice,
        "applied_tax_percent": appliedTaxPercent,
        "price": price,
        "total_base": totalBase,
        "total_taxes": totalTaxes,
        "pack_of": packOf,
        "pack_of_unit": packOfUnit,
        "sale_unit": saleUnit,
        "packets": packets,
        "discount_amount": discountAmount,
        "orderId": orderId,
        "productId": productId,
        "total_qty": totalQty,
        "product_name": productName,
      };
}

class Party {
  Party({
    this.id,
    this.mobile,
    this.person,
  });

  int id;
  String mobile;
  dynamic person;

  factory Party.fromJson(Map<String, dynamic> json) => Party(
        id: json.containsKey('id') ? json['id'] : 0,
        mobile: json["mobile"],
        person: json["person"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "mobile": mobile,
        "person": person,
      };
}

class OrderDataStatus {
  OrderDataStatus({
    this.id,
    this.statusText,
  });

  int id;
  String statusText;

  factory OrderDataStatus.fromJson(Map<String, dynamic> json) =>
      OrderDataStatus(
        id: json["id"],
        statusText: json["status_text"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "status_text": statusText,
      };
}

class Tid {
  Tid({
    this.id,
    this.businesstids,
    this.status,
  });

  int id;
  List<Businesstid> businesstids;
  List<StatusElement> status;

  factory Tid.fromJson(Map<String, dynamic> json) => Tid(
        id: json["id"],
        businesstids: List<Businesstid>.from(
            json["businesstids"].map((x) => Businesstid.fromJson(x))),
        status: List<StatusElement>.from(
            json["status"].map((x) => StatusElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "businesstids": List<dynamic>.from(businesstids.map((x) => x.toJson())),
        "status": List<dynamic>.from(status.map((x) => x.toJson())),
      };
}

class Businesstid {
  Businesstid({
    this.id,
    this.eventName,
    this.action,
    this.eventDate,
    this.eventTime,
    this.updatedBy,
    this.updatedAt,
  });

  int id;
  String eventName;
  String action;
  DateTime eventDate;
  String eventTime;
  String updatedBy;
  DateTime updatedAt;

  factory Businesstid.fromJson(Map<String, dynamic> json) => Businesstid(
        id: json["id"],
        eventName: json["event_name"],
        action: json["action"],
        eventDate: DateTime.parse(json["event_date"]),
        eventTime: json["event_time"],
        updatedBy: json["updatedBy"],
        updatedAt: DateTime.parse(json["updatedAt"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "event_name": eventName,
        "action": action,
        "event_date":
            "${eventDate.year.toString().padLeft(4, '0')}-${eventDate.month.toString().padLeft(2, '0')}-${eventDate.day.toString().padLeft(2, '0')}",
        "event_time": eventTime,
        "updatedBy": updatedBy,
        "updatedAt": updatedAt.toIso8601String(),
      };
}

class StatusElement {
  StatusElement({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory StatusElement.fromJson(Map<String, dynamic> json) => StatusElement(
        key: json["key"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "value": value,
      };

  bool isItemInPending() {
    return key.toLowerCase() == "itemin" && value.toLowerCase() == "pending";
  }

  bool isItemOutDone() {
    return key.toLowerCase() == "itemout" && value.toLowerCase() == "completed";
  }
}
