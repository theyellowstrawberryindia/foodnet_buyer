import 'dart:convert';

Product productFromJson(String str) => Product.fromJson(json.decode(str));

String productToJson(Product data) => json.encode(data.toJson());

class Product {
  Product({
    this.productId,
    this.id,
    this.productcatalogId,
    this.storeId,
    this.storeCode,
    this.size,
    this.sizevariants,
    this.productBaseRate,
    this.productTaxRate,
    this.productMrp,
    this.isProductInstock,
    this.sequenceNumber,
    this.categoryId,
    this.productCode,
    this.productName,
    this.subText,
    this.documentUrl,
    this.sizeUnitName,
    this.sizeUnit,
    this.saleUnitName,
    this.saleUnit,
    this.type,
    this.code,
    this.label,
    this.minQty,
    this.documents,
    this.description,
    this.sameDayDelivery,
    this.bestBefore,
    this.attributes,
    this.couponText,
    this.couponCode,
    this.sectionId,
    this.adCampaignId,
    this.sectionName,
    this.sectionTemplateId,
    this.adCampaignDocumentUrl,
    this.categoryName,
    this.navigationType,
    this.isParentCategory,
  });

  static const String TYPE_CATEGORY = "CATEGORY";
  static const String TYPE_PRODUCT = "PRODUCT";
  static const String TYPE_COUPON = "COUPON";

  int productId;
  int id;
  int productcatalogId;
  int storeId;
  String storeCode;
  String size;
  List<SizeVariant> sizevariants;
  String productBaseRate;
  String productTaxRate;
  String productMrp;
  int isProductInstock = 0;
  int sequenceNumber;
  int categoryId;
  String productCode;
  String productName;
  String subText;
  String documentUrl;
  String sizeUnitName;
  String sizeUnit;
  String saleUnitName;
  String saleUnit;
  String type;
  String code;
  String label;
  String description;
  String bestBefore;
  String couponText;
  String couponCode;
  bool sameDayDelivery;
  List<ProductDocument> documents;
  Map<String, dynamic> attributes;
  double minQty = 1;

  // Ad campaign
  String sectionId;
  String sectionName;
  String sectionTemplateId;
  String adCampaignId;
  String adCampaignDocumentUrl;

  // Offer field
  String categoryName;
  bool isParentCategory;
  String navigationType;

  bool isOfferCategory() => navigationType == TYPE_CATEGORY;

  bool isOfferProduct() => navigationType == TYPE_PRODUCT;

  bool isOfferCoupon() =>
      navigationType == TYPE_COUPON;
      //|| (adCampaignId != null && navigationType?.isEmpty == true);

  bool isOfferNoNavigation() =>
      navigationType == null || navigationType.isEmpty ||
      (isOfferCategory() && categoryId == null) ||
      (isOfferProduct() && productId == null);

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        productId: json["productId"],
        id: json["id"],
        productcatalogId: json["productcatalogId"],
        storeId: json.containsKey("storeId")
            ? json["storeId"] is String
                ? int.parse(json["storeId"])
                : json["storeId"]
            : 1,
        storeCode: json.containsKey("storeCode")
            ? json["storeCode"] != null
                ? json["storeCode"]
                : "ripul_cpt"
            : "ripul_cpt",
        size: json["size"],
        productBaseRate: json["product_base_rate"],
        productTaxRate: json["product_tax_rate"],
        productMrp: json["product_mrp"],
        isProductInstock: json["is_product_instock"] ?? 0,
        sequenceNumber: json["sequence_number"],
        categoryId: json["categoryId"],
        productCode: json["product_code"],
        productName: json["product_name"],
        subText: json["sub_text"],
        documentUrl: json["document_url"],
        sizeUnitName: json["sizeUnitName"],
        sizeUnit: json["sizeUnit"],
        saleUnitName: json["saleUnitName"],
        saleUnit: json["saleUnit"],
        minQty: json["min_order_qty"] != null ? double.parse(json["min_order_qty"].toString()) : 1,
        type: json["type"],
        code: json["code"],
        label: json["label"],
        couponText: json["coupon_text"],
        couponCode: json["coupon_code"],
        sameDayDelivery: json["same_day_delivery"] != null
            ? json["same_day_delivery"]
            : true,
        description: json["description"] != null ? json["description"] : "",
        bestBefore: json["best_before"] != null ? json["best_before"] : "",
        attributes: json["attrJSON"],
        documents: json["documents"] != null
            ? List.from(
                json["documents"].map((x) => ProductDocument.fromJson(x)))
            : null,
        sizevariants: json["sizevariants"] != null
            ? json["sizevariants"] is int
                ? null
                : List.from(
                    json["sizevariants"].map((x) => SizeVariant.fromJson(x)))
            : null,
        sectionId:
            json["sectionId"] != null ? json["sectionId"].toString() : null,
        sectionName: json["section_name"],
        sectionTemplateId: json["sectionTemplateId"] != null
            ? json["sectionTemplateId"].toString()
            : null,
        adCampaignId: json["adcampaignId"] != null
            ? json["adcampaignId"].toString()
            : null,
        adCampaignDocumentUrl: json["adcampaign_document_url"],
        categoryName: json["category_name"],
        navigationType: json["navigation_type"],
        isParentCategory: (json["isParentCategory"] ?? 0) != 0,
      );

  Map<String, dynamic> toJson() => {
        "productId": productId,
        "id": id,
        "productcatalogId": productcatalogId,
        "storeId": storeId,
        "storeCode": storeCode,
        "size": size,
        "min_order_qty": minQty,
        "sizevariants": sizevariants,
        "product_base_rate": productBaseRate,
        "product_tax_rate": productTaxRate,
        "product_mrp": productMrp,
        "is_product_instock": isProductInstock,
        "sequence_number": sequenceNumber,
        "categoryId": categoryId,
        "category_name": categoryName,
        "product_code": productCode,
        "product_name": productName,
        "sub_text": subText,
        "document_url": documentUrl,
        "sizeUnitName": sizeUnitName,
        "sizeUnit": sizeUnit,
        "saleUnitName": saleUnitName,
        "saleUnit": saleUnit,
        "type": type,
      };

  bool isOffer() => type?.toLowerCase() == "offercard";
  bool hasVariants() => sizevariants != null && sizevariants.isNotEmpty;
  bool isAvailable() => isProductInstock > 0;
}

class ProductDocument {
  ProductDocument({
    this.documentUrl,
    this.documentUsage,
  });

  String documentUrl;
  String documentUsage;

  factory ProductDocument.fromJson(Map<String, dynamic> json) =>
      ProductDocument(
        documentUrl: json["document_url"],
        documentUsage: json["document_usage"],
      );

  Map<String, dynamic> toJson() => {
        "document_url": documentUrl,
        "document_usage": documentUsage,
      };
}

class SizeVariant {
  SizeVariant({
    this.id,
    this.baseUom,
    this.saleQty,
    this.taxRate,
    this.salePrice,
    this.sellingUom,
    this.productPrice,
    this.productDisplayName,
  });

  String id;
  String baseUom;
  String saleQty;
  String taxRate;
  String salePrice;
  String sellingUom;
  String productPrice;
  String productDisplayName;

  factory SizeVariant.fromJson(Map<String, dynamic> json) => SizeVariant(
        id: json["id"],
        baseUom: json["baseUOM"],
        saleQty: json["saleQty"],
        taxRate: json["taxRate"],
        salePrice: json["salePrice"],
        sellingUom: json["sellingUOM"],
        productPrice: json["productPrice"],
        productDisplayName: json["productDisplayName"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "baseUOM": baseUom,
        "saleQty": saleQty,
        "taxRate": taxRate,
        "salePrice": salePrice,
        "sellingUOM": sellingUom,
        "productPrice": productPrice,
        "productDisplayName": productDisplayName,
      };
}
