import 'dart:io';

import 'package:foodnet_buyer/util/preference_manager.dart';

class HeaderUtils {
  HeaderUtils._();


  static Map<String,String> getEncodedHeader(){
    return  {
      "Content-Type": "application/x-www-form-urlencoded",
      "Content-type": "application/json",
      HttpHeaders.authorizationHeader: PreferenceManager.getAccessToken() != null ? "Bearer ${PreferenceManager.getAccessToken()}" : null,
    };
  }
}