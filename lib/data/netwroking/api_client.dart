import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:foodnet_buyer/data/netwroking/HeaderUtils.dart';
import 'package:foodnet_buyer/data/netwroking/api_path.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';

import 'custom_exception.dart';

class ApiClient {
  static ApiClient _instance = ApiClient.internal();

  factory ApiClient() {
//    _initializeDio(_instance);
    return _instance;
  }

  ApiClient.internal();

  static ApiClient getInstance() {
    return _instance;
  }

  static const int TOKEN_EXPIRE = 419;
  static const int UN_AUTHORISE = 401;

  int _refreshRetry = 0;

  void initializeDio({int timeOut = 20000}) {
    BaseOptions options = BaseOptions(
        receiveTimeout: timeOut, connectTimeout: timeOut, baseUrl: "");
    _dio = Dio(options);
    if (kDebugMode) {
      _dio.interceptors
        ..add(LogInterceptor(
            responseBody: true, requestBody: true, requestHeader: true));
    }
  }

  Dio _dio;

  dynamic _handleResponse(Response response) {
    _refreshRetry = 0;

    final int statusCode = response.statusCode;
    print("statusCode: $statusCode");

    final isSuccess = statusCode >= 200 && statusCode <= 299;

    if (statusCode == 204) {
      return true;
    } else if (isSuccess) {
      return response.data; //json.decode(response.data);
    }

    switch (statusCode) {
      case 500:
        throw CustomException(500, CustomException.ERROR_500_MSG);
      default:
        throw CustomException(
            CustomException.ERROR_DEFAULT, json.encode(response.data));
    }
  }

  dynamic _handleError(Exception exception) {
    _refreshRetry = 0;
    String errorMsg = "";
    int errorCode = 0;
    String apiUrl = "";
    try {
      if (exception is DioError) {
        errorCode = exception.response?.statusCode ?? 0;
        apiUrl = exception.request.uri.toString();

        if (exception.type == DioErrorType.CONNECT_TIMEOUT ||
            exception.type == DioErrorType.DEFAULT) {
          errorMsg = "${CustomException.DEFAULT_CONNECT_TIMEOUT_MSG}";
        } else {
          switch (exception.response.statusCode) {
            case 400:
            case 401:
            case 403:
              errorMsg = CustomException.ERROR_403_MSG;
              break;
            case 404:
              errorMsg = "${CustomException.ERROR_DEFAULT_API_MSG}";
              break;
            case 408:
            case 499:
              errorMsg = "${CustomException.ERROR_499_MSG}";
              break;
            case 300:
            case 500:
            case 501:
            case 502:
            case 503:
              errorMsg = "${CustomException.ERROR_500_MSG}";
              break;
            default:
              errorMsg = "${CustomException.ERROR_CRARSH_MSG}";
          }
        }
      } else if (exception is SocketException) {
        errorMsg = CustomException.ERROR_NO_INTERNETCONNECTION;
        errorCode = CustomException.ERROR_CONNECTION;
      } else {
        errorMsg = "${CustomException.ERROR_CRARSH_MSG}";
        errorCode = CustomException.ERROR_DEFAULT;
      }
    } catch (e) {
      errorMsg = "${CustomException.ERROR_CRARSH_MSG}";
      errorCode = CustomException.ERROR_DEFAULT;
    }

    throw CustomException(errorCode, errorMsg);
  }

  Future<bool> refreshSSOToken(dynamic exception) async {
    if (_refreshRetry > 0) {
      print("Token refreshed, but still getting same error");
      return false;
    }
    if (exception is DioError) {
      //return false;
      final int statusCode = exception.response?.statusCode ?? 0;
      if (statusCode == TOKEN_EXPIRE) {
        // refresh token logic
        var params = {"token": PreferenceManager.getRefreshToken()};
        var response = await post(ApiPath.REFRESH_TOKEN, body: params);
        PreferenceManager.setAccessToken(response["accessToken"]);
        _refreshRetry++;
        return true;
      } else if (statusCode == UN_AUTHORISE) {
        // send to login
        AppUtils.log("User unauthorised");
        return false;
      }
    }

    return false;
  }

  Future<dynamic> get(
    String url, {
    Map<String, String> headers,
  }) async {
    bool hasNet = await AppUtils.isConnectedToInternet();
    if (!hasNet) {
      throw CustomException(CustomException.ERROR_CONNECTION,
          CustomException.ERROR_NO_INTERNETCONNECTION);
    }
    headers = HeaderUtils.getEncodedHeader();

    Options options = Options(headers: headers);


    if(kDebugMode){
      log('URL -> \n $url');
      log('No Params ->');
    }

    try {
      var response = await _dio.get(url, options: options);
      if(kDebugMode){
        log('response -> \n $response');
      }
      return _handleResponse(response);
    } catch (exception) {
      if (await refreshSSOToken(exception)) {
        return await get(url, headers: headers);
      }
      _handleError(exception);
    }
    return null;
  }

  Future<dynamic> post(
    String url, {
    Map<String, String> headers,
    body,
    String contentType = Headers.jsonContentType,
  }) async {

    bool hasNet = await AppUtils.isConnectedToInternet();
    if (!hasNet) {
      throw CustomException(CustomException.ERROR_CONNECTION,
          CustomException.ERROR_NO_INTERNETCONNECTION);
    }

    headers = HeaderUtils.getEncodedHeader();

    Options options = Options(headers: headers, contentType: contentType);

    var bodyData =
        contentType == Headers.jsonContentType ? json.encode(body) : body;

    if(kDebugMode){
      log('URL -> \n $url');
      log('Params -> \n $body');
    }


    try {
      var response = await _dio.post(url, options: options, data: bodyData);
      if(kDebugMode){
        log('response -> \n $response');
      }
      return _handleResponse(response);
    } catch (exception) {
      if (await refreshSSOToken(exception)) {
        return await post(url,
            headers: headers, body: body, contentType: contentType);
      }
      return _handleError(exception);
    }
  }

  Future<dynamic> put(
    String url, {
    Map<String, String> headers,
    body,
  }) async {
    bool hasNet = await AppUtils.isConnectedToInternet();
    if (!hasNet) {
      throw CustomException(CustomException.ERROR_CONNECTION,
          CustomException.ERROR_NO_INTERNETCONNECTION);
    }

    headers = HeaderUtils.getEncodedHeader();

    Options options = Options(headers: headers);

    try {
      var response = await _dio.put(url, options: options, data: body);
      return _handleResponse(response);
    } catch (exception) {
      if (await refreshSSOToken(exception)) {
        return await put(url, headers: headers, body: body);
      }
      _handleError(exception);
    }
    return null;
  }

  Future<dynamic> delete(
    String url, {
    Map<String, String> headers,
    body,
  }) async {
    bool hasNet = await AppUtils.isConnectedToInternet();
    if (!hasNet) {
      throw CustomException(CustomException.ERROR_CONNECTION,
          CustomException.ERROR_NO_INTERNETCONNECTION);
    }

    headers = HeaderUtils.getEncodedHeader();

    Options options = Options(headers: headers);

    try {
      var response = await _dio.delete(url, options: options, data: body);
      return _handleResponse(response);
    } catch (exception) {
      if (await refreshSSOToken(exception)) {
        return await delete(
          url,
          headers: headers,
          body: body,
        );
      }
      _handleError(exception);
    }
    return null;
  }
}
