class ApiPath {

  static const String TYPE_LOGIN = "login";
  static const String TYPE_REGISTRATION = "registration";

  // static const String BASE_PATH = "http://44.235.129.240:10004/api/v1/";
  static const String BASE_PATH = "https://business.truptitech.com/api/v1/";
  // static const String AUTH_BASE_PATH = "https://auth.foodnet.co.in/api/v1/";
  static const String AUTH_BASE_PATH = "https://auth.truptitech.com/api/v1/";
  // static const String BASE_DMS = "https://dms.foodnet.co.in/api/v1/single/documents";
  static const String BASE_DMS = "https://dms.truptitech.com/api/v1/single/documents";

  static const String LOGIN = "${AUTH_BASE_PATH}login";
  static const String OTP = "${AUTH_BASE_PATH}otp";
  static const String CATEGORY_TREE_PATH =
      "${BASE_PATH}explore/products/categorytree";
  static const String ALL_PRODUCT_PATH =
      "${BASE_PATH}explore/products/productsbycategory/";
  static const String PRODUCT__DETAIL_PATH =
      "${BASE_PATH}explore/products/productdetail/";
  static const String SAVE_DEVICE_LOCATION =
      "${BASE_PATH}explore/devicelocations/save";
  static const String ADD_TO_CART = "${BASE_PATH}explore/shoppingcarts/add";
  static const String CART_LISTING =
      "${BASE_PATH}explore/shoppingcarts/shoppingcart";
  static const String ASSOCIATE_CART_WITH_USER =
      "${BASE_PATH}explore/shoppingcarts/cartassociation";
  static const String CHECKOUT = "${BASE_PATH}front/shoppingcarts/checkout/";
  static const String ORDER_DETAIL = "${BASE_PATH}front/orders/myorderdetails";
  static const String REFRESH_TOKEN = "${AUTH_BASE_PATH}token";
  static const String HOME_SECTION =
      "${BASE_PATH}explore/shopping/retreivescreensections/$TENANT_ID";
  static const String SECTION_CONTENT =
      "${BASE_PATH}explore/shopping/retreivesectionchildren/$TENANT_ID";
  static const String SEARCH = "${BASE_PATH}explore/search/products/$TENANT_ID";
  static const String ALL_ADDRESS =
      "${BASE_PATH}front/parties/partyaddresses/$TENANT_ID";
  static const String ADD_ADDRESS =
      "${BASE_PATH}front/consumer_registration/address";
  static const String UPDATE_ADDRESS =
      "${BASE_PATH}front/parties/addressupdate";
  static const String DELETE_ADDRESS =
      "${BASE_PATH}front/parties/addressremove";
  static const String MY_ORDERS = "${BASE_PATH}front/orders/myorders";
  static const String ORDER_DETAIL_PAGE =
      "${BASE_PATH}front/orders/myorderdetails";
  static const String DELIVERY_ADDRESS_CHANGE =
      "${BASE_PATH}explore/shoppingcarts/deliveryaddress";
  static const String NEAREST_STORE =
      "${BASE_PATH}explore/shoppingcarts/neareststore";
  static const String COUPON_LISTING =
      "${BASE_PATH}explore/coupons/retreivecouponswithpartyhistory/$TENANT_ID";
  static const String AD_CAMPAIGN_COUPON_DETAIL =
      "${BASE_PATH}explore/shopping/retreiveadcampaign/$TENANT_ID";
  static const String COUPON_DETAIL =
      "${BASE_PATH}explore/coupons/retreivecoupon/$TENANT_ID";
  static const String REORDER = "${BASE_PATH}explore/orders/reorder";
  static const String DELETE_CART = "${BASE_PATH}front/shoppingcarts/clear/";
  static const String SAVE_USER_DEVICE =
      "${BASE_PATH}explore/devicelocations/saveUserDevice";
  static const String NOTIFICATION_DETAILS =
      "${BASE_PATH}front/parties/partynotification/$TENANT_ID";
  static const String LOGOUT = "${BASE_PATH}explore/parties/logoff";
  static const String CATEGORY_ALL_PRODUCT =
      "${BASE_PATH}explore/products/productsbycategory/$TENANT_ID";
  static const String BUSINESS_ORDERS =
      "${BASE_PATH}front/orders/businessorders";
  static const String BUSINESS_ORDERS_DETAILS =
      "${BASE_PATH}front/orders/businessorderdetails";
  static const String BUSINESS_ORDERS_GRN_DETAILS =
      "${BASE_PATH}front/grn/grns";
  static const String BUSINESS_ORDERS_UPDATE =
      "${BASE_PATH}front/orders/update";
  static const String BUSINESS_ORDERS_UPDATE_GRN =
      "${BASE_PATH}front/grn/update";
  static const String BUSINESS_ORDERS_GROSS_WEIGHT =
      "${BASE_PATH}front/itemin/grossweight";
  static const String BUSINESS_ORDERS_ITEM_IN =
      "${BASE_PATH}front/orders/orderitemin";
  static const String BUSINESS_UPDATE_GRN_GROSS_WEIGHT = "${BASE_PATH}front/grn/grossweight";
  static const String BUSINESS_UPDATE_GRN_COMMENT = "${BASE_PATH}front/grn/comment";
  static const String BUSINESS_ORDERS_GRN = "${BASE_PATH}front/orders/ordergrn";
  static const String BUSINESS_ORDERS_PAYMENT = "${BASE_PATH}front/orders/payment";
  static const String BUSINESS_ORDERS_ITEM_IN_DETAILS =
      "${BASE_PATH}front/itemin/itemindetails";
  static const String BUSINESS_ORDERS_DELIVERY_CHALLAN_DETAILS =
      "${BASE_PATH}front/deliverychallans/deliverychallandetails";
  static const String BUSINESS_ORDERS_Invoices =
      "${BASE_PATH}front/invoice/invoices";
  static const String BUSINESS_ORDERS_ITEM_IN_DOC =
      "${BASE_PATH}front/itemin/documents";
  static const String BUSINESS_ORDERS_GRN_DOC =
      "${BASE_PATH}front/grn/documents";

  static const String SEND_PURCHASE_ORDER = "${BASE_PATH}front/orders/send";
  static const String UPDATE_ORDER = "${BASE_PATH}front/orders/update";
  static const String BUSINESS_ORDER_DETAILS =
      "${BASE_PATH}front/orders/businessorderdetails";

  static const String PURCHASE_ORDER = "${BASE_PATH}front/orders/purchaseorders";
  static const String ORDER_ALL_IMAGES = "${BASE_PATH}front/orders/documents";
  static const String ALL_ENTITY_COMMENTS = "${BASE_PATH}explore/comments/comment/${TENANT_ID}";
  static const String ALL_ORDER_COMMENTS = "${BASE_PATH}front/orders/comments";
  static const String ALL_ORDER_GROSS_WEIGHT = "${BASE_PATH}front/orders/grossweights";

  static const String TENANT_ID = "1";
  static const String APP_NAME = "BUSINESS";
  static const String TAB = "Purchase Order";
}
