class CustomException implements Exception {
  static const int ERROR_CONNECTION = 29;
  static const int ERROR_DEFAULT = 003;

  static const String ERROR_CONFIG_API_MSG =
      "We are unable to connect to the FoodNet service currently.";
  static const String ERROR_403_MSG = "Unauthorised request";
  static const String ERROR_404_MSG =
      "We are having trouble playing this content right now.";
  static const String ERROR_499_MSG =
      "We are unable to connect to FoodNet service currently. Please check your internet connection and try again.";
  static const String ERROR_500_MSG =
      "We are unable to connect to FoodNet service currently.";
  static const String ERROR_DEFAULT_API_MSG =
      "We are unable to connect to FoodNet service currently.";
  static const String ERROR_DEFAULT_MSG =
      "We are having trouble playing this content right now.";
  static const String ERROR_NO_INTERNETCONNECTION =
      "We are unable to connect to FoodNet service currently. Please check your internet connection and try again.";

  static const String DEFAULT_CONNECT_TIMEOUT_MSG =
      "We encountered an unexpected error";
  static const String ERROR_CRARSH_MSG =
      "Something went wrong, Please try again";

  final String _message;
  final int _code;

  CustomException([this._code = 0, this._message]);

  int getCode() => _code;

  String getMsg() => _message;

  String toString() {
    return "$_message";
  }
}
