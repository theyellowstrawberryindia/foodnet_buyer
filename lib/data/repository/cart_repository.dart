import 'package:foodnet_buyer/data/model/OrderModel.dart';
import 'package:foodnet_buyer/data/model/cart_response.dart';
import 'package:foodnet_buyer/data/netwroking/api_client.dart';
import 'package:foodnet_buyer/data/netwroking/api_path.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';

class CartRepository {
  static CartRepository _instance = CartRepository.get();

  factory CartRepository() {
    return _instance;
  }

  CartRepository.get();

  Future<dynamic> addToCart(Map<String, dynamic> updatedCart) async {
    var response = await ApiClient.getInstance()
        .post(ApiPath.ADD_TO_CART, body: updatedCart);
    return response;
  }

  Future<dynamic> updateOrder(Map<String, dynamic> updatedCart) async {
    var response = await ApiClient.getInstance()
        .post(ApiPath.UPDATE_ORDER, body: updatedCart);
    return response;
  }

  Future<CartResponse> getCart(String cartId, int partyId) async {
    var params = {"userPartyId": partyId, "shoppingcart_id": cartId};
    if (PreferenceManager.getLatitude() != null &&
        PreferenceManager.getLongitude() != null) {
      params["lat"] = PreferenceManager.getLatitude();
      params["lon"] = PreferenceManager.getLongitude();
    }
    var response =
        await ApiClient.getInstance().post(ApiPath.CART_LISTING, body: params);
    CartResponse cartResponse = CartResponse.fromJson(response["data"]);
    return cartResponse;
  }

  Future<CartResponse> getPOCart(int tidId) async {
    var params = {"tenantId": ApiPath.TENANT_ID, "tidId": tidId.toString()};
    /* if (PreferenceManager.getLatitude() != null &&
        PreferenceManager.getLongitude() != null) {.
      params["lat"] = PreferenceManager.getLatitude();
      params["lon"] = PreferenceManager.getLongitude();
    }*/
    var response = await ApiClient.getInstance()
        .post(ApiPath.BUSINESS_ORDER_DETAILS, body: params);
    CartResponse cartResponse = CartResponse.fromJson(response["data"]);
    return cartResponse;
  }

  Future<dynamic> associateCartWithUser(String cartId, String partyId) async {
    var params = {"shoppingcart_id": cartId, "partyId": partyId};
    var response = await ApiClient.getInstance()
        .post(ApiPath.ASSOCIATE_CART_WITH_USER, body: params);
    return response;
  }

  Future<List<CartResponse>> checkoutOrder(String couponCode) async {
    String cartId = PreferenceManager.getCartId();
    // FBAnalytics.sendCheckoutInitiatedEvent(cartId, transactionId);
    var params = {
      "shoppingcart_id": cartId,
      "partyId": PreferenceManager.getPartyId(),
      // "transaction_id": transactionId,
      // "payment_status": paymentStatus,
    };
    if (couponCode != null) {
      params["couponCode"] = couponCode;
    }
    var response =
        await ApiClient.getInstance().post(ApiPath.CHECKOUT, body: params);
    if (response["status"] == true) {
      // FBAnalytics.sendCheckoutEvent(cartId, transactionId);
      PreferenceManager.setCartId(null);
      PreferenceManager.saveCartResponse(null);
      List<CartResponse> orders = List<CartResponse>.from(
          response["data"].map((x) => CartResponse.fromJson(x)));
      return orders;
    } else {
      // FBAnalytics.onCheckoutFailed(cartId, transactionId);
    }
    return null;
  }

  Future<dynamic> getOrderDetail(String orderId) async {
    var params = {"tenantId": ApiPath.TENANT_ID, "tidId": orderId};
    var response =
        await ApiClient.getInstance().post(ApiPath.ORDER_DETAIL, body: params);
//    CartResponse cartResponse = CartResponse.fromJson(response["data"]);
    return response;
  }

  Future<dynamic> getChangeDeliveryAddress(String addressId) async {
    var params = {
      "shoppingcart_id": PreferenceManager.getCartId(),
      "store_code": PreferenceManager.getLocationData().storeCode,
      "addressId": addressId
    };
    var response = await ApiClient.getInstance()
        .post(ApiPath.DELIVERY_ADDRESS_CHANGE, body: params);
//    CartResponse cartResponse = CartResponse.fromJson(response["data"]);
    return response;
  }

  Future<dynamic> clearCart() async {
    if (PreferenceManager.isLoggedIn()) {
      var params = {"partyId": PreferenceManager.getPartyId()};
//      if (PreferenceManager.getCartId() != null) {
//        params["shoppingcart_id"] = PreferenceManager.getCartId();
//      } else {
//        params["partyId"] = PreferenceManager.getPartyId();
//      }
      PreferenceManager.setCartId(null);
      PreferenceManager.saveCartResponse(null);
      var response =
          await ApiClient.getInstance().post(ApiPath.DELETE_CART, body: params);
      return response;
    } else {
      PreferenceManager.setCartId(null);
      PreferenceManager.saveCartResponse(null);
    }
  }

  Future<OrderModel> getPurchaseOrder() async {
    var params = {
      "tenantId": ApiPath.TENANT_ID,
      "partyId": PreferenceManager.getPartyId(),
      "tab": ApiPath.TAB,
      "app_name": ApiPath.APP_NAME,
    };
    var response = await ApiClient.getInstance()
        .post(ApiPath.PURCHASE_ORDER, body: params);
    return OrderModel.fromJson(response);
  }

  Future<dynamic> sendPurchaseOrder(List<int> ids) async {
    var params = {"tenantId": ApiPath.TENANT_ID, "ids": ids};
    var response = await ApiClient.getInstance()
        .post(ApiPath.SEND_PURCHASE_ORDER, body: params);
    return response;
  }
}
