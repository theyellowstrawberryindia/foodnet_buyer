import 'dart:math';

import 'package:foodnet_buyer/data/model/LoginTokenResponse.dart';
import 'package:foodnet_buyer/data/model/category_response.dart';
import 'package:foodnet_buyer/data/model/coupon_item.dart';
import 'package:foodnet_buyer/data/model/home_section.dart';
import 'package:foodnet_buyer/data/model/location_data.dart';
import 'package:foodnet_buyer/data/model/product.dart';
import 'package:foodnet_buyer/data/netwroking/api_client.dart';
import 'package:foodnet_buyer/data/netwroking/api_path.dart';
import 'package:foodnet_buyer/util/device_util.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';

class MainRepository {
  static MainRepository _instance = MainRepository.get();

  factory MainRepository() {
    return _instance;
  }

  MainRepository.get();

  Future<CategoryResponse> getCategories({String lat, String lng}) async {
    var params = {
      "tenantId": 1,
      "location": {"latitude": lat, "longitude": lng}
    };
    var response = await ApiClient.getInstance()
        .post(ApiPath.CATEGORY_TREE_PATH, body: params);
    CategoryResponse categoryResponse = CategoryResponse.fromJson(response);
    return categoryResponse;
  }

  Future<List<Product>> getAllProductsOfSubCategory(
      String categoryId, String catalogId, bool isParentCategory) async {
    String path = isParentCategory
        ? "${ApiPath.CATEGORY_ALL_PRODUCT}?parentCategoryId=$categoryId&productcatalogId=$catalogId&tenantId=${ApiPath.TENANT_ID}&buyerId=${PreferenceManager.getBuyerId()}"
        : "${ApiPath.ALL_PRODUCT_PATH}${ApiPath.TENANT_ID}?categoryId=$categoryId&productcatalogId=$catalogId&tenantId=${ApiPath.TENANT_ID}&buyerId=${PreferenceManager.getBuyerId()}";
    var response = await ApiClient.getInstance().get(
      path,
    );
    List<Product> products =
        List<Product>.from(response["data"].map((x) => Product.fromJson(x)));
    return products;
  }

  Future<Product> getProductDetails(String productId, String catalogId) async {
    String path =
        "${ApiPath.PRODUCT__DETAIL_PATH}${ApiPath.TENANT_ID}?productId=$productId&productcatalogId=$catalogId";
    var response = await ApiClient.getInstance().get(
      path,
    );
    Product product = Product.fromJson(response["data"]);
    return product;
  }

  Future<LocationData> saveUserLocation(
      String latitude, String longitude) async {
    var params = {
      "model_name": DeviceUtil.getDeviceModel(),
      "device_uuid": DeviceUtil.getDeviceID(),
      "device_name": DeviceUtil.getDeviceName(),
      "latitude": latitude,
      "longitude": longitude,
      "tenantId": ApiPath.TENANT_ID
    };
    String path = "${ApiPath.SAVE_DEVICE_LOCATION}";
    var response = await ApiClient.getInstance().post(path, body: params);
    LocationData data = LocationData.fromJson(response["data"]);
    return data;
  }

  Future<dynamic> saveUserDeviceForNotification(String token) async {
    var params = {
      "device_uuid": DeviceUtil.getDeviceID(),
      "device_token": token,
      "partyId": PreferenceManager.getUserPartyId(),
      "app_name": "BUYER",
    };
    String path = "${ApiPath.SAVE_USER_DEVICE}";
    var response = await ApiClient.getInstance().post(path, body: params);
    return response;
  }

  Future<dynamic> logout() async {
    var params = {
      "device_uuid": DeviceUtil.getDeviceID(),
      "partyId": PreferenceManager.getUserPartyId()
    };
    var response =
        await ApiClient.getInstance().post(ApiPath.LOGOUT, body: params);
    return response;
  }

//  1?tenantId=1&storeId=1&screenId=
  Future<List<HomeSection>> getHomeSection() async {
    String path =
        "${ApiPath.HOME_SECTION}?tenantId=${ApiPath.TENANT_ID}&buyerId=${PreferenceManager.getBuyerId()}&screenId=1";
    var response = await ApiClient.getInstance().get(
      path,
    );
    List<HomeSection> sections = List<HomeSection>.from(
        response["data"].map((x) => HomeSection.fromJson(x)));
    return sections;
  }

  Future<List<dynamic>> getHomeSectionContent(String sectionId) async {
    String path =
        "${ApiPath.SECTION_CONTENT}?sectionId=$sectionId&storeId=${PreferenceManager.getLocationData().id}";
    var response = await ApiClient.getInstance().get(
      path,
    );
    if (sectionId == "2") {
      List<SuperCategory> categories = List<SuperCategory>.from(
          response["data"].map((x) => SuperCategory.fromJson(x)));
      return categories;
    }
    if (sectionId == "5") {
      List<Child> categories =
          List<Child>.from(response["data"].map((x) => Child.fromJson(x)));
      return categories;
    }
    List<Product> products =
        List<Product>.from(response["data"].map((x) => Product.fromJson(x)));
    return products;
  }

  Future<List<Product>> getSearchResult(String keyword) async {
    String path =
        "${ApiPath.SEARCH}?search=$keyword&storeId=1/*${PreferenceManager.getLocationData()?.store_id ?? 1}*/";
    var response = await ApiClient.getInstance().get(
      path,
    );
    List<Product> products =
        List<Product>.from(response["data"].map((x) => Product.fromJson(x)));
    return products;
  }

  Future<dynamic> getAllAddress({int pageNo = 1}) async {
    String path =
        "${ApiPath.ALL_ADDRESS}?partyId=${PreferenceManager.getPartyId()}";
    var response = await ApiClient.getInstance().get(
      path,
    );
    return response;
  }

  Future<dynamic> addUpdateAddress(Map<String, dynamic> params) async {
    String path =
        "${params.containsKey("id") ? ApiPath.UPDATE_ADDRESS : ApiPath.ADD_ADDRESS}";
    var response = await ApiClient.getInstance().post(path, body: params);
    return response;
  }

  Future<dynamic> deleteAddress(String addressId) async {
    String path = "${ApiPath.DELETE_ADDRESS}";
    dynamic addressSaved = PreferenceManager.getUserSelectedAddress();
    var response = await ApiClient.getInstance().post(path, body: {
      "partyId": PreferenceManager.getPartyId(),
      "addressId": addressId
    });
    if (response != null && response["status"] == true) {
      if (addressSaved != null && addressSaved["id"].toString() == addressId) {
        PreferenceManager.saveUserSelectedAddress(null);
        // PreferenceManager.setAddress(await MapsUtil.addressFromLocation(double.parse(addressSaved["lat"].toString()), double.parse(addressSaved["lon"].toString())));
      }
    }
    return response;
  }

  Future<dynamic> getMyOrders() async {
    var params = {
      "tenantId": ApiPath.TENANT_ID,
      "partyId": PreferenceManager.getPartyId()
    };
    var response =
        await ApiClient.getInstance().post(ApiPath.MY_ORDERS, body: params);
    return response;
  }

  Future<dynamic> getMyOrderDetail(String tid) async {
    var params = {"tenantId": ApiPath.TENANT_ID, "tidId": tid};
    var response = await ApiClient.getInstance()
        .post(ApiPath.ORDER_DETAIL_PAGE, body: params);
    return response;
  }

  Future<LocationData> checkNearByStore(
      String latitude, String longitude) async {
    var params = {
      "latitude": latitude,
      "longitude": longitude,
      "tenantId": ApiPath.TENANT_ID
    };
    String path = "${ApiPath.NEAREST_STORE}";
    var response = await ApiClient.getInstance().post(path, body: params);
    LocationData data = LocationData.fromJson(response["data"]);
    return data;
  }

  Future<dynamic> reorder(String orderId, String code,
      {bool delete = false}) async {
    var params = {
      "orderId": orderId,
      "partyId": PreferenceManager.getPartyId(),
      "delete": delete,
      "store_code": code
    };
    String path = "${ApiPath.REORDER}";
    var response = await ApiClient.getInstance().post(path, body: params);
    return response;
  }

  Future<List<CouponItem>> getCouponListing() async {
    String path =
        "${ApiPath.COUPON_LISTING}?partyId=${ApiPath.TENANT_ID}&consumerPartyId=${PreferenceManager.getPartyId()}";
    var response = await ApiClient.getInstance().get(
      path,
    );
    List<CouponItem> items = List<CouponItem>.from(
        response["data"].map((x) => CouponItem.fromJson(x)));
    return items;
  }

  Future<dynamic> getCouponDetails(String id, bool fromCoupon) async {
    String path = fromCoupon
        ? "${ApiPath.COUPON_DETAIL}?partyId=${ApiPath.TENANT_ID}&partyId=${PreferenceManager.getPartyId()}&couponId=$id"
        : "${ApiPath.AD_CAMPAIGN_COUPON_DETAIL}?adcampaignId=$id&partyId=${PreferenceManager.getPartyId()}";
    var response = await ApiClient.getInstance().get(
      path,
    );
    return response["data"];
  }

  Future<dynamic> getNotificationListing() async {
    String path =
        "${ApiPath.NOTIFICATION_DETAILS}?partyId=${PreferenceManager.getPartyId()}";
    var response = await ApiClient.getInstance().get(
      path,
    );
    return response != null && response["data"] != null ? response["data"] : [];
  }

  Future<dynamic> getAllProductOfCategory(Child parentCategory) async {
    String path =
        "${ApiPath.CATEGORY_ALL_PRODUCT}?parentCategoryId=${parentCategory.id}&productcatalogId=${PreferenceManager.getLocationData().productCatalogId}&tenantId=${ApiPath.TENANT_ID}&buyerId=${PreferenceManager.getPartyId()}";
    var response = await ApiClient.getInstance().get(
      path,
    );
    List<Product> products =
        List<Product>.from(response["data"].map((x) => Product.fromJson(x)));

//    Map<int, int> subCategoryItemCountMap = {};
    List<Map<String, double>> itemCountList = [];
    Map<int, Map<String, double>> subCategoryStartEndMap = {};
    List<Child> subCategories = parentCategory.children;
    double column1Height = 20;
    double column2Height = 20;
    int subCategoryCounter = 0;
    for (int i = 0; i < products.length; i++) {
      Product element = products[i];
      if (!element.isOffer() &&
          element.categoryId != subCategories[subCategoryCounter].id) {
        subCategoryCounter++;
      }

      double elementHeight = element.isOffer() ? 180 : 273;
      if (column1Height > column2Height) {
        column2Height += elementHeight;
      } else {
        column1Height += elementHeight;
      }

      subCategoryStartEndMap[subCategories[subCategoryCounter].id] = {
        "start": subCategoryCounter == 0
            ? 20
            : subCategoryStartEndMap[subCategories[subCategoryCounter - 1].id]
                ["end"],
        "end": min(column1Height, column2Height)
      };
    }

    subCategories.forEach((element) {
      itemCountList.add(subCategoryStartEndMap[element.id]);
    });

    return {"products": products, "itemCountList": itemCountList};
  }

  Future<LoginTokenResponse> loginUser(String number, String otp) async {
    var params = {
      "tenant_id": 1,
      "app_name": "BUYER",
      "username": number, //"7021789449",
      "otp": otp, //"3301"
    };
    String path = "${ApiPath.LOGIN}";
    var response = await ApiClient.getInstance().post(path, body: params);
    var data = LoginTokenResponse.fromJson(response);
    return data;
  }

  Future<dynamic> sendOtp(String number, String type) async {
    var params = {
      "tenantId": 1,
      "app_name": "BUYER",
      "username": number, //"7021789449",
    };
    if (type != null) {
      params["usage"] = type;
    }
    String path = "${ApiPath.OTP}";
    var response = await ApiClient.getInstance().post(path, body: params);
    return response;
  }
}
