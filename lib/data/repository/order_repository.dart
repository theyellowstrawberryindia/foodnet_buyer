import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:foodnet_buyer/data/model/AreaModel.dart';
import 'package:foodnet_buyer/data/model/ItemInDetailsModel.dart';
import 'package:foodnet_buyer/data/model/OrderModel.dart';
import 'package:foodnet_buyer/data/model/order_details_model.dart';
import 'package:foodnet_buyer/data/netwroking/HeaderUtils.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:path_provider/path_provider.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';
import '../../util/date_time_util.dart';
import '../netwroking/api_client.dart';
import '../netwroking/api_path.dart';
import 'package:http/http.dart' as http;

class OrderRepository {
  OrderRepository();

  Future<OrderModel> getOrders(
      DateTimeRange dateTimeRange, TimeOfDay timeOfDay, String tab) async {
    try {
      Map body = {
        "tenantId": ApiPath.TENANT_ID,
        "store_code": "ripul_cpt",
        "fromDate":
            "${DateTimeUtil.getDateFromDateTime(dateTimeRange.start, DateTimeUtil.DD_MM_YY)}",
        "toDate":
            "${DateTimeUtil.getDateFromDateTime(dateTimeRange.end, DateTimeUtil.DD_MM_YY)}",
        "fromTime": "00:00",
        "toTime": "23:00",
        "tab": "$tab",
        "app_name": "BUSINESS",
        'partyId': PreferenceManager.getPartyId(),
      };

      final response = await ApiClient.getInstance().post(
        ApiPath.BUSINESS_ORDERS,
        body: body,
      );
      if (kDebugMode) {
        print(body.toString());
        print(ApiPath.BUSINESS_ORDERS);
        print("Response: $response");
      }
      if (response != null)
        return OrderModel.fromJson(response);
      else
        return null;
    } catch (e) {
      print(e);
      AppUtils.showToast(e.toString());
      return null;
    }
  }

  Future<OrderModel> getSearchOrders(DateTimeRange dateTimeRange,
      TimeOfDay timeOfDay, String tab, String key) async {
    try {
      Map body = {
        "tenantId": ApiPath.TENANT_ID,
        "store_code": "ripul_cpt",
        "fromDate":
            "${DateTimeUtil.getDateFromDateTime(dateTimeRange.start, DateTimeUtil.DD_MM_YY)}",
        "toDate":
            "${DateTimeUtil.getDateFromDateTime(dateTimeRange.end, DateTimeUtil.DD_MM_YY)}",
        "fromTime": "00:00",
        "toTime": "23:00",
        "tab": "$tab",
        "app_name": "BUSINESS",
        'search': key,
        'partyId': PreferenceManager.getPartyId(),
      };
      final response = await ApiClient.getInstance().post(
        ApiPath.BUSINESS_ORDERS,
        body: body,
      );
      if (response != null)
        return OrderModel.fromJson(response);
      else
        return null;
    } catch (e) {
      AppUtils.showToast(e.toString());
      return null;
    }
  }

  Future<AreaModel> getAreaFilter() async {
    final response = await ApiClient.getInstance()
        .get('${ApiPath.BASE_PATH}front/addresses/areas/1');

    if (response != null)
      return AreaModel.fromJson(response);
    else
      return null;
  }

  Future<OrderDetailsModel> getOrderDetails(dynamic id) async {
    Map body = {"tenantId": ApiPath.TENANT_ID, "tidId": "$id"};
    try {
      final response = await ApiClient.getInstance()
          .post(ApiPath.BUSINESS_ORDERS_DETAILS, body: body);
      log(response.toString());
      if (response != null && response['status'])
        return OrderDetailsModel.fromJson(response);
      else
        return null;
    } catch (e) {
      return null;
    }
  }

  Future<OrderDetailsModel> getGRNDetails(dynamic id) async {
    Map body = {"tenantId": ApiPath.TENANT_ID, "tidId": "$id"};
    try {
      final response = await ApiClient.getInstance()
          .post(ApiPath.BUSINESS_ORDERS_GRN_DETAILS, body: body);
      log(response.toString());
      if (response != null && response['status'])
        return OrderDetailsModel.fromJson(response);
      else
        return null;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<bool> updateOrders(Map body, int mode) async {
    String url = mode == 1
        ? ApiPath.BUSINESS_ORDERS_UPDATE
        : ApiPath.BUSINESS_ORDERS_UPDATE_GRN;

    try {
      final response = await ApiClient.getInstance().post(url, body: body);
      log(response.toString());
      AppUtils.showToast(response['message']);
      return response['status'];
    } catch (e) {
      return false;
    }
  }

  Future<ItemInDetailsModel> getItemInDetails(dynamic id) async {
    Map body = {"tenantId": ApiPath.TENANT_ID, "tidId": "$id"};
    try {
      final response = await ApiClient.getInstance()
          .post(ApiPath.BUSINESS_ORDERS_ITEM_IN_DETAILS, body: body);
      log(response.toString());
      if (response != null && response['status'])
        return ItemInDetailsModel.fromJson(response);
      else
        return null;
    } catch (e) {
      return null;
    }
  }

  Future<OrderDetailsModel> getDeliveryChallanDetails(dynamic id) async {
    Map body = {"tenantId": ApiPath.TENANT_ID, "tidId": "$id"};
    try {
      final response = await ApiClient.getInstance()
          .post(ApiPath.BUSINESS_ORDERS_DELIVERY_CHALLAN_DETAILS, body: body);
      log(response.toString());
      if (response != null && response['status'])
        return OrderDetailsModel.fromJson(response);
      else
        return null;
    } catch (e) {
      return null;
    }
  }

  Future<OrderDetailsModel> getInvoicesDetails(dynamic id) async {
    Map body = {"tenantId": ApiPath.TENANT_ID, "tidId": "$id"};
    try {
      final response = await ApiClient.getInstance()
          .post(ApiPath.BUSINESS_ORDERS_Invoices, body: body);
      log(response.toString());
      if (response != null && response['status'])
        return OrderDetailsModel.fromJson(response);
      else
        return null;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<bool> orderItemIn(int ids) async {
    try {
      Map body = {
        "tenantId": ApiPath.TENANT_ID,
        "ids": [ids],
      };
      final response = await ApiClient.getInstance().post(
        ApiPath.BUSINESS_ORDERS_ITEM_IN,
        body: body,
      );
      if (response != null) {
        AppUtils.showToast(response['message'].toString());
        return response['status'];
      } else
        return false;
    } catch (e) {
      AppUtils.showToast(e.toString());
      return false;
    }
  }

  Future<bool> orderGrn(int ids) async {
    try {
      Map body = {
        "tenantId": ApiPath.TENANT_ID,
        "ids": [ids],
      };
      final response = await ApiClient.getInstance().post(
        ApiPath.BUSINESS_ORDERS_GRN,
        body: body,
      );
      if (response != null) {
        AppUtils.showToast(response['message'].toString());
        return response['status'];
      } else
        return false;
    } catch (e) {
      AppUtils.showToast(e.toString());
      return false;
    }
  }

  Future<bool> updateGRNGrossWeight(int id, dynamic weight) async {
    try {
      Map body = {
        "tenantId": ApiPath.TENANT_ID,
        "id": id,
        "gross_weight": weight
      };

    final response = await ApiClient.getInstance().post(
        ApiPath.BUSINESS_UPDATE_GRN_GROSS_WEIGHT,
        body: body,
      );
      if (response != null) {
        // AppUtils.showToast(response['message'].toString());
        return response['status'];
      } else
        return false;
    } catch (e) {
      // AppUtils.showToast(e.toString());
      return false;
    }
  }

  Future<bool> updateGRNComment(int id, String comment) async {
    try {
      Map body = {
        "tenantId": ApiPath.TENANT_ID,
        "id": id,
        "comment": comment
      };

      final response = await ApiClient.getInstance().post(
        ApiPath.BUSINESS_UPDATE_GRN_COMMENT,
        body: body,
      );
      if (response != null) {
        // AppUtils.showToast(response['message'].toString());
        return response['status'];
      } else
        return false;
    } catch (e) {
      // AppUtils.showToast(e.toString());
      return false;
    }
  }

  Future<bool> orderPayment(Map<String, dynamic> body) async {
    try {
      final response = await ApiClient.getInstance().post(
        ApiPath.BUSINESS_ORDERS_PAYMENT,
        body: body,
      );
      if (response != null) {
        AppUtils.showToast(response['message'].toString());
        return response['status'];
      } else
        return false;
    } catch (e) {
      AppUtils.showToast(e.toString());
      return false;
    }
  }

  Future<bool> addGrossWeight(String orderId, String weight) async {
    try {
      Map body = {
        "tenantId": ApiPath.TENANT_ID,
        "id": "$orderId",
        "gross_weight": weight
      };

      final response = await ApiClient.getInstance()
          .post(ApiPath.BUSINESS_ORDERS_GROSS_WEIGHT, body: body);

      if (response != null)
        return true;
      else
        return false;
    } catch (e) {
      return false;
    }
  }

  Future<String> uploadImage(String path, String ids) async {
    try {
      File finalFile = await testCompressAndGetFile(File(path));
      var request =
          http.MultipartRequest('POST', Uri.parse('${ApiPath.BASE_DMS}'));
      request.headers.addAll(HeaderUtils.getEncodedHeader());
      request.files.add(await http.MultipartFile.fromPath(
        'document',
        finalFile.path,
        filename: path.split('/').last,
      ));
      request.fields['objectType'] = 'ItemIn';
      request.fields['objectRef'] = '$ids';
      request.fields['appName'] = 'Business';
      request.fields['moduleName'] = 'ItemIn';
      request.fields['documentUsage'] = 'verification';

      http.Response response = await http.Response.fromStream(
              await request.send().timeout(Duration(seconds: 300)))
          .timeout(Duration(seconds: 300));
      log(response.request.toString());
      log(response.body);
      var data = jsonDecode(response.body);
      if (data['status'])
        return response.body;
      else
        return null;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<String> uploadGRNImage(String path, String ids) async {
    try {
      File finalFile = await testCompressAndGetFile(File(path));
      var request =
          http.MultipartRequest('POST', Uri.parse('${ApiPath.BASE_DMS}'));
      request.headers.addAll(HeaderUtils.getEncodedHeader());
      request.files.add(await http.MultipartFile.fromPath(
        'document',
        finalFile.path,
        filename: path.split('/').last,
      ));
      request.fields['objectType'] = 'GRN';
      request.fields['objectRef'] = '$ids';
      request.fields['appName'] = 'Business';
      request.fields['moduleName'] = 'GRN';
      request.fields['documentUsage'] = 'verification';

      http.Response response = await http.Response.fromStream(
              await request.send().timeout(Duration(seconds: 300)))
          .timeout(Duration(seconds: 300));
      log(response.request.toString());
      log(response.body);
      var data = jsonDecode(response.body);
      if (data['status'])
        return response.body;
      else
        return null;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<bool> uploadAllPicUrl(Map body) async {
    try {
      final response = await ApiClient.getInstance()
          .post(ApiPath.BUSINESS_ORDERS_ITEM_IN_DOC, body: body);

      if (response != null) {
        final message = response['message'] ?? 'Success';
        final status = response['status'];
        AppUtils.showToast(message);
        return status;
      } else
        return false;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> uploadAllGRNPicUrl(Map body) async {
    try {
      final response = await ApiClient.getInstance()
          .post(ApiPath.BUSINESS_ORDERS_GRN_DOC, body: body);

      if (response != null) {
        final message = response['message'] ?? 'Success';
        final status = response['status'];
        AppUtils.showToast(message);
        return status;
      } else
        return false;
    } catch (e) {
      print(e);
      return false;
    }
  }

  static Future<File> testCompressAndGetFile(File dataFile) async {
    final target = await getApplicationDocumentsDirectory();
    File file =
        File('${target.path}/${DateTime.now().millisecond.toString()}.jpg');
    var result = await FlutterImageCompress.compressAndGetFile(
      dataFile.absolute.path,
      file.path,
      quality: 60,
    );
    return result;
  }

  Future<dynamic> getAllImages(int tid, {String section = ""}) async {
    Map body = {
      "tenantId": ApiPath.TENANT_ID,
      "tids": [tid],
      "section": section
    };
    final response = await ApiClient.getInstance()
        .post(ApiPath.ORDER_ALL_IMAGES, body: body);
    return response;
  }

  Future<dynamic> getAllGRNComments() async {
    final path = "${ApiPath.ALL_ENTITY_COMMENTS}?tenantId=${ApiPath.TENANT_ID}&entityName=GRN";
    final response = await ApiClient.getInstance()
        .get(path,);
    return response;
  }

  Future<dynamic> getAllOrderComments(int tid) async {
    var body = {
      "tenantId": 1,
      "tids": [tid]
    };
    final path = "${ApiPath.ALL_ORDER_COMMENTS}";
    final response = await ApiClient.getInstance().post(path, body: body);
    return response;
  }

  Future<dynamic> getAllOrderGrossWeight(int tid) async {
    var body = {
      "tenantId": 1,
      "tids": [tid]
    };
    final path = "${ApiPath.ALL_ORDER_GROSS_WEIGHT}";
    final response = await ApiClient.getInstance().post(path, body: body);
    return response;
  }
}
