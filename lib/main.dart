import 'dart:convert';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:foodnet_buyer/constant/app_route.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/bloc/common_bloc.dart';
import 'package:foodnet_buyer/data/data_manager.dart';
import 'package:foodnet_buyer/data/netwroking/api_client.dart';
import 'package:foodnet_buyer/method_channel/app_version_channel.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/SplashScreen.dart';
import 'package:foodnet_buyer/ui/order_detail_screen.dart';
import 'package:foodnet_buyer/util/device_util.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';
import 'package:foodnet_buyer/util/razorpay_util.dart';

import 'util/app_utils.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  _initializeAndRun();
}

_initializeAndRun() async {
  await PreferenceManager.init();
  await DeviceUtil.initDeviceInfo();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  RazorPayUtil.instance.init();
  DataManager.get().appVersionName = await AppVersionChannel.get().getAppVersionName();
  DataManager.get().appVersionCode = await AppVersionChannel.get().getAppVersionCode();
  ApiClient.getInstance().initializeDio();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {

  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  static GlobalKey<NavigatorState> _globalKey = GlobalKey();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  @override
  void initState() {
    CommonBloc.get().init();
    _initFCM();
    super.initState();
  }

  _initFCM() async {
    FirebaseMessaging.onMessage.listen((RemoteMessage data) {
      var message = AppUtils.getMapFromRemoteMessage(data);
      AppUtils.log("PushNotification onMessage: $message");
      if (PreferenceManager.isLoggedIn()) {
        PreferenceManager.setShowNotificationBadge(true);
        CommonBloc.get().updateNotificationBadge();
        _showNotificationWithDefaultSound(message);
      }
    });
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print("PushNotification onLaunch:");
      // _getFCMData(message);
      var data = AppUtils.getMapFromRemoteMessage(message);
      AppUtils.log("PushNotification onResume: $data");
      _handleNotification(data, false);
    });

    _firebaseMessaging.getToken().then((value) {
      DataManager
          .get()
          .fcmToken = value;
      AppUtils.log("Main Token generated:  $value");
    });
    initLocalNotificationPlugin();
    NotificationSettings settings = await _firebaseMessaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
  }

  @override
  void dispose() {
    CommonBloc.get().dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext contextP) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      navigatorKey: _globalKey,
      debugShowCheckedModeBanner: false,
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: Style.getMediaQueryData(context),
          child: child,
        );
      },
      onGenerateRoute: (routeSettings) {
        switch (routeSettings.name) {
          case AppRoute.NOTIFICATION:
            var data = routeSettings.arguments;
            var dataMap = json.decode(data);
            var route = MaterialPageRoute(builder: (context) {
              return OrderDetailScreen(dataMap["tid"], dataMap["orderId"]);
            });
            return route;
          default:
            return null;
        }
      },
      theme: ThemeData.light().copyWith(accentColor: ColorTheme.primaryColor,
        primaryColor: ColorTheme.primarySwatch,),
      home: SplashScreen(),
    );
  }

  initLocalNotificationPlugin() {
    // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    // If you have skipped STEP 3 then change app_icon to @mipmap/ic_launcher
    var initializationSettingsAndroid =
    new AndroidInitializationSettings('ic_notification');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  Future onSelectNotification(String payload) async {
    if (PreferenceManager.isLoggedIn()) {
      // AppUtils.showToast("Notification clicked");
      _globalKey.currentState.pushNamed(AppRoute.NOTIFICATION, arguments: payload);
    }
  }

  Future _showNotificationWithDefaultSound(Map<String, dynamic> message) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        "FoodNetBuyer_id", "FoodNetBuyerLocalNotificationChannel",
        'Notification chanel',
        sound: RawResourceAndroidNotificationSound("mixkit_bell"),
        importance: Importance.max, priority: Priority.high);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails(
        sound: "mixkit_bell.wav",
        presentAlert: true, presentBadge: true, presentSound: true);
    var platformChannelSpecifics = new NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      DateTime
          .now()
          .second
          .toInt(),
      message["notification"]["title"],
      message["notification"]["body"],
      platformChannelSpecifics,
      payload: json.encode(message["data"]),
    );
  }

  _handleNotification(Map<String, dynamic> message, bool isOnLaunch) {
    PreferenceManager.setShowNotificationBadge(true);
    CommonBloc.get().updateNotificationBadge();
    if (PreferenceManager.isLoggedIn()) {
      _globalKey.currentState.pushNamed(AppRoute.NOTIFICATION, arguments: json.encode(message["data"]));
      // AppUtils.showToast("Notification clicked");
    }
  }
}
  
Future<dynamic> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  AppUtils.log("PushNotification bacground: $message");
}