import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/util/app_utils.dart';

class Style {

  static getMediaQueryData(BuildContext context) {
    // Obtain the current media query information.
    final mediaQueryData = MediaQuery.of(context);

    // Take the textScaleFactor from system and make
    // sure that it's no less than 1.0, but no more
    // than 1.5.
    final constrainedTextScaleFactor =
    mediaQueryData.textScaleFactor.clamp(1.0, 1.0);

    return mediaQueryData.copyWith(textScaleFactor: constrainedTextScaleFactor);

  }

  static Widget getPoppinsLightText(String text, double size, Color color, {TextAlign align = TextAlign.start, TextOverflow overflow = TextOverflow.ellipsis, int maxLines = 1, TextDecoration decoration = TextDecoration.none, bool softWrap}) {
    return Text(
      text,
      textAlign: align,
      overflow: overflow,
      maxLines: maxLines,
      softWrap: softWrap,
      style: getTextStyleLight(size, color, decoration: decoration)
    );
  }

  static Widget getPoppinsRegularText(String text, double size, Color color, {TextAlign align = TextAlign.start, int maxLines = 1, TextOverflow overflow = TextOverflow.ellipsis}) {
    return Text(
      text,
      textAlign: align,
      maxLines: maxLines,
      overflow: overflow,
      style: getTextStyleRegular(size, color)
    );
  }

  static Widget getPoppinsMediumText(String text, double size, Color color, {TextAlign align = TextAlign.start, int maxLines = 1, TextOverflow overflow = TextOverflow.ellipsis}) {
    return Text(
      text,
      textAlign: align,
      maxLines: maxLines,
      overflow: overflow,
      style: getTextStyleMedium(size, color)
    );
  }

  static Widget getPoppinsBoldText(String text, double size, Color color, {TextAlign align = TextAlign.start, int maxLines = 1, TextOverflow overflow = TextOverflow.ellipsis}) {
    return Text(
      text,
      textAlign: align,
      maxLines: maxLines,
      overflow: overflow,
      style: getTextStyleBold(size, color));
  }

  static Widget rupeeTextView(String text, double textSize, Color color,
      {FontWeight weight = FontWeight.normal}) {
    try{
      final currencyData = AppUtils.formatCurrency(text);
      final firstResult = currencyData.split('.').first??'0';
      final secondResult = currencyData.split('.').last??'0';
      return Text.rich(
        TextSpan(
          children: [
            TextSpan(
              text: "",
              style: TextStyle(
                color: color,
                fontSize: textSize,
                fontFamily: 'Roboto-Regular',
              ),
            ),
            TextSpan(
              text: firstResult??'00',
              style:
              TextStyle(color: color, fontSize: textSize, fontWeight: weight),
            ),
            WidgetSpan(
              child: Transform.translate(
                offset: Offset(0, -textSize/2.5),
                child: Text(
                  secondResult??'00',
                  //superscript is usually smaller in size
                  textScaleFactor: 0.60,
                  style: TextStyle(
                      color: color, fontSize: textSize , fontWeight: weight),
                ),
              ),
            ),
          ],
        ),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        textScaleFactor: 1.0,
      );
    }catch(e){
      return Container();
    }
  }


  static TextStyle getTextStyleLight(double size, Color color, {TextDecoration decoration = TextDecoration.none}) {
    return TextStyle(
        color: color,
        fontSize: size,
        fontFamily: 'Poppins-Light',
        decoration: decoration);
  }

  static TextStyle getTextStyleLightPicker(double size, Color color, {TextDecoration decoration = TextDecoration.none}) {
    return TextStyle(
        letterSpacing: 2,
        wordSpacing: 5,
        color: color,
        fontSize: size,
        fontFamily: 'Poppins-Light',
        decoration: decoration);
  }

  static TextStyle getTextStyleRegular(double size, Color color) {
    return TextStyle(
        color: color,
        fontSize: size,
        fontFamily: 'Poppins-Regular',
        decoration: TextDecoration.none);
  }

  static TextStyle getTextStyleMedium(double size, Color color) {
    return TextStyle(
        color: color,
        fontSize: size,
        fontFamily: 'Poppins-Medium',
        decoration: TextDecoration.none);
  }


  static TextStyle getTextStyleBold(double size, Color color) {
    return TextStyle(
        color: color,
        fontSize: size,
        fontFamily: 'Poppins-Bold',
        decoration: TextDecoration.none);
  }

  static Widget getTextButton(double width, double height, String btnName, {Function onClick, bool showBorder = true, double padding = 6}) {
    return Material(
      type: MaterialType.transparency,
      child: Ink(
        child: InkWell(
          borderRadius: BorderRadius.circular(height/2),
          onTap: () {
            onClick?.call();
          },
          child: Container(
              width: width,
              height: height,
              padding: EdgeInsets.all(padding),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(height/2),
                border: showBorder ? Border.all(color: ColorTheme.primaryColor, width: 1) : Border(),
              ),
              child: Center(child: getPoppinsLightText(btnName, AppFonts.btnTextSize, ColorTheme.primaryColor))
          ),
        ),
      ),
    );
  }

  static Widget getTextWithIconButton(double width, double height, String btnName, Widget icon, {Function onClick}) {
    return Material(
      type: MaterialType.transparency,
      child: Ink(
        child: InkWell(
          borderRadius: BorderRadius.circular(height/2),
          onTap: () {
            onClick?.call();
          },
          child: Container(
              width: width,
              height: height,
              padding: const EdgeInsets.all(6),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(height/2),
                border: Border.all(color: ColorTheme.primaryColor, width: 1),
              ),
              child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    icon,
                    SizedBox(width: 10,),
                    getPoppinsLightText(btnName, AppFonts.textSize14, ColorTheme.primaryColor)
                  ]
              )
          ),
        ),
      ),
    );
  }

  static Widget getTextBtnWithElevation(String btnName, Function onTap, {double width, double height = 50, double textSize = AppFonts.btnTextSize,}) {
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 2),
        child: Material(
          elevation: 1.0,
          color: ColorTheme.white,
          shadowColor: ColorTheme.darkGrey.withOpacity(.4),
          borderRadius: BorderRadius.circular(30.0),
          child: Container(
              width: width,
              height: height,
              padding: const EdgeInsets.all(6),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(height/2),
                border: Border.all(color: ColorTheme.primaryColor, width: 1),
              ),
              child: Center(child: getPoppinsLightText(btnName, 16, ColorTheme.primaryColor))
          )
        ),
      ),
    );
  }

  static Widget getTextInputField(String hintName, {double height = 46, TextEditingController controller, Function(String) onChanged, TextInputType inputType = TextInputType.text, EdgeInsets margin, int maxLength, bool showBorder = true, String prefill = "", List<TextInputFormatter> inputFormatters, bool enabled = true, int minLines, int maxLines}) {
    return Container(
      height: height,
      margin: margin,
      child: TextField(
        enabled: enabled,
        controller: controller,
        textAlign: TextAlign.start,
        onChanged: onChanged,
        maxLength: maxLength,
        style: getTextStyleLight(14, ColorTheme.FF333333),
        decoration: InputDecoration(
            counterText: "",
            enabledBorder: showBorder ? OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              borderSide:
              BorderSide(color: ColorTheme.FFE3E3E3, width: 0.5),
            ) : InputBorder.none,
            focusedBorder: showBorder ? OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              borderSide: BorderSide(color: ColorTheme.FFF0674C, width: 0.5),
            ) : InputBorder.none,
            hintText: "$hintName",
            contentPadding: new EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
            hintStyle: getTextStyleLight(14, ColorTheme.FF999999),
        ),
        minLines: minLines,
        maxLines: maxLines,
        keyboardType: inputType,
        inputFormatters: inputFormatters,
      ),
    );
  }


  static Widget getTestInputFieldWithCircle(String hint,{TextEditingController controller,TextInputType textInputType=TextInputType.text,Function(String vale) onChange,int maxNum=10}){
    return Container(
      height: 40,
      color: ColorTheme.white,
      child: TextField(
        controller: controller,
        style: TextStyle(fontSize: 16,color: ColorTheme.black,),
        textInputAction: TextInputAction.done,
        keyboardType: textInputType,
        onChanged: onChange,
        maxLength: maxNum,
        decoration: InputDecoration(
          counter: null,
            counterText: '',
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30)
            ),
            hintText: '$hint',
            contentPadding: const EdgeInsets.symmetric(vertical: 4,horizontal: 16),
            hintStyle: TextStyle(fontSize: 16,color: ColorTheme.darkGrey,)
        ),
      ),
    );
  }
}