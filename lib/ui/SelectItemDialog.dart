import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/model/ItemInDetailsModel.dart';
import 'package:foodnet_buyer/data/model/SearchModel.dart';
import 'package:foodnet_buyer/data/netwroking/api_client.dart';
import 'package:foodnet_buyer/data/netwroking/api_path.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/widgets/search_view.dart';
import 'package:foodnet_buyer/util/app_utils.dart';

class SelectItemDialog extends StatefulWidget {
  final String storeId;
  final Function(List<SizeVariant> selectedData) selectedItem;

  SelectItemDialog(this.storeId, {this.selectedItem});

  @override
  _SelectItemDialogState createState() => _SelectItemDialogState();
}

class _SelectItemDialogState extends State<SelectItemDialog> {
  final _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BottomSheet(
      backgroundColor: Colors.transparent,
      enableDrag: true,
      onClosing: () {
        Navigator.of(context).pop();
      },
      builder: (_) => Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(10), topLeft: Radius.circular(10))),
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.all(10),
              alignment: Alignment.center,
              child:
                  Style.getPoppinsRegularText('Add Product', 16, Colors.black),
            ),
            SearchView(
              onCancel: () {
                FocusScope.of(context).unfocus();
                _controller.text = '';
                setState(() {});
              },
              onTextChanged: (value) {
                print(value);
                _getSearchData(value);
              },
              thresholdLength: 2,
            ),
            Expanded(
              child: ListView.builder(
                itemBuilder: (_, index) {
                  String type = searchData[index].type;
                  return type != 'OfferCard'
                      ? ExpansionTile(
                          initiallyExpanded: false,
                          title: AppUtils.getProductName(searchData[index].productName, searchData[index].subText, 14, ColorTheme.black,),
                          subtitle: Style.getPoppinsRegularText(
                              'Variants Type: ${searchData[index].sizeUnit}',
                              14,
                              ColorTheme.darkGrey),
                          children: searchData[index]
                              .sizeVariants
                              .map(
                                (e) => Container(
                                  color:
                                      ColorTheme.primaryColor.withOpacity(0.1),
                                  child: CheckboxListTile(
                                    value: e.checked,
                                    onChanged: (value) {
                                      addItem(e);
                                      e.setChecked = value;
                                      setState(() {});
                                    },
                                    title: Style.getPoppinsRegularText(
                                        '${e.productDisplayName} ( ${e.size} ${e.sizeUnit}) ',
                                        14,
                                        Colors.black),
                                    subtitle: Style.rupeeTextView(
                                        e.salePrice, 12, ColorTheme.darkGrey),
                                  ),
                                ),
                              )
                              .toList(),
                        )
                      : Container();
                },
                itemCount: searchData.length,
              ),
            ),
            if (selectedData.length>0)
              Container(
                margin: const EdgeInsets.all(10),
                child: AbsorbPointer(
                  absorbing: false,
                  child: Style.getTextButton(180,40,'Add Product', onClick: () {
                    Navigator.of(context).pop();
                    widget.selectedItem(selectedData);
                  },),
                ),
              ),
          ],
        ),
      ),
    );
  }

  List<SearchDatum> searchData = [];
  List<SizeVariant> selectedData = [];

  addItem(SizeVariant data) {
    if (selectedData.contains(data)) {
      selectedData.remove(data);
    } else {
      selectedData.add(data);
    }
  }

  _getSearchData(value) async {
    String path ="${ApiPath.SEARCH}?search=${value}&storeId=1/*${widget.storeId}*/";
    try {
      var response = await ApiClient.getInstance().get( path);
      log(response.toString());
      if (response != null) {
        final model = SearchModel.fromJson(response);
        searchData.clear();
        List<SearchDatum> temp = [];
        temp.addAll(model.searchData);
        temp.forEach((element) {
          List<SizeVariant> sizeVariant = [];

          if (element.sizeVariants.length == 0) {
            sizeVariant.add(SizeVariant(
              id: element.id.toString(),
              minQty: element.minQty,
              size: element.size,
              saleUnit: element.saleUnit,
              productDisplayName: element.productName,
              productPrice: element.productMrp,
              salePrice: element.productBaseRate,
              sizeUnit: element.sizeUnit,
              taxRate: element.productTaxRate,
              subText: element.subText,
            ));
          } else {
            element.sizeVariants.forEach((child) {
              child.setId = element.id.toString();
              child.setSubText = element.subText;
              child.minQty = element.minQty;
              sizeVariant.add(child);
            });
          }
          element.sizeVariants = sizeVariant;
        });
        searchData.addAll(temp);
        setState(() {});
      } else {}
    } catch (e) {
      print(e.toString());
    }
  }
}
