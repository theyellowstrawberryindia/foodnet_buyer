import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:foodnet_buyer/ui/address/address_listing_screen.dart';
import 'package:foodnet_buyer/ui/dashboard/dashboard_screen.dart';
import 'package:foodnet_buyer/ui/login/StartScreen.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';

class SplashScreen extends StatefulWidget {

  SplashScreen();

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 2), () => _navigateUser());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.only(right: 20, left: 20),
        child: Image.asset(
          "assets/images/logo.png",
          fit: BoxFit.contain,
          height: 75.0,
          alignment: Alignment.center,
        ),
      ),
    );
  }

  _navigateUser() async {
    Widget navigateTo = PreferenceManager.isLoggedIn()  ?  PreferenceManager.isAddressPicked() ? DashboardScreen() : AddressListingScreen(): StartScreen();
    NavigationUtil.clearAllAndAdd(context, navigateTo);
  }
}
