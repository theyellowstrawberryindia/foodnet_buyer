import 'dart:convert';
import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/model/ItemInDetailsModel.dart';
import 'package:foodnet_buyer/data/model/OrderModel.dart';
import 'package:foodnet_buyer/data/model/order_details_model.dart';
import 'package:foodnet_buyer/data/netwroking/api_path.dart';
import 'package:foodnet_buyer/data/repository/order_repository.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/image_dialog.dart';
import 'package:foodnet_buyer/ui/verify_screen.dart';
import 'package:foodnet_buyer/ui/widgets/dialog/verify_dialog.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/custome_loader.dart';
import 'package:foodnet_buyer/util/permission_utils.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class AddGrossWeight extends StatefulWidget {
  final OrderData orderData;

  AddGrossWeight(this.orderData);

  @override
  _AddGrossWeightState createState() => _AddGrossWeightState();
}

class _AddGrossWeightState extends State<AddGrossWeight> {
  final grossWeightController = TextEditingController();
  final orderRepo = OrderRepository();
  List<UploadPhotos> _uploadPhotos =[];
  ItemInDetailsModel _itemInDetailsModel;
  File _image;
  final picker = ImagePicker();
  List<ImageData> _list = [];

  @override
  void initState() {
    super.initState();
    _list.add(ImageData(1, ''));

    Future.delayed(Duration(milliseconds: 500),_getItemInDetails);
  }

  @override
  void dispose() {
    super.dispose();
    Loader.hide();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop(true);
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: ColorTheme.white,
          title: Style.getPoppinsRegularText('Details', 16, ColorTheme.black),
          iconTheme: IconThemeData(color: Colors.black87),
        ),
        body: Container(
          padding: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                child: Container(
                  height: 40,
                  color: ColorTheme.white,
                  child: TextField(
                    controller: grossWeightController,
                    style: TextStyle(fontSize: 16,color: ColorTheme.black,),
                    textInputAction: TextInputAction.done,
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp('[0-9.]')),
                    ],
                    onChanged: (vale){},
                    maxLength: 10,
                    decoration: InputDecoration(
                        counter: null,
                        counterText: '',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30)
                        ),
                        hintText: 'Enter gross weight',
                        contentPadding: const EdgeInsets.symmetric(vertical: 4,horizontal: 16),
                        hintStyle: TextStyle(fontSize: 16,color: ColorTheme.darkGrey,),
                        suffix: Container(
                          width: 24,
                          child: Style.getPoppinsBoldText('Kg', 14, Colors.black54),
                        )
                    ),
                  ),
                )
              ),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
              child: Style.getPoppinsLightText('Add Photos', 14, ColorTheme.darkGrey),
              ),
              Expanded(
                  child:GridView.count(
                    crossAxisCount: 3,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10,
                    children: _list.map((e) {
                      if(e.type==1)
                        return GestureDetector(
                          onTap: ()=>_requestPermission(),
                          child: Container(
                            width: 80,
                            height: 80,
                            child: DottedBorder(
                              radius: Radius.circular(10),
                                strokeWidth:1,
                                dashPattern: [2,5],
                                borderType: BorderType.RRect,
                                strokeCap: StrokeCap.round,
                                child: Center(child: Icon(Icons.add,color: ColorTheme.darkGrey,),)
                            ),
                          ),
                        );
                      else
                        return GestureDetector(
                          onTap: (){
                            showModalBottomSheet(
                                isScrollControlled: true,
                                context: context,
                                backgroundColor: Colors.transparent,
                                builder: (_)=>Container(height: MediaQuery.of(context).size.height*0.95,child: ImageDialog(true, e.path,onDelete: (path){
                                  Navigator.of(context).pop();

                                  _list.removeWhere((element) => element.path==path) ;
                                  _uploadPhotos.removeWhere((element) => element.path==path);

                                  setState(() {

                                  });
                                },)));

                          },
                          child: Container(
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: Image.file(File(e.path),fit: BoxFit.fill,),
                            ),
                          ),
                        );
                    }).toList(),
                  )
              ),
              Container(
                child: Row(
                  children: [
                    Expanded(child: FlatButton(onPressed: ()=>_navigate(),child: Style.getPoppinsRegularText('Skip', 14, ColorTheme.primaryColor),)),
                    Expanded(child:Style.getTextBtnWithElevation("Next", () {
                      FocusScope.of(context).unfocus();
                      if (grossWeightController.text.isEmpty) {
                        AppUtils.showToast("Enter gross weight");
                        return;
                      }
                      showDialog<bool>(context: context,builder: (_)=>VerifyDialog(grossWeightController.text, _list)).then((value){
                        if(value!=null && value){
                          _addData();
                        }
                      });
                    },
                      height: 40
                    ),),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _navigate(){
    Navigator.of(context).push(MaterialPageRoute(builder: (_)=>VerifyScreen(widget.orderData)));
  }

  _requestPermission()async{
    final result = await PermissionUtil.instance.requestSinglePermission(Permission.storage);
    if(result){
      getImage();
    }
  }

   getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        _list.add(ImageData(2, pickedFile.path));
        _addSinglePic(pickedFile.path);
      }
    });
  }

  _getItemInDetails()async{
    Loader.show(context);
    _itemInDetailsModel = await orderRepo.getItemInDetails(widget.orderData.tid.id);
    Loader.hide();
    setState(() {

    });
  }

  _addData()async{
    if(grossWeightController.text.isNotEmpty){
      _addGrossWeight();
    }else if(_uploadPhotos.length>0){
      _uploadPic();
    }else{
      AppUtils.showToast('Seems you are not added any data');
    }
  }

  _addGrossWeight()async{
    final response = await orderRepo.addGrossWeight(_itemInDetailsModel.itemInData.id.toString()  , grossWeightController.text.toString());

    if(response!=null){
      if(_uploadPhotos.length>0){
        _uploadPic();
      }else{
        _navigate();
      }
    }
  }

  _addSinglePic(String path)async{
    Loader.show(context);
    final uploadedData = await orderRepo.uploadImage( _image.path,_itemInDetailsModel.itemInData.id.toString());
    if(uploadedData!=null){
      var data = jsonDecode(uploadedData);
      _uploadPhotos.add(UploadPhotos(docId:data['data']['id'].toString(),docUrl: data['data']['url'].toString(),path:path,docUsage: 'itemin' ));
    }
    Loader.hide();

  }

  _uploadPic()async{
    Map body = {
      "tenantId": ApiPath.TENANT_ID,
      "id": "${_itemInDetailsModel.itemInData.id}",
      "photos": List<dynamic>.from(_uploadPhotos.map((e) => e.toJson())),
    };

    final request = await orderRepo.uploadAllPicUrl(body);

    if(request){
      _navigate();
    }
  }


}

class ImageData{
  int type;
  String path;

  ImageData(this.type, this.path);

}

class UploadPhotos{
  String docId;
  String docUrl;
  String docUsage;
  String path;
  UploadPhotos({this.docId, this.docUrl, this.docUsage,this.path});

  factory UploadPhotos.fromJson(Map<String, dynamic> json) => UploadPhotos(
   docUrl: json['document_url']
  );

   Map<String , dynamic> toJson()=>{
     "document_id": docId, "document_url": "$docUrl", "document_usage": "$docUsage"
   };
}
