import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/bloc/common_bloc.dart';
import 'package:foodnet_buyer/data/model/location_data.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/base_state.dart';
import 'package:foodnet_buyer/ui/dashboard/dashboard_screen.dart';
import 'package:foodnet_buyer/ui/widgets/CustomAppBar.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';

class AddressListingScreen extends StatefulWidget {
  final String title;
  final bool allowSelection;

  AddressListingScreen(
      {this.title = "Select Address", this.allowSelection = true});

  _AddressListingScreenState createState() => _AddressListingScreenState();
}

class _AddressListingScreenState extends BaseState<AddressListingScreen> {
  int selectedIndex = 0;
  List<dynamic> address;

  _AddressListingScreenState() {
    address = PreferenceManager.getUserAddress();
  }

  @override
  void initState() {
    dynamic userSelectedAddress = PreferenceManager.getUserSelectedAddress();
    selectedIndex = address.indexWhere((element) => userSelectedAddress != null && element["id"] == userSelectedAddress["id"]);
    if (selectedIndex == -1) {
      selectedIndex = 0;
    }
    super.initState();
  }

  @override
  void dispose() {
    CommonBloc.get().closeAddressListener();
    super.dispose();
  }

  @override
  void onResumed() {
    super.onResumed();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorTheme.FFFDFDFD,
        appBar: CustomAppBar(
          widget.title,
          automaticallyImplyLeading: true,
        ),
        body: _allAddressView(context, address));
  }

  Widget _allAddressView(BuildContext context, List<dynamic> address) {
    PreferenceManager.saveUserAddress(address);
    int length = address.length;
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20, top: length == 1 ? 0 : 20),
      child: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: length,
              itemBuilder: (ctx, index) {
                bool isSelected =
                    widget.allowSelection && index == selectedIndex;
                return Padding(
                  padding: EdgeInsets.only(bottom: 10),
                  child: InkWell(
                    borderRadius: BorderRadius.circular(10),
                    onTap: () {
                      setState(() {
                        selectedIndex = index;
                      });
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width - 40,
                      height: 130,
                      padding: EdgeInsets.only(left: 20, bottom: 20),
                      decoration: BoxDecoration(
                          color: ColorTheme.white,
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                              color: isSelected
                                  ? ColorTheme.FFF97062
                                  : ColorTheme.FFE3E3E3,
                              width: .5)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(top: 20, right: 5),
                            child: Image.asset(
                              address[index]["address_name"] == "Home"
                                  ? AppAssets.home
                                  : AppAssets.location,
                              width: 18,
                              height: 18,
                              color: ColorTheme.FF333333,
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 18,
                              ),
                              Style.getPoppinsLightText(
                                  address[index]["address_name"],
                                  16,
                                  ColorTheme.FF333333),
                              SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width - 110,
                                  child: Style.getPoppinsLightText(
                                      "${AppUtils.addressFromObj(address[index])}",
                                      14,
                                      ColorTheme.FF999999,
                                      maxLines: 3)),
                            ],
                          ),
                          Expanded(
                            child: SizedBox(),
                          ),
                          isSelected
                              ? Container(
                                  width: 20,
                                  height: 20,
                                  padding: EdgeInsets.all(6),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: ColorTheme.FFF97062,
                                          width: .5),
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(10),
                                          bottomLeft: Radius.circular(5))),
                                  child: Image.asset(
                                    AppAssets.selectedTick,
                                    width: 8,
                                    height: 8,
                                  ),
                                )
                              : SizedBox(
                                  width: 20,
                                  height: 20,
                                ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
          widget.allowSelection
              ? Container(
                  margin: EdgeInsets.all(20),
                  child: Style.getTextBtnWithElevation("Select", () {
                    AppUtils.navigateAfterAddressSelection(context, address[selectedIndex]);
                  }),
                )
              : SizedBox()
        ],
      ),
    );
  }
}
