import 'package:flutter/material.dart';

abstract class BaseState<T extends StatefulWidget> extends State<T>
    with WidgetsBindingObserver {
  bool inBackground = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed:
        inBackground = false;
        onResumed();
        break;
      case AppLifecycleState.paused:
        inBackground = true;
        onPaused();
        break;
      default:
        break;
    }
  }

  void onResumed() {}

  void onPaused() {}

  rebuild() {
    if (mounted) {
      setState(() {});
    }
  }
}
