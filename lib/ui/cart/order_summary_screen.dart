import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/constant/text_msgs.dart';
import 'package:foodnet_buyer/data/bloc/cartbloc/cart_bloc.dart';
import 'package:foodnet_buyer/data/bloc/cartbloc/cart_event.dart';
import 'package:foodnet_buyer/data/bloc/cartbloc/cart_state.dart';
import 'package:foodnet_buyer/data/model/cart_response.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/dashboard/dashboard_screen.dart';
import 'package:foodnet_buyer/ui/widgets/CustomAppBar.dart';
import 'package:foodnet_buyer/ui/widgets/order_row_view.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';

class OrderSummaryScreen extends StatefulWidget {
  final String couponCode;

  OrderSummaryScreen(this.couponCode);

  _OrderSummaryScreenState createState() => _OrderSummaryScreenState();
}

class _OrderSummaryScreenState extends State<OrderSummaryScreen> {
  CartBloc _cartBloc;
  bool isExpanded = true;

  @override
  void initState() {
    _cartBloc = CartBloc();
    _cartBloc.add(CheckoutCart(widget.couponCode));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        _onBack();
        return true;
      },
      child: Scaffold(
        appBar: CustomAppBar(
          "Summary",
          leadingWidget: InkWell(
            onTap: () {
              _onBack();
            },
            child: Icon(Icons.arrow_back),
          ),
        ),
        backgroundColor: ColorTheme.white,
        body: BlocBuilder<CartBloc, CartState>(
          bloc: _cartBloc,
          builder: (ctx, state) {
            return state is OrderPlacedState
                ? (state?.response?.length ?? 0) > 0
                    ? _buildView(state.response)
                    : _errorView(TextMsgs.FAILED_TO_LOAD_SUMMARY)
                : state is CheckoutApiFailedState
                    ? _errorView(TextMsgs.FAILED_TO_LOAD_SUMMARY)
                    : _loadingView();
          },
        ),
      ),
    );
  }

  _onBack() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      NavigationUtil.clearAllAndAdd(context, DashboardScreen());
    }
  }

  Widget _loadingView() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget _errorView(String text) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(20),
      child: Style.getPoppinsLightText(text, 14, ColorTheme.FF333333),
    );
  }

  Widget _buildView(List<CartResponse> response) {
    List<Widget> views = [];
    views.add(Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
      child: Image.asset(
        AppAssets.orderPlaced,
        width: 200,
        height: 160,
      ),
    ));

    views.add(Padding(
      padding: EdgeInsets.only(left: 20, right: 20, bottom: 40),
      child: Style.getPoppinsRegularText("Your Order has been Placed !",
          AppFonts.textSize14, ColorTheme.FF333333,
          align: TextAlign.center),
    ));

    views.add(_getDivider2());

    _addStoreRow(views, response);

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: CustomScrollView(
              slivers: [SliverList(delegate: SliverChildListDelegate(views))]),
        ),
      ],
    );
  }

  _addStoreRow(List<Widget> widgets, List<CartResponse> orders) {
    orders.forEach((response) {
      widgets.add(OrderRowView(response));
    });
  }

  Widget _getDivider2() => Divider(
        height: 5,
        color: ColorTheme.FFF4F4F4,
        thickness: 5,
      );
}
