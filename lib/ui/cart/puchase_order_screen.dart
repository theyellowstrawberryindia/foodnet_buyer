import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/constant/text_msgs.dart';
import 'package:foodnet_buyer/data/bloc/cartbloc/cart_bloc.dart';
import 'package:foodnet_buyer/data/bloc/cartbloc/cart_event.dart';
import 'package:foodnet_buyer/data/bloc/cartbloc/cart_state.dart';
import 'package:foodnet_buyer/data/model/OrderModel.dart';
import 'package:foodnet_buyer/data/repository/cart_repository.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/dashboard/dashboard_screen.dart';
import 'package:foodnet_buyer/ui/edit_order_screen.dart';
import 'package:foodnet_buyer/ui/widgets/CustomAppBar.dart';
import 'package:foodnet_buyer/ui/widgets/cell/cell_orders.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/custome_loader.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';

class PurchaseOrderScreen extends StatefulWidget {
  PurchaseOrderScreen();

  @override
  _PurchaseOrderScreenState createState() => _PurchaseOrderScreenState();
}

class _PurchaseOrderScreenState extends State<PurchaseOrderScreen> {
  CartBloc _cartBloc;
  List<OrderItem> _orderList = [];
  List<int> ids = [];

  @override
  void initState() {
    _cartBloc = CartBloc();
    _cartBloc.add(GetPurchaseOrderDetailsEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        //_onBack();
        return true;
      },
      child: Scaffold(
        appBar: CustomAppBar(
          "Purchase Orders",
          leadingWidget: InkWell(
            onTap: () {
              _onBack();
            },
            child: Icon(Icons.arrow_back),
          ),
        ),
        bottomNavigationBar: Ink(
          height: 70,
          decoration: BoxDecoration(color: ColorTheme.white, boxShadow: [
            BoxShadow(
                color: ColorTheme.lightGrey.withOpacity(.8),
                blurRadius: 10.0,
                offset: Offset(0, -5),
                spreadRadius: 0),
          ]),
          padding: EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 15),
          width: double.infinity,
          child: Style.getTextButton(double.infinity, 40, "Send Purchase Order",
              onClick: () {
            sendPurchaseOrder();
          }),
        ),
        backgroundColor: ColorTheme.white,
        body: BlocBuilder<CartBloc, CartState>(
          bloc: _cartBloc,
          builder: (ctx, state) {
            if (state is CheckoutApiFailedState) {
              return _errorView(TextMsgs.FAILED_CHECKOUT_API);
            } else if (state is GetPurchaseOrderDetailsState) {
              _orderList = state.response.orderItems;
              return CustomScrollView(
                slivers: [
                  SliverList(
                      delegate: SliverChildListDelegate(
                    _orderList
                        .map<Widget>((order) => CellOrders(
                              order,
                              "New",
                              onClick: (OrderItem item) {
                                _navigateToEdit(item);
                              },
                            ))
                        .toList(),
                  ))
                ],
              );
            } else if (state is ApiFailedState) {
              return _errorView(TextMsgs.FAILED_PURCHASE_API);
            } else {
              return _loadingView();
            }
          },
        ),
      ),
    );
  }

  _navigateToEdit(OrderItem data) async {
    final result = await Navigator.of(context).push<bool>(
        MaterialPageRoute(builder: (_) => EditOrderScreen(data.tid.id, 1)));

    if (result != null && result) {
      _cartBloc.add(GetPurchaseOrderDetailsEvent());
    }
  }

  Widget _loadingView() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget _errorView(String text) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(20),
      child: Style.getPoppinsLightText(text, 14, ColorTheme.FF333333),
    );
  }

  _onBack() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      NavigationUtil.clearAllAndAdd(context, DashboardScreen());
    }
  }

  void sendPurchaseOrder() async {
    try {
      Loader.show(context);
      for (int i = 0; i < _orderList.length; i++) {
        ids.add(_orderList[i].id);
      }
      var data = await CartRepository.get().sendPurchaseOrder(ids);
      Loader.hide();
      if (data != null && data["status"] == true) {
        AppUtils.showToast(data["message"]);
        NavigationUtil.clearAllAndAdd(context, DashboardScreen());
      } else {
        AppUtils.showToast(TextMsgs.COMMON_ERROR);
      }
    } catch (e) {
      print(e.toString());
      AppUtils.showToast(TextMsgs.COMMON_ERROR);
    }
  }
}
