import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/constant/text_msgs.dart';
import 'package:foodnet_buyer/data/bloc/cartbloc/cart_bloc.dart';
import 'package:foodnet_buyer/data/bloc/cartbloc/cart_event.dart';
import 'package:foodnet_buyer/data/bloc/cartbloc/cart_state.dart';
import 'package:foodnet_buyer/data/bloc/common_bloc.dart';
import 'package:foodnet_buyer/data/model/cart_product.dart';
import 'package:foodnet_buyer/data/model/cart_response.dart';
import 'package:foodnet_buyer/data/model/location_data.dart';
import 'package:foodnet_buyer/data/netwroking/custom_exception.dart';
import 'package:foodnet_buyer/data/repository/cart_repository.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/cart/puchase_order_screen.dart';
import 'package:foodnet_buyer/ui/coupon/coupon_listing.dart';
import 'package:foodnet_buyer/ui/dashboard/dashboard_screen.dart';
import 'package:foodnet_buyer/ui/widgets/CustomAppBar.dart';
import 'package:foodnet_buyer/ui/widgets/animated_flip_counter.dart';
import 'package:foodnet_buyer/ui/widgets/cart_product_view.dart';
import 'package:foodnet_buyer/ui/widgets/dialog/custom_date_time_picker.dart';
import 'package:foodnet_buyer/ui/widgets/dialog/edit_quantity_dialog.dart';
import 'package:foodnet_buyer/ui/widgets/dialog/msg_dialog.dart';
import 'package:foodnet_buyer/ui/widgets/expand_collapse_view.dart';
import 'package:foodnet_buyer/ui/widgets/netwrok_image_view.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/custome_loader.dart';
import 'package:foodnet_buyer/util/date_time_util.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';
import 'package:foodnet_buyer/util/shopping_cart_util.dart';

class ShoppingCartScreen extends StatefulWidget {
  _ShoppingCartScreenState createState() => _ShoppingCartScreenState();
}

class _ShoppingCartScreenState extends State<ShoppingCartScreen> {
  CartBloc _cartBloc;
  bool isExpanded = true;
  bool _allowed = true;
  bool _showAddressConfirmation = true;
  List<dynamic> items;
//  bool isPurchaseEdit = false;

  @override
  void initState() {
    _cartBloc = CartBloc();
    if (PreferenceManager.getPartyId() != null ||
        PreferenceManager.getCartId() != null) {
      _cartBloc.add(GetCartDetailEvent());
    } else {
      _cartBloc.add(UpdateCartEvent(CartResponse(shoppingcartitems: [])));
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        "Shopping Cart",
        leadingWidget: InkWell(
          onTap: () {
            _onBack();
          },
          child: Icon(Icons.arrow_back),
        ),
        actions: [
          InkWell(
            onTap: () {
              _onClearCart();
            },
            child: Container(
                width: 40,
                height: 30,
                alignment: Alignment.center,
                child: PreferenceManager.getCartResponse() != null &&
                        PreferenceManager.getCartResponse().isNotEmpty()
                    ? Image.asset(
                        AppAssets.deleteIcon,
                        width: 24,
                        height: 24,
                      )
                    : SizedBox()),
          )
        ],
      ),
      backgroundColor: ColorTheme.white,
      body: WillPopScope(
        onWillPop: () async {
          _onBack();
          return true;
        },
        child: BlocBuilder<CartBloc, CartState>(
          bloc: _cartBloc,
          builder: (ctx, state) {
            return state is CartReceivedSate
                ? state.response.shoppingcartitems.length > 0
                    ? _buildCartView(state.response, context)
                    : _placeHolderView(context)
                : state is ApiFailedState
                    ? _errorView(TextMsgs.FAILED_TO_LOAD_CART)
                    : _loadingView();
          },
        ),
      ),
    );
  }

  _onBack() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      NavigationUtil.clearAllAndAdd(context, DashboardScreen());
    }
  }

  Widget _placeHolderView(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Style.getPoppinsLightText(
              "You haven't added any item in cart.",
              14,
              ColorTheme.FF333333,
              maxLines: 3,
              align: TextAlign.center,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Style.getTextButton(
            150,
            40,
            "Add Now",
            onClick: () {
              NavigationUtil.clearAllAndAdd(context, DashboardScreen());
            },
          ),
        ],
      ),
    );
  }

  Widget _loadingView() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget _errorView(String text) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(20),
      child: Style.getPoppinsLightText(text, 14, ColorTheme.FF333333),
    );
  }

  Widget _buildCartView(CartResponse response, BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: CustomScrollView(slivers: [
            SliverList(
                delegate: SliverChildListDelegate([
              ListTile(
                leading: Container(
                    alignment: Alignment.center,
                    width: 30,
                    child: NetworkImageView(
                      PreferenceManager.getLocationData()?.sellerurl ?? "",
                      30,
                      30,
                      fit: BoxFit.scaleDown,
                    )),
                trailing: AppUtils.getWidgetWithRupee(" ${response.finalTotal}",
                    AppFonts.textSize14, ColorTheme.FF333333),
                title: Style.getPoppinsLightText(
                    PreferenceManager.getLocationData()?.groupName ?? "NA",
                    14,
                    ColorTheme.FF333333),
                subtitle: Style.getPoppinsRegularText(
                    "${response.lineItems} Products", 12, ColorTheme.FF999999),
                onTap: () {
                  setState(() {
                    isExpanded = !isExpanded;
                  });
                },
                contentPadding: EdgeInsets.only(left: 20, right: 20),
              ),
              ExpandedSection(
                expand: isExpanded,
                child: Column(
                  children: response.shoppingcartitems.map((e) {
                    String msg = _getMessage(e);
                    return Container(
                      height: (182 + (msg != null ? 45 : 0)).toDouble(),
                      padding: EdgeInsets.only(
                          top: 5, bottom: 5, left: 20, right: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          msg != null
                              ? Padding(
                                  padding: EdgeInsets.only(bottom: 5),
                                  child: Style.getPoppinsRegularText(
                                      msg, 12, ColorTheme.FFF97062,
                                      maxLines: 2))
                              : SizedBox(),
                          Expanded(
                            child: Container(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                      width: 120,
//                                          height: 170,
                                      child: CartProductView(
                                        product: e,
                                        onRemove: (product) {
                                          _onRemoveProduct(product);
                                        },
                                      )),
                                  Expanded(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Row(
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            InkWell(
                                              borderRadius:
                                                  BorderRadius.circular(15),
                                              onTap: () {
                                                double _qty = e.quantity;
                                                if (_qty > e.minQty) {
                                                  _qty -= e.minQty;
                                                  if (_qty < e.minQty) {
                                                    _qty = e.minQty;
                                                  }
                                                  _onProductQtyChanged(e, _qty);
                                                } else {
                                                  AppUtils.showToast("Item quantity cannot be less then ${AppUtils.getDisplayValue(e.minQty)}");
                                                }
                                              },
                                              child: Image.asset(
                                                AppAssets.decrementCounter,
                                                width: 30,
                                                height: 30,
                                              ),
                                            ),
                                            SizedBox(width: 10,),
                                            InkWell(
                                              onTap: () {
                                                EditQuantityDialog.showEditQtyDialog(context, e.quantity,  e.isKg(), (qty){
                                                  _onProductQtyChanged(e, qty);
                                                }, minQty: e.minQty);
                                              },
                                              child: Column(
                                                children: [
                                                  Container(
                                                    decoration: BoxDecoration(
                                                      border: Border.all(color: ColorTheme.FF333333, width: .5)
                                                    ),
                                                    alignment: Alignment.center,
                                                    padding: EdgeInsets.only(
                                                        right: 10, left: 10, top: 5, bottom: 5),
                                                    child: Style.getPoppinsRegularText(e.getDisplayQty(), 12, ColorTheme.FF666666),
                                                  ),
                                                  Padding(
                                                    padding: EdgeInsets.only(top: 4),
                                                    child: Icon(Icons.edit, color: ColorTheme.FF333333, size: 14,),
                                                  )
                                                ],
                                              ),
                                            ),
                                            SizedBox(width: 10,),
                                            InkWell(
                                              borderRadius:
                                                  BorderRadius.circular(15),
                                              onTap: () {
                                                double qty = e.quantity + e.minQty;
                                                _onProductQtyChanged(e, qty);
                                              },
                                              child: Image.asset(
                                                AppAssets.incrementCounter,
                                                width: 30,
                                                height: 30,
                                              ),
                                            ),
                                          ],
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(top: 10),
                                          child: AppUtils.getWidgetWithRupee(
                                              " ${e.getTotalPrice()}",
                                              AppFonts.textSize10,
                                              ColorTheme.FF333333),
                                        ),
                                        Expanded(
                                          child: SizedBox(),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisSize: MainAxisSize.max,
                                          children: [
                                            InkWell(
                                              onTap: () {
                                                CustomDateTimePickerView.show(
                                                    context,
                                                    min: ShoppingCartUtil
                                                        .getDeliveryDateTime(
                                                            null),
                                                    max: PreferenceManager
                                                            .getLocationData()
                                                        ?.getMaxOrderDate(),
                                                    onComplete:
                                                        (selectedDateTime,
                                                            forAll) {
                                                  _onProductDateChanged(e,
                                                      selectedDateTime, forAll);
                                                }, allowPop: false);
//
                                              },
                                              child: Container(
                                                width: 30,
                                                height: 30,
                                                margin: EdgeInsets.only(
                                                    left: 10, right: 10),
                                                child: Icon(
                                                  Icons.calendar_today,
                                                  size: 20,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: [
                                                Style.getPoppinsRegularText(
                                                    "Delivery >",
                                                    AppFonts.textSize12,
                                                    ColorTheme.FF333333,
                                                    align: TextAlign.right),
                                                Style.getPoppinsLightText(
                                                    "${DateTimeUtil.getFormatedDate(e.deliveryDate)}\n${DateTimeUtil.getFormatedTime(e.deliveryTime)}",
                                                    AppFonts.textSize12,
                                                    ColorTheme.FF333333,
                                                    maxLines: 3,
                                                    align: TextAlign.right),
                                              ],
                                            )
                                          ],
                                        ),
                                        SizedBox(
                                          height: 5,
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  }).toList(),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              PreferenceManager.isLoggedIn() ? _getDivider() : SizedBox(),
              PreferenceManager.isLoggedIn()
                  ? Padding(
                      padding: EdgeInsets.only(
                          left: 20, right: 16, top: 12, bottom: 12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(AppAssets.couponCode,
                              width: 20, height: 20),
                          SizedBox(
                            width: 10,
                          ),
                          Style.getPoppinsLightText(
                              "${response.isCouponApplied() ? '1 Coupon Applied' : 'Apply Coupon'}",
                              AppFonts.textSize12,
                              ColorTheme.FF333333),
                          Expanded(
                            child: SizedBox(),
                          ),
                          InkWell(
                            onTap: () async {
                              if (response.isCouponApplied()) {
                                _clearCoupon();
                              } else {
                                bool result =
                                    await NavigationUtil.pushToNewScreen(
                                        context, CouponListingScreen());
                                if (result == true) {
                                  _refreshCart();
                                }
                              }
                            },
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: 10,
                              ),
                              child: Icon(
                                response.isCouponApplied()
                                    ? Icons.close
                                    : Icons.chevron_right,
                                color: ColorTheme.FF666666,
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : SizedBox(),
              _getDivider(),
              Padding(
                padding:
                    EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 15),
                child: Style.getPoppinsLightText(
                    "Bill Details", AppFonts.textSize14, ColorTheme.FF333333),
              ),
              _getBillRow("Item Total (Base Price)", response.totalBase),
              _getBillRow("GST", response.totalTaxes),
              _getBillRow("Convenience Fee", response.convenienceCharges),
              _getBillRow("Discount", response.discountAmount),
              _getBillRow("Total to Pay", response.finalTotal, makeBold: true),
              SizedBox(
                height: 10,
              ),
              _getDivider(),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: EdgeInsets.only(left: 20, right: 20, bottom: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Style.getPoppinsLightText("Delivery Address",
                        AppFonts.textSize14, ColorTheme.FF333333),
                    // InkWell(
                    //     onTap: () {
                    //       _onEditAddress();
                    //     },
                    //     child: PreferenceManager.isLoggedIn()
                    //         ? Style.getPoppinsLightText("Edit >",
                    //             AppFonts.textSize14, ColorTheme.FFF0674C)
                    //         : SizedBox()),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 20, right: 20, bottom: 0),
                child: Style.getPoppinsLightText(
                    PreferenceManager.getName() ?? "Guest",
                    AppFonts.textSize12,
                    ColorTheme.FF333333),
              ),
              Padding(
                padding: EdgeInsets.only(left: 20, right: 20, bottom: 3),
                child: Style.getPoppinsLightText(
                    PreferenceManager.getDisplayAddress(),
                    AppFonts.textSize12,
                    ColorTheme.FF666666,
                    maxLines: 3),
              ),
              PreferenceManager.getMobile() != null
                  ? Padding(
                      padding: EdgeInsets.only(left: 20, right: 20, bottom: 3),
                      child: Style.getPoppinsLightText(
                        "Phone no.: ${PreferenceManager.getMobile() ?? "NA"}",
                        AppFonts.textSize12,
                        ColorTheme.FF666666,
                      ),
                    )
                  : SizedBox(),
              SizedBox(
                height: 20,
              )
            ]))
          ]),
        ),
        Ink(
          height: 70,
          decoration: BoxDecoration(color: ColorTheme.white, boxShadow: [
            BoxShadow(
                color: ColorTheme.lightGrey.withOpacity(.8),
                blurRadius: 10.0,
                offset: Offset(0, -5),
                spreadRadius: 0),
          ]),
          padding: EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 15),
          width: double.infinity,
          child: Style.getTextButton(
              double.infinity, 40, "Raise Purchase Order", onClick: () {
            _handlePayClick(response);
          }),
        ),
        SizedBox(
          height: 10,
        )
      ],
    );
  }

  _onEditAddress() async {
    // var result = await NavigationUtil.pushToNewScreen(context, AddressListingScreen(title: "Select Address",));
    // _handleAddressChange(result);
  }

  _handleAddressChange(dynamic result) async {
    if (result != null) {
//        Loader.show(context);
      try {
        dynamic address = result["address"];
        LocationData data = result[
            "location"]; //await MainRepository.get().saveUserLocation(address["lat"]?.toString(), address["lon"]?.toString());
//          Loader.hide();
        PreferenceManager.saveUserSelectedAddress(address);
        PreferenceManager.setAddress(AppUtils.addressFromObj(address));
        PreferenceManager.setLatitude(address["lat"]?.toString());
        PreferenceManager.setLongitude(address["lon"]?.toString());
        PreferenceManager.saveLocationData(data);
        if (PreferenceManager.getCartResponse()?.isNotEmpty() == true) {
          await _updateAddress();
        } else {
          _refreshCart();
        }
        _showAddressConfirmation = false;
      } catch (e) {}
    }
  }

  _handlePayClick(
    CartResponse response,
  ) async {
    if (_allowed == false) {
      AppUtils.showToast(TextMsgs.ERROR_IN_CART);
      return;
    } else {
      checkout(response);
      // AppUtils.delayTask(() async {
      //   /* NavigationUtil.clearAllAndAdd(
      //       context, PurchaseOrderScreen(response.couponCode));*/
      //   checkout(response);
      // }, delay: Duration(milliseconds: 600));
    }

    // var userSelectedAddress = PreferenceManager.getUserSelectedAddress();
    //
    // if (_showAddressConfirmation || userSelectedAddress == null) {
    //   MsgDialog.showMsgDialog(context, "Use current address as delivery address?", "Yes",
    //           () async {
    //
    //         if (userSelectedAddress == null) {
    //           Map<String, dynamic> address = await MapsUtil.addressObjFromLocation(PreferenceManager.getLatitude(), PreferenceManager.getLongitude());
    //           var result = await NavigationUtil.pushToNewScreen(context, AddUpdateAddressScreen(params: address,));
    //           _handleAddressChange(result);
    //         } else {
    //           await _updateAddress();
    //           _showAddressConfirmation = false;
    //           if (_allowed) {
    //             await ShoppingCartUtil.get().updateCartToServer(context);
    //             AppUtils.delayTask(() {
    //               _onPay();
    //             }, delay: Duration(milliseconds: 600));
    //           }
    //         }
    //
    //       }, btnName2: "Select Address", onNegativeClick: () {
    //         _onEditAddress();
    //       }, isDismissable: true);
    // } else {
    //   await _updateAddress();
    //   if (_allowed) {
    //     await ShoppingCartUtil.get().updateCartToServer(context);
    //     AppUtils.delayTask(() {
    //       _onPay();
    //     }, delay: Duration(milliseconds: 600));
    //   }
    // }
  }

  void checkout(response) async {
    try {
      Loader.show(context);
      await ShoppingCartUtil.get().updateCartToServer(context, showLoader: false);
      try {
        var data =
        await CartRepository.get().checkoutOrder(response.couponCode);
        Loader.hide();
        if (data != null) {
          //navigateToPurchaseScreen();
          AppUtils.delayTask((){
            NavigationUtil.clearAllAndAdd(context, DashboardScreen(currentIndex: 1,));
          }, delay: Duration(milliseconds: 50));
        } else {
          AppUtils.showToast("Something went wrong");
        }
      } on CustomException catch (e) {
        Loader.hide();
        if (e.getCode() == 419) {
          AppUtils.showToast("Token Expired");
        } else {
          AppUtils.showToast(e.toString());
        }
      }
    } catch (e) {
      Loader.hide();
      AppUtils.showCustomErrorMsg(e.toString());
    }
  }

  void navigateToPurchaseScreen() async {
    NavigationUtil.pushAndReplaceToNewScreen(context, PurchaseOrderScreen());
  }

  _onProductQtyChanged(CartProduct product, double newQty) {
    ShoppingCartUtil.get().updateProductQuantity(context, product, newQty,
        onComplete: () {
      _refreshCart();
    });
  }

  _onRemoveProduct(CartProduct product) {
    MsgDialog.showMsgDialog(context, TextMsgs.REMOVE_PRODUCT_FROM_CART, "Yes",
        () {
      ShoppingCartUtil.get().removeProduct(context, product, onComplete: () {
        _refreshCart();
      });
    }, btnName2: "No", onNegativeClick: () {}, isDismissable: true);
  }

  _onClearCart() {
    MsgDialog.showMsgDialog(context, TextMsgs.CLEAR_CART, "Yes", () async {
      try {
        Loader.show(context);
        String cartId = PreferenceManager.getCartId();
        String itemCount = PreferenceManager.getCartResponse()
                ?.shoppingcartitems
                ?.length
                ?.toString() ??
            "0";
        await CartRepository.get().clearCart();
        // FBAnalytics.cartCleared(cartId, itemCount);
        Loader.hide();
        _refreshCart();
      } catch (e) {
        _refreshCart();
        Loader.hide();
      }
    }, btnName2: "No", onNegativeClick: () {}, isDismissable: true);
  }

  _onProductDateChanged(
      CartProduct product, DateTime selectedDateTime, bool forAll) {
    if (ShoppingCartUtil.get()
        .isSelectedDateTimeDifferent(product, selectedDateTime, forAll)) {
      MsgDialog.showMsgDialog(
          context,
          TextMsgs.ADDITIONAL_CHARGES,
          "Ok",
          () {
            Navigator.pop(context);
            ShoppingCartUtil.get().updateProductDateTime(
                context, product, selectedDateTime, forAll, onComplete: () {
              _refreshCart();
            });
          },
          btnName2: "Cancel",
          onNegativeClick: () {
            Navigator.pop(context);
          });
      return;
    }
    Navigator.pop(context);
    ShoppingCartUtil.get().updateProductDateTime(
        context, product, selectedDateTime, forAll, onComplete: () {
      _refreshCart();
    });
  }

  _clearCoupon() {
    ShoppingCartUtil.get().updateCouponCode(context, null, onComplete: () {
      _refreshCart();
    });
  }

  _updateAddress() async {
    try {
      Loader.show(context);
      var address = PreferenceManager.getUserSelectedAddress();
      var response = await CartRepository.get()
          .getChangeDeliveryAddress(address["id"]?.toString());
      Loader.hide();
      if (response != null && response["data"] != null) {
        _allowed = response["data"]["allowed"] ?? true;
        if (response["data"]["shoppingcartitems"] != null) {
          items = response["data"]["shoppingcartitems"];
        }
        if (response["data"]["message"] != null) {
          AppUtils.showToast(response["data"]["message"]);
        }
      }
    } catch (e) {
      _allowed = false;
      AppUtils.showToast(e.toString());
      Loader.hide();
      return;
    }

    try {
      Loader.show(context);
      await CartRepository.get().getCart(
          PreferenceManager.getCartId(), PreferenceManager.getUserPartyId());
      Loader.hide();
    } catch (e) {
      Loader.hide();
    }
    _refreshCart(allow: _allowed);
    if (!_allowed) {
      AppUtils.showToast(TextMsgs.ERROR_IN_CART);
    }
  }

  _refreshCart({bool allow = true}) {
    _allowed = allow;
    if (allow) {
      items = null;
    }
    _cartBloc.add(UpdateCartEvent(PreferenceManager.getCartResponse()));
    CommonBloc.get().updateBadge();
    if (PreferenceManager.getCartResponse() == null ||
        (PreferenceManager.getCartResponse().shoppingcartitems?.length ?? 0) ==
            0) {
      setState(() {});
    }
  }

  Widget _getBillRow(String leftText, String rightText,
      {bool makeBold = false}) {
    return Padding(
      padding: EdgeInsets.only(left: 20, right: 20, bottom: 3),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Style.getPoppinsLightText(
              leftText,
              makeBold ? AppFonts.textSize14 : AppFonts.textSize12,
              makeBold ? ColorTheme.FF333333 : ColorTheme.FF666666),
          SizedBox(
            width: 10,
          ),
          AppUtils.getWidgetWithRupee(
              " $rightText",
              makeBold ? AppFonts.textSize14 : AppFonts.textSize12,
              makeBold ? ColorTheme.FF333333 : ColorTheme.FF666666),
        ],
      ),
    );
  }

  Widget _getDivider() => Padding(
        padding: EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        child: Divider(
          height: .5,
          color: ColorTheme.FFE3E3E3,
        ),
      );

  String _getMessage(CartProduct product) {
    if (items == null) {
      return null;
    }
    String msg;
    items.forEach((element) {
      if (element["productId"] != null &&
          element["productId"].toString().contains("*") &&
          element["productId"].toString().replaceAll("*", "") ==
              product.productId.toString() &&
          element["message"] != null) {
        msg = element["message"]?.toString();
        AppUtils.log("Error Message: $msg");
        return;
      }
    });

    return msg;
  }
}
