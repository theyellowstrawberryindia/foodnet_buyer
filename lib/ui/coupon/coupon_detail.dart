import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/bloc/coupon/coupon_bloc.dart';
import 'package:foodnet_buyer/data/bloc/coupon/coupon_event.dart';
import 'package:foodnet_buyer/data/bloc/coupon/coupon_state.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/widgets/CustomAppBar.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CouponDetailScreen extends StatefulWidget {

  final String id;
  final bool fromCoupon;
  CouponDetailScreen(this.id, {this.fromCoupon = true});

  _CouponDetailScreenState createState() => _CouponDetailScreenState();
}

class _CouponDetailScreenState extends State<CouponDetailScreen> {

  CouponBloc _bloc;
  Completer<WebViewController> _controller;
  bool isLoaded = false;

  @override
  void initState() {
    super.initState();
    _controller = Completer<WebViewController>();
    _bloc = CouponBloc();
    AppUtils.delayTask(() {
      _bloc.add(GetCouponDetailEvent(widget.id, widget.fromCoupon));
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: CustomAppBar("Coupon Details"),
      backgroundColor: ColorTheme.FFFDFDFD,
      body: BlocBuilder(
          bloc: _bloc,
          builder: (ctx, state) {

            if (state is ApiFailedState) {
              return _getNoItemsFoundView();
            } else if (state is FetchingDataState) {
              return Container(
                      alignment: Alignment.center,
                      width: double.maxFinite,
                      height: double.maxFinite,
                      color: ColorTheme.FFFDFDFD,
                      child: CircularProgressIndicator(),
                    );
            } else {
              return Stack(
                  children: [
                    WebView(
                      javascriptMode: JavascriptMode.unrestricted,
                      initialUrl: "",
                      onPageFinished: (url){
                        setState(() {
                          isLoaded = true;
                        });
                      },
                      onWebViewCreated: (WebViewController webViewController) {
                        _controller.complete(webViewController);
                        if (state is CouponDetailReceivedState) {
                          setState(() {
                            isLoaded = false;
                          });
                          webViewController.loadUrl(Uri.dataFromString(state.data["description"],
                              mimeType: 'text/html', encoding: Encoding.getByName('utf-8'))
                              .toString());
                        }
                      },
//                      navigationDelegate: (NavigationRequest request) {
//                        if(request.url == widget.initialUrl){
//                          return NavigationDecision.navigate;
//                        }
//                        return NavigationDecision.prevent;
//                      },
                    ),
                    !isLoaded || state is FetchingDataState ? Container(
                      alignment: Alignment.center,
                      width: double.maxFinite,
                      height: double.maxFinite,
                      color: ColorTheme.FFFDFDFD,
                      child: CircularProgressIndicator(),
                    ) : SizedBox()
                  ]
              );
            }
          }
      ),
    );
  }

  Widget _getNoItemsFoundView() {
    return Center(
      child: Style.getPoppinsLightText("Failed to load details", 14, ColorTheme.FF333333),
    );
  }
}