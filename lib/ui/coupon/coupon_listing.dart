import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/bloc/coupon/coupon_bloc.dart';
import 'package:foodnet_buyer/data/bloc/coupon/coupon_event.dart';
import 'package:foodnet_buyer/data/bloc/coupon/coupon_state.dart';
import 'package:foodnet_buyer/data/model/coupon_item.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/coupon/coupon_detail.dart';
import 'package:foodnet_buyer/ui/widgets/netwrok_image_view.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';
import 'package:foodnet_buyer/util/shopping_cart_util.dart';

class CouponListingScreen extends StatefulWidget {
  _CouponListingScreenState createState() => _CouponListingScreenState();
}

class _CouponListingScreenState extends State<CouponListingScreen> {
  CouponBloc _bloc;
  int selectedIndex = 0;
  TextEditingController _couponController;
  @override
  void initState() {
    super.initState();
    _couponController = TextEditingController();
    _bloc = CouponBloc();
    AppUtils.delayTask(() {
      _bloc.add(GetAllCouponEvent());
    });
  }

  @override
  void dispose() {
    _couponController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 2,
        iconTheme: IconThemeData(
          color: ColorTheme.FF333333, //change your color here
        ),
        title: Style.getPoppinsLightText(
            "Coupons", AppFonts.titleTextSize, ColorTheme.FF333333),
        backgroundColor: Colors.white,
        bottom: PreferredSize(
          child: Container(
            margin: EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
            padding: EdgeInsets.only(top: 3, bottom: 5, right: 10),
            decoration: BoxDecoration(
                color: ColorTheme.white,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(color: ColorTheme.FFE3E3E3, width: .5)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                    width: MediaQuery.of(context).size.width - 140,
                    child: Style.getTextInputField("Enter coupon code",
                        controller: _couponController,
                        showBorder: false,
                        height: 40)),
                InkWell(
                    onTap: () {
                      if (_couponController.text.trim().isNotEmpty) {
                        _applyCoupon(_couponController.text.trim());
                      } else {
                        AppUtils.showToast("Coupon code is required");
                      }
                    },
                    child: Style.getPoppinsLightText(
                        "Apply >", 14, ColorTheme.FFF0674C))
              ],
            ),
          ),
          preferredSize: Size(double.maxFinite, 80),
        ),
      ),
      backgroundColor: ColorTheme.FFFDFDFD,
      body: BlocBuilder(
          bloc: _bloc,
          builder: (ctx, state) {
            return state is ApiFailedState
                ? _getNoItemsFoundView()
                : state is AllCouponReceivedState
                    ? state.items.isNotEmpty
                        ? _buildView(state.items)
                        : _getNoItemsFoundView()
                    : _loadingView();
          }),
    );
  }

  _buildView(List<CouponItem> items) {
    return Column(
      children: [
        Expanded(
          child: ListView.builder(
            itemCount: items.length,
            itemBuilder: (ct, index) => _itemView(index, items[index]),
            keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
          ),
        ),
        Ink(
          height: 70,
          decoration: BoxDecoration(color: ColorTheme.white, boxShadow: [
            BoxShadow(
                color: ColorTheme.lightGrey.withOpacity(.8),
                blurRadius: 10.0,
                offset: Offset(0, -5),
                spreadRadius: 0),
          ]),
          padding: EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 15),
          width: double.infinity,
          child: Style.getTextButton(double.infinity, 40, "Apply", onClick: () {
            if (selectedIndex != -1) {
              _applyCoupon(items[selectedIndex].couponCode);
            } else {
              AppUtils.showToast("Kindly select a coupon to apply");
            }
          }),
        )
      ],
    );
  }

  Widget _getNoItemsFoundView() {
    return Center(
      child:
          Style.getPoppinsLightText("No items found", 14, ColorTheme.FF333333),
    );
  }

  Widget _loadingView() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget _itemView(int index, CouponItem item) {
    return InkWell(
      onTap: () {
        selectedIndex = selectedIndex == index ? -1 : index;
        setState(() {});
      },
      child: Container(
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(width: .5, color: ColorTheme.FFE3E3E3))),
        padding: EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            Image.asset(
              selectedIndex == index ? AppAssets.checked : AppAssets.unchecked,
              width: 20,
              height: 20,
            ),
            SizedBox(
              width: 10,
            ),
            NetworkImageView(item.documentUrl, 75, 75),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 2,
                  ),
                  Style.getPoppinsLightText(
                      item.couponText, 14, ColorTheme.FF333333,
                      maxLines: 2),
                  Style.getPoppinsLightText(
                      "Use Code ${item.couponCode}", 12, ColorTheme.FF333333),
                  SizedBox(
                    height: 2,
                  ),
                  Style.getPoppinsLightText(
                      "Used ${item.usage} out of ${item.maxUsage}",
                      10,
                      ColorTheme.FF999999),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      InkWell(
                          onTap: () {
                            AppUtils.unfocusScope(context);
                            NavigationUtil.pushToNewScreen(context,
                                CouponDetailScreen(item.couponId?.toString()));
                          },
                          child: Style.getPoppinsMediumText("Details >",
                              AppFonts.btnTextSize, ColorTheme.FFF0674C))
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _applyCoupon(String code) async {
    ShoppingCartUtil.get().updateCouponCode(context, code, onComplete: () {
      NavigationUtil.pop(context, result: true);
    });
  }
}
