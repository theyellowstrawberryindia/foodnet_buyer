import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/data_manager.dart';
import 'package:foodnet_buyer/data/model/cart_response.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/address/address_listing_screen.dart';
import 'package:foodnet_buyer/ui/dashboard/navigation_drawer.dart';
import 'package:foodnet_buyer/ui/order_detail_screen.dart';
import 'package:foodnet_buyer/ui/scanner/scanner_screen.dart';
import 'package:foodnet_buyer/ui/tab/home_tab_screen.dart';
import 'package:foodnet_buyer/ui/tab/order_tab_screen.dart';
import 'package:foodnet_buyer/ui/widgets/image_view.dart';
import 'package:foodnet_buyer/ui/widgets/notification_icon.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';

class DashboardScreen extends StatefulWidget {
  final int currentIndex;
  final int innerIndex;

  DashboardScreen({this.currentIndex = 0, this.innerIndex = 0});

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  int selectedIndex = 0;
  bool initialized = true;
  bool delayLoading = true;

  final GlobalKey<ScaffoldState> rootKey = new GlobalKey<ScaffoldState>();
  GlobalKey cartKey = GlobalKey();
  GlobalKey drawerKey = GlobalKey();
  GlobalKey categoryKey = GlobalKey();
  GlobalKey homeKey = GlobalKey();
  GlobalKey orderKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    selectedIndex = widget.currentIndex;
    _handleInitialMsg();
  }

  _handleInitialMsg() async {
    var remoteMsg = await FirebaseMessaging.instance.getInitialMessage();
    if (remoteMsg != null) {
      var data = AppUtils.getMapFromRemoteMessage(remoteMsg);
      String orderId = data["data"]["orderId"];
      String tId = data["data"]["tid"];
      DataManager.get().notificationData = null;
      AppUtils.delayTask((){
        NavigationUtil.pushToNewScreen(context, OrderDetailScreen(tId, orderId));
      }, delay: Duration(milliseconds: 700));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: rootKey,
        backgroundColor: ColorTheme.FFFDFDFD,
        appBar: AppBar(
          leading: null,
          elevation: 0.0,
          backgroundColor: ColorTheme.white,
          automaticallyImplyLeading: false,
          titleSpacing: 0.0,
          title: Container(
            padding: const EdgeInsets.all(6),
            child: Row(
              children: <Widget>[
                const SizedBox(
                  width: 10,
                ),
                InkWell(
                  onTap: () {
                    AppUtils.clearFocus(context);
                    rootKey.currentState.openDrawer();
                  },
                  child: SizedBox(
                    width: 24,
                    height: 24,
                    child: Image.asset(
                      AppAssets.menu,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                InkWell(
                  onTap: () {
                    CartResponse response = PreferenceManager.getCartResponse();
                    if (response != null &&
                        response.shoppingcartitems.length > 0) {
                      AppUtils.showToast(
                          "Please send or remove added purchase order before change the address.");
                    } else {
                      NavigationUtil.clearAllAndAdd(
                          context, AddressListingScreen());
                    }
                  },
                  child: SizedBox(
                    width: 24,
                    height: 24,
                    child: Image.asset(
                      AppAssets.location,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Container(
                    height: 36,
                    child: Center(
                      child: Image.asset(
                        AppAssets.appLogo,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
            SizedBox(
              width: 24,
              height: 24,),//NotificationIcon(),
                const SizedBox(
                  width: 20,
                ),
                AppUtils.getCartWidget(context, key: cartKey),
                const SizedBox(
                  width: 10,
                ),
              ],
            ),
          ),
        ),
        drawer: NavigationDrawer(key: drawerKey,),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: ColorTheme.white,
          currentIndex: selectedIndex,
          type: BottomNavigationBarType.fixed,
          unselectedItemColor: ColorTheme.FF999999,
          selectedItemColor: ColorTheme.FFF97062,
          iconSize: 28,
          selectedLabelStyle:
              Style.getTextStyleRegular(10, ColorTheme.FFF97062),
          unselectedLabelStyle:
              Style.getTextStyleRegular(10, ColorTheme.FF999999),
          onTap: (index) {
            selectedIndex = index;
            delayLoading = false;
            setState(() {});
          },
          elevation: 6.0,
          items: [
            BottomNavigationBarItem(
              label: 'STORE',
              icon: ImageView(
                asset: AppAssets.store,
                color: ColorTheme.FF999999,
              ),
              activeIcon: ImageView(
                asset: AppAssets.store,
                color: ColorTheme.FFF97062,
              ),
            ),
            BottomNavigationBarItem(
              label: 'ORDERS',
              icon: ImageView(
                asset: AppAssets.order,
                color: ColorTheme.FF999999,
              ),
              activeIcon: ImageView(
                asset: AppAssets.order,
                color: ColorTheme.FFF97062,
              ),
            ),
            BottomNavigationBarItem(
              label: 'SCANNER',
              icon: ImageView(
                asset: AppAssets.scanner,
                color: ColorTheme.FF999999,
              ),
              activeIcon: ImageView(
                asset: AppAssets.scanner,
                color: ColorTheme.FFF97062,
              ),
            ),
            BottomNavigationBarItem(
              label: 'SELLER',
              icon: ImageView(
                asset: AppAssets.seller,
                color: ColorTheme.FF999999,
              ),
              activeIcon: ImageView(
                asset: AppAssets.seller,
                color: ColorTheme.FFF97062,
              ),
            ),
            BottomNavigationBarItem(
              label: 'DASHBOARD',
              icon: ImageView(
                asset: AppAssets.dashboardIcon,
                color: ColorTheme.FF999999,
              ),
              activeIcon: ImageView(
                asset: AppAssets.dashboardIcon,
                color: ColorTheme.FFF97062,
              ),
            ),
          ],
        ),
        body: !initialized
            ? FutureBuilder(
                future: Future.delayed(Duration(milliseconds: 600)),
                builder: (ctx, snapshot) {
                  if (snapshot.connectionState != ConnectionState.done) {
                    return Container(
                      color: ColorTheme.FFFDFDFD,
                      alignment: Alignment.center,
                      child: CircularProgressIndicator(),
                    );
                  }
                  initialized = true;
                  return _getView();
                })
            : _getView());
  }

  _getView() {
    switch (selectedIndex) {
      case 0:
        return HomeTabScreen(
          rootKey: rootKey,
          cartKey: cartKey,
          delayLoading: delayLoading,
          key: homeKey,
        );
      case 1:
        return OrderTabScreen(
          currentIndex: widget.innerIndex,
        );
      case 2:
        return ScannerScreen(showAppBar: false, showItemInToast: widget.innerIndex != 0,);
      default:
        return Container(
          child: Center(
            child: Style.getPoppinsLightText(
                "Coming Soon", 14, ColorTheme.FF333333),
          ),
        );
    }
  }
}
