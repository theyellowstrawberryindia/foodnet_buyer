import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/constant/text_msgs.dart';
import 'package:foodnet_buyer/data/data_manager.dart';
import 'package:foodnet_buyer/data/model/location_data.dart';
import 'package:foodnet_buyer/data/repository/main_reporitory.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/address/address_listing_screen.dart';
import 'package:foodnet_buyer/ui/dashboard/webview_screen.dart';
import 'package:foodnet_buyer/ui/login/StartScreen.dart';
import 'package:foodnet_buyer/ui/widgets/dialog/msg_dialog.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/custome_loader.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';

class NavigationDrawer extends StatefulWidget {

  static const String tcUrl = "https://www.truptitech.com/terms-conditions";
  static const String ppUrl = "https://www.truptitech.com/privacy-policy";

  const NavigationDrawer({Key key}): super(key: key);

  _NavigationDrawerState createState() => _NavigationDrawerState();
}

class _NavigationDrawerState extends State<NavigationDrawer> {

  List<String> items = PreferenceManager.isLoggedIn() ? [/*"FAQ",*/ "Terms & Conditions", "Privacy Policy", "Logout"] : [/*"FAQ",*/ "Terms & Conditions", "Privacy Policy"];

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorTheme.FFFDFDFD,
      height: double.maxFinite,
      child: Builder(
        builder: (ctx) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      _getTopHeaderView(),
                      SizedBox(height: 15,),
                      for(int i = 0; i < items.length; i++)
                        Material(
                          type: MaterialType.transparency,
                          child: Ink(
                            child: InkWell(
                              onTap: () {
                                _handelClick(i);
                              },
                              child: Container(
                                alignment: Alignment.center,
                                height: 40,
                                margin: EdgeInsets.only(left: 20, right: 20),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment
                                      .spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Style.getPoppinsLightText(
                                        items[i], AppFonts.textSize14,
                                        ColorTheme.FF333333),
                                    SizedBox(width: 20,),
                                    Style.getPoppinsLightText(
                                        ">", AppFonts.textSize16,
                                        ColorTheme.FF333333)
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Style.getPoppinsLightText("Version ${DataManager.get().appVersionName}(${DataManager.get().appVersionCode})", 12, ColorTheme.FF999999),
              SizedBox(height: 15,)
            ],
          );
        }
      ),
    );
  }

  Widget _getTopHeaderView() {
    return Container(
      decoration: BoxDecoration(
        color: ColorTheme.FF171F2B,
        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20), bottomRight: Radius.circular(20))
      ),

      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 40,),
          Stack(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: Icon(Icons.arrow_back, color: ColorTheme.FFFDFDFD,),
                  ),
                ),
              ),
              
              Align(
                alignment: Alignment.center,
                child: Style.getPoppinsLightText("Menu", 16, ColorTheme.FFFDFDFD),
              )
            ],
          ),
          SizedBox(height: 30,),
          ListTile(
            leading: Image.asset(AppAssets.appLogo2, width: 40, height: 40),
            title: Style.getPoppinsLightText(PreferenceManager.isLoggedIn() ? PreferenceManager.getName() ?? "Guest" : "Guest", 16, ColorTheme.FFFDFDFD),
            subtitle: Style.getPoppinsLightText(PreferenceManager.isLoggedIn() ? "${PreferenceManager.getMobile()}\n${PreferenceManager.getEmail()}" : "Login/Register", 12, ColorTheme.FFFDFDFD),
            isThreeLine: PreferenceManager.isLoggedIn(),
//            trailing: InkWell(
//              onTap: () {
//
//              },
//                child: Style.getPoppinsLightText(PreferenceManager.isLoggedIn() ? "Edit >" : "", 12, ColorTheme.FFF0674C)),
            onTap: () {
              if (!PreferenceManager.isLoggedIn()) {
                AppUtils.delayTask((){
                  NavigationUtil.pushToNewScreen(context, StartScreen());
                }, delay: Duration(milliseconds: 150));
              }
            },
          ),
          SizedBox(height: 20,)
        ],
      ),
    );
  }

  _handelClick(int index) {
    if (PreferenceManager.isLoggedIn() && items[index] == "Logout") {
      MsgDialog.showMsgDialog(context, "Are you sure you want to log out?", "OK", () {
        AppUtils.delayTask(() async {
          try {
            Loader.show(context);
            await MainRepository.get().logout();
            Loader.hide();
            // FBAnalytics.logout(PreferenceManager.getMobile());
            PreferenceManager.clearAll();
            NavigationUtil.clearAllAndAdd(context, StartScreen());
          } catch (e) {
            Loader.hide();
            Navigator.pop(context);
            AppUtils.showToast(TextMsgs.COMMON_ERROR);
          }
        }, delay: Duration(milliseconds: 100));
      }, btnName2: "Cancel", onNegativeClick: () {
        Navigator.pop(context);
      }, shouldAutoPop: false);
    } else if (items[index] == "Saved Address") {
      Navigator.pop(context);
      NavigationUtil.pushToNewScreen(context, AddressListingScreen(allowSelection: false,));
    } else {
      Navigator.pop(context);
      NavigationUtil.pushToNewScreen(context, WebViewScreen(name: items[index], initialUrl: _getUrl(items[index])));
    }
  }

  String _getUrl(String name) {
    LocationData data = PreferenceManager.getLocationData();
    return name =="FAQ" ? data?.faqUrl ?? "" : name == "Privacy Policy" ? NavigationDrawer.ppUrl ?? "" : NavigationDrawer.tcUrl ?? "";
  }
}