import 'dart:async';

import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/ui/widgets/CustomAppBar.dart';
import 'package:webview_flutter/webview_flutter.dart';

// ignore: must_be_immutable
class WebViewScreen extends StatefulWidget {
  final String initialUrl;
  final String name;

  WebViewScreen({this.name, this.initialUrl,});

  @override
  _WebViewScreenState createState() => _WebViewScreenState();
}

class _WebViewScreenState extends State<WebViewScreen> {
  Completer<WebViewController> _controller;
  bool isLoaded = false;
  @override
  void initState() {
    _controller = Completer<WebViewController>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(widget.name),
        backgroundColor: ColorTheme.FFFDFDFD,
        body: Stack(
            children: [
              WebView(
                javascriptMode: JavascriptMode.unrestricted,
                initialUrl: widget.initialUrl,
                onPageFinished: (url){
                  setState(() {
                    isLoaded = true;
                  });
                },
                onWebViewCreated: (WebViewController webViewController) {
                  _controller.complete(webViewController);
                },
                navigationDelegate: (NavigationRequest request) {
                  if(request.url == widget.initialUrl){
                    return NavigationDecision.navigate;
                  }
                  return NavigationDecision.prevent;
                },
              ),
              !isLoaded ? Container(
                alignment: Alignment.center,
                width: double.maxFinite,
                height: double.maxFinite,
                color: ColorTheme.FFFDFDFD,
                child: CircularProgressIndicator(),
              ) : SizedBox()
            ]
        )
    );
  }
}