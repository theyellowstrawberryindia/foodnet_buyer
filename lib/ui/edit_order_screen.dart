import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/model/ItemInDetailsModel.dart';
import 'package:foodnet_buyer/data/model/SearchModel.dart';
import 'package:foodnet_buyer/data/model/order_details_model.dart';
import 'package:foodnet_buyer/data/netwroking/api_path.dart';
import 'package:foodnet_buyer/data/repository/order_repository.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/SelectItemDialog.dart';
import 'package:foodnet_buyer/ui/widgets/cell/cell_item_in_list.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/custome_loader.dart';
import 'package:foodnet_buyer/util/date_time_util.dart';

import 'widgets/dialog/edit_quantity_dialog.dart';

class EditOrderScreen extends StatefulWidget {
  final  dynamic tids;
  final   int mode;
  EditOrderScreen(this.tids,this.mode);

  @override
  _EditOrderScreenState createState() => _EditOrderScreenState();
}

class _EditOrderScreenState extends State<EditOrderScreen> {
  final orderRepo = OrderRepository();
  OrderDetailsModel _itemInDetailsModel;
  List<Iteminitem> _list=[];
  bool loading = true;
  bool error = false;
  String message = '';

  @override
  void initState() {
    super.initState();
    _getItemInData();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
        backgroundColor: ColorTheme.white,
        title: Style.getPoppinsRegularText('Edit Orders', 16, ColorTheme.black),
        iconTheme: IconThemeData(color: Colors.black87),
        actions: [
          IconButton(icon: Icon(Icons.add,color: ColorTheme.primaryColor,), onPressed: (){
            if(_itemInDetailsModel!=null){
              _showAddItems();
            }
          })
        ],
      ),
      body:loading ?Center(
        child: error?Style.getPoppinsRegularText('No Data!', 16, ColorTheme.darkGrey):CircularProgressIndicator(),
      ):Container(
        padding: const EdgeInsets.all(6),
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(itemBuilder: (_,index){
              Iteminitem item = _list[index];
              return Container(
                padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    CellItemInList(item),
                    Container(
                      color: ColorTheme.white,
                      child:Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          GestureDetector(
                            onTap:(){
                              double minQty = widget.mode == 2 ? 1 : item.minQty;
                              double _qty = double.parse(item.quantity);
                              if (_qty > minQty) {
                                _qty -= minQty;
                                if (_qty < minQty) {
                                  _qty = minQty;
                                }
                                item.setQty = _qty.toString();
                                item.setTotalPrice = item.basePrice;
                                setState(() {});
                              } else {
                                AppUtils.showToast("Item quantity cannot be less then ${AppUtils.getDisplayValue(item.minQty)}");
                              }
                            },
                            child: Container(
                              margin: const EdgeInsets.only(left: 20),
                              width: 30,
                              height: 30,
                              decoration: BoxDecoration(
                                color: ColorTheme.white,
                                shape: BoxShape.circle,
                                border: Border.all(color: ColorTheme.primaryColor)
                              ),
                              child: Icon(Icons.remove,color: ColorTheme.primaryColor,size: 20,),
                            ),
                          ),
                          SizedBox(width: 10,),
                          InkWell(
                            onTap: () {
                              EditQuantityDialog.showEditQtyDialog(context, double.parse(item.quantity), item.isKg(), (qty){
                                item.setQty = qty.toString();
                                item.setTotalPrice = item.basePrice;
                                setState(() {
                                });
                              }, minQty: widget.mode == 2 ? 1 : item.minQty);
                            },
                            child: Column(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(color: ColorTheme.FF333333, width: .5)
                                  ),
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.only(
                                      right: 10, left: 10, top: 5, bottom: 5),
                                  child: Style.getPoppinsRegularText(item.getDisplayQty(), 16, ColorTheme.black),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 4, bottom: 4),
                                  child: Icon(Icons.edit, color: ColorTheme.FF333333, size: 14,),
                                )
                              ],
                            ),
                          ),
                          SizedBox(width: 10,),
                          GestureDetector(
                            onTap:(){
                                double minQty = widget.mode == 2 ? 1 : item.minQty;
                                item.setQty = (double.parse(item.quantity)+minQty).toString();
                                item.setTotalPrice = item.basePrice;
                                setState(() {

                                });
                            },
                            child: Container(
                              width: 30,
                              height: 30,
                              decoration: BoxDecoration(
                                  color: ColorTheme.white,
                                  shape: BoxShape.circle,
                                  border: Border.all(color: ColorTheme.primaryColor)
                              ),
                              child: Icon(Icons.add,color: ColorTheme.primaryColor,size: 20,),
                            ),
                          ),
                          Expanded(child: Align(alignment: Alignment.centerRight,child: IconButton(
                            onPressed: (){
                              _list.remove(item);
                              setState(() {

                              });
                            },
                            icon: Icon(Icons.delete,color: Colors.redAccent,)
                            ,),))
                        ],
                      ),
                    ),
                  ],
                ),
              );
            },
              itemCount: _list.length,
            ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              color: ColorTheme.white,
              alignment: Alignment.center,
              child:Style.getTextBtnWithElevation('Update', (){
                if( _list.length>0){
                  _updateOrders();
                }else{

                }
              },height: 40),
            )
          ],
        ),
      ),
    );
  }

  _showAddItems(){
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (cntx) => Container(
        height: MediaQuery.of(context).size.height * 0.95,
        child: SelectItemDialog(
         _itemInDetailsModel.orderData.storeId.toString(),
          selectedItem: (List<SizeVariant> selectedData){
            List<Iteminitem> items = [];
            selectedData.forEach((element) {
              items.add(Iteminitem(
                minQty: widget.mode == 2 ? 1 : element.minQty,
                basePrice: element.salePrice,
                packOfUnit: element.sizeUnit,
                totalQty: '${element.size} ${element.saleUnit}',
                productId: int.parse(element.id),
                productName: element.productDisplayName,
                subtext: element.subText,
                saleUnit: element.saleUnit,
                totalTaxes: element.taxRate,
                totalBase: element.salePrice,
                quantity: widget.mode == 2 ? '1' : element.minQty.toString(),
                packOf: element.size,
                price: element.productPrice,
                packets: widget.mode == 2 ? 1 : element.minQty.toInt(),
              ));
            });

            _list.addAll(items);
            setState(() {});
          },
        ),
      ),
    );
  }

  _getItemInData()async{
    if(widget.mode==1)
    _itemInDetailsModel = await orderRepo.getOrderDetails(widget.tids);
    if(widget.mode==2)
      _itemInDetailsModel = await orderRepo.getGRNDetails(widget.tids);
    loading = false;
    if(_itemInDetailsModel==null)
      error=true;
    else{
      if(widget.mode==1){
        _list = _itemInDetailsModel.orderData.orderitems;
      }else{
        _list = _itemInDetailsModel.orderData.grnItems;
      }
    }

    setState(() {

    });
  }

  _updateOrders()async{
    String date= DateTimeUtil.getDateFromDateTime(_itemInDetailsModel.orderData.deliveryDate, DateTimeUtil.DD_MM_YYYY);
    Map body ={
      "tenantId": ApiPath.TENANT_ID,
      "id": "${_itemInDetailsModel.orderData.id}",
      "addressId": "${_itemInDetailsModel.orderData.addressId}",
      "delivery_date": "${date}",
      "delivery_time": "${_itemInDetailsModel.orderData.deliveryTime}",
      "convenience_fee": "${_itemInDetailsModel.orderData.convenienceCharges}",
      "products":List<dynamic>.from(_list.map((e) => Products(
        id: e.productId,qty: double.parse(e.quantity),sequence:(_list.indexOf(e)+1).toString(),
        packOf: e.packOf,packets: e.packets?.toString(),
        deliveryDate: "${date}",
        deliveryTime: "${_itemInDetailsModel.orderData.deliveryTime}",
        discount:'',
        price: e.price,
        base_price: e.basePrice,
        applied_tax_percent: e.appliedTaxPercent,
      ).toJson())),
    };
    Loader.show(context);
    final result = await orderRepo.updateOrders(body,widget.mode  );
    Loader.hide();
    if(result){
      Navigator.of(context).pop(true);
    }
  }

}

class Products {
  Products({
    this.id,
    this.qty,
    this.sequence,
    this.packOf,
    this.packOfUnit,
    this.packets,
    this.deliveryDate,
    this.deliveryTime,
    this.discount,
    this.price,
    this.applied_tax_percent,
    this.base_price,
  });

  int id;
  double qty;
  String sequence;
  String packOf;
  String packOfUnit;
  String packets;
  String deliveryDate;
  String deliveryTime;
  dynamic discount;
  dynamic base_price;
  dynamic applied_tax_percent;
  dynamic price;


  Map<String, dynamic> toJson() => {
    "id": id,
    "qty": qty,
    "sequence": sequence,
    "packOf": double.parse(packOf).round(),
    "packOfUnit": packOfUnit??'',
    "packets": packets,
    "delivery_date": deliveryDate,
    "delivery_time": deliveryTime,
    "discount": discount,/*
    "base_price":base_price,
    "applied_tax_percent":applied_tax_percent,
    "price":price,*/
  };
}
