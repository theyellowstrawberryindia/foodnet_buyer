import 'dart:io';

import 'package:flutter/material.dart';
import 'package:foodnet_buyer/ui/widgets/netwrok_image_view.dart';

class ImageDialog extends StatelessWidget {

  final bool isfile;
  final String path;
  final Function(String path) onDelete;

  ImageDialog(this.isfile, this.path,{this.onDelete});

  @override
  Widget build(BuildContext context) {
    return BottomSheet(
        onClosing: (){},
        backgroundColor: Colors.transparent,
        builder: (_)=>Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(8),
          topRight: Radius.circular(8),
        )
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                IconButton(icon: Icon(Icons.clear), onPressed: ()=>Navigator.of(context).pop()),
                Expanded(child: Container()),
                if(isfile)
                  IconButton(icon: Icon(Icons.delete), onPressed: (){
                    onDelete?.call(path);
                  }),
              ],
            ),
          ),
          Expanded(
              child: isfile
                  ?Image.file(File(path))
                  :NetworkImageView(path, double.infinity, double.infinity,fit: BoxFit.contain,)
          )
        ],
      ),
    ));
  }
}
