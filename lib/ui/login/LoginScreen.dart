
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/netwroking/api_path.dart';
import 'package:foodnet_buyer/data/repository/main_reporitory.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/login/OtpScreen.dart';
import 'package:foodnet_buyer/ui/widgets/CustomAppBar.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/custome_loader.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String mobile = '';
  TextEditingController mobileController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar("Login"),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (_) {
            AppUtils.clearFocusForScreen(context);
          },
          child: new Column(
//          crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                  alignment: Alignment.centerLeft,
                  margin: const EdgeInsets.only(left: 30, top: 10, right: 30, bottom: 5),
                  child: Style.getPoppinsLightText("Mobile No", AppFonts.textSize12, ColorTheme.FF999999)
              ),
              Style.getTextInputField("Enter mobile number", controller: mobileController, inputType: TextInputType.number, margin: EdgeInsets.only(
                  left: 20, right: 20, bottom: 0), onChanged: (value) {
                mobile = value;
              }, maxLength: 10),
              Padding(
                padding: const EdgeInsets.all(40.0),
                child: Style.getTextBtnWithElevation("Get OTP", () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                  _getOtpLogin();
                }),
              ),
              Expanded(
                child: SizedBox(),
              ),
              Container(
                padding: EdgeInsets.only(left: 30, right: 30, bottom: 20),
                child: Image.asset(AppAssets.registerPlaceHolder, fit: BoxFit.contain,),
              )
            ],
          ),
        ));
  }

  ///this method will call for otp
  _getOtpLogin() async {

    if (mobile.trim().isEmpty) {
      AppUtils.showToast("Please enter mobile no");
      return;
    } else if (mobile.trim().length != 10) {
      AppUtils.showToast("Please enter a valid 10 digit mobile no");
      return;
    }

    if (mobile.trim().isNotEmpty) {
      PreferenceManager.isLogin(true);
      PreferenceManager.setMobile(mobile.trim());
      Loader.show(context);
      try {
        var response = await MainRepository.get().sendOtp(
            mobile, ApiPath.TYPE_LOGIN);
        Loader.hide();
        if (response != null && response["status"]) {
             AppUtils.showToast(response["otp"]?.toString());
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => OtpScreen(),
            ),
          );
        } else {
          if (response != null) {
            AppUtils.showToast(response["message"]?.toString());
          } else {
            AppUtils.showToast("Something went wrong");
          }
        }
      } catch(e) {
        Loader.hide();
      }

    } else {
      AppUtils.showToast("Please Enter Mobile No");
    }
  }
}
