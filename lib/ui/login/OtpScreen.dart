import 'dart:async';

import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/constant/text_msgs.dart';
import 'package:foodnet_buyer/data/netwroking/api_path.dart';
import 'package:foodnet_buyer/data/repository/main_reporitory.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/login/OtpSuccessDialog.dart';
import 'package:foodnet_buyer/ui/widgets/CustomAppBar.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/custome_loader.dart';
import 'package:foodnet_buyer/util/login_util.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';

class OtpScreen extends StatefulWidget {
  @override
  _OtpScreenState createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  // String otpValue = '2212';
  String otpValue;

  Future<void> getDialog() async {
    await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return OtpSuccessDialog();
        });
  }

  static const _timerDuration = 60;
  StreamController _timerStream = new StreamController<int>();
  int timerCounter;
  Timer _resendCodeTimer;
  // TextEditingController otpController = TextEditingController(text: "3301");
  TextEditingController otpController = TextEditingController();

  @override
  void initState() {
    activeCounter();

    super.initState();
  }

  @override
  dispose() {
    _timerStream.close();
    _resendCodeTimer.cancel();

    super.dispose();
  }

  activeCounter() {
    _resendCodeTimer = new Timer.periodic(Duration(seconds: 1), (Timer timer) {
      if (_timerDuration - timer.tick > 0)
        _timerStream.sink.add(_timerDuration - timer.tick);
      else {
        _timerStream.sink.add(0);
        _resendCodeTimer.cancel();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar("Enter OTP"),
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onPanDown: (_) {
          AppUtils.clearFocusForScreen(context);
        },
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(
                  left: 20, top: 20, right: 20, bottom: 4),
              child: Style.getPoppinsRegularText(
                  'Enter OTP received on your registered mobile number.',
                  AppFonts.textSize14,
                  ColorTheme.FF999999,
                  maxLines: 2),
            ),
            Container(
                margin: const EdgeInsets.only(left: 30, right: 30, bottom: 5),
                child: Style.getPoppinsRegularText(
                    "OTP", AppFonts.textSize12, ColorTheme.FF999999)),
            Style.getTextInputField("Enter OTP",
                controller: otpController,
                inputType: TextInputType.number,
                margin: EdgeInsets.only(left: 20, right: 20, bottom: 0),
                onChanged: (value) {
              otpValue = value;
            }),
            Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 19),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Style.getPoppinsLightText("OTP not received?",
                          AppFonts.textSize14, ColorTheme.FF999999),
                      SizedBox(
                        width: 6.0,
                      ),
                      StreamBuilder(
                        stream: _timerStream.stream,
                        builder: (context, snapshot) => GestureDetector(
                          onTap: () {
                            if (snapshot.data == 0) {
                              _getOtpLogin();
                              _timerStream.sink.add(_timerDuration);
                              activeCounter();
                            }
                          },
                          child: Style.getPoppinsRegularText(
                              snapshot.data == 0
                                  ? 'Resend'
                                  : '${snapshot.hasData ? snapshot.data.toString() : _timerDuration} Secs ',
                              AppFonts.textSize14,
                              ColorTheme.FFF0674C),
                        ),
                      )
                    ])),
            Expanded(
              child: SizedBox(),
            ),
            Padding(
              padding: const EdgeInsets.all(40.0),
              child: Style.getTextBtnWithElevation("Verify", () {
                FocusScope.of(context).requestFocus(new FocusNode());
                _validateOtp();
              }),
            )
          ],
        ),
      ),
    );
  }

  _validateOtp() async {
    if (otpValue.isNotEmpty) {
      Loader.show(context);
      PreferenceManager.setOtp(otpValue);
      bool comingFromLoginScreen = PreferenceManager.getLogin();
      var mobile = PreferenceManager.getMobile();
      var email = PreferenceManager.getEmail();
      var firstName = PreferenceManager.getFirstName();
      var lastName = PreferenceManager.getLastName();

      if (comingFromLoginScreen) {
        try {
          Loader.show(context);
          var otpResponse =
              await MainRepository.get().loginUser(mobile, otpValue);
          Loader.hide();
          if (otpResponse != null) {
            AppUtils.showToast(otpResponse.message);
            LoginUtil.onLogin(context, otpResponse);
          }
        } catch (e) {
          Loader.hide();
          AppUtils.showToast(TextMsgs.COMMON_ERROR);
        }
      } else {
        getDialog();
      }
    } else {
      AppUtils.showToast("Please enter OTP");
    }
  }

  _getOtpLogin() async {
      try {
        Loader.show(context);
        var response = await MainRepository.get().sendOtp(
            PreferenceManager.getMobile(), PreferenceManager.getLogin() ? ApiPath.TYPE_LOGIN : ApiPath.TYPE_REGISTRATION);
        Loader.hide();
        if (response != null && response["status"]) {
          AppUtils.showToast(response["otp"]?.toString());
        } else {
          if (response != null) {
            AppUtils.showToast(response["message"]?.toString());
          } else {
            AppUtils.showToast("Something went wrong");
          }
        }
      } catch(e) {
        Loader.hide();
      }
  }
}
