
import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/dashboard/dashboard_screen.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';

class OtpSuccessDialog extends StatefulWidget {
  @override
  _OtpSuccessDialogState createState() => _OtpSuccessDialogState();
}

class _OtpSuccessDialogState extends State<OtpSuccessDialog> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(32.0))),
          contentPadding: EdgeInsets.only(top: 10.0),
          content: Container(
            width: 315,
            height: 465.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 5, top: 66, right: 5, bottom: 30),
                  child: Image.asset(
                    "assets/images/otp_success.png",
                    fit: BoxFit.cover,
                    height: 190.0,
                    alignment: Alignment.center,
                  ),
                ),
                Style.getPoppinsRegularText('Registration\nSuccessful', AppFonts.textSize18, ColorTheme.FF333333, maxLines: 2, align: TextAlign.center),
                Padding(
                  padding: const EdgeInsets.only(left: 60, right: 60, bottom: 30.0, top: 30),
                  child: Style.getTextBtnWithElevation("Next", () {
                    _validateLogin();
                  }),
                )
              ],
            ),
          )),
    );
  }

  _validateLogin() async {
    var mobile = PreferenceManager.getMobile();
    var otp = PreferenceManager.getOtp();
    // TODO: call login api
    NavigationUtil.clearAllAndAdd(context, DashboardScreen());
  }
}
