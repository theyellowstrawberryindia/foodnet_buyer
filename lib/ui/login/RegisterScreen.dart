
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/dashboard/navigation_drawer.dart';
import 'package:foodnet_buyer/ui/dashboard/webview_screen.dart';
import 'package:foodnet_buyer/ui/login/OtpScreen.dart';
import 'package:foodnet_buyer/ui/widgets/CustomAppBar.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
//  final _mobileController = TextEditingController();
  String mobile = '';
  String email = '';
  String firstName = '';
  String lastName = '';
  bool monVal = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar("Register"),
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onPanDown: (_) {
          AppUtils.clearFocusForScreen(context);
        },
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(left: 30, top: 25, right: 30, bottom: 5),
                child: Style.getPoppinsLightText("First Name", AppFonts.textSize12, ColorTheme.FF999999)
              ),
              Style.getTextInputField("Enter first name", inputType: TextInputType.name, margin: EdgeInsets.only(
                  left: 20, right: 20, bottom: 0), onChanged: (value) {
                firstName = value;
              }),

              Container(
                  margin: const EdgeInsets.only(left: 30, top: 25, right: 30, bottom: 5),
                  child: Style.getPoppinsLightText("Last Name", AppFonts.textSize12, ColorTheme.FF999999)
              ),
              Style.getTextInputField("Enter last name", inputType: TextInputType.name, margin: EdgeInsets.only(
                  left: 20, right: 20, bottom: 0), onChanged: (value) {
                lastName = value;
              }),
              Container(
                  margin: const EdgeInsets.only(left: 30, top: 25, right: 30, bottom: 5),
                  child: Style.getPoppinsLightText("Mobile No", AppFonts.textSize12, ColorTheme.FF999999)
              ),
              Style.getTextInputField("Enter mobile number", inputType: TextInputType.number, margin: EdgeInsets.only(
                  left: 20, right: 20, bottom: 0), onChanged: (value) {
                mobile = value;
              }, maxLength: 10),
              // Container(
              //     margin: const EdgeInsets.only(left: 30, top: 25, right: 30, bottom: 5),
              //     child: Style.getPoppinsLightText("Email Id", AppFonts.textSize12, ColorTheme.FF999999)
              // ),
              // Style.getTextInputField("Enter email id", inputType: TextInputType.emailAddress, margin: EdgeInsets.only(
              //     left: 20, right: 20, bottom: 0), onChanged: (value) {
              //   email = value;
              // }),

              Container(
                margin:
                    const EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          monVal = !monVal;
                        });
                      },
                      child: monVal
                          ? Icon(
                              Icons.check_box,
                              color: ColorTheme.FFF0674C,
                            )
                          : Icon(Icons.check_box_outline_blank,
                              color: ColorTheme.FFD9D9D9),
                    ),
                    SizedBox(width: 10,),
                    RichText(
                        text: TextSpan(children: [
                      TextSpan(
                          text: 'I Accepts',
                          style: Style.getTextStyleRegular(AppFonts.textSize12, ColorTheme.FF999999)),
                      TextSpan(
                          text: ' Terms & Conditions',
                          recognizer: TapGestureRecognizer()..onTap= () {
                            NavigationUtil.pushToNewScreen(context, WebViewScreen(name: "Terms & Conditions", initialUrl: NavigationDrawer.tcUrl));
                          },
                          style: Style.getTextStyleRegular(AppFonts.textSize12, ColorTheme.FF333333))
                    ]))
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 40, right: 40, top: 40, bottom: 50),
                child: Style.getTextBtnWithElevation("Get OTP", () {
                  _getOTP();
                }),
              ),

              Container(
                alignment: Alignment.bottomCenter,
                padding: EdgeInsets.only(left: 30, right: 30, bottom: 20),
                child: Image.asset(AppAssets.registerPlaceHolder,  fit: BoxFit.cover,),
              )
            ],
          ),
        ),
      ),
    );
  }

  ///this method will call for otp
  _getOTP() async {
    if (_isValidated()) {
      PreferenceManager.isLogin(false);
      PreferenceManager.setMobile(mobile.trim());
      PreferenceManager.setEmail(email.trim());
      PreferenceManager.setFirstName(firstName.trim());
      PreferenceManager.setLastName(lastName.trim());
      NavigationUtil.pushToNewScreen(context, OtpScreen());
    }
  }

  _isValidated() {
    if (firstName.trim().isEmpty) {
      AppUtils.showToast("Please enter first name");
      return false;
    } else if (lastName.trim().isEmpty) {
      AppUtils.showToast("Please enter last name");
      return false;
    } else if (mobile.trim().isEmpty) {
      AppUtils.showToast("Please enter mobile no");
      return false;
    } else if (mobile.trim().length != 10) {
      AppUtils.showToast("Please enter a valid 10 digit mobile no");
      return false;
    } else if (email.trim().isEmpty) {
      AppUtils.showToast("Please enter email id");
      return false;
    }  else if (!AppUtils.isValidEmail(email)) {
      AppUtils.showToast("Please enter valid email id");
      return false;
    } else if (!monVal) {
      AppUtils.showToast("Please accept Terms and Conditions");
      return false;
    }
    return true;
  }
}
