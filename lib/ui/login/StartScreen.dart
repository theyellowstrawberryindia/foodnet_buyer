import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/dashboard/dashboard_screen.dart';
import 'package:foodnet_buyer/ui/login/LoginScreen.dart';
import 'package:foodnet_buyer/ui/login/RegisterScreen.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';

class StartScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    bool showBack = Navigator.canPop(context);
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          automaticallyImplyLeading: true,
            elevation: 0,
            iconTheme: IconThemeData(
              color: ColorTheme.FF333333, //change your color here
            )
        ),
        body: new Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(top: showBack ? 0: 45, bottom: 20),
              child: Image.asset(
                "assets/images/logo.png",
                height: 60,
                fit: BoxFit.contain,
              ),
            ),
            Container(
              margin: const EdgeInsets.only(
                  left: 40, top: 40, bottom: 21, right: 40),
              color: Colors.transparent,
              width: MediaQuery.of(context).size.width,
              child: Style.getTextBtnWithElevation("Register", () {
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //     builder: (context) => RegisterScreen(),
                //   ),
                // );
                AppUtils.showToast("Coming Soon");
              })
            ),
            Container(
//              margin:  const EdgeInsets.only(left: 19, top: 20, right: 19, bottom: 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Style.getPoppinsLightText("Already Registered?", AppFonts.textSize14, ColorTheme.FF666666),
                  SizedBox(
                    width: 10.0,
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => LoginScreen(),
                        ),
                      );
                    },
                    child: Style.getPoppinsRegularText("Login", AppFonts.textSize14, ColorTheme.FFF0674C)
                  )
                ],
              ),
            ),
            Expanded(
              child: SizedBox()
            ),
            Container(
              alignment: Alignment.bottomCenter,
              margin:
              EdgeInsets.only(left: 23, top: 80, right: 23, bottom: 20),
//              child: Image(image: AssetImage('assets/images/registerImage.png')),
              child: Image.asset(
                'assets/images/startplaceholder.png',
                height: 200,
                fit: BoxFit.fill,
              ),
            ),
          ],
        ));
  }
}
