
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/constant/text_msgs.dart';
import 'package:foodnet_buyer/data/repository/order_repository.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/custome_loader.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';

class CommentScreen extends StatefulWidget {
  final String status;
  final int grnId;
  final String preFillComment;
  final int tid;
  final bool allowEdit;

  CommentScreen(this.status, this.tid, {this.grnId, this.preFillComment, this.allowEdit = false});

  @override
  _CommentScreenState createState() => _CommentScreenState();
}

class _CommentScreenState extends State<CommentScreen> {
  final _controller = TextEditingController();
  final orderRepo = OrderRepository();
  bool showLoader = true;
  var commentsResponse;
  String dcComment;
  String grnComment;

  @override
  void initState() {
    super.initState();
    _getComments();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          Navigator.of(context).pop(false);
          return true;
        },
        child: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              backgroundColor: ColorTheme.white,
              title: Style.getPoppinsRegularText('Comments', 16, ColorTheme.black),
              iconTheme: IconThemeData(color: Colors.black87),
            ),
            body: showLoader
                ? Center(child: CircularProgressIndicator(),)
                : Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (AppUtils.isValid(dcComment))
                        Container(
                            padding: EdgeInsets.only(bottom: 10),
                            width: MediaQuery.of(context).size.width - 20,
                            child: Row(
                              children: [
                                Style.getPoppinsMediumText("DC Comment: ", 14, ColorTheme.FF333333),
                                SizedBox(width: 10,),
                                Style.getPoppinsLightText("${AppUtils.getDisplayValue(dcComment)}", 14, ColorTheme.FF666666, maxLines: 3)
                              ],
                            )
                        ),
                      if (AppUtils.isValid(grnComment))
                        Container(
                            padding: EdgeInsets.only(bottom: 10),
                            width: MediaQuery.of(context).size.width - 20,
                            child: Row(
                              children: [
                                Style.getPoppinsMediumText("GRN Comment: ", 14, ColorTheme.FF333333),
                                SizedBox(width: 10,),
                                Style.getPoppinsLightText("${AppUtils.getDisplayValue(grnComment)}", 14, ColorTheme.FF666666, maxLines: 3)
                              ],
                            )
                        ),
                      // Container(
                      //   width: MediaQuery.of(context).size.width - 30,
                      //   margin: EdgeInsets.only(bottom: 20, top: 10),
                      //   child: Row(
                      //     children: [
                      //       Style.getPoppinsMediumText("Previous Comment: ", 14, ColorTheme.FF333333),
                      //       SizedBox(width: 10,),
                      //       Expanded(child: Style.getPoppinsLightText("${AppUtils.isValid(widget.preFillComment) ? widget.preFillComment : "NA"}", 14, ColorTheme.FF666666, maxLines: 4))
                      //     ],
                      //   ),
                      // ),
                      SizedBox(height: 20,),
                      widget.allowEdit ? Style.getTextInputField("Enter ${widget.status} Comment", prefill: "", controller: _controller, minLines: 3, maxLines: 3, height: 100) :  SizedBox(),
                      widget.allowEdit ? Style.getTextBtnWithElevation("Done", () {
                        FocusScope.of(context).unfocus();
                        if (_controller.text.isEmpty) {
                          AppUtils.showToast("Enter Comment");
                          return;
                        }
                        _addData();
                      },
                          height: 40
                      ) : SizedBox(),
                      SizedBox(height: widget.allowEdit ? 10 : 0,),
                      widget.allowEdit ? Style.getPoppinsRegularText("Suggestions", 16, ColorTheme.FF333333) : SizedBox(),
                      SizedBox(height: widget.allowEdit ? 5 : 0,),
                      commentsResponse != null  &&  widget.allowEdit ? Expanded(
                        child: ListView.builder(
                            itemCount: commentsResponse.length,
                            itemBuilder: (ctx, index) {
                              return InkWell(
                                onTap: () {
                                  _controller.text = commentsResponse[index]["comment"];
                                },
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  child: Style.getPoppinsLightText(commentsResponse[index]["comment"], 14, ColorTheme.FF333333),
                                ),
                              );
                            }),
                      ): Container()
                    ],
                  ),
                )
        ));
  }

  _getComments() async {
    setState(() {
      showLoader = true;
    });
    try {
      var allResponse = await orderRepo.getAllOrderComments(widget.tid);
      if (allResponse != null && allResponse["status"]) {
        var data = allResponse["data"][0];
        dcComment = data["dccomment"]?.toString();
        grnComment = data["grncomment"]?.toString();
      }
    } catch (e) {}

    try {
      if (widget.allowEdit) {
        var response = await orderRepo.getAllGRNComments();
        if (response != null && response["status"]) {
          commentsResponse = response["data"];
        }
      }
    } catch (e) {}

    setState(() {
      showLoader = false;
    });
  }

  _addData()async{
    if(_controller.text.isNotEmpty) {
      try {
        Loader.show(context);
        var response = await orderRepo.updateGRNComment(
            widget.grnId, _controller.text.toString());
        Loader.hide();
        if (response) {
          AppUtils.showToast("Comment added successfully");
          NavigationUtil.pop(context, result: true);
        } else {
          AppUtils.showToast(TextMsgs.COMMON_ERROR);
        }
      } catch (e) {
        Loader.hide();
        AppUtils.showToast(TextMsgs.COMMON_ERROR);
      }
    }
  }
}