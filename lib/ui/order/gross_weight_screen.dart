
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/constant/text_msgs.dart';
import 'package:foodnet_buyer/data/repository/order_repository.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/custome_loader.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';

class GrossWeightScreen extends StatefulWidget {
  final String status;
  final int itemInId;
  final int grnId;
  final String preFillGrossWeight;
  final bool allowEdit;
  final int tid;
  final bool showApiData;

  GrossWeightScreen(this.status, this.tid, {this.itemInId, this.grnId, this.preFillGrossWeight, this.allowEdit = true, this.showApiData = true});

  @override
  _GrossWeightScreenState createState() => _GrossWeightScreenState();
}

class _GrossWeightScreenState extends State<GrossWeightScreen> {
  final grossWeightController = TextEditingController();
  final orderRepo = OrderRepository();
  bool allowEdit = true;
  bool showLoader = true;
  String dcGrossWeight;
  String itemInGrossWeight;
  String grnGrossWeight;

  @override
  void initState() {
    super.initState();
    allowEdit = widget.allowEdit;
    //grossWeightController.text = AppUtils.getDisplayValue(widget.preFillGrossWeight ?? "");
    if (widget.showApiData) {
      _getData();
    } else {
      showLoader = false;
    }
  }

  @override
  void dispose() {
    super.dispose();
    Loader.hide();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop(false);
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: ColorTheme.white,
          title: Style.getPoppinsRegularText('Gross Weight', 16, ColorTheme.black),
          iconTheme: IconThemeData(color: Colors.black87),
        ),
        body: showLoader ? Center(child: CircularProgressIndicator(),) : Container(
          padding: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (AppUtils.isValid(dcGrossWeight))
                Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: Row(
                      children: [
                        Style.getPoppinsMediumText("DC Gross Weight: ", 14, ColorTheme.FF333333),
                        SizedBox(width: 10,),
                        Style.getPoppinsLightText("${AppUtils.getDisplayValue(dcGrossWeight)} Kg", 14, ColorTheme.FF666666)
                      ],
                    )
                ),
              if (AppUtils.isValid(itemInGrossWeight))
                Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: Row(
                      children: [
                        Style.getPoppinsMediumText("ItemIn Gross Weight: ", 14, ColorTheme.FF333333),
                        SizedBox(width: 10,),
                        Style.getPoppinsLightText("${AppUtils.getDisplayValue(itemInGrossWeight)} Kg", 14, ColorTheme.FF666666)
                      ],
                    )
                ),
              if (AppUtils.isValid(grnGrossWeight))
                Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: Row(
                      children: [
                        Style.getPoppinsMediumText("GRN Gross Weight: ", 14, ColorTheme.FF333333),
                        SizedBox(width: 10,),
                        Style.getPoppinsLightText("${AppUtils.getDisplayValue(grnGrossWeight)} Kg", 14, ColorTheme.FF666666)
                      ],
                    )
                ),
              allowEdit ? Container(
                margin: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                child: Container(
                  height: 40,
                  
                  decoration: BoxDecoration(
                    color: ColorTheme.white,
                    borderRadius: BorderRadius.all(Radius.circular(30))
                  ),
                  child: TextField(
                    controller: grossWeightController,
                    style: TextStyle(fontSize: 16,color: ColorTheme.black,),
                    textInputAction: TextInputAction.done,
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp('[0-9.]')),
                    ],
                    onChanged: (vale){},
                    maxLength: 10,
                    decoration: InputDecoration(
                        counter: null,
                        counterText: '',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30)
                        ),
                        hintText: 'Enter ${widget.status} gross weight',
                        contentPadding: const EdgeInsets.symmetric(vertical: 4,horizontal: 16),
                        hintStyle: TextStyle(fontSize: 16,color: ColorTheme.darkGrey,),
                        suffix: Container(
                          width: 24,
                          child: Style.getPoppinsBoldText('Kg', 14, Colors.black54),
                        )
                    ),
                  ),
                )
              ) :  SizedBox(),
              Expanded(child: SizedBox(),),
              allowEdit ? Style.getTextBtnWithElevation("Done", () {
                FocusScope.of(context).unfocus();
                if (grossWeightController.text.isEmpty) {
                  AppUtils.showToast("Enter gross weight");
                  return;
                }
                _addData();
              },
                  height: 40
              ) : SizedBox(),
              SizedBox(height: 10,)
            ],
          ),
        ),
      ),
    );
  }
  
  _getData() async {
    setState(() {
      showLoader = true;
    });
    
    var response = await orderRepo.getAllOrderGrossWeight(widget.tid);

    if (response != null && response["status"]) {
      var data = response["data"][0];
      dcGrossWeight = data["dcgrossweight"]?.toString();
      itemInGrossWeight = data["itemingrossweight"]?.toString();
      grnGrossWeight = data["grngrossweight"]?.toString();
    }
    setState(() {
      showLoader = false;
    });
  }

  _addData()async{
    if(grossWeightController.text.isNotEmpty){
      _addGrossWeight();
    }
  }

  _addGrossWeight()async{
    var response;
    try {
      Loader.show(context);
      if (widget.status == "ItemIn") {
        response = await orderRepo.addGrossWeight(
            "${widget.itemInId}", grossWeightController.text.toString());
      } else {
        response = await orderRepo.updateGRNGrossWeight(
            widget.grnId, grossWeightController.text.toString());
      }
      Loader.hide();
      if (response != null) {
        AppUtils.showToast("Gross weight has been updated successfully");
        NavigationUtil.pop(context, result: true);
      } else {
        AppUtils.showToast(TextMsgs.COMMON_ERROR);
      }
    } catch (e) {
      Loader.hide();
      AppUtils.showToast(TextMsgs.COMMON_ERROR);
    }
  }
}