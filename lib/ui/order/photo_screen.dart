import 'dart:convert';
import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/model/ItemInDetailsModel.dart';
import 'package:foodnet_buyer/data/model/OrderModel.dart';
import 'package:foodnet_buyer/data/model/order_details_model.dart';
import 'package:foodnet_buyer/data/netwroking/api_path.dart';
import 'package:foodnet_buyer/data/repository/order_repository.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/image_dialog.dart';
import 'package:foodnet_buyer/ui/verify_screen.dart';
import 'package:foodnet_buyer/ui/widgets/dialog/verify_dialog.dart';
import 'package:foodnet_buyer/ui/widgets/netwrok_image_view.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/custome_loader.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';
import 'package:foodnet_buyer/util/permission_utils.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class PhotoScreen extends StatefulWidget {
  final String status;
  final int itemInId;
  final int grnId;
  final int tid;
  final bool allowAddPhotos;

  PhotoScreen(this.status, this.tid, {this.itemInId, this.grnId, this.allowAddPhotos = true});

  @override
  _PhotoScreenState createState() => _PhotoScreenState();
}

class _PhotoScreenState extends State<PhotoScreen> {
  final orderRepo = OrderRepository();
  List<UploadPhotos> _uploadPhotos =[];
  bool showLoader = true;
  File _image;
  final picker = ImagePicker();
  List<ImageData> _list = [];
  List<Map<String, dynamic>> existingImageItems = [];

  @override
  void initState() {
    super.initState();
    _list.add(ImageData(1, ''));
    _getImages();
  }

  @override
  void dispose() {
    super.dispose();
    Loader.hide();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop(false);
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: ColorTheme.white,
          title: Style.getPoppinsRegularText('Photos', 16, ColorTheme.black),
          iconTheme: IconThemeData(color: Colors.black87),
        ),
        body: showLoader
            ? Center(child: CircularProgressIndicator(),)
            : SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    for (int i=0;i<existingImageItems.length;i++)
                      Container(
                        height: 100,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Style.getPoppinsLightText(existingImageItems[i]["name"], 14, ColorTheme.FF333333),
                            SizedBox(height: 5,),
                            Container(
                                height: 60,
                                child: ListView(
                                  shrinkWrap: true,
                                  scrollDirection: Axis.horizontal,
                                  children: existingImageItems[i]["images"].map<Widget>((e) {
                                    return GestureDetector(
                                      onTap: () {
                                        showModalBottomSheet(
                                            isScrollControlled: true,
                                            context: context,
                                            backgroundColor: Colors.transparent,
                                            builder: (_) => Container(
                                                height: MediaQuery.of(context).size.height * 0.95,
                                                child: ImageDialog(false, e)));
                                      },
                                      child: Container(
                                        width: 60,
                                        height: 60,
                                        padding: EdgeInsets.all(0),
                                        margin: const EdgeInsets.only(right: 10),
                                        child: ClipRRect(
                                            borderRadius: BorderRadius.circular(8),
                                            child: NetworkImageView(e, 60, 60)),
                                      ),
                                    );
                                  }).toList(),
                                )),
                            SizedBox(height: 10,),
                          ],
                        ),
                      ),

                    widget.allowAddPhotos ? Container(
                      margin: const EdgeInsets.only(top: 10, bottom: 10),
                    child: Style.getPoppinsLightText('Add Photos', 14, ColorTheme.FF333333),
                    ) : SizedBox(),
                    widget.allowAddPhotos ? SizedBox(
                      height: 60,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: _list.length,
                          itemBuilder: (ctx, index) {
                            var e = _list[index];
                            if(e.type==1)
                              return GestureDetector(
                                onTap: ()=>_requestPermission(),
                                child: Container(
                                  width: 60,
                                  height: 60,
                                  margin: EdgeInsets.only(right: 10),
                                  child: DottedBorder(
                                      radius: Radius.circular(10),
                                      strokeWidth:1,
                                      dashPattern: [2,5],
                                      borderType: BorderType.RRect,
                                      strokeCap: StrokeCap.round,
                                      child: Center(child: Icon(Icons.add,color: ColorTheme.darkGrey,),)
                                  ),
                                ),
                              );
                            else
                              return GestureDetector(
                                onTap: (){
                                  showModalBottomSheet(
                                      isScrollControlled: true,
                                      context: context,
                                      backgroundColor: Colors.transparent,
                                      builder: (_)=>Container(height: MediaQuery.of(context).size.height*0.95,child: ImageDialog(true, e.path,onDelete: (path){
                                        Navigator.of(context).pop();

                                        _list.removeWhere((element) => element.path==path) ;
                                        _uploadPhotos.removeWhere((element) => element.path==path);
                                        setState(() {
                                        });
                                      },)));

                                },
                                child: Container(
                                  margin: EdgeInsets.only(right: 10),
                                  height: 60,
                                  width: 60,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Image.file(File(e.path),fit: BoxFit.fill,),
                                  ),
                                ),
                              );
                          },
                      ),
                    ) : SizedBox(),
                    widget.allowAddPhotos ? Container(
                      margin: EdgeInsets.all(40),
                      child: Style.getTextBtnWithElevation("Done", () {
                          _addData();
                        },
                          height: 40
                      ),
                    ):  SizedBox(),
                  ],
                ),
              ),
            ),
      ),
    );
  }

  _requestPermission()async{
    final result = await PermissionUtil.instance.requestSinglePermission(Permission.storage);
    if(result){
      getImage();
    }
  }

   getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        _list.add(ImageData(2, pickedFile.path));
        _addSinglePic(pickedFile.path);
      }
    });
  }

  _addData()async{
    if(_uploadPhotos.length>0){
      _uploadPic();
    }else{
      AppUtils.showToast('You have not added any pictures');
    }
  }

  _addSinglePic(String path)async{
    Loader.show(context);
    final uploadedData = await orderRepo.uploadImage( _image.path, "${widget.itemInId}");
    if(uploadedData!=null){
      var data = jsonDecode(uploadedData);
      _uploadPhotos.add(UploadPhotos(docId:data['data']['id'].toString(),docUrl: data['data']['url'].toString(),path:path,docUsage: widget.status == "DC" ? "deliverychallan" : widget.status.toLowerCase()));
    }
    Loader.hide();

  }

  _uploadPic()async{
    Map body = {
      "tenantId": ApiPath.TENANT_ID,
      "id": widget.status == "ItemIn" ? "${widget.itemInId}" : "${widget.grnId}",
      "photos": List<dynamic>.from(_uploadPhotos.map((e) => e.toJson())),
    };
    
    var request;
    Loader.show(context);
    if (widget.status == "ItemIn") {
      request = await orderRepo.uploadAllPicUrl(body);
    } else {
      request = await orderRepo.uploadAllGRNPicUrl(body);
    }
    Loader.hide();

    if(request){
      NavigationUtil.pop(context, result: true);
    }
  }

  _getImages() async {
    setState(() {
      showLoader = true;
    });

    var response = await orderRepo.getAllImages(widget.tid, section: widget.status == "Invoice" || widget.status == "Payment" ? "Invoice" : "");
    List<Map<String, dynamic>> items = [];
    if (response != null && response["status"]) {
      var data = response["data"][0];
      if (data["dcdocuments"] != null && data["dcdocuments"].length > 0) {
        items.add(parseData("DC Images", data["dcdocuments"]));
      }
      if (data["itemindocuments"] != null && data["itemindocuments"].length > 0) {
        items.add(parseData("ItemIn Images", data["itemindocuments"]));
      }
      if (data["grndocuments"] != null && data["grndocuments"].length > 0) {
        items.add(parseData("GRN Images", data["grndocuments"]));
      }
      if (data["invoicedocuments"] != null && data["invoicedocuments"].length > 0) {
        items.add(parseData("Invoice Images", data["invoicedocuments"]));
      }
    }

    AppUtils.log("Items Size: ${items.length}");
    existingImageItems.clear();
    existingImageItems.addAll(items);

    setState(() {
      showLoader = false;
    });
  }

  Map<String, dynamic> parseData(String title, dynamic data) {
    List<String> images = [];
    data.forEach((element){
      if (element["document_url"] != null) {
        images.add(element["document_url"]);
      }
    });
    return { "name" : title, "images" : images};
  }

}

class ImageData{
  int type;
  String path;

  ImageData(this.type, this.path);

}

class UploadPhotos{
  String docId;
  String docUrl;
  String docUsage;
  String path;
  UploadPhotos({this.docId, this.docUrl, this.docUsage,this.path});

  factory UploadPhotos.fromJson(Map<String, dynamic> json) => UploadPhotos(
   docUrl: json['document_url']
  );

   Map<String , dynamic> toJson()=>{
     "document_id": docId, "document_url": "$docUrl", "document_usage": "$docUsage"
   };
}
