import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/constant/text_msgs.dart';
import 'package:foodnet_buyer/data/model/ItemInDetailsModel.dart';
import 'package:foodnet_buyer/data/model/order_details_model.dart';
import 'package:foodnet_buyer/data/model/order_details_model.dart' as unique;
import 'package:foodnet_buyer/data/netwroking/api_path.dart';
import 'package:foodnet_buyer/data/repository/order_repository.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/add_gross_weight.dart';
import 'package:foodnet_buyer/ui/edit_order_screen.dart';
import 'package:foodnet_buyer/ui/image_dialog.dart';
import 'package:foodnet_buyer/ui/widgets/cell/cell_item_in_list.dart';
import 'package:foodnet_buyer/ui/widgets/cell/cell_tracking.dart';
import 'package:foodnet_buyer/ui/widgets/netwrok_image_view.dart';
import 'package:foodnet_buyer/ui/widgets/order_operation_view.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/custome_loader.dart';
import 'package:foodnet_buyer/util/date_time_util.dart';
import 'package:foodnet_buyer/util/permission_utils.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';
import 'package:foodnet_buyer/util/razorpay_util.dart';
import 'package:foodnet_buyer/util/svgImages.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class OrderDetailScreen extends StatefulWidget {
  final String tId;
  final String orderId;
  final bool grn;

  OrderDetailScreen(this.tId, this.orderId, {this.grn = true});

  @override
  _OrderDetailScreenState createState() => _OrderDetailScreenState();
}

class _OrderDetailScreenState extends State<OrderDetailScreen> {
  ScrollController _scrollController = ScrollController();
  TextEditingController _grnGrossWeightController = TextEditingController();
  TextEditingController _grnCommentController = TextEditingController();
  final orderRepository = OrderRepository();
  List<Tracking> _tracking = [];
  bool loader = true;
  OrderDetailsModel _detailsModel;
  OrderDetailsModel _grnDetailsModel;
  OrderDetailsModel _deliveryChallan;
  OrderDetailsModel _invoice;
  ItemInDetailsModel _itemInDetailsModel;
  bool allowAutoScroll = true;

  List<String> currentTab  =['PO','DC','ItemOut','ItemIn','GRN','Invoice','Payment'];
  String pendingStatus = "";

  _OrderDetailScreenState();

  @override
  void initState() {
    super.initState();
    _getData();
  }

  @override
  void dispose() {
    _grnGrossWeightController.dispose();
    _grnCommentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorTheme.white,
        centerTitle: true,
        title:
            Style.getPoppinsRegularText('Order Details', 16, ColorTheme.black),
        iconTheme: IconThemeData(color: Colors.black87),
      ),
      body: CustomScrollView(
        controller: _scrollController,
        slivers: loader
            ? [
                SliverFillRemaining(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                )
              ]
            : [
                SliverToBoxAdapter(
                  child: Container(
                    padding: const EdgeInsets.all(14),
                    color: ColorTheme.white,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            if (_detailsModel?.orderData?.sellerLogo != null)
                              Expanded(
                                  child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: NetworkImageView(
                                        _detailsModel.orderData.sellerLogo ?? "",
                                        40,
                                        40,
                                        fit: BoxFit.contain,
                                      ))),
                            Expanded(
                                child: SvgPicture.asset(
                              fssai,
                              fit: BoxFit.contain,
                              width: 60,
                            )),
                            if (_detailsModel?.orderData?.buyerLogo != null)
                              Expanded(
                                  child: Align(
                                      alignment: Alignment.centerRight,
                                      child: NetworkImageView(
                                          _detailsModel.orderData.buyerLogo ?? "", 40, 40,
                                          fit: BoxFit.contain))),
                          ],
                        ),
                        _spacer(),
                        Style.getPoppinsLightText(
                            'TID:${_detailsModel?.orderData?.tid?.id}',
                            14,
                            ColorTheme.FF999999),
                        _spacer(),
                        Divider(
                          color: ColorTheme.FF999999,
                        ),
                      ],
                    ),
                  ),
                ),
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      Container(
                        color: ColorTheme.white,
                        child: ExpansionTile(
                          title: Style.getPoppinsLightText(
                              'Tracking Details', 16, ColorTheme.FF999999),
                          initiallyExpanded: true,
                          children: [
                            Container(
                              padding:EdgeInsets.only(top: 10, left: 10, right:10),
                              decoration: BoxDecoration(
                                  color: Colors.grey.shade100,
                                  border: Border.all(color: ColorTheme.FF333333, width: .4)
                              ),
                              child: ListView(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                children: _tracking
                                    .map((e) =>
                                        CellTracking(e, e == _tracking.last, color: Colors.grey.shade100,))
                                    .toList(),
                              ),
                            )
                          ],
                        ),
                      ),
                      if (_detailsModel != null)
                        Container(
                          color: ColorTheme.white,
                          child: ExpansionTile(
                            title: Style.getPoppinsLightText(
                                'Purchase Order', 16, ColorTheme.FF999999),
                            initiallyExpanded: pendingStatus == "PO",
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.grey.shade100,
                                  border: Border.all(color: ColorTheme.FF333333, width: .4)
                                ),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    _getHeader(),
                                    Container(
                                      child: ListView(
                                        shrinkWrap: true,
                                        physics: NeverScrollableScrollPhysics(),
                                        children: _detailsModel
                                            .orderData.orderitems
                                            .map<Widget>(
                                                (e) => CellItemInList(e, color: Colors.grey.shade100,))
                                            .toList(),
                                      ),
                                    ),
                                    _getChargesView('Item Total(Base Price)',
                                        _detailsModel.orderData.totalBase),
                                    _getChargesView('GST ',
                                        _detailsModel.orderData.totalTaxes),
                                    _getChargesView(
                                        'Delivery Charges ',
                                        _detailsModel
                                            .orderData.convenienceCharges),
                                    _getChargesView('Discount',
                                        _detailsModel.orderData.discountAmount),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    _getTotalToPay(
                                        _detailsModel.orderData.finalTotal),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    _getDevileryOn(
                                        _detailsModel.orderData.deliveryDate),
                                    _getAddress(
                                        '${_detailsModel.orderData.address.toString()}'),
                                    // if (AppUtils.isPendingAt(_detailsModel.orderData.tid.status, "PO"))
                                      OrderOperationView(_detailsModel.orderData, "PO", ()=>{_updateView()}),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      if (_deliveryChallan != null)
                        Container(
                          color: ColorTheme.white,
                          child: ExpansionTile(
                            title: Style.getPoppinsLightText(
                                'Delivery Challan', 16, ColorTheme.FF999999),
                            initiallyExpanded: pendingStatus == "DC",
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    color: Colors.grey.shade100,
                                    border: Border.all(color: ColorTheme.FF333333, width: .4)
                                ),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    _getHeader(),
                                    Container(
                                      child: ListView(
                                        shrinkWrap: true,
                                        physics: NeverScrollableScrollPhysics(),
                                        children: _deliveryChallan
                                            .orderData.deliverychallanitems
                                            .map<Widget>(
                                                (e) => CellItemInList(e, color: Colors.grey.shade100,))
                                            .toList(),
                                      ),
                                    ),
                                    _getChargesView('Item Total(Base Price)',
                                        _deliveryChallan.orderData.totalBase),
                                    _getChargesView('GST ',
                                        _deliveryChallan.orderData.totalTaxes),
                                    _getChargesView(
                                        'Delivery Charges ',
                                        _deliveryChallan
                                            .orderData.convenienceCharges),
                                    _getChargesView(
                                        'Discount',
                                        _deliveryChallan
                                            .orderData.discountAmount),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    _getTotalToPay(
                                        _deliveryChallan.orderData.finalTotal),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    _getDevileryOn(_deliveryChallan
                                        .orderData.deliveryDate),
                                    _getAddress(
                                        '${_deliveryChallan.orderData.address.toString()}'),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    // _getWeightView(
                                    //     '${_deliveryChallan.orderData.grossWeight}'),
                                    // const SizedBox(
                                    //   height: 10,
                                    // ),
                                    // _getImageView(_deliveryChallan
                                    //     .orderData.itemindocuments),
                                    // const SizedBox(
                                    //   height: 10,
                                    // ),
                                    // if (AppUtils.isPendingAt(_detailsModel.orderData.tid.status, "DC"))
                                      OrderOperationView(_detailsModel.orderData, "DC", ()=>{_updateView()}, dcGrossWeight: _deliveryChallan.orderData.grossWeight,),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      if (_detailsModel != null && AppUtils.isCompletedAt(_detailsModel.orderData.tid.status, "ItemOut"))
                      Container(
                        color: ColorTheme.white,
                        child: ExpansionTile(
                          title: Style.getPoppinsLightText(
                              'ItemOut', 16, ColorTheme.FF999999),
                          initiallyExpanded: pendingStatus == "ItemOut",
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.grey.shade100,
                                  border: Border.all(color: ColorTheme.FF333333, width: .4)
                              ),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  _getHeader(),
                                  Container(
                                    child: ListView(
                                      shrinkWrap: true,
                                      physics: NeverScrollableScrollPhysics(),
                                      children: _detailsModel
                                          .orderData.orderitems
                                          .map<Widget>((e) => CellItemInList(e, color: Colors.grey.shade100,))
                                          .toList(),
                                    ),
                                  ),
                                  _getChargesView('Item Total(Base Price)',
                                      _detailsModel.orderData.totalBase),
                                  _getChargesView('GST ',
                                      _detailsModel.orderData.totalTaxes),
                                  _getChargesView(
                                      'Delivery Charges ',
                                      _detailsModel
                                          .orderData.convenienceCharges),
                                  _getChargesView('Discount',
                                      _detailsModel.orderData.discountAmount),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  _getTotalToPay(
                                      _detailsModel.orderData.finalTotal),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  _getDevileryOn(
                                      _detailsModel.orderData.deliveryDate),
                                  _getAddress(
                                      '${_detailsModel.orderData.address.toString()}'),
                                  // if (AppUtils.isPendingAt(_detailsModel.orderData.tid.status, "ItemOut"))
                                    OrderOperationView(_detailsModel.orderData, "ItemOut", ()=>{_updateView()}, dcGrossWeight: _deliveryChallan?.orderData?.grossWeight,),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      if (_itemInDetailsModel != null)
                        Container(
                          color: ColorTheme.white,
                          child: ExpansionTile(
                            title: Style.getPoppinsLightText(
                                'ItemIn', 16, ColorTheme.FF999999),
                            initiallyExpanded: pendingStatus == "ItemIn",
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    color: Colors.grey.shade100,
                                    border: Border.all(color: ColorTheme.FF333333, width: .4)
                                ),
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 0, vertical: 8),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    _getHeader(),
                                    Container(
                                      child: ListView(
                                        shrinkWrap: true,
                                        physics: NeverScrollableScrollPhysics(),
                                        children: _itemInDetailsModel
                                            .itemInData.iteminitems
                                            .map<Widget>(
                                                (e) => CellItemInList(e, color: Colors.grey.shade100,))
                                            .toList(),
                                      ),
                                    ),
                                    _getChargesView(
                                        'Item Total(Base Price)',
                                        _itemInDetailsModel
                                            .itemInData.totalBase),
                                    _getChargesView(
                                        'GST ',
                                        _itemInDetailsModel
                                            .itemInData.totalTaxes),
                                    _getChargesView(
                                        'Delivery Charges ',
                                        _itemInDetailsModel
                                            .itemInData.convenienceCharges),
                                    _getChargesView(
                                        'Discount',
                                        _itemInDetailsModel
                                            .itemInData.discountAmount),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    _getTotalToPay(_itemInDetailsModel
                                        .itemInData.finalTotal),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    _getDevileryOn(_itemInDetailsModel
                                        .itemInData.deliveryDate),
                                    _getAddress(''),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    // _getWeightView(
                                    //     '${_itemInDetailsModel.itemInData.grossWeight}'),
                                    // const SizedBox(
                                    //   height: 10,
                                    // ),
                                    // _getImageView(_itemInDetailsModel
                                    //     .itemInData.itemindocuments),
                                    // const SizedBox(
                                    //   height: 10,
                                    // ),
                                    // if (AppUtils.isPendingAt(_detailsModel.orderData.tid.status, "ItemIn"))
                                      OrderOperationView(_detailsModel.orderData, "ItemIn", ()=>{_updateView()}, itemInId: _itemInDetailsModel.itemInData.id, preFillGrossWeight: null, grwtDone: _itemInDetailsModel.itemInData.isAssignedGrossWeight, photoDone: _itemInDetailsModel.itemInData.itemindocuments.length > 0),
                                    SizedBox(height: 10,),
                                    Container(alignment: Alignment.center, margin: EdgeInsets.only(bottom: 10),
                                      child: Style.getTextButton(200, 40, 'Send ItemIn', onClick: () async {
                                        _itemIn();
                                      }),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      if (_grnDetailsModel != null && _detailsModel != null)
                        Container(
                          color: ColorTheme.white,
                          child: ExpansionTile(
                            title: Style.getPoppinsLightText(
                                'GRN', 16, ColorTheme.FF999999),
                            initiallyExpanded: pendingStatus == "GRN",
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    color: Colors.grey.shade100,
                                    border: Border.all(color: ColorTheme.FF333333, width: .4)
                                ),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    _getHeader(),
                                    Container(
                                      child: ListView(
                                        shrinkWrap: true,
                                        physics: NeverScrollableScrollPhysics(),
                                        children: _grnDetailsModel
                                            .orderData.grnItems
                                            .map<Widget>(
                                                (e) => CellItemInList(e, color: Colors.grey.shade100,))
                                            .toList(),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 6),
                                      child: Row(
                                        children: [
                                          Expanded(child: Container()),
                                          Style.getPoppinsRegularText(
                                              'GST', 14, ColorTheme.FF999999),
                                          Expanded(child: Container()),
                                          Style.rupeeTextView(
                                              '${_grnDetailsModel.orderData.totalTaxes}',
                                              14,
                                              ColorTheme.FF999999),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 6),
                                      child: Row(
                                        children: [
                                          Expanded(child: Container()),
                                          Style.getPoppinsRegularText(
                                              'Total to Pay',
                                              20,
                                              ColorTheme.FF333333),
                                          Expanded(child: Container()),
                                          Style.rupeeTextView(
                                              '${_grnDetailsModel.orderData.totalBase}',
                                              20,
                                              ColorTheme.FF333333),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 24,
                                    ),
                                    Stack(

                                      children: [
                                        Center(
                                          child: Container(
                                            width: double.infinity,

                                            margin: const EdgeInsets.fromLTRB(
                                                14, 14, 30, 14),
                                            padding: const EdgeInsets.all(14),
                                            decoration: BoxDecoration(
                                              color: Colors.grey.shade100,
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              border: Border.all(
                                                  color: ColorTheme.FFE3E3E3),
                                              // color: ColorTheme.FFFDFDFD,
                                            ),
                                            child: Row(
                                              children: [
                                                Expanded(
                                                    child: Style
                                                        .getPoppinsRegularText(
                                                            'Details:',
                                                            14,
                                                            ColorTheme.black)),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Positioned(
                                          right: 14,
                                          top: -10,
                                          child: Image.asset(
                                            AppAssets.itemin,
                                            width: 100,
                                            height: 100,
                                          ),
                                        ),
                                      ],
                                    ),
                                    if (AppUtils.isValid(_grnDetailsModel.orderData.comment))
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        SizedBox(width: 15,),
                                        Style.getPoppinsRegularText('Comment:', 14, ColorTheme.black),
                                        SizedBox(width: 15,),
                                        Expanded(child: Style.getPoppinsLightText(_grnDetailsModel.orderData.comment, 14, ColorTheme.FF666666, maxLines: 3)),
                                        SizedBox(width: 15,)
                                    ],),
                                    SizedBox(height: 10),
                                    // if (AppUtils.isPendingAt(_detailsModel.orderData.tid.status, "GRN"))
                                      OrderOperationView(_detailsModel.orderData, "GRN", ()=>{_updateView()}, grnId: _grnDetailsModel.orderData.id,  preFillGrossWeight: _grnDetailsModel.orderData.grossWeight, dcGrossWeight: _grnDetailsModel.orderData.dcGrossWeight, itemInGrossWeight: _grnDetailsModel.orderData.itemInGrossWeight, preFillComment: _grnDetailsModel.orderData.comment, grwtDone: _grnDetailsModel.orderData.isAssignedGrossWeight, photoDone: _grnDetailsModel.orderData.grndocuments.length > 0, commentDone: AppUtils.isValid(_grnDetailsModel.orderData.comment)),
                                    if (AppUtils.isPendingAt(_detailsModel.orderData.tid.status, "GRN"))
                                    SizedBox(height: 10,),
                                    if (AppUtils.isPendingAt(_detailsModel.orderData.tid.status, "GRN"))
                                    Container(alignment: Alignment.center, margin: EdgeInsets.only(bottom: 10), child: Style.getTextButton(200, 40, 'Send GRN', onClick: ()=>_orderGRN())),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      if (_invoice != null && _detailsModel != null)
                        Container(
                          color: ColorTheme.white,
                          child: ExpansionTile(
                            title: Style.getPoppinsLightText(
                                'Invoice', 16, ColorTheme.FF999999),
                            initiallyExpanded: pendingStatus == "Invoice",
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    color: Colors.grey.shade100,
                                    border: Border.all(color: ColorTheme.FF333333, width: .4)
                                ),
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 8),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    _getHeader(),
                                    Container(
                                      child: ListView(
                                        shrinkWrap: true,
                                        physics: NeverScrollableScrollPhysics(),
                                        children: _invoice
                                            .orderData.invoiceitems
                                            .map<Widget>(
                                                (e) => CellItemInList(e, color: Colors.grey.shade100,))
                                            .toList(),
                                      ),
                                    ),
                                    _getChargesView('Item Total(Base Price)',
                                        _invoice.orderData.totalBase),
                                    _getChargesView(
                                        'GST ', _invoice.orderData.totalTaxes),
                                    _getChargesView('Delivery Charges ',
                                        _invoice.orderData.convenienceCharges),
                                    _getChargesView('Discount',
                                        _invoice.orderData.discountAmount),
                                    _spacer(),
                                    _getTotalToPay(
                                        _invoice.orderData.finalTotal),
                                    _spacer(),
                                    _getAddress(
                                        '${_invoice.orderData.invoiceAddress.toString()}',
                                        invoice: true),
                                    _spacer(),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10),
                                      child: Style.getPoppinsRegularText(
                                          'Invoice No.${_invoice.orderData.invoiceNumber ?? ""}',
                                          16,
                                          ColorTheme.FF333333),
                                    ),
                                    // _spacer(),
                                    // _getWeightView(
                                    //     '${_invoice.orderData.grossWeight ?? ''}'),
                                    // _spacer(),
                                    // _getImageView(
                                    //     _invoice.orderData.invoicedocuments),
                                    _spacer(),
                                    OrderOperationView(_detailsModel.orderData, "Invoice", ()=>{_updateView()}, dcGrossWeight: _grnDetailsModel?.orderData?.dcGrossWeight, itemInGrossWeight: _grnDetailsModel?.orderData?.itemInGrossWeight, preFillGrossWeight: _grnDetailsModel?.orderData?.grossWeight),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      if (_invoice != null && _detailsModel != null && AppUtils.isCompletedAt(_detailsModel.orderData.tid.status, "Invoice"))
                        AbsorbPointer(
                          absorbing: widget.grn,
                          child: Container(
                            color: ColorTheme.white,
                            child: ExpansionTile(
                              title: Style.getPoppinsLightText(
                                  'Payment', 16, ColorTheme.FF999999),
                              initiallyExpanded: pendingStatus == "Payment",
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                      color: Colors.grey.shade100,
                                      border: Border.all(color: ColorTheme.FF333333, width: .4)
                                  ),
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 8),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      SizedBox(
                                        height: 80,
                                        width: double.infinity,
                                        child: Center(
                                          child: NetworkImageView(
                                              _detailsModel.orderData.buyerLogo ?? "", 50, 50,
                                              fit: BoxFit.contain),
                                        ),
                                      ),
                                      Container(
                                        margin: const EdgeInsets.symmetric(
                                            horizontal: 30, vertical: 20),
                                        padding: const EdgeInsets.all(10),
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                            border: Border(
                                          top: BorderSide(
                                              color: ColorTheme.FF333333),
                                          bottom: BorderSide(
                                              color: ColorTheme.FF333333),
                                        )),
                                        child: Style.getPoppinsLightText(
                                            'Thank you for your Business',
                                            16,
                                            ColorTheme.FF999999),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 10),
                                        child: Style.getPoppinsRegularText(
                                            'Invoice Date: ${DateTimeUtil.formatDateTime(_invoice.orderData.createdAt)}',
                                            16,
                                            ColorTheme.FF666666),
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 10),
                                        child: Style.getPoppinsRegularText(
                                            'Invoice No: ${_invoice.orderData.invoiceNumber ?? ""}',
                                            16,
                                            ColorTheme.FF666666),
                                      ),
                                      const SizedBox(
                                        height: 20,
                                      ),
                                      _getHeader(),
                                      Container(
                                        child: ListView(
                                          shrinkWrap: true,
                                          physics:
                                              NeverScrollableScrollPhysics(),
                                          children: _invoice
                                              .orderData.invoiceitems
                                              .map<Widget>(
                                                  (e) => CellItemInList(e, color: Colors.grey.shade100,))
                                              .toList(),
                                        ),
                                      ),
                                      _getChargesView('Item Total(Base Price)',
                                          _invoice.orderData.totalBase),
                                      _getChargesView('GST ',
                                          _invoice.orderData.totalTaxes),
                                      _getChargesView(
                                          'Delivery Charges ',
                                          _invoice
                                              .orderData.convenienceCharges),
                                      _getChargesView('Discount',
                                          _invoice.orderData.discountAmount),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      _getGSTView(
                                          _invoice.orderData.totalTaxes),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      _getTotalToPay(
                                          _invoice.orderData.finalTotal),
                                      Stack(
                                        fit: StackFit.loose,
                                        children: [
                                          Container(
                                              margin:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 24,
                                                      vertical: 20),
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                                border: Border.all(
                                                    color: ColorTheme.FFE3E3E3),
                                              ),
                                              child: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  _getAddress(
                                                      '${_invoice.orderData.invoiceAddress.toString()}',
                                                      invoice: true,
                                                      image: false),
                                                  Container(
                                                    width: double.infinity,
                                                    padding: const EdgeInsets.all(10),
                                                    margin: const EdgeInsets.all(10),
                                                    decoration: BoxDecoration(
                                                        color: ColorTheme.transparent,
                                                        border: Border(top: BorderSide(color: ColorTheme.FFE3E3E3))),
                                                    child: Style.getPoppinsLightText('Due Date:  ${_invoice.orderData.dueDate ?? ""}', 16, ColorTheme.FF999999),
                                                  )
                                                ],
                                              )),
                                          Positioned(
                                            child: Container(
                                              height: 100,
                                              width: 100,
                                              child: Image.asset(
                                                _detailsModel.orderData.tid
                                                            .status
                                                            .firstWhere(
                                                                (element) =>
                                                                    element
                                                                        .key ==
                                                                    'Payment')
                                                            .value ==
                                                        'Completed'
                                                    ? AppAssets.accept
                                                    : AppAssets.due,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                            top: -1,
                                            right: 0,
                                          )
                                        ],
                                      ),
                                      // if (AppUtils.isPendingAt(_detailsModel.orderData.tid.status, "Payment"))
                                        OrderOperationView(_detailsModel.orderData, "Payment", ()=>{_updateView()}, dcGrossWeight: _grnDetailsModel?.orderData?.dcGrossWeight, itemInGrossWeight: _grnDetailsModel?.orderData?.itemInGrossWeight, preFillGrossWeight: _grnDetailsModel?.orderData?.grossWeight),
                                      SizedBox(height: 10,),
                                      if (_detailsModel.orderData.tid.status
                                              .firstWhere((element) =>
                                                  element.key == 'Payment')
                                              .value ==
                                          'Pending')
                                        Center(
                                          child: Style.getTextButton(
                                              200, 40, 'Pay', onClick: () {
                                            _pay();
                                          }),
                                        ),
                                      const SizedBox(
                                        height: 20,
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                    ],
                  ),
                ),
              ],
      ),
    );
  }

  Widget _spacer() {
    return Container(
      height: 10,
    );
  }

  Widget _getHeader() {
    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
          color: ColorTheme.FFF4F4F4,
        ))),
        child: Row(
          children: [
            Style.getPoppinsRegularText(
                'Items'.toUpperCase(), 14, ColorTheme.FF333333),
            Expanded(child: Container()),
            Style.getPoppinsRegularText(
                'Total'.toUpperCase(), 14, ColorTheme.FF333333),
          ],
        ));
  }

  Widget _getChargesView(String lable, dynamic data, {bool rupee = true}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 4),
      child: Row(
        children: [
          Expanded(
              child: Style.getPoppinsLightText(lable, 12, ColorTheme.FF999999)),
          if (rupee)
            Container(
                child: Style.rupeeTextView(
                    data.toString(), 12, ColorTheme.FF999999)),
          if (!rupee)
            Container(
                child: Style.getPoppinsLightText(
                    data.toString(), 12, ColorTheme.FF999999)),
        ],
      ),
    );
  }

  Widget _getTotalToPay(dynamic data) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 4),
      child: Row(
        children: [
          Expanded(child: Container()),
          Container(
              child: Style.getPoppinsLightText(
                  'Total To Pay:', 18, ColorTheme.FF333333)),
          Expanded(child: Container()),
          Container(
              child:
                  Style.rupeeTextView(data.toString(), 16, ColorTheme.FF333333))
        ],
      ),
    );
  }

  Widget _getGSTView(dynamic data) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 4),
      child: Row(
        children: [
          Expanded(child: Container()),
          Container(
              child:
                  Style.getPoppinsLightText('GST:', 16, ColorTheme.FF999999)),
          Expanded(child: Container()),
          Container(
              child:
                  Style.rupeeTextView(data.toString(), 16, ColorTheme.FF999999))
        ],
      ),
    );
  }

  Widget _getDevileryOn(DateTime time) {
    return Container(
        padding: const EdgeInsets.all(10),
        child: Style.getPoppinsLightText(
            'Delivery on ${DateTimeUtil.getDateFromDateTime(time, DateTimeUtil.DD_MMM)}',
            16,
            ColorTheme.FF333333));
  }

  Widget _getAddress(String address,
      {bool invoice = false, bool image = true}) {
    return Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  child: Text.rich(
                    TextSpan(children: [
                      TextSpan(
                          text: invoice
                              ? 'Invoice To \n\n'
                              : 'Delivery Address\n',
                          style: TextStyle(
                              color: ColorTheme.FF333333, fontSize: 16)),
                      TextSpan(
                          text: '$address',
                          style: TextStyle(
                              color: ColorTheme.FF999999, fontSize: 14))
                    ]),
                    textScaleFactor: 1.0,
                    maxLines: 6,
                  ),
                ),
                if (image)
                  Container(
                    height: 100,
                    width: 100,
                    child: Image.asset(AppAssets.accept),
                  )
              ],
            )
          ],
        ));
  }

  Widget _getWeightView(String data, {bool editable = false}) {
    if (editable) {
      _grnGrossWeightController.text = AppUtils.getDisplayValue(data ?? '');
    }
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.all(14),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: ColorTheme.FFE3E3E3),
        color: ColorTheme.FFFDFDFD,
      ),
      child: Row(
        children: [
          Expanded(
              child: editable ? Style.getTextInputField("Gross weight", height: 30, showBorder: false, controller:_grnGrossWeightController, inputType: TextInputType.numberWithOptions(decimal: true))
                  : Style.getPoppinsRegularText(AppUtils.getDisplayValue(data ?? ''), 14, ColorTheme.black)),
          Style.getPoppinsRegularText('Kg', 14, ColorTheme.black)
        ],
      ),
    );
  }

  Widget _getImageView(List<UploadPhotos> photos) {
    return Container(
        height: 60,
        child: ListView(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          children: photos.map((e) {
            return GestureDetector(
              onTap: () {
                showModalBottomSheet(
                    isScrollControlled: true,
                    context: context,
                    backgroundColor: Colors.transparent,
                    builder: (_) => Container(
                        height: MediaQuery.of(context).size.height * 0.95,
                        child: ImageDialog(false, e.docUrl)));
              },
              child: Container(
                width: 60,
                height: 60,
                margin: const EdgeInsets.symmetric(horizontal: 10),
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: NetworkImageView(e.docUrl, 60, 60)),
              ),
            );
          }).toList(),
        ));
  }

  _navigate() async {
    final result = await Navigator.of(context).push<bool>(
        MaterialPageRoute(builder: (_) => EditOrderScreen(widget.tId, 2)));

    if (result != null && result) {
      loader = true;
      setState(() {});
      _getData();
    }
  }

  _getData({bool showLoader = false}) async {
    if (showLoader) {
      Loader.show(context);
    } else {
      setState(() {
        loader = true;
      });
    }
    final response = await orderRepository.getOrderDetails(widget.tId);
    final grnDetails = await orderRepository.getGRNDetails(widget.tId);
    final itemIn =
        await orderRepository.getItemInDetails(widget.tId.toString());
    final deliveryChallan =
        await orderRepository.getDeliveryChallanDetails(widget.tId.toString());
    final invoices =
        await orderRepository.getInvoicesDetails(widget.tId.toString());

    loader = false;
    _detailsModel = response;
    _grnDetailsModel = grnDetails;
    _itemInDetailsModel = itemIn;
    _deliveryChallan = deliveryChallan;
    _invoice = invoices;
    if (_detailsModel != null) {
      _tracking.clear();
      pendingStatus = AppUtils.getPendingAtStatus(_detailsModel.orderData.tid.status);

      _detailsModel.orderData.tid.status.forEach((trac) {
        List<unique.Businesstid> list = [];
        String status = '';
        _detailsModel.orderData.tid.businesstids.forEach((element) {
          if (trac.key == element.eventName) {
            list.add(element);
            status = element.action;
          }
        });
        _tracking.add(Tracking(trac.key, status, list));
      });
    }
    setState(() {});
    if (showLoader) {
      Loader.hide();
    }
    if (allowAutoScroll) {
      allowAutoScroll = false;
      // if (widget.grn) {
      //   _scrollController.animateTo(150.0 * 7,
      //       duration: Duration(milliseconds: 300), curve: Curves.fastOutSlowIn);
      // } else {
      //   _scrollController.animateTo(150.0 * 8,
      //       duration: Duration(milliseconds: 300), curve: Curves.fastOutSlowIn);
      // }
      _scroll();
    }
  }

  _scroll(){
    _scrollController.animateTo(300.0 * currentTab.indexOf(pendingStatus),
        duration: Duration(milliseconds: 300),
        curve: Curves.fastOutSlowIn);
  }

  _orderGRN() async {
    Loader.show(context);
    // if (_uploadPhotos.length > 0) {
    //   Map body = {
    //     "tenantId": ApiPath.TENANT_ID,
    //     "id": "${_grnDetailsModel.orderData.id}",
    //     "photos": List<dynamic>.from(_uploadPhotos.map((e) => e.toJson())),
    //   };
    //   final request = await orderRepository.uploadAllGRNPicUrl(body);
    // }
    // final grossWeightUpdate = await orderRepository.updateGRNGrossWeight(_grnDetailsModel.orderData.id, _grnGrossWeightController.text.trim());
    // final commentUpdate = await orderRepository.updateGRNComment(_grnDetailsModel.orderData.id, _grnCommentController.text.trim());
    // if (grossWeightUpdate) {
    //   Loader.hide();
    try {
      Loader.show(context);
      final result = await orderRepository.orderGrn(_detailsModel.orderData.id);
      Loader.hide();
      if (result) Navigator.of(context).pop(result);
    } catch (e) {
      AppUtils.showToast(TextMsgs.COMMON_ERROR);
    }
    // } else {
    //   AppUtils.showToast("Failed to process your request");
    //   Loader.hide();
    // }
  }

  _pay() async {
    RazorPayUtil.instance.open(
        double.parse(_invoice.orderData.finalTotal.toString()),
        'FoodNet',
        'Order Payment', success: (paymentId) {
      _payment(paymentId);
    }, failure: (error) {
      AppUtils.showToast(error);
    });
  }

  _payment(String paymentId) async {
    Map<String, dynamic> body = {
      "tenantId": ApiPath.TENANT_ID,
      "ids": [_detailsModel.orderData.id],
      "partyId": PreferenceManager.getPartyId(),
      "transaction_id": "$paymentId",
      "payment_status": "done",
      "invoice_amount": _invoice.orderData.finalTotal,
      "paid_amount": _invoice.orderData.finalTotal,
    };
    Loader.show(context);
    final result = await orderRepository.orderPayment(body);
    Loader.hide();
    if (result) Navigator.of(context).pop(result);
  }

  File _image;
  final picker = ImagePicker();
  List<ImageData> _list = [];

  List<UploadPhotos> _uploadPhotos = [];

  _requestPermission() async {
    final result = await PermissionUtil.instance
        .requestSinglePermission(Permission.storage);
    if (result) {
      getImage();
    }
  }

  getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        _list.add(ImageData(2, pickedFile.path));
        _addSinglePic(pickedFile.path);
      }
    });
  }

  _addSinglePic(String path) async {
    Loader.show(context);
    final uploadedData = await orderRepository.uploadGRNImage(
        _image.path, _itemInDetailsModel.itemInData.id.toString());
    if (uploadedData != null) {
      var data = jsonDecode(uploadedData);
      _uploadPhotos.add(UploadPhotos(
          docId: data['data']['id'].toString(),
          docUrl: data['data']['url'].toString(),
          path: path,
          docUsage: 'grn'));
    }
    Loader.hide();
  }

  _itemIn()async{
    try {
      Loader.show(context);
      final result = await orderRepository.orderItemIn(
          _detailsModel.orderData.id);
      Loader.hide();
      Navigator.of(context).pop(result);
    } catch (e) {
      Loader.hide();
      AppUtils.showToast(TextMsgs.COMMON_ERROR);
    }
  }

  _updateView() {
    _getData(showLoader: true);
  }
}

class Tracking {
  String action;
  String status;
  List<unique.Businesstid> list = [];

  Tracking(this.action, this.status, this.list);
}
