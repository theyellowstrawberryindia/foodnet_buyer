import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/constant/text_msgs.dart';
import 'package:foodnet_buyer/data/bloc/productbloc/product_bloc.dart';
import 'package:foodnet_buyer/data/bloc/productbloc/product_event.dart';
import 'package:foodnet_buyer/data/bloc/productbloc/product_state.dart';
import 'package:foodnet_buyer/data/model/product.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/widgets/animated_flip_counter.dart';
import 'package:foodnet_buyer/ui/widgets/carousel_with_indicator.dart';
import 'package:foodnet_buyer/ui/widgets/dialog/edit_quantity_dialog.dart';
import 'package:foodnet_buyer/ui/widgets/netwrok_image_view.dart';
import 'package:foodnet_buyer/util/add_to_cart_animation.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';
import 'package:foodnet_buyer/util/shopping_cart_util.dart';

class ProductDetailScreen extends StatefulWidget {
  final Product product;
  final String productCatalogId;
  final Key key;

  ProductDetailScreen({this.product, this.productCatalogId, this.key});

  @override
  _ProductDetailScreenState createState() => _ProductDetailScreenState(product);
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  int currentIndex = 0;
  int selectedVariantIndex = 0;
  double _qty = 1;

  double basePrice;
  double mrpPrice;
  double rate;
  String unitName;
  String saleUnit;
  String packOffValueString;
  double packOffValue;
  SizeVariant selectedVariant;
  bool initialised = false;
  bool minQtySet = false;

  List<String> images = [];
  Product product;
  ProductBloc _bloc;
  GlobalKey rootKey = GlobalKey();
  GlobalKey startWidgetKey = GlobalKey();
  GlobalKey endWidgetKey = GlobalKey();

  _ProductDetailScreenState(this.product);

  @override
  void initState() {
    super.initState();
    _bloc = ProductBloc();

    AppUtils.delayTask(() {
      _bloc.add(GetProductDetailEvent(
          productId: (product.productId ?? product.id).toString(),
          productCatalogId: widget.productCatalogId));
      setState(() {
        initialised = true;
      });
    }, delay: Duration(milliseconds: 1100));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorTheme.FFFDFDFD,
        appBar: AppBar(
            centerTitle: true,
            elevation: 0.0,
            backgroundColor: ColorTheme.white,
            titleSpacing: 0.0,
            iconTheme: IconThemeData(color: ColorTheme.FF333333),
            title: Style.getPoppinsLightText(
                'Product Details', AppFonts.titleTextSize, ColorTheme.FF333333),
            actions: [
              Padding(
                padding:
                    const EdgeInsets.only(right: 20.0, top: 10, bottom: 10),
                child: AppUtils.getCartWidget(context, key: endWidgetKey),
              )
            ]),
        body: !initialised
            ? /*Container(
                child: CircularProgressIndicator(),
                alignment: Alignment.center,
              )*/
            buildLoading()
            : BlocBuilder(
                key: rootKey,
                bloc: _bloc,
                builder: (ctx, state) {
                  if (state is ProductDetailReceivedState) {
                    product = state.product;
                    if (!minQtySet) {
                      _qty = product.minQty;
                      minQtySet = true;
                    }
                    _processData();
                  }
                  return state is ProductDetailReceivedState
                      ? Container(
                          color: ColorTheme.white,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: CustomScrollView(
                                  slivers: [
                                    SliverList(
                                        delegate: SliverChildListDelegate([
                                      SizedBox(
                                        height: 10,
                                      ),
                                      // Hero(
                                      //   tag: "Hero${product.id}",
                                      Hero(
                                        tag: widget.key.toString(),
                                        child: CarouselWithIndicator(
                                          images,
                                          (image) => image,
                                          useHeroTag: false,
                                          key: UniqueKey(),
                                          infinite: false,
                                          autoScroll: false,
                                          height: 200,
                                          fit: BoxFit.scaleDown,
                                          //    useHeroTag: false,
                                          onClick: (value) {
                                            // NavigationUtil.pushToNewScreen(context, GalleryPhotoViewWrapper(
                                            //   galleryItems: images,
                                            //   backgroundDecoration: const BoxDecoration(
                                            //     color: ColorTheme.FF333333,
                                            //   ),
                                            //   initialIndex: images.indexOf(value),
                                            //   scrollDirection: Axis.horizontal,
                                            // ));
                                          },
                                        ),
                                      ),
                                      //),
                                      Container(
                                        color: ColorTheme.FFFDFDFD,
                                        height: 10,
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                            color: ColorTheme.white,
                                            borderRadius: BorderRadius.only(
                                                topRight: Radius.circular(20),
                                                topLeft: Radius.circular(20)),
                                            boxShadow: [
                                              BoxShadow(
                                                  color: ColorTheme.FFE3E3E3
                                                      .withOpacity(.3),
                                                  blurRadius: 1.0,
                                                  offset: Offset(0, -1),
                                                  spreadRadius: 0),
                                            ]),
                                        height: 20,
                                        width: double.infinity,
                                      ),
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        padding: EdgeInsets.only(left: 20),
                                        child: NetworkImageView(
                                          PreferenceManager.getLocationData()
                                                  ?.sellerurl ??
                                              "",
                                          40,
                                          15,
                                          fit: BoxFit.fitWidth,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            left: 20,
                                            right: 20,
                                            bottom: 16,
                                            top: 8),
                                        child: AppUtils.getProductName(
                                            product.productName,
                                            product.subText,
                                            AppFonts.textSize14,
                                            ColorTheme.FF333333,
                                            maxLines: 3),
                                      ),
                                      Material(
                                        type: MaterialType.transparency,
                                        child: Ink(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              if (product.hasVariants())
                                                AbsorbPointer(
                                                  absorbing:
                                                      !product.isAvailable(),
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        left: 20, right: 20),
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              16),
                                                      border: Border.all(
                                                          width: 0.5,
                                                          color: ColorTheme
                                                              .FF333333),
                                                    ),
                                                    child: DropdownButton(
                                                      onTap:
                                                          !product.isAvailable()
                                                              ? () {}
                                                              : null,
                                                      underline: Container(),
                                                      isDense: true,
                                                      icon: Icon(
                                                        Icons
                                                            .keyboard_arrow_down,
                                                        size: 20,
                                                        color:
                                                            ColorTheme.FF333333,
                                                      ),
                                                      value:
                                                          selectedVariantIndex,
                                                      onChanged: (int) {
                                                        selectedVariantIndex =
                                                            int;
                                                        _updateProductValues();
                                                        setState(() {});
                                                      },
                                                      items: product
                                                          .sizevariants
                                                          .map((e) =>
                                                              DropdownMenuItem(
                                                                value: product
                                                                    .sizevariants
                                                                    .indexOf(e),
                                                                child: Padding(
                                                                  padding: const EdgeInsets
                                                                          .symmetric(
                                                                      horizontal:
                                                                          20),
                                                                  child: Style.getPoppinsLightText(
                                                                      "${e?.saleQty} ${e?.baseUom}",
                                                                      AppFonts
                                                                          .textSize10,
                                                                      ColorTheme
                                                                          .FF333333),
                                                                ),
                                                              ))
                                                          .toList(),
                                                    ),
                                                  ),
                                                ),
                                              SizedBox(),
                                              Container(
                                                  margin: EdgeInsets.only(
                                                      left: 20, right: 20),
                                                  child: Row(
                                                    mainAxisSize:
                                                        MainAxisSize.min,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      InkWell(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(15),
                                                        onTap: () {
                                                          if (!product
                                                              .isAvailable()) {
                                                            AppUtils.showToast(
                                                                TextMsgs
                                                                    .OUT_OF_STOCK);
                                                            return;
                                                          }
                                                          setState(() {
                                                            if (_qty > product.minQty) {
                                                              _qty -= product.minQty;
                                                              if (_qty < product.minQty) {
                                                                _qty = product.minQty;
                                                              }
                                                            } else {
                                                              AppUtils.showToast("Item quantity cannot be less then ${AppUtils.getDisplayValue(product.minQty)}");
                                                            }
                                                          });
                                                        },
                                                        child: Image.asset(
                                                          AppAssets
                                                              .decrementCounter,
                                                          width: 30,
                                                          height: 30,
                                                        ),
                                                      ),
                                                      SizedBox(width: 10),
                                                      InkWell(
                                                        onTap: () {
                                                          if (product.isAvailable()) {
                                                            EditQuantityDialog.showEditQtyDialog(context, _qty, unitName?.toLowerCase() == "kg", (value) {
                                                              setState(() {
                                                                _qty = value;
                                                              });
                                                            }, minQty: product.minQty);
                                                          } else {
                                                            AppUtils.showToast(TextMsgs.OUT_OF_STOCK);
                                                          }
                                                        },
                                                        child: Column(
                                                          children: [
                                                            Container(
                                                              decoration: BoxDecoration(
                                                                border: Border.all(color: ColorTheme.FF333333, width: 0.5)
                                                              ),
                                                              alignment:
                                                                  Alignment.center,
                                                              padding:
                                                                  EdgeInsets.only(
                                                                      bottom: 8, left: 10, right: 10),
                                                              child:
                                                                  AnimatedFlipCounter(
                                                                duration: Duration(
                                                                    milliseconds:
                                                                        300),
                                                                value: _qty,
                                                                /* pass in a number like 2014 */
                                                                color: ColorTheme
                                                                    .FF333333,
                                                                size: 14,
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding: EdgeInsets.only(top: 4),
                                                              child: Icon(Icons.edit, color: ColorTheme.FF333333, size: 14,),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      SizedBox(width: 10),
                                                      InkWell(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(15),
                                                        onTap: () {
                                                          if (!product
                                                              .isAvailable()) {
                                                            AppUtils.showToast(
                                                                TextMsgs
                                                                    .OUT_OF_STOCK);
                                                            return;
                                                          }
                                                          _qty += product.minQty;
                                                          setState(() {
                                                          });
                                                        },
                                                        child: Image.asset(
                                                          AppAssets
                                                              .incrementCounter,
                                                          width: 30,
                                                          height: 30,
                                                        ),
                                                      ),
                                                    ],
                                                  )),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.centerRight,
                                        child: Container(
                                          margin: EdgeInsets.symmetric(
                                              horizontal: 20, vertical: 20),
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 20, vertical: 10),
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(20)),
                                              color: ColorTheme.FFF8F8F8),
                                          child: Style.getPoppinsLightText(
                                              "$packOffValueString $unitName x ${AppUtils.getDisplayValue(_qty)} Qty = ${AppUtils.getDisplayValue(packOffValue * _qty)} $unitName",
                                              10,
                                              ColorTheme.FF999999),
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 20, right: 20),
                                        child: Row(
                                          children: [
//                                        Style.getPoppinsLightText('${AppUtils.getRupeeSymbol()}$mrpPrice', AppFonts.textSize10, ColorTheme.FF333333, decoration: TextDecoration.lineThrough),
//                                        SizedBox(
//                                          width: 6,
//                                        ),
                                            AppUtils.getWidgetWithRupee(
                                                ' ${AppUtils.showDoubleString(mrpPrice * _qty)}',
                                                AppFonts.textSize10,
                                                ColorTheme.FF333333),
                                            Expanded(
                                              child: Container(
                                                  alignment: Alignment
                                                      .centerRight,
                                                  child:
                                                      Style.getPoppinsLightText(
                                                          product.isAvailable()
                                                              ? '✓ In Stock'
                                                              : "Out of Stock",
                                                          AppFonts.textSize10,
                                                          ColorTheme.FFF0674C,
                                                          align:
                                                              TextAlign.right)),
                                            )
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 20.0),
                                        child: Divider(
                                          color: ColorTheme.FFE3E3E3,
                                          height: 0.5,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            left: 20,
                                            right: 20,
                                            top: 13,
                                            bottom: 13),
                                        child: Style.getPoppinsLightText(
                                            "Best Before : ${product.bestBefore ?? ""}",
                                            AppFonts.textSize11,
                                            ColorTheme.FF050202),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 20.0),
                                        child: Divider(
                                          color: ColorTheme.FFE3E3E3,
                                          height: 0.5,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            left: 20.0, top: 20),
                                        child: Style.getPoppinsRegularText(
                                            'Description:',
                                            14,
                                            ColorTheme.FF333333),
                                      ),
                                      Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 20.0),
                                        child: Html(
                                            data: product.description,
                                            defaultTextStyle:
                                                Style.getTextStyleLight(
                                                    AppFonts.textSize12,
                                                    ColorTheme
                                                        .FF666666)), //Style.getPoppinsLightText(product.description, AppFonts.textSize10, ColorTheme.FF666666, maxLines: null, softWrap: true),
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                    ])),
                                  ],
                                ),
                              ),
                              Ink(
                                key: startWidgetKey,
                                height: 70,
                                decoration: BoxDecoration(
                                    color: ColorTheme.white,
                                    boxShadow: [
                                      BoxShadow(
                                          color: ColorTheme.FF0000001A,
                                          blurRadius: 10.0,
                                          offset: Offset(0, -5),
                                          spreadRadius: 0),
                                    ]),
                                padding: EdgeInsets.only(
                                    left: 20, right: 20, top: 15, bottom: 15),
                                width: double.infinity,
                                child: Style.getTextWithIconButton(
                                    double.infinity,
                                    40,
                                    "Add to Cart",
                                    Image.asset(
                                      AppAssets.cart,
                                      width: 21,
                                      height: 18,
                                      color: ColorTheme.FFF0674C,
                                    ), onClick: () {
                                  if (product.isAvailable()) {
                                    _addToCart();
                                  } else {
                                    AppUtils.showToast(TextMsgs.OUT_OF_STOCK);
                                  }
                                }),
                              )
                            ],
                          ))
                      : buildLoading();
//                  : _placeHolderView();
                },
              ) /*;
        }
      ),*/
        );
  }

  _processData() {
    updateProductImages();
    _updateProductValues();
  }

  _addToCart() async {
    if (_qty > 0) {
      ShoppingCartUtil.get().addProductToCart(
          context, product, _qty, packOffValueString, unitName, saleUnit,
          onComplete: () {
        Offset start = AddToCartAnimation.getWidgetOffset(startWidgetKey);
        Offset end = AddToCartAnimation.getWidgetOffset(endWidgetKey);
        AddToCartAnimation.show(rootKey, start, end, onFinish: () {
          setState(() {});
        });
      }, screen: "Product detail");
    }
  }

  String padLeft(var value) {
    return value.toString().padLeft(2, "0");
  }

  _updateProductValues() {
    selectedVariant = product.hasVariants()
        ? product.sizevariants[selectedVariantIndex]
        : null;
    if (selectedVariant != null) {
      mrpPrice = double.parse(selectedVariant.salePrice);
      basePrice = double.parse(selectedVariant.productPrice);
      rate = double.parse(selectedVariant.taxRate);
      unitName = selectedVariant.baseUom;
      saleUnit = selectedVariant.sellingUom;
      packOffValueString = selectedVariant.saleQty;
      packOffValue = double.parse(selectedVariant.saleQty);
    } else {
      mrpPrice = double.parse(product.productMrp);
      basePrice = double.parse(product.productBaseRate);
      rate = double.parse(product.productTaxRate);
      unitName = product.sizeUnit;
      saleUnit = product.saleUnit;
      packOffValueString = product.size;
      packOffValue = double.parse(product.size);
    }
  }

  updateProductImages() {
    images.clear();
    if (product.documents != null) {
      product.documents.forEach((element) {
        images.add(element.documentUrl);
      });
    } else {
      images.add(product.documentUrl);
    }
  }

  buildLoading() {
    return Container(
        color: ColorTheme.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: CustomScrollView(
                slivers: [
                  SliverList(
                      delegate: SliverChildListDelegate([
                    SizedBox(
                      height: 10,
                    ),
                    // Hero(
                    //   tag: "Hero${product.id}",
                    Hero(
                      tag: widget.key.toString(),
                      child: Center(
                        child:
                            /*NetworkImageView(
                          product.documentUrl ??
                              "https://www.happyeater.com/images/default-food-image.jpg",
                          80,
                          200,
                        )*/
                            Container(
                          height: 200,
                        ),
                      ),
                    ),
                    //),
                    Container(
                      color: ColorTheme.FFFDFDFD,
                      height: 10,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: ColorTheme.white,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(20),
                              topLeft: Radius.circular(20)),
                          boxShadow: [
                            BoxShadow(
                                color: ColorTheme.FFE3E3E3.withOpacity(.3),
                                blurRadius: 1.0,
                                offset: Offset(0, -1),
                                spreadRadius: 0),
                          ]),
                      height: 20,
                      width: double.infinity,
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.only(left: 20),
                      child: NetworkImageView(
                        PreferenceManager.getLocationData()?.sellerurl ?? "",
                        40,
                        15,
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 20, right: 20, bottom: 16, top: 8),
                      child: AppUtils.getProductName(
                          product.productName,
                          product.subText,
                          AppFonts.textSize14,
                          ColorTheme.FF333333,
                          maxLines: 3),
                    ),
                    Material(
                      type: MaterialType.transparency,
                      child: Ink(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            if (product.hasVariants())
                              AbsorbPointer(
                                absorbing: !product.isAvailable(),
                                child: Container(
                                  margin: EdgeInsets.only(left: 20, right: 20),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    border: Border.all(
                                        width: 0.5, color: ColorTheme.FF333333),
                                  ),
                                  child: DropdownButton(
                                    onTap:
                                        !product.isAvailable() ? () {} : null,
                                    underline: Container(),
                                    isDense: true,
                                    icon: Icon(
                                      Icons.keyboard_arrow_down,
                                      size: 20,
                                      color: ColorTheme.FF333333,
                                    ),
                                    value: selectedVariantIndex,
                                    onChanged: (int) {
                                      selectedVariantIndex = int;
                                      _updateProductValues();
                                      setState(() {});
                                    },
                                    items: product.sizevariants
                                        .map((e) => DropdownMenuItem(
                                              value: product.sizevariants
                                                  .indexOf(e),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 20),
                                                child: Style.getPoppinsLightText(
                                                    "${e?.saleQty} ${e?.baseUom}",
                                                    AppFonts.textSize10,
                                                    ColorTheme.FF333333),
                                              ),
                                            ))
                                        .toList(),
                                  ),
                                ),
                              ),
                            SizedBox(),
                            Container(
                                margin: EdgeInsets.only(left: 20, right: 20),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    InkWell(
                                      borderRadius: BorderRadius.circular(15),
                                      child: Image.asset(
                                        AppAssets.decrementCounter,
                                        width: 30,
                                        height: 30,
                                      ),
                                    ),
                                    Container(
                                      alignment: Alignment.center,
                                      padding: EdgeInsets.only(bottom: 8),
                                      margin:
                                          EdgeInsets.only(right: 20, left: 20),
                                      child: AnimatedFlipCounter(
                                        duration: Duration(milliseconds: 300),
                                        value: _qty,
                                        /* pass in a number like 2014 */
                                        color: ColorTheme.FF333333,
                                        size: 14,
                                      ),
                                    ),
                                    InkWell(
                                      borderRadius: BorderRadius.circular(15),
                                      child: Image.asset(
                                        AppAssets.incrementCounter,
                                        width: 30,
                                        height: 30,
                                      ),
                                    ),
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Container(
                        margin:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                        padding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            color: ColorTheme.FFF8F8F8),
                        child: Style.getPoppinsLightText(
                            "$packOffValueString $unitName x ${_qty ?? 0} Qty = ${(packOffValue ?? 0 * _qty ?? 0).toStringAsFixed(0)} $unitName",
                            10,
                            ColorTheme.FF999999),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Row(
                        children: [
//                                        Style.getPoppinsLightText('${AppUtils.getRupeeSymbol()}$mrpPrice', AppFonts.textSize10, ColorTheme.FF333333, decoration: TextDecoration.lineThrough),
//                                        SizedBox(
//                                          width: 6,
//                                        ),
                          AppUtils.getWidgetWithRupee(
                              ' ${AppUtils.showDoubleString(mrpPrice ?? 0.0 * _qty ?? 0)}',
                              AppFonts.textSize10,
                              ColorTheme.FF333333),
                          Expanded(
                            child: Container(
                                alignment: Alignment.centerRight,
                                child: Style.getPoppinsLightText(
                                    product.isAvailable()
                                        ? '✓ In Stock'
                                        : "Out of Stock",
                                    AppFonts.textSize10,
                                    ColorTheme.FFF0674C,
                                    align: TextAlign.right)),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: Divider(
                        color: ColorTheme.FFE3E3E3,
                        height: 0.5,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 20, right: 20, top: 13, bottom: 13),
                      child: Style.getPoppinsLightText(
                          "Best Before : ${product.bestBefore ?? ""}",
                          AppFonts.textSize11,
                          ColorTheme.FF050202),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: Divider(
                        color: ColorTheme.FFE3E3E3,
                        height: 0.5,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20.0, top: 20),
                      child: Style.getPoppinsRegularText(
                          'Description:', 14, ColorTheme.FF333333),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: Html(
                          data: product.description,
                          defaultTextStyle: Style.getTextStyleLight(
                              AppFonts.textSize12,
                              ColorTheme
                                  .FF666666)), //Style.getPoppinsLightText(product.description, AppFonts.textSize10, ColorTheme.FF666666, maxLines: null, softWrap: true),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                  ])),
                ],
              ),
            ),
            /* Ink(
              key: startWidgetKey,
              height: 70,
              decoration: BoxDecoration(color: ColorTheme.white, boxShadow: [
                BoxShadow(
                    color: ColorTheme.FF0000001A,
                    blurRadius: 10.0,
                    offset: Offset(0, -5),
                    spreadRadius: 0),
              ]),
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 15),
              width: double.infinity,
              child: Style.getTextWithIconButton(
                  double.infinity,
                  40,
                  "Add to Cart",
                  Image.asset(
                    AppAssets.cart,
                    width: 21,
                    height: 18,
                    color: ColorTheme.FFF0674C,
                  ), onClick: () {
                if (product.isAvailable()) {
                  _addToCart();
                } else {
                  AppUtils.showToast(TextMsgs.OUT_OF_STOCK);
                }
              }),
            )*/
          ],
        ));
  }
}
