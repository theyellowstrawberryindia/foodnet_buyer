import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_qrcode_scanner/flutter_qrcode_scanner.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/model/OrderModel.dart';
import 'package:foodnet_buyer/data/model/order_details_model.dart';
import 'package:foodnet_buyer/data/repository/order_repository.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/add_gross_weight.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/custome_loader.dart';
import 'package:foodnet_buyer/util/toast_utils.dart';

const flashOn = 'FLASH ON';
const flashOff = 'FLASH OFF';
const frontCamera = 'FRONT CAMERA';
const backCamera = 'BACK CAMERA';

class ScannerScreen extends StatefulWidget {
  final bool showAppBar;
  final bool showItemInToast;
  ScannerScreen({this.showAppBar = true, this.showItemInToast});

  @override
  _ScannerScreenState createState() => _ScannerScreenState();
}

class _ScannerScreenState extends State<ScannerScreen> {
  final orderRepository = OrderRepository();
  final _textController = TextEditingController();
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  var qrText = '';
  var flashState = flashOn;
  var cameraState = frontCamera;
  QRViewController controller;

  @override
  void dispose() {
    controller.dispose();
    Loader.hide();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _showToast();
  }
  _showToast() {
    if (widget.showItemInToast) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        ToastUtils.init(context).showSuccessToast('ItemIn successful');
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: widget.showAppBar ? AppBar(
        backgroundColor: ColorTheme.white,
        title: Style.getPoppinsRegularText('Scan', 16, ColorTheme.black),
        iconTheme: IconThemeData(color: Colors.black87),
      ) : null,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.all(10),
      child: Stack(
        children: [
          Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: 250,
                  height: 250,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: QRView(
                      key: qrKey,
                      onQRViewCreated: _onQRViewCreated,
                      overlay: QrScannerOverlayShape(
                        borderColor: Colors.white,
                        borderRadius: 10,
                        borderLength: 60,
                        borderWidth: 5,
                        cutOutSize: 240,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 20,),
                Style.getPoppinsLightText('Scan the QR Code from ItemIn', 16, ColorTheme.darkGrey)
              ],
            ),
          ),
          Positioned(
            child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              // Icon(Icons.power,color: Colors.black54,),
              const SizedBox(height: 30,),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      height: 40,
                      color: ColorTheme.white,
                      child: TextField(
                      controller: _textController,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 16,color: ColorTheme.black,),
                        textInputAction: TextInputAction.done,
                        keyboardType: TextInputType.number,
                        onSubmitted: (value){
                          if(value.trim().isNotEmpty){
                            FocusScope.of(context).unfocus();
                            _validateTid(_textController.text.toString().trim());
                          }
                        },
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30)
                          ),
                          hintText: 'Enter TID',
                          contentPadding: const EdgeInsets.symmetric(vertical: 4),
                          hintStyle: TextStyle(fontSize: 16,color: ColorTheme.darkGrey,)
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(width: 10,),
                  Container(
                    height: 50,
                    width: 50,
                    child: GestureDetector(
                      onTap: (){
                        if(_textController.text.toString().trim().isNotEmpty){
                          FocusScope.of(context).unfocus();
                          _validateTid(_textController.text.toString().trim());
                        }
                      },
                      child: Container(
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                          color: ColorTheme.white,
                          shape: BoxShape.circle,
                          boxShadow: AppUtils.getBoxShadow(),
                          border: Border.all(color: ColorTheme.primaryColor,)
                        ),
                        alignment: Alignment.center,
                        child: Icon(Icons.arrow_forward_ios,size: 10,color: ColorTheme.primaryColor,),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
            left: 0,
            right: 0,
            bottom: 0,
          )
        ],
      ),
      ),
    );
  }

  bool _isFlashOn(String current) {
    return flashOn == current;
  }

  bool _isBackCamera(String current) {
    return backCamera == current;
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      if(scanData!=null){
        setState(() {
          qrText = scanData;
          _validateTid(qrText);
          // if(widget.item.tid.id.toString()==scanData){
          //   _orderItemIn(widget.item.id);
          //   this.controller.pauseCamera();
          // }else{
          //   this.controller.pauseCamera();
          //   AppUtils.showToast('Seems you are scanning different order.');
          //   Future.delayed(Duration(seconds: 2),_restart);
          // }
        });
      }

    });
  }

  _restart(){
   setState(() {
     this.controller.resumeCamera();
   });
  }

  _validateTid(dynamic tid) async {
    this.controller.pauseCamera();
    Loader.show(context);
    try {
      var response = await orderRepository.getOrderDetails(tid);
      Loader.hide();
      if (response != null && response.status) {
        bool itemInPending = false;
        bool itemOutDone = false;
        for (int i=0;i<response.orderData.tid.status.length;i++) {
          if (response.orderData.tid.status[i]?.isItemOutDone() == true) {
            itemOutDone = true;
          }
          if (response.orderData.tid.status[i]?.isItemInPending() == true) {
            itemInPending = true;
          }
        }
        if (itemOutDone && itemInPending) {
          _orderItemIn(response.orderData);
          return;
        }
      }
    } catch (e) {
      Loader.hide();
    }
    AppUtils.showToast('Seems you are scanning different order.');
    Future.delayed(Duration(seconds: 2),_restart);
  }

  _orderItemIn(OrderData orderData)async{
    _textController.text = "";
    var result = await Navigator.of(context).push(MaterialPageRoute(builder: (_)=>AddGrossWeight(orderData) ));
    if (result != null && result) {
      _restart();
    }
  }
}
