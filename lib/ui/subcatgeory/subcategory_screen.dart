
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/bloc/productbloc/product_bloc.dart';
import 'package:foodnet_buyer/data/bloc/productbloc/product_event.dart';
import 'package:foodnet_buyer/data/bloc/productbloc/product_state.dart';
import 'package:foodnet_buyer/data/model/category_response.dart';
import 'package:foodnet_buyer/data/model/product.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/widgets/scroll_item_view.dart';
import 'package:foodnet_buyer/util/app_utils.dart';

class SubCategoryScreen extends StatefulWidget {

  final List<Child> allChild;
  final int categoryIndex;
  final String catalogId;
  final int selectedTab;

  SubCategoryScreen(this.allChild, this.categoryIndex, this.catalogId, {this.selectedTab = 0});

  @override
  SubCategoryScreenState createState() => SubCategoryScreenState();
}

class SubCategoryScreenState extends State<SubCategoryScreen> with TickerProviderStateMixin {

  TabController _tabController;
//  StreamController<int> _pageController = StreamController<int>();
  Child category;

  int _currentIndex= 0;
  int _categoryIndex;

  List<GlobalKey> _tabKeys = [];
  ProductBloc _productBloc;
  double screenWidth;

  bool _topTabScrollable = false;
  var _controller = ScrollController();
  GlobalKey cartKey = GlobalKey();
  GlobalKey rootKey = GlobalKey();
  bool allDropDownChange = false;
  List<Map<String, double>> itemCountList = [];
  ScrollController _scrollController;
  bool allowScrollListener = false;
  bool autoNavigate = true;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _categoryIndex = widget.categoryIndex;
    category = widget.allChild[widget.categoryIndex];
    _currentIndex = widget.selectedTab;
    _productBloc = ProductBloc();
    category.children?.forEach((element) {
      _tabKeys.add(GlobalKey());
    });
    _tabController = TabController(initialIndex: _currentIndex, length: category?.children?.length ?? 0, vsync: this);
    _callAPI();
    _scrollController.addListener(() {
      if(allowScrollListener) {
        for (int i = 0; i < itemCountList.length; i++) {
          if (itemCountList[i]["start"] <= _scrollController.position.pixels &&
              itemCountList[i]["end"] >= _scrollController.position.pixels) {
            try {
              print("Move to sub tab: $i");
              _currentIndex = i;
              _tabController.animateTo(i);
            } catch(e){
              print(e);
            }
            break;
          }
        }
      }
    });
  }

  onCategoryChanged(int index) {
    _categoryIndex = index;
    itemCountList = [];
    category = widget.allChild[index];
    _tabKeys.clear();
    category.children.forEach((element) {
      _tabKeys.add(GlobalKey());
    });
    _currentIndex = 0;
    _tabController.dispose();
    _tabController = TabController(initialIndex: _currentIndex, length: category.children.length, vsync: this);
    _callAPI();
    setState(() {
      allDropDownChange = false;
    });
    // FBAnalytics.categoryClicked(widget.allChild[_categoryIndex].id?.toString(), widget.allChild[_categoryIndex].categoryName, "Sub Category Listing drop down");
  }

  _callAPI() {
    _productBloc..add(GetAllProductOfCategoryEvent(category: category, productCatalogId: widget.catalogId));
  }

  @override
  void dispose() {
//    _pageController.close();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((time) {
      allDropDownChange = true;
    });
    screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
        key: rootKey,
        backgroundColor: ColorTheme.FFFDFDFD,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: ColorTheme.white,
          titleSpacing: 0.0,
          iconTheme: IconThemeData(color: ColorTheme.FF333333),
          title: Container(
            margin: EdgeInsets.only(left: 20),
//            width: 150,
            child: DropdownButton(
              isExpanded: false,
              underline: Container(),
              isDense: true,
              icon: Icon(
                Icons.keyboard_arrow_down,
                size: 20,
                color: ColorTheme.FF333333,
              ),
              value: _categoryIndex,
              onChanged: (int) {
                if (allDropDownChange && _categoryIndex != int) {
                  onCategoryChanged(int);
                }
              },
              items: widget.allChild
                  .map((e) => DropdownMenuItem(
                value: widget.allChild.indexOf(e),
                child: Container(
                  width: 100,
//                  padding: EdgeInsets.symmetric(
//                      horizontal: 10),
                  child: Style.getPoppinsLightText("${e.categoryName}", AppFonts.textSize16, ColorTheme.FF333333),
                ),
              )).toList(),
            ),
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 20.0, top:10, bottom: 10),
              child: AppUtils.getCartWidget(context, key: cartKey),
            )
          ],
          bottom: TabBar(tabs: getTopTabsView(context),
            labelColor: ColorTheme.FFF97062,
            labelStyle: Style.getTextStyleMedium(12, ColorTheme.FFF97062),
            unselectedLabelColor: ColorTheme.FF999999,
            unselectedLabelStyle: Style.getTextStyleLight(12, ColorTheme.FF999999),
            indicator: null,
            indicatorColor: ColorTheme.transparent,
            indicatorWeight: 1,
            isScrollable: _topTabScrollable,
            controller:  _tabController,
            onTap: (index){
              if (allDropDownChange) {
                _onTabChange(index);
              }
            },),
        ),
        body: GestureDetector(
          onPanDown: (data){
            allowScrollListener = true;
          },
          child: Container(
            width: double.maxFinite,
            padding: EdgeInsets.only(left: 20, right: 20,),
            child: _getTabView(0)/*StreamBuilder<int>(
                stream: _pageController.stream,
                initialData: _currentIndex,
                builder: (context, snapshot) {
                  return _getTabView(snapshot.data);
                }
            )*/,
          ),
        )
    );
  }

  _onTabChange(int index, {bool callEvent = true}) {
    _currentIndex = index;
    _tabController.animateTo(index);
//    _pageController.sink.add(index);
    // scroll to product
    allowScrollListener = false;
    _scrollController.animateTo(itemCountList[index]["start"], duration: Duration(milliseconds: 500), curve: Curves.linear);
    if (callEvent) {
      // FBAnalytics.subCategoryClicked(widget.allChild[_categoryIndex].children[index].id?.toString(), widget.allChild[_categoryIndex].children[index].categoryName, "Sub Category Listing");
    }
  }

  Widget _getTabView(int index) {
    return BlocBuilder<ProductBloc, ProductState>(
//        key: _tabKeys[index],
        bloc: _productBloc,
        builder: (c, state) {
          if (state is FetchingDataState) {
            return Center(child: CircularProgressIndicator(),);
          } else if (state is AllProductOfCategoryReceivedState) {
            if (state.response != null) {
              Map<String, dynamic> data = state.response;
              if (data["products"].length > 0) {
                if (itemCountList.isEmpty) {
                  itemCountList.clear();
                  itemCountList = data["itemCountList"];
                }
                if (_currentIndex != 0 && autoNavigate) {
                  WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                    autoNavigate = false;
                    _onTabChange(_currentIndex, callEvent: false);
                  });
                }
                return _dataView(data["products"]);
              } else {
                return getNoItemsFoundScreen();
              }
            } else {
              return getNoItemsFoundScreen();
            }
          } else {
            return getNoItemsFoundScreen();
          }
        });
  }

  Widget _dataView(List<Product> products) {
    return CustomScrollView(
      controller: _scrollController,
      slivers: [
        SliverList(
          delegate: SliverChildListDelegate(
            [
              Container(
                height: 20,
              ),

            ],
          ),
        ),
        SliverStaggeredGrid.countBuilder(
          crossAxisCount: 2,
          mainAxisSpacing: 15.0,
          crossAxisSpacing: 15.0,
          key: UniqueKey(),
          itemCount: products.length,
          itemBuilder: (ctx, index) {
            Product e = products[index];
            return AnimationConfiguration.staggeredGrid(
              position: index,
              duration: Duration(milliseconds: _shouldAnimate()  ? 200 : 0),
              columnCount: 2,
              child: ScrollItemView(e, cartKey, rootKey, "Sub Category Listing", showAnimation: _shouldAnimate(), onClick: (value){
                allowScrollListener = value;
              }, key: UniqueKey(),),
            );
          },
          staggeredTileBuilder: (index) {
            Product e = products[index];
            return StaggeredTile.count(1, e.isOffer() ? 0.95 : (258/((MediaQuery.of(context).size.width-45)/2)));
          },
        ),
        SliverList(
          delegate: SliverChildListDelegate(
            [
              Container(
                height: 10,
              ),

            ],
          ),
        ),
      ],
    );
  }

  bool _shouldAnimate() {
    return allowScrollListener && _scrollController.position.userScrollDirection == ScrollDirection.reverse;
  }

  List<Widget> getTopTabsView(BuildContext context) {
    double width = 0;
    List<Widget> tabItems = List();
    double minWidth = 85;
    category.children.forEach((element) {
      double itemWidth = AppUtils.getTextWidth(element.categoryName, 12, 'Poppins-Medium', maxWidth: MediaQuery.of(context).size.width) + 50;
//      itemWidth = itemWidth > minWidth ? itemWidth : minWidth;
      width += itemWidth;
      tabItems.add(Container(
//          padding: EdgeInsets.only(left: 10, right: 10,),
          constraints: BoxConstraints(minWidth: itemWidth, minHeight: 40, maxHeight: 40),
          alignment: Alignment.center,
          child: Text(element.categoryName)));
    });
    _topTabScrollable = width > MediaQuery.of(context).size.width;
    return tabItems;
  }

  Widget getNoItemsFoundScreen() {
    return Center(
      child: Style.getPoppinsLightText("No items found", 14, ColorTheme.FF333333),
    );
  }
}