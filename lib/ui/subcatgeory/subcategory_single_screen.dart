
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/bloc/productbloc/product_bloc.dart';
import 'package:foodnet_buyer/data/bloc/productbloc/product_event.dart';
import 'package:foodnet_buyer/data/bloc/productbloc/product_state.dart';
import 'package:foodnet_buyer/data/model/product.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/widgets/CustomAppBar.dart';
import 'package:foodnet_buyer/ui/widgets/scroll_item_view.dart';
import 'package:foodnet_buyer/util/app_utils.dart';

class SubCategorySingleScreen extends StatefulWidget {

  final String catalogId;
  final String title;
  final String subCategoryId;
  final bool isParentCategory;

  SubCategorySingleScreen(this.catalogId, this.subCategoryId, this.title, this.isParentCategory);

  @override
  SubCategorySingleScreenState createState() => SubCategorySingleScreenState();
}

class SubCategorySingleScreenState extends State<SubCategorySingleScreen> with TickerProviderStateMixin {

  ProductBloc _productBloc;
  GlobalKey cartKey = GlobalKey();
  GlobalKey rootKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    _productBloc = ProductBloc();
    AppUtils.delayTask((){
      _productBloc..add(GetAllProductOfSubCategoryEvent(categoryId: widget.subCategoryId, productCatalogId: widget.catalogId, isParentCategory: widget.isParentCategory));
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: rootKey,
        backgroundColor: ColorTheme.FFFDFDFD,
        appBar: CustomAppBar(widget.title, actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20.0, top:10, bottom: 10),
            child: AppUtils.getCartWidget(context, key: cartKey),
          )
        ]),
        body: Container(
          width: double.maxFinite,
          padding: EdgeInsets.only(left: 20, right: 20,),
          child: BlocBuilder<ProductBloc, ProductState>(
              bloc: _productBloc,
              builder: (c, state) {
                if (state is FetchingDataState) {
                  return Center(child: CircularProgressIndicator(),);
                } else if (state is AllProductReceivedState) {
                  if (state.response.length > 0) {
                    var items = List<Product>();
                    items.addAll(state.response);
                    return _dataView(items);
                  } else {
                    return getNoItemsFoundScreen();
                  }
                } else {
                  return getNoItemsFoundScreen();
                }
              }),
        )
    );
  }

  Widget _dataView(List<Product> products) {
    return CustomScrollView(
      slivers: [
        SliverList(
          delegate: SliverChildListDelegate(
            [
              Container(
                height: 20,
              ),

            ],
          ),
        ),
        SliverStaggeredGrid.countBuilder(
          crossAxisCount: 2,
          mainAxisSpacing: 15.0,
          crossAxisSpacing: 15.0,
          key: UniqueKey(),
          itemCount: products.length,
          itemBuilder: (ctx, index) {
            Product e = products[index];
            return AnimationConfiguration.staggeredGrid(
              position: index,
              duration: Duration(milliseconds: 200),
              columnCount: 2,
              child: ScrollItemView(e, cartKey, rootKey, "Sub catgeory listing (Offer/Ad Campain/Promotional)", key: UniqueKey(),)
            );
          },
          staggeredTileBuilder: (index) {
            Product e = products[index];
            return StaggeredTile.count(1, e.isOffer() ? 0.95 : (258/((MediaQuery.of(context).size.width-45)/2)));
          },
        ),
        SliverList(
          delegate: SliverChildListDelegate(
            [
              Container(
                height: 10,
              ),

            ],
          ),
        ),
      ],
    );
  }

  Widget getNoItemsFoundScreen() {
    return Center(
      child: Style.getPoppinsLightText("No items found", 14, ColorTheme.FF333333),
    );
  }
}