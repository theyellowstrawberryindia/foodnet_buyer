import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/bloc/common_bloc.dart';
import 'package:foodnet_buyer/data/bloc/tabbloc/tab_bloc.dart';
import 'package:foodnet_buyer/data/bloc/tabbloc/tab_event.dart';
import 'package:foodnet_buyer/data/bloc/tabbloc/tab_state.dart';
import 'package:foodnet_buyer/data/model/category_response.dart';
import 'package:foodnet_buyer/data/model/home_section.dart';
import 'package:foodnet_buyer/data/model/product.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/product_detail/product_detail_screen.dart';
import 'package:foodnet_buyer/ui/subcatgeory/subcategory_screen.dart';
import 'package:foodnet_buyer/ui/subcatgeory/subcategory_single_screen.dart';
import 'package:foodnet_buyer/ui/widgets/carousel_with_indicator.dart';
import 'package:foodnet_buyer/ui/widgets/netwrok_image_view.dart';
import 'package:foodnet_buyer/ui/widgets/product_staggered_view.dart';
import 'package:foodnet_buyer/ui/widgets/search_view.dart';
import 'package:foodnet_buyer/ui/widgets/sub_categories_list.dart';
import 'package:foodnet_buyer/ui/widgets/super_categories_list.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';

class HomeTabScreen extends StatefulWidget {
  final GlobalKey rootKey;
  final GlobalKey cartKey;
  final bool delayLoading;

  const HomeTabScreen({this.rootKey, this.cartKey, this.delayLoading = false, Key key}): super(key: key);

  @override
  _HomeTabScreenState createState() => _HomeTabScreenState(delayLoading);
}

class _HomeTabScreenState extends State<HomeTabScreen> {
  final controller = ScrollController();
  TabBloc _bloc;
  List<HomeSection> homeSections;

  bool delayLoading = false;
  final searchKey = UniqueKey();
  Widget mainView;

  _HomeTabScreenState(this.delayLoading);
  
  @override
  void initState() {
    _bloc = TabBloc();
    AppUtils.delayTask((){
      _bloc.add(GetAllHomeSectionEvent());
    }, delay: Duration(milliseconds: 400));
    super.initState();
    CommonBloc.get().addHomeListener(update);
  }

  void update() {
    _bloc.add(GetAllHomeSectionEvent());
  }

  @override
  void dispose() {
    CommonBloc.get().removeHomeListener(update);
    super.dispose();
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {

    if (mainView == null) {
      mainView = BlocBuilder<TabBloc, TabState>(
        bloc: _bloc,
        builder: (ctx, state) {
          List<Widget> scrollItems = [];
          scrollItems.add(const SizedBox(height: 25,));
          scrollItems.add(SearchView(key: searchKey,
            preFilledText: _bloc.lastSearchedKeyword,
            onTextChanged: (value) {
              // FBAnalytics.searchProduct(value, "Home Tab");
              _bloc.add(GetSearchResultEvent(value.trim()));
            },
            onCancel: () {
              _bloc.add(CancelSearchEvent());
            },));
          scrollItems.add(const SizedBox(height: 20,));
          scrollItems.addAll(_loadAllViews(context, state));
          return /*GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (_) {
            AppUtils.clearFocus(context);
          },
          child: SingleChildScrollView(
            child: Column(
              children: scrollItems,
            ),
          ),
        );*/
            ListView.custom(
              cacheExtent: 6,
              childrenDelegate: SliverChildListDelegate(scrollItems),
              keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior
                  .onDrag,);
        },
      );
    }

    return mainView;
  }



  List<Widget> _loadAllViews(BuildContext context, state) {
    List<Widget> scrollItems = [];
    if (state is AllHomeSectionReceivedState) {
      if (state.sections?.isNotEmpty ?? false) {
//            state.sections.sort((one, two) => one.sequenceNumber.compareTo(two.sequenceNumber));
        homeSections = state.sections;
        scrollItems.addAll(_buildView(context, state.sections));
      } else {
        scrollItems.add(_mainMsgView(context));
      }
    } else if (state is SearchResultState) {
      if (state.result.isNotEmpty) {
        scrollItems.add(_displayProductView(context, state.result));
      } else {
        scrollItems.add(_mainMsgView(context));
      }
    } else if (state is ApiFailedState) {
      scrollItems.add(_mainMsgView(context));
    } else if (state is SearchIdleState) {
      if (homeSections?.isNotEmpty ?? false) {
        scrollItems.addAll(_buildView(context, homeSections));
      } else {
        scrollItems.add(_mainLoadingView(context));
        _bloc.add(GetAllHomeSectionEvent());
      }
    } else {
      scrollItems.add(_mainLoadingView(context));
    }
    return scrollItems;
  }

  List<Widget> _buildView(BuildContext context, List<HomeSection> sections) {
    double sectionGap = 10;
    double headingGap = 15;
    List<Widget> children = [];
//    children.add(_getVerticalGap(20));
    sections.forEach((e) {
      if (e.sectionTemplate.isAdCampaignType()) {
        children.add(_displayAdCampainView(e.items));
        children.add(_getVerticalGap(15));
      }
      if (e.sectionTemplate.isSuperCategoriesType()) {
        children.add(_getVerticalGap(sectionGap));
        children.add(_getDivider());
        children.add(_displaySuperCategoryView(e.items));
        children.add(_getDivider());
      }
      if (e.sectionTemplate.isProductType()) {
        children.add(_getVerticalGap(sectionGap));
        children.add(_getProductSectionHeading(e, e.sequenceNumber == 1 ? AppAssets.topPicks : AppAssets.bestSelling));
        children.add(_getVerticalGap(headingGap));
        children.add(_displayProductView(context, e.items));
      }
      if (e.sectionTemplate.isPromotionalCampaignType()) {
        children.add(_getVerticalGap(sectionGap));
        children.add(_getProductSectionHeading(e, AppAssets.topOffer));
        children.add(_getVerticalGap(headingGap));
        children.add(_displayTopOfferView(e.items));
        children.add(_getVerticalGap(headingGap));
      }
      if (e.sectionTemplate.isCategoriesType()) {
        children.add(_getVerticalGap(sectionGap));
        children.add(_getDivider());
        children.add(_displayCategoryView(e.items));
        children.add(_getDivider());
        children.add(_getVerticalGap(headingGap));
      }
    });
    children.add(_getVerticalGap(headingGap));
    return children;
  }

  Widget _displayAdCampainView(List<Product> items) {
    return CarouselWithIndicator(items, (product)=> (product as Product).adCampaignDocumentUrl, key: UniqueKey(), onClick: (item) {
      AppUtils.unfocusScope(context);
      AppUtils.handleOfferNavigation(context, item);
      if (item is Product) {
        // FBAnalytics.adBannerClicked(item.navigationType, item.adCampaignId, item.categoryId?.toString(), (item.productId ?? item.id)?.toString(), item.isParentCategory);
      }
    }, height: 160, );
  }

  Widget _displaySuperCategoryView(List<SuperCategory> items) {
    return SuperCategoriesList(-1, items, onClick: (index){
      AppUtils.unfocusScope(context);
      // NavigationUtil.pushToNewScreen(context, CategoryListingScreen(items[index]));
      // FBAnalytics.superCategoryClicked(items[index].id?.toString(), items[index].superCategoryName, "Home page");
    }, bgColor: ColorTheme.white, key: UniqueKey(),);
  }

  Widget _displayProductView(BuildContext context, List<Product> products) {
    return ProductStaggeredView(products, widget.rootKey, widget.cartKey, "Home Tab (Search)", key: UniqueKey(),);
  }

  Widget _displayTopOfferView(List<Product> items) {
    return CarouselWithIndicator(items, (item) => (item as Product).documentUrl,
      onClick: (i) {
        AppUtils.handleOfferNavigation(context, i);
        if (i is Product) {
          // FBAnalytics.promotionalBannerClicked(i.navigationType, i.adCampaignId, i.categoryId?.toString(), (i.productId ?? i.id)?.toString(), i.isParentCategory);
        }
    }, viewPort: .6, height: 140, showIndicator: false, key: UniqueKey(),);
  }

  Widget _displayCategoryView(List<Child> items) {
    return SubCategoriesList(-1, items, onClick: (index){
      AppUtils.unfocusScope(context);
      // FBAnalytics.categoryClicked(items[index].id?.toString(), items[index].categoryName, "Home page");
      NavigationUtil.pushToNewScreen(context, SubCategoryScreen(items, index, PreferenceManager.getLocationData().productCatalogId,));
    }, bgColor: ColorTheme.white, key: UniqueKey(),);
  }

  Widget _getDivider() => Divider(height: .5, color: ColorTheme.FFE3E3E3,);


  Widget _getProductSectionHeading(HomeSection section, String image) {
    return Row(
            children: [
              SizedBox(width: 20,),
              NetworkImageView(section.documentUrl, 24, 24, radius: 12,),
              SizedBox(width: 10,),
              Style.getPoppinsLightText(section.sectionName ?? "", 16, ColorTheme.FF333333),
              SizedBox(width: 20,)
            ],
          );
  }

  Widget _getVerticalGap(double height) {
    return SizedBox(height: height,);
  }

  Widget _mainMsgView(BuildContext context, {String text = "No items found"}) {
    return Container(
            alignment: Alignment.center,
            height: MediaQuery.of(context).size.height - 300,
            margin: EdgeInsets.only(left: 20, right: 20),
            child: Style.getPoppinsLightText(text, 14, ColorTheme.FF333333)
        );
  }

  Widget _mainLoadingView(BuildContext context) {
    return Container(
            alignment: Alignment.center,
            height: MediaQuery.of(context).size.height - 300,
            child: CircularProgressIndicator()
        );
  }
}
