import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/model/AreaModel.dart';
import 'package:foodnet_buyer/data/model/OrderModel.dart';
import 'package:foodnet_buyer/data/repository/order_repository.dart';
import 'package:foodnet_buyer/ui/edit_order_screen.dart';
import 'package:foodnet_buyer/ui/order_detail_screen.dart';
import 'package:foodnet_buyer/ui/scanner/scanner_screen.dart';
import 'package:foodnet_buyer/ui/widgets/cell/cell_orders.dart';
import 'package:foodnet_buyer/util/toast_utils.dart';
import '../../style/style.dart';
import '../../util/date_time_util.dart';
import '../widgets/search_view.dart';

class OrderTabScreen extends StatefulWidget {
  final int currentIndex;

  OrderTabScreen({this.currentIndex = 0});

  @override
  _OrderTabScreenState createState() => _OrderTabScreenState();
}

class _OrderTabScreenState extends State<OrderTabScreen>
    with TickerProviderStateMixin {
  final OrderRepository _orderRepository = OrderRepository();
  final List<String> tabs = ['New', 'ItemIn', 'GRN', 'Payment'];
  DateTimeRange _fromRange = DateTimeRange(
      start: DateTime.now(),
      end: DateTime.now());
  TimeOfDay _fromTime = TimeOfDay.fromDateTime(DateTime.now());
  List<OrderItem> _orderList = [];
  List<OrderItem> _orderAllList = [];
  TabController _tabController;
  int currentIndex = 0;
  List<Areadatum> _area = [];
  bool loading = true;
  String filter = 'All';

  @override
  void initState() {
    super.initState();
    currentIndex = widget.currentIndex;
    _tabController =
        TabController(length: 4, vsync: this, initialIndex: currentIndex);
    _area.add(Areadatum(area: 'All'));
    _getOrders();
    _getAreaFilter();
    _showToast(context);
  }

  String _getRangeText() {
    return (_fromRange.start.day == _fromRange.end.day
        && _fromRange.start.month == _fromRange.end.month
        && _fromRange.start.year == _fromRange.end.year)
        ? '${DateTimeUtil.getDateFromDateTime(_fromRange.start, DateTimeUtil.DD_MMM)}'
        : '${DateTimeUtil.getDateFromDateTime(_fromRange.start, DateTimeUtil.DD_MMM)} - ${DateTimeUtil.getDateFromDateTime(_fromRange.end, DateTimeUtil.DD_MMM)}';

  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              margin: const EdgeInsets.symmetric(vertical: 25),
              child: SearchView(
                thresholdLength: 2,
                onTextChanged: (value) {
                  _getSearchOrders(value);
                },
                onCancel: () {
                  _getOrders();
                },
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              decoration: BoxDecoration(
                  border: Border(
                bottom: BorderSide(color: Colors.grey, width: 0.5),
              )),
              height: 40,
              child: TabBar(
                controller: _tabController,
                indicatorSize: TabBarIndicatorSize.tab,
                indicatorColor: ColorTheme.primaryColor,
                unselectedLabelColor: ColorTheme.darkGrey,
                labelColor: ColorTheme.primaryColor,
                unselectedLabelStyle: TextStyle(fontSize: 12),
                labelStyle:
                    TextStyle(fontSize: 13, fontWeight: FontWeight.w500),
                onTap: (int index) {
                  setState(() {
                    currentIndex = index;
                    _tabController.index = index;
                    filter = 'All';
                    _getOrders();
                  });
                },
                tabs: tabs
                    .map<Tab>((e) => Tab(
                          text: e,
                        ))
                    .toList(),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  _getContainerView(
                      _getRangeText(),
                      Icons.calendar_today_outlined,
                      () => _showDateRangePicker()),
                  // _getContainerView(
                  //     '${DateTimeUtil.getTimeIn12FormatFromTimeOfDay(_fromTime)}',
                  //     Icons.keyboard_arrow_down,
                  //     () => _timePicker()),
                  _getDropDown(),
                ],
              ),
            ),
          ),
          if (_orderList.length == 0)
            SliverFillRemaining(
              child: Container(
                child: Center(
                  child: loading
                      ? CircularProgressIndicator()
                      : Style.getPoppinsLightText(
                          "No Data", 14, ColorTheme.FF333333),
                ),
              ),
            ),
          if (_orderList.length > 0)
            SliverList(
              delegate: SliverChildListDelegate(
                _orderList
                    .map<Widget>((order) => CellOrders(
                          order,
                          tabs[currentIndex],
                          onClick: (OrderItem item) {
                            // if (tabs[currentIndex] == 'ItemIn')
                            //   _navigate(item);
                            // else if (tabs[currentIndex] == 'New' && order.canEdit())
                            //   _navigateToEdit(item);
                            // else
                              _navigateToDetails(item);
                          },
                        ))
                    .toList(),
              ),
            )
        ],
      ),
    );
  }

  _showToast(contex) {
    if (widget.currentIndex != 0) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        ToastUtils.init(context).showSuccessToast('ItemIn successful');
      });
    }
  }

  _navigate(data) async {
    final result = await Navigator.of(context)
        .push<bool>(MaterialPageRoute(builder: (_) => ScannerScreen()));

    if (result != null && result) {
      _getOrders();
    }
  }

  _navigateToEdit(OrderItem data) async {
    final result = await Navigator.of(context).push<bool>(
        MaterialPageRoute(builder: (_) => EditOrderScreen(data.tid.id, 1)));

    if (result != null && result) {
      _getOrders();
    }
  }

  _navigateToDetails(OrderItem data) async {
    final result = await Navigator.of(context).push<bool>(MaterialPageRoute(
        builder: (_) => OrderDetailScreen(
              data.tid.id.toString(),
              data.id.toString(),
              grn: tabs[currentIndex] == 'GRN',
            )));

    if (result != null && result) {
      _getOrders();
    }
  }

  _getContainerView(String text, IconData icons, Function onClick) {
    return GestureDetector(
      onTap: () => onClick?.call(),
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 10),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 6),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(
            // color: ColorTheme.darkGrey,
            color: ColorTheme.FFE5E5E5,
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
                child: Text(
              text,
              style: TextStyle(
                fontSize: 12,
                color: ColorTheme.darkGrey,
              ),
            )),
            const SizedBox(
              width: 6,
            ),
            Icon(
              icons,
              color: ColorTheme.darkGrey,
              size: 20,
            )
          ],
        ),
      ),
    );
  }

  _getDropDown() {
    return Container(
      height: 34,
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 6),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(
            // color: ColorTheme.darkGrey,
            color: ColorTheme.FFE5E5E5,
          )),
      child: DropdownButton(
        underline: Container(),
        icon: Icon(
          Icons.keyboard_arrow_down,
          size: 18,
          color: ColorTheme.darkGrey,
        ),
        items: _area
            .map<DropdownMenuItem>((e) => DropdownMenuItem(
                  child: Style.getPoppinsLightText(
                    e.area,
                    12,
                    ColorTheme.darkGrey,
                  ),
                  value: e.area,
                ))
            .toList(),
        elevation: 2,
        value: filter,
        onChanged: (data) {
          setState(() {
            filter = data;
            _filterOrders();
          });
        },
      ),
    );
  }

  _timePicker() async {
    showTimePicker(
      context: context,
      initialTime: TimeOfDay(hour: 10, minute: 47),
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: false),
          child: child,
        );
      },
    ).then((value) {
      if (value != null) _fromTime = value;
      _getOrders();
    });
  }

  _showDateRangePicker() async {
    final picked = await showDateRangePicker(
      saveText: 'Done',
      helpText: 'Select Date Range',
      cancelText: 'Cancel',
      confirmText: 'Done',
      useRootNavigator: true,
      context: context,
      firstDate: DateTime(DateTime.now().year - 5),
      lastDate: DateTime(DateTime.now().year + 5),
      currentDate: DateTime.now(),
      initialEntryMode: DatePickerEntryMode.calendar,
    );
    if (picked != null) {
      _fromRange = picked;
      _getOrders();
    }
  }

  _getOrders() async {
    // Loader.show(context);
    loading = true;
    _orderList.clear();
    _orderAllList.clear();
    setState(() {});
    final response = await _orderRepository.getOrders(
        _fromRange, _fromTime, tabs[currentIndex]);
    loading = false;
    _orderList.clear();
    _orderAllList.clear();
    if (response != null) {
      _orderList = response.orderItems;
      _orderAllList.addAll(_orderList);
    }
    setState(() {});
  }

  _getSearchOrders(dynamic searchKey) async {
    // Loader.show(context);
    //loading = true;
    _orderList.clear();
    _orderAllList.clear();
    final response = await _orderRepository.getSearchOrders(
        _fromRange, _fromTime, tabs[currentIndex], searchKey);
    loading = false;
    _orderList.clear();
    if (response != null) {
      _orderList = response.orderItems;
      _orderAllList.addAll(_orderList);
    }
    setState(() {});
  }

  _filterOrders() {
    _orderList.clear();
    print(filter);
    if (filter == 'All') {
      _orderList.addAll(_orderAllList);
    } else
      _orderAllList.forEach((element) {
        if (element.address?.area
            .toLowerCase()
            .contains(filter.toLowerCase())) {
          _orderList.add(element);
        }
      });

    setState(() {});
  }

  _getAreaFilter() async {
    final response = await _orderRepository.getAreaFilter();
    if (response != null) {
      _area.clear();
      _area.add(Areadatum(area: 'All'));
      _area.addAll(response.areadata);
      setState(() {});
    }
  }
}
