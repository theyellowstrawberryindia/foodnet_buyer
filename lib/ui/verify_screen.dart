import 'dart:io';

import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/model/ItemInDetailsModel.dart';
import 'package:foodnet_buyer/data/model/OrderModel.dart';
import 'package:foodnet_buyer/data/model/order_details_model.dart';
import 'package:foodnet_buyer/data/repository/order_repository.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/dashboard/dashboard_screen.dart';
import 'package:foodnet_buyer/ui/widgets/cell/cell_item_in_list.dart';
import 'package:foodnet_buyer/util/custome_loader.dart';

class VerifyScreen extends StatefulWidget {
  final OrderData _orderData;

  VerifyScreen(this._orderData);

  @override
  _VerifyScreenState createState() => _VerifyScreenState();
}

class _VerifyScreenState extends State<VerifyScreen> {

  final orderRepo = OrderRepository();
  ItemInDetailsModel _itemInDetailsModel;
  List<Iteminitem> _list =[];
  bool loader= true;
  bool error= false;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds: 500),_getItemInDetails);
  }

  @override
  void dispose() {
    Loader.hide();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => true,
      child: Scaffold(
        appBar:  AppBar(
          centerTitle: true,
          backgroundColor: ColorTheme.white,
          title: Style.getPoppinsRegularText('Verify', 16, ColorTheme.black),
          iconTheme: IconThemeData(color: Colors.black87),
        ),
        body: loader?Center(child: error?Style.getPoppinsRegularText('No Data', 16, ColorTheme.darkGrey): CircularProgressIndicator(),): Container(
          color: ColorTheme.white,
          child: CustomScrollView(
            slivers: [

              SliverToBoxAdapter(
                child: Container(
                  color: Colors.grey[50],
                  padding: const EdgeInsets.all(10),
                  child: Card(
                    elevation: 1,
                    color: ColorTheme.white,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Style.getPoppinsRegularText('ItemIn Details', 16, ColorTheme.black),
                          const SizedBox(height: 10,),
                          Row(
                            children: [
                              Expanded(child: Style.getPoppinsLightText('Gross Weight', 14, ColorTheme.FF999999)),
                              Expanded(child: Style.getPoppinsLightText(':  ${_itemInDetailsModel.itemInData.grossWeight} Kg', 14, ColorTheme.FF999999)),
                            ],
                          ),
                          const SizedBox(height: 10,),
                          Row(
                            children: [
                              Expanded(child: Style.getPoppinsLightText('Photos ', 14, ColorTheme.FF999999)),
                              Container(child: Style.getPoppinsLightText(' : ', 14, ColorTheme.FF999999)),
                              Expanded(child:Container(height: 50,child: ListView(shrinkWrap: true,scrollDirection: Axis.horizontal,
                                  children: _itemInDetailsModel.itemInData.itemindocuments.map((e) {

                                      return Container(
                                        width: 50,
                                        height: 50,
                                        margin: const EdgeInsets.only(right: 6),
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(10),
                                          child: Image.network(e.docUrl,fit: BoxFit.fill,),
                                        ),
                                      );

                                  }).toList(),
                              ))),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SliverList(
                delegate: SliverChildListDelegate(
              [
                Container(height: 20,color: ColorTheme.FFF4F4F4,),
                Container(
                  padding: const EdgeInsets.all(10),
                  child:  Style.getPoppinsRegularText('Items in Scanned Package', 16, ColorTheme.FF999999),
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 8),
                  child:  Row(
                    children: [
                      Style.getPoppinsRegularText('Items'.toUpperCase(), 14, ColorTheme.FF333333),
                      Expanded(child: Container()),
                      Style.getPoppinsRegularText('Total'.toUpperCase(), 14, ColorTheme.FF333333),
                    ],
                  )
                ),
                Container(height: 1,color: ColorTheme.FFF4F4F4,),
              ]  ,
              ),
              ),
              SliverList(delegate: SliverChildListDelegate(
                _list.map<Widget>((e) => CellItemInList(e)).toList(),
              ),

              ),
              SliverList(delegate: SliverChildListDelegate([
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 6),
                  child: Row(
                    children: [
                      Style.getPoppinsRegularText('Item Total (Base Price)', 14, ColorTheme.FF999999),
                      Expanded(child: Container()),
                      Style.rupeeTextView('${_itemInDetailsModel.itemInData.totalBase}', 14, ColorTheme.FF999999),
                    ],
                  ),
                ),
                Padding(
                  padding:  const EdgeInsets.symmetric(horizontal: 10,vertical: 6),
                  child: Row(
                    children: [
                      Style.getPoppinsRegularText('Discount | 0%', 14, ColorTheme.FF999999),
                      Expanded(child: Container()),
                      Style.rupeeTextView('${_itemInDetailsModel.itemInData.discountAmount}', 14, ColorTheme.FF999999),
                    ],
                  ),
                ),
                Padding(
                  padding:  const EdgeInsets.symmetric(horizontal: 10,vertical: 6),
                  child: Row(
                    children: [
                      Style.getPoppinsRegularText('Delivery Charge | Free', 14, ColorTheme.FF999999),
                      Expanded(child: Container()),
                      Style.rupeeTextView('${_itemInDetailsModel.itemInData.convenienceCharges}', 14, ColorTheme.FF999999),
                    ],
                  ),
                ),
                Container(height: 1,color: ColorTheme.FFF4F4F4,margin: const EdgeInsets.symmetric(vertical: 10),),
                Padding(
                  padding:  const EdgeInsets.symmetric(horizontal: 10,vertical: 6),
                  child: Row(
                    children: [
                      Expanded(child: Container()),
                      Style.getPoppinsRegularText('GST', 14, ColorTheme.FF999999),
                      Expanded(child: Container()),
                      Style.rupeeTextView('${_itemInDetailsModel.itemInData.totalTaxes}', 14, ColorTheme.FF999999),
                    ],
                  ),
                ),
                Padding(
                  padding:  const EdgeInsets.symmetric(horizontal: 10,vertical: 6),
                  child: Row(
                    children: [
                      Expanded(child: Container()),
                      Style.getPoppinsRegularText('Total to Pay', 20, ColorTheme.FF333333),
                      Expanded(child: Container()),
                      Style.rupeeTextView('${_itemInDetailsModel.itemInData.totalBase}', 20, ColorTheme.FF333333),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Image.asset(AppAssets.item_out,width: 100,height: 100,fit: BoxFit.contain,),
                ),
                Container(height: 30,),
                Container(
                  alignment: Alignment.center,
                  child:Style.getTextBtnWithElevation('ItemIn', ()=>_itemIn(),width: 200,height: 40),
                ),
                Container(height: 30,),
              ]))
            ],
          ),
        ),
      ),
    );
  }

  _getItemInDetails()async{
    _itemInDetailsModel = await orderRepo.getItemInDetails(widget._orderData.tid.id);
    if(_itemInDetailsModel!=null){
      _list = _itemInDetailsModel.itemInData.iteminitems;
      loader= false;
      error = false;
    }else{
      error = true;
      loader = true;
    }

    setState(() {

    });
  }

  _itemIn()async{
    Loader.show(context);
    final result = await orderRepo.orderItemIn(widget._orderData.id);
    Loader.hide();
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (_)=>DashboardScreen(currentIndex: 2, innerIndex: 1,)), (route) => false);
  }
}
