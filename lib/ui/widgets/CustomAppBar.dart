import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/style/style.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String name;
  final bool automaticallyImplyLeading;
  final Widget leadingWidget;
  final List<Widget> actions;

  CustomAppBar(this.name, {this.automaticallyImplyLeading, this.leadingWidget, this.actions});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      elevation: 2,
      leading: Navigator.canPop(context) ? null : leadingWidget,
//      shadowColor: ColorCode().borderColor,
      automaticallyImplyLeading: automaticallyImplyLeading ?? true,
      iconTheme: IconThemeData(
        color: ColorTheme.FF333333, //change your color here
      ),
      title: Style.getPoppinsLightText(name, AppFonts.titleTextSize, ColorTheme.FF333333),
      backgroundColor: Colors.white,
      actions: actions,
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(kToolbarHeight);
}
