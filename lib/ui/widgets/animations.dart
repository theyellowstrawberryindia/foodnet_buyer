
import 'package:animations/animations.dart';
import 'package:flutter/material.dart';

class OpenContainerWrapper extends StatelessWidget {
  const OpenContainerWrapper({
    this.key,
    this.closedBuilder,
    this.transitionType,
    this.onClosed,
    this.widgetToOpen
  });

  final OpenContainerBuilder closedBuilder;
  final ContainerTransitionType transitionType;
  final ClosedCallback<bool> onClosed;
  final Widget widgetToOpen;
  final Key key;

  @override
  Widget build(BuildContext context) {
    return OpenContainer<bool>(
      key: key,
      transitionType: transitionType,
      transitionDuration: Duration(milliseconds: 400),
      openBuilder: (BuildContext context, VoidCallback _) {
        return widgetToOpen;
      },
      onClosed: onClosed,
      tappable: true,
      closedBuilder: closedBuilder,
      closedShape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(20))
      ),
    );
  }
}
