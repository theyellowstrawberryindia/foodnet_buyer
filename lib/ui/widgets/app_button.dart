import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/ui/widgets/image_view.dart';
import 'package:foodnet_buyer/ui/widgets/text_view.dart';

class AppButton extends StatelessWidget {
  final Key key;
  final String text;
  final String assets;
  final TextStyle style;
  final Function() onClick;

  AppButton({this.key, this.text, this.assets, this.style, this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onClick();
      },
      child: Container(
          width: double.infinity,
          padding: const EdgeInsets.all(6),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(24),
            border: Border.all(color: ColorTheme.primaryColor, width: 0.5),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (assets != null)
                ImageView(
                  asset: assets,
                  color: ColorTheme.primaryColor,
                ),
              const SizedBox(
                width: 10,
              ),
              TextView(
                text: text,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: style,
              ),
            ],
          )),
    );
  }
}
