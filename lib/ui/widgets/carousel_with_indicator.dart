import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/model/product.dart';
import 'package:foodnet_buyer/ui/widgets/netwrok_image_view.dart';

class CarouselWithIndicator extends StatefulWidget {

  final List<dynamic> items;
  final Function(dynamic) getUrl;
  final double height;
  final bool autoScroll;
  final bool infinite;
  final bool showIndicator;
  final double viewPort;
  final BoxFit fit;
  final Function(dynamic) onClick;
  final bool useHeroTag;

  CarouselWithIndicator(this.items, this.getUrl, {Key key, this.height = 140, this.autoScroll = true, this.infinite = true, this.fit = BoxFit.fill, this.onClick, this.showIndicator = true, this.viewPort = 1.0, this.useHeroTag = false}): super(key: key);

  _CarouselWithIndicatorState createState() => _CarouselWithIndicatorState();
}

class _CarouselWithIndicatorState extends State<CarouselWithIndicator> {

  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CarouselSlider(
          options: CarouselOptions(
              height: widget.height,
              viewportFraction: widget.viewPort,
              autoPlay: widget.autoScroll,
              initialPage: 0,
              enableInfiniteScroll: widget.infinite,
              onPageChanged: (index, reason) {
                if (widget.showIndicator && widget.items.length > 1) {
                  setState(() {
                    currentIndex = index;
                  });
                }
              }),
          items: widget.items.map((item) {
            return Builder(
              builder: (BuildContext context) {
                return InkWell(
                  onTap: () {
                    widget.onClick?.call(item);
                  },
                  child: widget.useHeroTag ? Hero(
                    tag: widget.getUrl(item) ?? item.toString(),
                    child: NetworkImageView(widget.getUrl(item),
                        MediaQuery
                            .of(context)
                            .size
                            .width-40,
                        widget.height,
                        radius: 20,
                        margin: EdgeInsets.symmetric(horizontal: 5.0), fit: widget.fit,),
                  ) : NetworkImageView(widget.getUrl(item),
                    MediaQuery
                        .of(context)
                        .size
                        .width-40,
                    widget.height,
                    radius: 20,
                    margin: EdgeInsets.symmetric(horizontal: 5.0), fit: widget.fit,),
                );
              },
            );
          }).toList(),
        ),
        SizedBox(
          height: widget.items.length > 1 ? 6 : 0,
        ),
        widget.items.length > 1 && widget.showIndicator ? Container(
          alignment: Alignment.center,
          height: 3,
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (cntx, index) {
              return Container(
                margin: const EdgeInsets.symmetric(horizontal: 4),
                width: 10,
                height: 1,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(1),
                  color: currentIndex == index
                      ? ColorTheme.primaryColor
                      : ColorTheme.lightGrey,
                ),
              );
            },
            itemCount: widget.items.length,
          ),
        ): SizedBox()
      ],
    );
  }
}