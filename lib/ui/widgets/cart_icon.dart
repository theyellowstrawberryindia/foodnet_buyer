import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/bloc/common_bloc.dart';
import 'package:foodnet_buyer/data/model/cart_response.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';

class CartIcon extends StatefulWidget {

    CartIcon({Key key}): super(key: key);

  _CartIconState createState() => _CartIconState();
}

class _CartIconState extends State<CartIcon> {

  @override
  void initState() {
    CommonBloc.get().addBadgeListener(onBadgeChange);
    super.initState();
  }

  onBadgeChange() {
    setState(() {});
  }

  @override
  void dispose() {
    CommonBloc.get().removeBadgeListener(onBadgeChange);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    CartResponse response = PreferenceManager.getCartResponse();
    int cartCount = 0;
    if (response != null) {
      cartCount = response.shoppingcartitems?.length ?? 0;
    }
    return InkWell(
      onTap: () {
        AppUtils.openCartScreen(context);
      },
      child: Badge(
        toAnimate: true,
        badgeContent: Style.getPoppinsLightText("$cartCount", 10, ColorTheme.white),
        badgeColor: ColorTheme.FFF0674C,
        showBadge: cartCount>0,
        child: SizedBox(
          width: 24,
          height: 24,
          child: Image.asset(
            AppAssets.cart,
            fit: BoxFit.fill,
          ),
        ),
      ),
    );
  }
}