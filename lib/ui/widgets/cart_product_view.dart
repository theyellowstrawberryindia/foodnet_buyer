import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/model/cart_product.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/widgets/netwrok_image_view.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';

class CartProductView extends StatelessWidget {
  final Key key;
  final CartProduct product;
  final Function(CartProduct) onClick;
  final Function(CartProduct) onRemove;
  final double width;
  final double height;

  CartProductView({this.key, this.product, this.onClick, this.onRemove, this.width, this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Ink(
        child: InkWell(
          borderRadius: BorderRadius.circular(20),
          onTap: () {onClick?.call(product);},
          child: Container(
            width: width,
            height: height,
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
              boxShadow: AppUtils.getBoxShadow(),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    NetworkImageView(
                      PreferenceManager.getLocationData()?.sellerurl ?? "", 30, 10, fit: BoxFit.scaleDown,),
                    InkWell(
                      onTap: (){
                        onRemove?.call(product);
                      },
                      child: Image.asset(AppAssets.deleteIcon, width: 16, height: 16,),
                    )
                  ],
                ),
                SizedBox(height: 5,),
                Center(
                  child: NetworkImageView(
                    product.imgUrl,
                    70,
                    70,
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Expanded(
                  child: SizedBox(),
                ),
                AppUtils.getProductName(product.productName, product.subText, 10, ColorTheme.FF333333,),
                Row(
                  children: <Widget>[
                    AppUtils.getWidgetWithRupee(' ${product.price}', AppFonts.textSize9, ColorTheme.FF333333),
//                  const SizedBox(width: 6,),
//                  Style.getPoppinsRegularText('${AppUtils.getRupeeSymbol()}${product.productBaseRate}', AppFonts.textSize8, ColorTheme.FF333333),
//                  SizedBox(width: 6,),
//                  Style.getPoppinsRegularText('${AppUtils.getRupeeSymbol()}${product.productTaxRate}% OFF', AppFonts.textSize9, ColorTheme.FFF97062),
                  ],
                ),

                Style.getPoppinsRegularText("${product.getDisplayPackOff()} ${product.packOfUnit}", AppFonts.textSize9, ColorTheme.FF999999),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
