import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/model/category_response.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/subcatgeory/subcategory_screen.dart';
import 'package:foodnet_buyer/ui/widgets/expand_collapse_view.dart';
import 'package:foodnet_buyer/ui/widgets/netwrok_image_view.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';

class CategoryRow extends StatefulWidget {

  final int childIndex;
  final List<Child> allCategories;
  final String catalogId;
  final Color baseColor;
  final Color boxBaseColor;

  CategoryRow(this.allCategories, this.childIndex, this.catalogId, this.baseColor, this.boxBaseColor, {Key key}): super(key: key);

  @override
  CategoryRowState createState() => CategoryRowState(allCategories[childIndex], baseColor, boxBaseColor);

}

class CategoryRowState extends State<CategoryRow> {

  Child child;
  Color baseColor;
  Color boxBaseColor;
  bool isExpanded = false;

  CategoryRowState(this.child, this.baseColor, this.boxBaseColor);

  @override
  Widget build(BuildContext context) {

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        InkWell(
          onTap: () {
            setState(() {
              isExpanded = !isExpanded;
            });
            if (isExpanded) {
              // FBAnalytics.categoryClicked(child.id?.toString(), child.categoryName, "Category Listing");
            }
          },
          child: Container(
            alignment: Alignment.center,
            width: double.maxFinite,
            height: 120,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  baseColor,
                  ColorTheme.white
                ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight
              ),
              boxShadow: [
                BoxShadow(
                      color: ColorTheme.FF00000014,
                      blurRadius: 4,
                      offset: Offset(2, 0))
              ]
            ),
            child: _getTopRow(),
          ),
        ),
//        isExpanded ? Padding(padding: EdgeInsets.only(left: 20), child: Icon(Icons.arrow_drop_down, size: 20, color: widget.baseColor,),) : SizedBox(),
        _expandedView(),
        SizedBox(height: 2,)
      ],
    );
  }

  _getTopRow() {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width - 149,
          padding: EdgeInsets.only(left: 20, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Style.getPoppinsLightText(child.categoryName, AppFonts.titleTextSize, ColorTheme.FF333333),
                  SizedBox(width: 5,),
                  Image.asset(isExpanded ? AppAssets.upArrow : AppAssets.downArrow, width: 10, height: 10,)
                ],
              ),
              Style.getPoppinsLightText(getChildrenNames(), AppFonts.textSize10, ColorTheme.FF333333)
            ],
          ),
        ),

        Container(
          width: 149,
          height: 120,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                    boxBaseColor,
                    ColorTheme.white
                  ],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight
              ),
              borderRadius: BorderRadius.only(topLeft: Radius.circular(60)),
          ),
          child: NetworkImageView(
            child.categorydocuments[0].documentUrl,
            90,
            90
          ),
        )
      ],
    );
  }

  _expandedView() {
    List<Widget> items = [];
    items.add(Padding(padding: EdgeInsets.only(left: 20), child: Image.asset(AppAssets.downArrowFilled, width: 16, height: 8, color: baseColor,),));
    items.addAll(List.generate(child.children.length, (index) {
      return InkWell(
        onTap: () {
          AppUtils.unfocusScope(context);
          // FBAnalytics.subCategoryClicked(child.children[index].id?.toString(), child.children[index].categoryName, "Category Listing");
          NavigationUtil.pushToNewScreen(context, SubCategoryScreen(widget.allCategories, widget.childIndex, widget.catalogId, selectedTab: index,));
        },
        child: Container(
          alignment: Alignment.center,
          height: 35,
          margin: EdgeInsets.only(left: 40, right: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Style.getPoppinsLightText(child.children[index].categoryName, AppFonts.textSize12, ColorTheme.FF333333),
              SizedBox(width: 20,),
              Style.getPoppinsLightText(">", AppFonts.textSize14, ColorTheme.FF333333)
            ],
          ),
        ),
      );
    }));
    return ExpandedSection(
       expand: isExpanded,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: items
      ),
    );
  }

  String getChildrenNames() {
    return child.children?.map((e) => e.categoryName)?.join(", ") ?? "";
  }
}