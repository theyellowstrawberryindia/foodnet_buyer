import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/widgets/netwrok_image_view.dart';
import 'package:foodnet_buyer/ui/widgets/text_view.dart';

class CellCategory extends StatelessWidget {
  final Key key;
  final String title;
  final String image;
  final Category data;
  final Function onClick;
  final bool isSelected;
  final double width;
  final double height;

  CellCategory({this.key, this.data, this.title, this.image,this.onClick, this.isSelected = false, this.width = 34, this.height = 34}):super(key:key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()=>onClick(),
      child: Container(
        width: 90,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FittedBox(child: data != null ? Image.asset(data.icon,width: width,height: height,color: ColorTheme.FF333333,) : NetworkImageView(image, width, height, color: isSelected ? ColorTheme.FFF97062 : ColorTheme.FF333333,)),
            const SizedBox(height: 4,),
            FittedBox(child: Style.getPoppinsLightText(data != null ? data.title : title, 12, isSelected ? ColorTheme.FFF97062 : ColorTheme.FF333333))
          ],
        ),
      ),
    );
  }

}


class Category{
  int id;
  String title;
  String icon;

  Category(this.id, this.title, this.icon);
}

