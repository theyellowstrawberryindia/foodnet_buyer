import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/model/OrderModel.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:spring/spring.dart';

class CellHorizontalTracing extends StatelessWidget {
  final OrderItem _item;
  final String tabName;

  CellHorizontalTracing(this._item, {this.tabName});

  String eventName = '';
  @override
  Widget build(BuildContext context) {
    // List<int> ids = List<int>.from(_item.tid.businesstids.map((e) => e.id));
    //
    // int lastIds = 0;
    //
    // ids.forEach((element) {
    //   if (lastIds < element) lastIds = element;
    // });
    // if (_item.tid.businesstids.length > 0)
    //   eventName = _item.tid.businesstids
    //       .firstWhere((element) => element.id == lastIds)
    //       .eventName;
      eventName = _item.tid.status
          .firstWhere((element) => element.value.toLowerCase() == "pending",
          orElse: () => null)
          ?.key ?? "";

    return Container(
      // width: MediaQuery.of(context).size.width,
      // alignment: Alignment.center,
      height: 70,
      width: MediaQuery.of(context).size.width * 0.93,
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Center(
        child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: _item.tid.status.length,
          itemBuilder: (_, index) {
            return Container(
              // width: 80,
              height: 70,
              width: MediaQuery.of(context).size.width / 8.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      index.isEven
                          ? SizedBox(
                              height: 20,
                              child: Style.getPoppinsLightText(
                                  _item.tid.status[index].key,
                                  10,
                                  ColorTheme.darkGrey))
                          : SizedBox(
                              height: 20,
                            )
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                          child: index == 0
                              ? Container()
                              : _line(AppUtils.getStatusColor(
                                  _item.tid.status[index - 1].value))),
                      Container(
                          child: _item.tid.status[index].key == eventName
                              ? _activeCircle()
                              : _circle(
                                  8,
                                  AppUtils.getStatusColor(
                                      _item.tid.status[index].value))),
                      Expanded(
                          child:
                              _item.tid.status.last == _item.tid.status[index]
                                  ? Container()
                                  : _line(AppUtils.getStatusColor(
                                      _item.tid.status[index].value))),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      index.isOdd
                          ? SizedBox(
                              height: 20,
                              child: Style.getPoppinsLightText(
                                  _item.tid.status[index].key,
                                  10,
                                  ColorTheme.darkGrey))
                          : SizedBox(
                              height: 20,
                            ),
                      if (index == 6) Expanded(child: SizedBox()),
                    ],
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  _circle(double size, Color color) {
    return Container(
      height: size,
      width: size,
      decoration: BoxDecoration(shape: BoxShape.circle, color: color),
    );
  }

  _activeCircle() {
    return Spring(
      delay: Duration(milliseconds: 1500),
      animType: AnimType.PoP,
      motion: Motion.Loop,
      curve: Curves.easeInOut,
      animStatus: (status) => null,
      child: Container(
        height: 20,
        width: 20,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: ColorTheme.primaryColor.withOpacity(0.3),
        ),
        alignment: Alignment.center,
        child: _circle(6, ColorTheme.primaryColor),
      ),
    );
  }

  _line(Color color) {
    return Container(
      color: color,
      height: 1,
    );
  }
}
