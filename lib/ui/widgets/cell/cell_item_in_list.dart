import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/model/ItemInDetailsModel.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/util/app_utils.dart';

class CellItemInList extends StatelessWidget {
  final Iteminitem _iteminitem;
  final Color color;

  CellItemInList(this._iteminitem, {this.color = Colors.white});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color,
      padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 6),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              AppUtils.getProductName(_iteminitem.productName, _iteminitem.subtext, 16, ColorTheme.FF333333),
              Expanded(child: Container()),
             Style.rupeeTextView('${_iteminitem.totalBase}', 16, ColorTheme.FF333333),
            ],
          ),
          const SizedBox(height: 10,),
          Row(
            children: [
              Expanded(child: _getItems('Base Rate', _iteminitem.basePrice ,rupee: true)),
              Container(width: 0.8,height: 24,color: ColorTheme.FFF4F4F4,margin: const EdgeInsets.only(right: 8),),
              Expanded(child: _getItems('Qty','${_iteminitem.getDisplayQty()} ${_iteminitem.saleUnit??''}' ,rupee: false)),
              Container(width: 60,),
            ],
          ),
          /*Row(
            children: [
              Expanded(child: _getItems('SGST 2.5%', double.parse(_iteminitem.totalTaxes)/2 ,rupee: true)),
              Container(width: 0.8,height: 24,color: ColorTheme.FFF4F4F4,margin: const EdgeInsets.only(right: 8),),
              Expanded(child: _getItems('CGST 2.5%', double.parse(_iteminitem.totalTaxes)/2 ,rupee: true)),
              Container(width: 80,),
            ],
          ),*/
          Row(
            children: [
              Expanded(child: _getItems('Total Qty','${_iteminitem.totalQty}' ,rupee: false)),
              Container(width: 0.8,height: 24,color: ColorTheme.FFF4F4F4,margin: const EdgeInsets.only(right: 8),),
              Expanded(child: SizedBox()),
              Container(width: 60,),
            ],
          ),
          Row(
            children: [
              Expanded(child: _getItems('Unit', _iteminitem.packOfUnit??'' ,rupee: true)),
              Container(width: 0.8,height: 24,color: ColorTheme.FFF4F4F4,margin: const EdgeInsets.only(right: 8),),
              Expanded(child: _getItems('Size', _iteminitem.totalQty ,rupee: false)),
              Container(width: 60,),
            ],
          ),
          Container(height: 1,color: ColorTheme.FFF4F4F4,margin: const EdgeInsets.symmetric(vertical: 10),),
        ],
      ),
    );
  }

  _getItems(String lable,dynamic data, {bool rupee=false}){
    return Row(
      children: [
        Expanded(child: Style.getPoppinsLightText(lable, 12, ColorTheme.FF999999)) ,
        Style.getPoppinsLightText(':  ', 12, ColorTheme.FF999999),
        if(rupee)
        Expanded(child:  Style.rupeeTextView(data.toString(), 12, ColorTheme.FF999999)) ,
        if(!rupee)
          Expanded(child:  Style.getPoppinsLightText(data.toString(), 12, ColorTheme.FF999999)) ,
      ],
    );
  }
}
