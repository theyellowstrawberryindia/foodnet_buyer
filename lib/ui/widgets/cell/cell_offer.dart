import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/ui/widgets/cell/cell_product.dart';
import 'package:foodnet_buyer/ui/widgets/text_view.dart';
import 'package:foodnet_buyer/util/app_utils.dart';

class CellOffer extends StatelessWidget {
  final Key key;
  final Product model;

  CellOffer({this.key, @required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: ColorTheme.primaryColor,
        boxShadow: AppUtils.getBoxShadow(),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          TextView(
            text: 'Flat',
            style: TextStyle(
                fontSize: AppFonts.textSize16, color: ColorTheme.white),
          ),
          TextView(
            text: '${model.discount}% Off'.toUpperCase(),
            style: TextStyle(
                fontSize: AppFonts.textSize20,
                fontWeight: FontWeight.bold,
                color: ColorTheme.white),
          ),
          const SizedBox(
            height: 4,
          ),
          DottedBorder(
            borderType: BorderType.RRect,
            strokeWidth: 2,
            dashPattern: [5,4],
            radius: Radius.circular(12),
            color: ColorTheme.white,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(12)),
              child: Container(
                padding: const EdgeInsets.all(4),
                color: ColorTheme.white.withOpacity(0.3),
                child: Center(
                  child: TextView(
                    text: 'NEW100'.toUpperCase(),
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: AppFonts.textSize16,
                        color: ColorTheme.white),
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 4,
          ),
          TextView(
            text: 'Use this code',
            style: TextStyle(
                fontSize: AppFonts.textSize12, color: ColorTheme.white),
          ),
        ],
      ),
    );
  }
}
