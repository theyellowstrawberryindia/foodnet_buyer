import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/ui/widgets/cell/cell_category.dart';
import 'package:foodnet_buyer/ui/widgets/netwrok_image_view.dart';
import 'package:foodnet_buyer/ui/widgets/text_view.dart';

class CellOfferCategory extends StatelessWidget {

  final Key key;
  final Category data;
  final bool networkImage;
  final Function onClick;

  CellOfferCategory({this.key, this.data, this.networkImage=true,this.onClick}):super(key:key);

  @override
  Widget build(BuildContext context) {
    return  InkWell(
      onTap: ()=>onClick(data.id),
      child: Container(
        width: 100,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FittedBox(child:networkImage ? NetworkImageView(data.icon, 50, 50, radius: 25,): Image.asset(data.icon,width: 34,height: 34,color: ColorTheme.FF333333,)),
            const SizedBox(height: 4,),
            FittedBox(child: TextView(text: data.title,style: TextStyle(fontSize: AppFonts.textSize12),))
          ],
        ),
      ),
    );
  }
}
