import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/model/OrderModel.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/widgets/cell/cell_horizontal_tracking.dart';
import 'package:foodnet_buyer/ui/widgets/custom/app_text.dart';
import 'package:foodnet_buyer/ui/widgets/netwrok_image_view.dart';
import 'package:foodnet_buyer/ui/widgets/text_view.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/date_time_util.dart';
import 'package:foodnet_buyer/util/svgImages.dart';

class CellOrders extends StatelessWidget {
  final OrderItem _item;
  final String tabName;
  final Function(OrderItem item) onClick;
  CellOrders(this._item, this.tabName, {this.onClick});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: const EdgeInsets.all(10),
      child: GestureDetector(
        onTap: () => onClick?.call(_item),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              // margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
              // margin: const EdgeInsets.symmetric(horizontal: 10),
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: AppUtils.getCardColor(_item.status),
                borderRadius: BorderRadius.circular(10),
                border: Border.all(
                  color: AppUtils.getColorBasedOnStatus(_item.status),
                  width: 1.0,
                ),
                // boxShadow: AppUtils.shadow(),
                boxShadow: AppUtils.getBoxShadow(spreadRadius: 0.2),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: NetworkImageView(
                            _item.sellerLogo,
                            40,
                            40,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      Expanded(
                          child: SvgPicture.asset(
                        fssai,
                        height: 30,
                        width: 50,
                      )),
                      Expanded(
                          child: Align(
                              alignment: Alignment.centerRight,
                              child: NetworkImageView(_item.buyerLogo, 40, 40,
                                  fit: BoxFit.contain))),
                    ],
                  ),
                  // const SizedBox(
                  //   height: 20,
                  // ),
                  Container(
                    // height: 100,
                    // color: Colors.red[100],
                    // alignment: Alignment.centerLeft,
                    width: MediaQuery.of(context).size.width * 0.9,
                    padding: const EdgeInsets.only(right: 14.0),
                    child: CellHorizontalTracing(_item),
                  ),
                  // const SizedBox(
                  //   height: 20,
                  // ),
                  Row(
                    children: [
                      Style.getPoppinsLightText(
                          'TID:${_item.tid.id}', 12, ColorTheme.darkGrey),
                      const SizedBox(
                        width: 6,
                      ),
                      Style.getPoppinsLightText(
                          '• ${DateTimeUtil.getDateFromDateTime(_item.deliveryDate, DateTimeUtil.DD_MMM)}',
                          12,
                          ColorTheme.darkGrey),
                      const SizedBox(
                        width: 6,
                      ),
                      Style.getPoppinsLightText(
                          '• ${DateTimeUtil.getFormatedTime(_item.deliveryTime)}',
                          12,
                          ColorTheme.darkGrey),
                      Expanded(
                          child: Align(
                        alignment: Alignment.centerRight,
                        child: _getButtonText(
                            tabName == 'New' && _item.canEdit() ? 'Edit > ' : '$tabName > ',
                            () => onClick?.call(_item)),
                      ))
                    ],
                  )
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              margin: const EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: ColorTheme.white,
                boxShadow: AppUtils.getBoxShadow(spreadRadius: 0.2),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextView(
                          text:
                              'Total Item:${double.parse(_item.lineItems).toStringAsFixed(0)}',
                          style: TextStyle(
                            fontSize: AppFonts.textSize16,
                            color: ColorTheme.black,
                          ),
                        ),
                        AppText.rupeeTextView(_item.finalTotal,
                            AppFonts.textSize14, ColorTheme.darkGrey),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.centerRight,
                      child: TextView(
                        text: _item.status == 'Accepted'
                            ? '${AppAssets.check} Accepted'
                            : '${_item.status}',
                        style: TextStyle(
                          fontSize: AppFonts.textSize16,
                          color: _item.status == 'Rejected'
                              ? ColorTheme.red
                              : ColorTheme.cardStatusGreen,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _getButtonText(String text, Function onClick) {
    return GestureDetector(
      onTap: () => onClick?.call(),
      child: Style.getPoppinsRegularText(text, 14, ColorTheme.primaryColor),
    );
  }
}
