import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/util/app_utils.dart';

import '../netwrok_image_view.dart';

class CellProduct extends StatelessWidget {
  final Key key;
  final Product product;
  final Function onClick;
  final Function onAddCartClick;

  CellProduct({this.key, this.product, this.onClick, this.onAddCartClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onClick(product),
      borderRadius: BorderRadius.circular(20),
      child: Ink(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
          boxShadow: AppUtils.getBoxShadow(),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 15),
                  child: NetworkImageView(
                      product.brandLogo, 40, 20),
                ),
                Container(
                  width: 37,
                  height: 39,
                  alignment: Alignment.topRight,
                  decoration: BoxDecoration(
                      color: ColorTheme.FFF8F8F8,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(0),
                        topRight: Radius.circular(20),
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(0),

                      )
                  ),
                  child: IconButton(
                    onPressed: () => onAddCartClick(product),
                    icon: Icon(
                      Icons.add,
                      color: ColorTheme.darkGrey,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 5,),
            Hero(
              tag: product.id.toString(),
              child: Center(
                child: NetworkImageView(
                  product.image,
                  135,
                  135,
                ),
              ),
            ),
            const SizedBox(
              height: 5,
            ),

            Padding(
              padding: EdgeInsets.only(left: 15),
              child: Style.getPoppinsLightText(product.title, 14, ColorTheme.FF333333),
            ),
            Padding(
              padding: EdgeInsets.only(left: 15),
              child: Row(
                children: <Widget>[
                  AppUtils.getWidgetWithRupee(' ${product.price}', 10, ColorTheme.FF333333, isLight: false),
                  const SizedBox(width: 6,),
                  AppUtils.getWidgetWithRupee(' ${product.offerPrice}', 10, ColorTheme.FF333333, isLight: false),
                  SizedBox(width: 6,),
                  AppUtils.getWidgetWithRupee(' ${product.discount}% OFF', 10, ColorTheme.FFF97062, isLight: false),
                ],
              ),
            ),

            Padding(
              padding: EdgeInsets.only(left: 15),
              child: Style.getPoppinsRegularText(product.tag, 10, ColorTheme.FF999999),
            ),
          ],
        ),
      ),
    );
  }
}

class Product {
  int id;
  String brandLogo;
  String image;
  String title;
  String price;
  String offerPrice;
  String discount;
  String tag;
  String type;

  Product(this.id, this.brandLogo, this.image, this.title, this.price,
      this.offerPrice, this.discount, this.tag,this.type);
}
