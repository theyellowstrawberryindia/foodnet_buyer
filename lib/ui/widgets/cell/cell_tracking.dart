import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/date_time_util.dart';
import 'package:spring/spring.dart';
import '../../order_detail_screen.dart';
class CellTracking extends StatelessWidget {
  final Tracking tracking;
  final bool last;
  final Color color;
  CellTracking(this.tracking,this.last, {this.color = Colors.white});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color,
        child: Stack(
          children: [
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 24,vertical: 0),
              color: color,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 6),
                    child: Style.getPoppinsLightText('${tracking.action}',16, ColorTheme.black)
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 6),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: tracking.list
                          .map((e) => Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: '${e.action} > \n',
                              style: TextStyle(
                                fontSize: 14,
                                color: ColorTheme.black,
                              ),
                            ),
                            TextSpan(
                              text:DateTimeUtil.formatDateTime(e.eventDate),
                              style: TextStyle(
                                fontSize: 12,
                                color: ColorTheme.FF333333,
                              ),
                            ),
                            TextSpan(
                              text:' at ${DateTimeUtil.getFormatedTime(e.eventTime)}',
                              style: TextStyle(
                                fontSize: 12,
                                color: ColorTheme.FF333333,
                              ),
                            ),
                            TextSpan(
                              text:' by ${e.updatedBy} \n',
                              style: TextStyle(
                                fontSize: 12,
                                color: ColorTheme.FF333333,
                              ),
                            ),
                          ],
                        ),
                        textScaleFactor: 1.0,
                      ))
                          .toList(),
                    ),
                  )
                ],
              ),
            ),
            Positioned(
              left: 0,
              top: 0,
              bottom: 0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  (tracking.status == 'Created')
                      ? Spring(
                    delay: Duration(milliseconds: 2000),
                    animType: AnimType.PoP,
                    motion: Motion.Loop,
                    curve: Curves.easeInOut,
                    animStatus: (status) => null,
                    child: ActiveCircle(),
                  )
                      : Circle(AppUtils.getStatusColor(tracking.status)),
                  Expanded(
                    child: last
                        ? Container()
                        : VerticalSeparator(
                        AppUtils.getStatusColor(tracking.status)),
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
class VerticalSeparator extends StatelessWidget {
  final Color color;

  VerticalSeparator(this.color);

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: 1.0,
      color: color,
    );
  }
}

class Circle extends StatelessWidget {
  final Color color;
  final double margin;
  Circle(this.color,{this.margin=6});

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: 8,
      height: 8,
      margin:  EdgeInsets.symmetric(horizontal: margin),
      decoration: BoxDecoration(
        color: color,
        shape: BoxShape.circle,
      ),
    );
  }
}

class ActiveCircle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      width: 18,
      height: 18,
      decoration: BoxDecoration(
        color: ColorTheme.primaryColor.withOpacity(0.3),
        shape: BoxShape.circle,
      ),
      child: Center(
        child: Circle(ColorTheme.primaryColor),
      ),
    );
  }
}