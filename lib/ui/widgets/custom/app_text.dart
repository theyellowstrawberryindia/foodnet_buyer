import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/util/app_utils.dart';

class AppText {
  AppText._();

  static Widget rupeeTextView(String text, double textSize, Color color,
      {FontWeight weight = FontWeight.normal}) {
    try {
      final currencyData = AppUtils.formatCurrency(text);
      final firstResult = currencyData.split('.').first ?? '0';
      final secondResult = currencyData.split('.').last ?? '0';
      return Text.rich(
        TextSpan(
          children: [
            TextSpan(
              text: "",
              style: TextStyle(
                color: color,
                fontSize: textSize,
                fontFamily: 'Roboto-Regular',
              ),
            ),
            TextSpan(
              text: firstResult ?? '00',
              style: TextStyle(
                  color: color, fontSize: textSize, fontWeight: weight),
            ),
            WidgetSpan(
              child: Transform.translate(
                offset: Offset(0, -textSize / 2),
                child: Text(
                  secondResult ?? '00',
                  //superscript is usually smaller in size
                  textScaleFactor: 0.65,
                  style: TextStyle(
                      color: color, fontSize: textSize, fontWeight: weight),
                ),
              ),
            ),
          ],
        ),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        textScaleFactor: 1.0,
      );
    } catch (e) {
      return Container();
    }
  }

  static Widget getText(String text, double textSize, Color color,
      {bool isLight = true}) {
    return Text(
      text ?? '',
      style: TextStyle(
        color: color,
        fontFamily: isLight ? AppFonts.poppinsLight : AppFonts.poppinsRegular,
        fontSize: textSize,
      ),
    );
  }

  static Widget getSubText(String text1, String text2, double textSize) {
    return Text.rich(
      TextSpan(
        children: [
          TextSpan(
            text: text1 ?? '',
            style: TextStyle(
              fontSize: textSize,
              color: ColorTheme.black,
            ),
          ),
          TextSpan(
            text: text2?.isNotEmpty ? '(${text2 ?? ''})' : '',
            style: TextStyle(
              fontSize: (textSize / 2) + 4,
              color: ColorTheme.black,
            ),
          ),
        ],
      ),
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
    );
  }

  static Widget getErrorText(String text) {
    return Center(
      child: Text(
        text ?? '',
        textAlign: TextAlign.center,
        maxLines: 1,
        style: TextStyle(
          color: Colors.grey,
          fontFamily: AppFonts.poppinsRegular,
          fontSize: 16,
        ),
      ),
    );
  }
}
