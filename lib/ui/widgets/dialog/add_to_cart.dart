import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/constant/text_msgs.dart';
import 'package:foodnet_buyer/data/model/location_data.dart';
import 'package:foodnet_buyer/data/model/product.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/widgets/animated_flip_counter.dart';
import 'package:foodnet_buyer/ui/widgets/dialog/edit_quantity_dialog.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';
import 'package:foodnet_buyer/util/shopping_cart_util.dart';

import '../netwrok_image_view.dart';

// ignore: must_be_immutable
class AddToCartDialog extends StatefulWidget {
  Product product;
  Function onComplete;

  AddToCartDialog(this.product, {this.onComplete});

  @override
  _AddToCartDialogState createState() => _AddToCartDialogState(product);
}

class _AddToCartDialogState extends State<AddToCartDialog> {
  int selectedVariantIndex = 0;
  double _qty = 1;
  Product product;
  LocationData locationData;
  double basePrice;
  double mrpPrice;
  double rate;
  String unitName;
  String saleUnit;
  String packOffValueString;
  double packOffValue;
  SizeVariant selectedVariant;

  _AddToCartDialogState(this.product) {
    _qty = product.minQty;
  }

  @override
  void initState() {
    locationData = PreferenceManager.getLocationData();
    _updateProductValues();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Ink(
      width: 280,
      height: product.hasVariants() ? 410 : 360,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: ColorTheme.white,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Align(
            alignment: Alignment.topRight,
            child: InkWell(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(20),
                  bottomLeft: Radius.circular(20)),
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      bottomLeft: Radius.circular(20)),
                  color: ColorTheme.FFF8F8F8,
                ),
                child: Center(
                  child: Icon(
                    Icons.clear,
                    color: Colors.black87,
                    size: 18,
                  ),
                ),
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(left: 30, right: 30, top: 10, bottom: 5),
            child: AppUtils.getProductName(
              product.productName,
              product.subText,
              16,
              ColorTheme.FF333333,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 30, right: 30, bottom: 20),
            child: NetworkImageView(
              PreferenceManager.getLocationData()?.sellerurl ?? "",
              40,
              15,
              fit: BoxFit.fitWidth,
            ),
          ),

          product.hasVariants()
              ? AbsorbPointer(
                  absorbing: !product.isAvailable(),
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    margin: EdgeInsets.only(bottom: 20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16),
                      border:
                          Border.all(width: 0.5, color: ColorTheme.FF333333),
                    ),
                    child: DropdownButton(
                      underline: Container(),
                      isDense: true,
                      icon: Icon(
                        Icons.keyboard_arrow_down,
                        size: 15,
                        color: ColorTheme.FF333333,
                      ),
                      value: selectedVariantIndex,
                      onChanged: (int) {
                        selectedVariantIndex = int;
                        _updateProductValues();
                        setState(() {});
                      },
                      items: product.sizevariants
                          .map((e) => DropdownMenuItem(
                                value: product.sizevariants.indexOf(e),
                                child: Padding(
                                  padding: EdgeInsets.only(right: 20, left: 5),
                                  child: Style.getPoppinsLightText(
                                      "${e?.saleQty} ${e?.baseUom}",
                                      12,
                                      ColorTheme.FF333333),
                                ),
                              ))
                          .toList(),
                    ),
                  ),
                )
              : SizedBox(),
          Container(
              margin: EdgeInsets.only(left: 30, right: 30, bottom: 20),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    borderRadius: BorderRadius.circular(15),
                    onTap: () {
                      if (!product.isAvailable()) {
                        AppUtils.showToast(TextMsgs.OUT_OF_STOCK);
                        return;
                      }
                      setState(() {
                        if (_qty > product.minQty) {
                          _qty -= product.minQty;
                          if (_qty < product.minQty) {
                            _qty = product.minQty;
                          }
                        } else {
                          AppUtils.showToast("Item quantity cannot be less then ${AppUtils.getDisplayValue(product.minQty)}");
                        }
                      });
                    },
                    child: Image.asset(
                      AppAssets.decrementCounter,
                      width: 30,
                      height: 30,
                    ),
                  ),
                  SizedBox(width: 10),
                  InkWell(
                    onTap: () {
                      if (product.isAvailable()) {
                        EditQuantityDialog.showEditQtyDialog(context, _qty, unitName?.toLowerCase() == "kg", (value) {
                          setState(() {
                            _qty = value;
                          });
                        }, minQty: product.minQty);
                      } else {
                        AppUtils.showToast(TextMsgs.OUT_OF_STOCK);
                      }
                    },
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(bottom: 8, left: 10, right: 10),
                          decoration: BoxDecoration(
                            border: Border.all(color: ColorTheme.FF333333, width: .5)
                          ),
                          child: AnimatedFlipCounter(
                            duration: Duration(milliseconds: 300),
                            value: _qty,
                            /* pass in a number like 2014 */
                            color: ColorTheme.FF333333,
                            size: 14,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 4),
                          child: Icon(Icons.edit, color: ColorTheme.FF333333, size: 14,),
                        )
                      ],
                    ),
                  ),
                  SizedBox(width: 10),
                  InkWell(
                    borderRadius: BorderRadius.circular(15),
                    onTap: () {
                      if (!product.isAvailable()) {
                        AppUtils.showToast(TextMsgs.OUT_OF_STOCK);
                        return;
                      }
                      setState(() {
                        setState(() {
                          _qty += product.minQty;
                        });
                      });
                    },
                    child: Image.asset(
                      AppAssets.incrementCounter,
                      width: 30,
                      height: 30,
                    ),
                  ),
                ],
              )),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 6),
            margin: EdgeInsets.only(bottom: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                color: ColorTheme.FFF8F8F8),
            child: Style.getPoppinsLightText(
                "$packOffValueString $unitName x ${AppUtils.getDisplayValue(_qty)} Qty = ${AppUtils.getDisplayValue(packOffValue * _qty)} $unitName",
                10,
                ColorTheme.FF999999),
          ),
          Container(
            height: 35,
            alignment: Alignment.bottomCenter,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 13),
                  child: Text(
                    "${AppUtils.getRupeeSymbol()} ",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: ColorTheme.FF333333, fontSize: 16),
                  ),
                ),
                AnimatedFlipCounter(
                  duration: Duration(milliseconds: 500),
                  value: (mrpPrice * _qty).toDouble(),
                  /* pass in a number like 2014 */
                  color: ColorTheme.FF333333,
                  size: 18,
                  weight: FontWeight.bold,
                ),
              ],
            ),
          ),
          // Container(
          //   alignment: Alignment.center,
          //     width: double.infinity,
          //     height: locationData.isInOperationalHours() ? 57 : 45,
          //     color: ColorTheme.FFF8F8F8,
          //     margin: EdgeInsets.only(top: 20, bottom: 30),
          //     child: Column(
          //         mainAxisAlignment: MainAxisAlignment.start,
          //         crossAxisAlignment: CrossAxisAlignment.center,
          //         children: [
          //           Padding(
          //             padding: EdgeInsets.only(left: 13, right: 13, top: 11 , bottom: locationData.isInOperationalHours() ? 1 : 11),
          //             child: Style.getPoppinsLightText(locationData.isInOperationalHours() ? "Delivery: ${AppUtils.getDeliveryTime(product)}" : "Store timing: ${locationData.getOperationalHoursForDisplay()}", AppFonts.textSize11, ColorTheme.FF050202),
          //           ),
          //           locationData.isInOperationalHours() ? Padding(
          //             padding: EdgeInsets.only(left: 13, right: 13, top: 0, bottom: 11),
          //             child: Style.getPoppinsLightText(AppUtils.getOrderWithinTime(product), AppFonts.textSize11, ColorTheme.FF666666),
          //           ) : SizedBox(),
          //         ]
          //     )
          // ),
          SizedBox(height: 30),
          Style.getTextWithIconButton(
              150,
              40,
              "Add to Cart",
              Image.asset(
                AppAssets.cart,
                width: 21,
                height: 18,
                color: ColorTheme.FFF0674C,
              ), onClick: () {
            if (product.isAvailable()) {
              _addToCart();
            } else {
              AppUtils.showToast(TextMsgs.OUT_OF_STOCK);
            }
          })
        ],
      ),
    );
  }

  _addToCart() async {
    ShoppingCartUtil.get().addProductToCart(
        context, product, _qty, packOffValueString, unitName, saleUnit,
        onComplete: () {
      AppUtils.showToast(TextMsgs.ADDED_TO_CART);
      widget.onComplete?.call();
    }, screen: "Product short detail");
  }

  _updateProductValues() {
    selectedVariant = product.hasVariants()
        ? product.sizevariants[selectedVariantIndex]
        : null;
    if (selectedVariant != null) {
      mrpPrice = double.parse(selectedVariant.salePrice);
      basePrice = double.parse(selectedVariant.productPrice);
      rate = double.parse(selectedVariant.taxRate);
      unitName = selectedVariant.baseUom;
      saleUnit = selectedVariant.sellingUom;
      packOffValueString = selectedVariant.saleQty;
      packOffValue = double.parse(selectedVariant.saleQty);
    } else {
      mrpPrice = double.parse(product.productMrp);
      basePrice = double.parse(product.productBaseRate);
      rate = double.parse(product.productTaxRate);
      unitName = product.sizeUnit;
      saleUnit = product.saleUnit;
      packOffValueString = product.size;
      packOffValue = double.parse(product.size);
    }
  }
}
