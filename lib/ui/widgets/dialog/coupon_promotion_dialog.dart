import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/util/app_utils.dart';

class CouponPromotionDialog extends StatelessWidget {

  final Function onAllow;
  final String title;
  final String msg;

  CouponPromotionDialog([this.title, this.msg, this.onAllow,]);

  @override
  Widget build(BuildContext context) {

    return Ink(
      width: 290,
      height: 445,
      decoration: BoxDecoration(
          color: ColorTheme.white,
          borderRadius: BorderRadius.all(Radius.circular(20))
      ),

      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 30,),
          Style.getPoppinsLightText("Congratulations !", AppFonts.titleTextSize, ColorTheme.FF333333),
          SizedBox(height: 19,),
          Image.asset(AppAssets.couponPlaceholder, width: 214, height: 191,),
          SizedBox(height: 27,),
          Padding(
              padding: EdgeInsets.only(left: 40, right: 40),
              child: Style.getPoppinsLightText(title, AppFonts.textSize14, ColorTheme.FF333333, align: TextAlign.center)
          ),

          Padding(
              padding: EdgeInsets.only(left: 40, right: 40),
              child: Style.getPoppinsMediumText(msg, AppFonts.textSize14, ColorTheme.FF333333, align: TextAlign.center)
          ),
          SizedBox(height: 30,),
          Style.getTextButton(200, 40, "Ok", onClick: onAllow),
          SizedBox(height: 12,),
        ],
      ),
    );
  }

  static showCouponPromotionDialog(BuildContext context, String title, String msg, Function onAllow) {
    AppUtils.showAnimatedDialog(context, CouponPromotionDialog(title, msg, onAllow), false,);
  }
}