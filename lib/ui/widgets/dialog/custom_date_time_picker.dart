import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/util/app_utils.dart';

// ignore: must_be_immutable
class CustomDateTimePickerView extends StatefulWidget {

  DateTime minDate;
  DateTime maxDate;
  Function(DateTime, bool) onComplete;
  bool allowPop;

  CustomDateTimePickerView({this.minDate, this.maxDate, this.onComplete, this.allowPop = true}) {
    if (minDate == null) {
      minDate = DateTime.now();
    }
  }

  CustomDateTimePickerViewState createState() => CustomDateTimePickerViewState(this.minDate, this.maxDate);

  static show(BuildContext context, {DateTime min, DateTime max, Function(DateTime, bool) onComplete, bool allowPop = true}) {
    DateTime minDate = DateTime.now().add(Duration(hours: 2));
    DateTime minimumDate = min.isAfter(minDate) ? min : minDate;
    DateTime maxDate = max != null ? max : minDate.add(Duration(days: 30));
    AppUtils.showBottomSheetView(context, CustomDateTimePickerView(minDate: minimumDate, maxDate: maxDate, onComplete: onComplete, allowPop: allowPop,));
  }
}

class CustomDateTimePickerViewState extends State<CustomDateTimePickerView> {

  DateTime minDate;
  DateTime maxDate;
  bool isChecked = false;
  DateTime selectedDateTime;

  CustomDateTimePickerViewState(this.minDate, this.maxDate) {
    if (minDate == null) {
      minDate = DateTime.now();
    }
  }

  @override
  Widget build(BuildContext context) {

    return Ink(
      color: ColorTheme.white,
      height: (MediaQuery.of(context).copyWith().size.height / 3.5) + 120,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 15, left: 15, right: 15, bottom: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {
                    _onFinish(context, cancel: true);
                  },
                  child: Padding(padding: EdgeInsets.all(5)
                      ,child: Style.getPoppinsRegularText("Cancel", AppFonts.btnTextSize, ColorTheme.FF333333)),
                ),

                InkWell(
                  onTap: () {
                    _onFinish(context);
                  },
                  child: Padding(padding: EdgeInsets.all(5),
                      child: Style.getPoppinsRegularText("Done", AppFonts.btnTextSize, ColorTheme.FFF0674C)),
                ),
              ],
            ),
          ),

          Padding(
            padding: EdgeInsets.only(left: 5, right: 15,),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Checkbox(value: isChecked,
                  activeColor: ColorTheme.FFF0674C,
                  focusColor: ColorTheme.FFF0674C,
                  onChanged: (value){
                    setState(() {
                      isChecked = value;
                    });
                  },
                  checkColor: ColorTheme.white,
                ),
                SizedBox(width: 5,),
                Style.getPoppinsLightText("Change for all same day product", 12, ColorTheme.FF333333),
              ],
            ),
          ),
          SizedBox(height: 10,),

          Container(
            height: (MediaQuery.of(context).copyWith().size.height / 3.5),
            child: Theme(
              data: ThemeData.light().copyWith(
                cupertinoOverrideTheme: CupertinoThemeData(
                  textTheme: CupertinoTextThemeData(
                    dateTimePickerTextStyle: Style.getTextStyleLightPicker(14, ColorTheme.FF333333),
                  )
                ),
              ),
              child: CupertinoDatePicker(
                initialDateTime: minDate,
                onDateTimeChanged: (DateTime newdate) {
                  selectedDateTime = newdate;
                },
                use24hFormat: false,
                minimumDate: minDate,
                maximumDate: maxDate,
                minuteInterval: 1,
                mode: CupertinoDatePickerMode.dateAndTime,
              ),
            ),
          ),
        ],
      ),
    );
  }

  _onFinish(BuildContext context, {bool cancel = false}) {
    if (!cancel) {
      if (widget.allowPop) {
        Navigator.pop(context);
      }
      widget.onComplete?.call(selectedDateTime ?? minDate, isChecked);
    } else {
      Navigator.pop(context);
    }
  }
}