import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/constant/text_msgs.dart';
import 'package:foodnet_buyer/data/model/cart_product.dart';
import 'package:foodnet_buyer/data/model/location_data.dart';
import 'package:foodnet_buyer/data/model/product.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/widgets/animated_flip_counter.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';
import 'package:foodnet_buyer/util/shopping_cart_util.dart';

import '../netwrok_image_view.dart';

// ignore: must_be_immutable
class EditQuantityDialog extends StatefulWidget {
  double qty;
  bool allowDecimal;
  double minQty;
  Function(double) onComplete;

  EditQuantityDialog({this.qty = 0, this.minQty = 1, this.allowDecimal = true, this.onComplete}) {
    if (qty == 0) {
      qty = minQty;
    }
  }

  @override
  _EditQuantityDialogState createState() => _EditQuantityDialogState();

  static showEditQtyDialog(BuildContext context, double qty, bool allowDecimal, Function(double) onComplete, {double minQty = 1}) {
    AppUtils.showAnimatedDialog(context, EditQuantityDialog(qty: qty, allowDecimal: allowDecimal, onComplete: onComplete, minQty: minQty,), true);
  }
}

class _EditQuantityDialogState extends State<EditQuantityDialog> {
    TextEditingController _qtyController = TextEditingController();
  _EditQuantityDialogState();

  @override
  void initState() {
    super.initState();
    _qtyController.text = AppUtils.getDisplayValue(widget.qty);
  }

  @override
  void dispose() {
    _qtyController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Ink(
      width: 280,
      height: 300,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: ColorTheme.white,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Align(
            alignment: Alignment.topRight,
            child: InkWell(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(20),
                  bottomLeft: Radius.circular(20)),
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      bottomLeft: Radius.circular(20)),
                  color: ColorTheme.FFF8F8F8,
                ),
                child: Center(
                  child: Icon(
                    Icons.clear,
                    color: Colors.black87,
                    size: 18,
                  ),
                ),
              ),
            ),
          ),
          Container(
              alignment: Alignment.centerLeft,
              margin: const EdgeInsets.only(left: 30, top: 30, right: 30, bottom: 5),
              child: Style.getPoppinsLightText("Quantity", AppFonts.textSize12, ColorTheme.FF333333)
          ),
          Style.getTextInputField("Enter quantity value", controller: _qtyController, inputType: TextInputType.numberWithOptions(decimal: widget.allowDecimal),
              inputFormatters: widget.allowDecimal ? [FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,3}'))] : [FilteringTextInputFormatter.digitsOnly],
              margin: EdgeInsets.only(
              left: 20, right: 20, bottom: 0), onChanged: (value) {
            
          }, maxLength: 10),
          Padding(
            padding: const EdgeInsets.only(left: 50, right: 50, top: 80.0),
            child: Style.getTextBtnWithElevation("Update", () {
              String qty = _qtyController.text;
              double qtyDouble = 0;
              try {
                qtyDouble = double.parse(qty);
              } catch(e) {
                AppUtils.showToast("Invalid value");
                return;
              }
              if (qtyDouble<widget.minQty) {
                AppUtils.showToast("Quantity should be greater then or equal to minimum order quantity(${AppUtils.getDisplayValue(widget.minQty)})");
                return;
              }
              if (!widget.allowDecimal && qty.contains(".")) {
                AppUtils.showToast("Decimal value not allowed");
                return;
              } else if (widget.allowDecimal) {
                int decimalIndex = qty.indexOf(".");
                if (decimalIndex != -1 && (qty.length-decimalIndex-1)>3) {
                  AppUtils.showToast("Max 3 decimal place is allowed");
                  return;
                }
              }
              widget.onComplete(double.parse(qty));
              NavigationUtil.pop(context);
            }),
          ),
        ],
      ),
    );
  }
}
