import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/style/style.dart';

// ignore: must_be_immutable
class ListOptionDialog extends StatefulWidget {
  List<dynamic> options;
  Function(int) onComplete;
  String Function(dynamic) displayValue;

  ListOptionDialog(this.options, this.onComplete, this.displayValue,);

  @override
  _ListOptionDialogState createState() => _ListOptionDialogState();
}

class _ListOptionDialogState extends State<ListOptionDialog> {

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Ink(
        width: 200,
        height: 240,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: ColorTheme.white,
        ),
        child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Align(
                alignment: Alignment.topRight,
                child: InkWell(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      bottomLeft: Radius.circular(20)),
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    width: 30,
                    height: 30,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(20),
                          bottomLeft: Radius.circular(20)),
                      color: ColorTheme.FFF8F8F8,
                    ),
                    child: Center(
                      child: Icon(
                        Icons.clear,
                        color: Colors.black87,
                        size: 18,
                      ),
                    ),
                  ),
                ),
              ),

              SizedBox(height: 20,),

              for(int i=0;i<widget.options.length;i++)
                InkWell(
                  onTap: (){
                    Navigator.pop(context);
                    widget.onComplete(i);
                  },
                  child: Container(
                    width: double.maxFinite,
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(left: 30, right: 30, top: 10, bottom: 10),
                  margin: EdgeInsets.only(bottom: 10),

                  child: Style.getPoppinsLightText(widget.displayValue(widget.options[i]), 16, ColorTheme.FF333333),
              ),
                ),
            ]
        )

    );
  }
}
