import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/util/app_utils.dart';

class MsgDialog extends StatelessWidget {

  final Function onBtnClick;
  final Function onNegativeClick;
  final String btnName;
  final String btnName2;
  final String msg;

  MsgDialog(this.msg, this.btnName, this.onBtnClick, {this.btnName2, this.onNegativeClick});

  @override
  Widget build(BuildContext context) {

    return Ink(
      width: 320,
      height: btnName2 != null ? 210 : 170,
      decoration: BoxDecoration(
          color: ColorTheme.white,
          borderRadius: BorderRadius.all(Radius.circular(20))
      ),

      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 20,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Style.getPoppinsLightText(msg, AppFonts.textSize14, ColorTheme.FF333333, maxLines: 3),
          ),
          Expanded(child: SizedBox(height: 20,)),
          Style.getTextButton(180, 40, btnName, onClick: onBtnClick),
          SizedBox(height: btnName2 != null ? 12 : 0,),
          btnName2 != null ? Style.getTextButton(180, 40, btnName2, onClick: onNegativeClick) : SizedBox(),
          SizedBox(height: 20,)
        ],
      ),
    );
  }

  static showMsgDialog(BuildContext context, String msg, String btnName, Function onBtnClick, {String btnName2, Function onNegativeClick, bool shouldAutoPop = true, bool isDismissable = false}) {
    AppUtils.showAnimatedDialog(context,
        MsgDialog(msg, btnName, () {
          if (shouldAutoPop) {
            Navigator.pop(context);
          }
          onBtnClick?.call();
        }, btnName2: btnName2, onNegativeClick: () {
          if (shouldAutoPop) {
            Navigator.pop(context);
          }
          onNegativeClick?.call();
        },), isDismissable);
  }
}