import 'dart:io';

import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/add_gross_weight.dart';
import 'package:foodnet_buyer/ui/verify_screen.dart';

class VerifyDialog extends StatelessWidget {
  final String grossWeight;
  final List<ImageData> images;


  VerifyDialog(this.grossWeight, this.images);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
              Container(
                child: Style.getPoppinsRegularText('Please confirm the details you have entered.', 16, ColorTheme.FF333333,maxLines: 2,align: TextAlign.center),
                padding: const EdgeInsets.all(10),
              ),
            Container(
              height: 1,
              color: ColorTheme.FF0000001A,
              margin: const EdgeInsets.symmetric(vertical: 20),
            ),
            Style.getPoppinsRegularText('ItemIn Details:', 16, ColorTheme.FF333333),
            const SizedBox(height: 10,),
            Style.getPoppinsLightText('Gross Weight : ${grossWeight} Kg', 14, ColorTheme.FF999999),
            const SizedBox(height: 10,),
            Container(height: 50,child: ListView(shrinkWrap: true,scrollDirection: Axis.horizontal,
              children: images.map((e) {
                if(e.type!=1)
                  return Container(
                    width: 50,
                    height: 50,
                    margin: const EdgeInsets.only(right: 6),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.file(File(e.path),fit: BoxFit.fill,),
                    ),
                  );
                else return Container();
              }).toList(),
            ),
            ),
            const SizedBox(height: 30,),
            Row(
              children: [
                Expanded(child: FlatButton(onPressed: ()=>Navigator.of(context).pop(false),child: Style.getPoppinsRegularText('Back', 14, ColorTheme.primaryColor),)),
                Expanded(child:Style.getTextBtnWithElevation("Confirm", () {
                  Navigator.of(context).pop(true);
                },
                    height: 40
                ),),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
