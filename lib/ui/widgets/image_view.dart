import 'package:flutter/material.dart';

class ImageView extends StatelessWidget {
  final Key key;
  final String asset;
  final Color color;

  ImageView({this.key, this.asset, this.color = const Color(0xff000000)}):super(key:key);

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      asset,
      color: color,
      width: 24,
      height: 24,
      fit: BoxFit.fill,
    );
  }
}
