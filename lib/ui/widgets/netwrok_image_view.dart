import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';

class NetworkImageView extends StatelessWidget {

  final String image;
  final double width;
  final double height;
  final double radius;
  final EdgeInsets margin;
  final Widget placeHolder;
  final BoxFit fit;
  final Color color;


  NetworkImageView(this.image, this.width, this.height, {this.radius = 4, this.placeHolder, this.margin, this.fit = BoxFit.fill, this.color});

  @override
  Widget build(BuildContext context) {
//    Utils.log("Image Url: $image");
    return image == null
        ? _defaultPlaceHolder()
        : isSVG(image) ? _getSVGImage() : _getCacheNetwrokImage();
  }

  Widget _defaultPlaceHolder() {
    return Container(
      width: width,
      height: height,
      margin: margin,
      decoration: BoxDecoration(
          color: ColorTheme.lightGrey,
          borderRadius: BorderRadius.all(Radius.circular(radius))
      ),
    );
  }

  bool isSVG(String url) => url?.toLowerCase()?.endsWith(".svg") ?? false;

  _getCacheNetwrokImage() {
    return CachedNetworkImage(
      imageUrl: image,
      imageBuilder: (context, imageProvider) => Container(
        width: width,
        height: height,
        margin: margin,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(radius)),
          image: DecorationImage(
//            colorFilter: color != null ? ColorFilter.mode(color, BlendMode.color) : null,
            image: imageProvider,
            fit: fit,),
        ),
      ),
      placeholder: (context, url) => placeHolder ?? _defaultPlaceHolder(),
      errorWidget: (context, url, error) => placeHolder ?? _defaultPlaceHolder(),
    );
  }

  _getSVGImage() {
    return SvgPicture.network(image,
      placeholderBuilder: (context) => placeHolder ?? _defaultPlaceHolder(),
      width: width,
      height: height,
      fit: fit,
      color: color,
    );
  }
}