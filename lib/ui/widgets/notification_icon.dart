import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/bloc/common_bloc.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';

class NotificationIcon extends StatefulWidget {

    NotificationIcon({Key key}): super(key: key);

  _NotificationIconState createState() => _NotificationIconState();
}

class _NotificationIconState extends State<NotificationIcon> {

  @override
  void initState() {
    CommonBloc.get().addNotificationBadgeListener(onBadgeChange);
    super.initState();
  }

  onBadgeChange() {
    setState(() {});
  }

  @override
  void dispose() {
    CommonBloc.get().removeNotificationBadgeListener(onBadgeChange);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (PreferenceManager.isLoggedIn()) {
          // NavigationUtil.pushToNewScreen(context, NotificationListingScreen());
        }
      },
      child: SizedBox(
        width: 24,
        height: 24,
        child: Badge(
          toAnimate: true,
          badgeContent: Style.getPoppinsLightText("", 10, ColorTheme.white),
          badgeColor: ColorTheme.FFF0674C,
          showBadge: PreferenceManager.showNotificationBadge(),
          position: BadgePosition.topEnd(end: 0),
          child: Image.asset(
            AppAssets.notification,
            fit: BoxFit.fill,
          ),
        ),
      ),
    );
  }
}