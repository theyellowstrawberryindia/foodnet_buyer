import 'package:flutter/material.dart';
import 'package:foodnet_buyer/data/data_manager.dart';
import 'package:foodnet_buyer/data/model/product.dart';
import 'package:foodnet_buyer/ui/widgets/netwrok_image_view.dart';
import 'package:foodnet_buyer/util/app_utils.dart';

// ignore: must_be_immutable
class OfferView extends StatelessWidget {
  final Key key;
  final Product model;
  String catalogId;

  OfferView({this.key, @required this.model}) : super(key: key) {
    catalogId = "1"/*DataManager.get().getLocationData().productCatalogId*/;
  }

  @override
  Widget build(BuildContext context) {

      return Container(
        height: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          boxShadow: AppUtils.getBoxShadow(),
        ),
        child: NetworkImageView(model.documentUrl, double.infinity, double.infinity, radius: 10,),
      );
  }
}
