import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/model/order_details_model.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/order/comment_screen.dart';
import 'package:foodnet_buyer/ui/order/photo_screen.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';

import '../edit_order_screen.dart';
import '../order/gross_weight_screen.dart';
class OrderOperationView extends StatefulWidget {
  final Function updateCaller;
  final OrderData orderData;
  final String status;
  final int itemInId;
  final int grnId;
  final String preFillGrossWeight;
  final String preFillComment;
  final String dcGrossWeight;
  final String itemInGrossWeight;
  final bool photoDone;
  final bool grwtDone;
  final bool commentDone;

  OrderOperationView(this.orderData, this.status, this.updateCaller, {this.itemInId, this.grnId, this.preFillGrossWeight, this.preFillComment, this.dcGrossWeight, this.itemInGrossWeight, this.photoDone = true, this.grwtDone = true, this.commentDone = true});

  @override
  OrderOperationViewState createState() => OrderOperationViewState(orderData, status, updateCaller, itemInId: itemInId, grnId: grnId, preFillGrossWeight: preFillGrossWeight, preFillComment: preFillComment, dcGrossWeight: dcGrossWeight, itemInGrossWeight: itemInGrossWeight, photoDone: photoDone, grwtDone: grwtDone, commentDone: commentDone);

}

class OrderOperationViewState extends State<OrderOperationView> {
  static const String _EDIT = "Edit";
  static const String _PHOTO = "Photos";
  static const String _GROSS_WEIGHT = "Gr Wt";
  static const String _COMMENT = "Comment";
  static const String _PRINT = "Print";

  final Function updateCaller;
  final OrderData orderData;
  String status;
  final List<Map<String,dynamic>> options = [];
  final int itemInId;
  final int grnId;
  final String preFillGrossWeight;
  final String preFillComment;
  final String dcGrossWeight;
  final String itemInGrossWeight;
  final bool photoDone;
  bool grwtDone;
  bool commentDone;


  OrderOperationViewState(this.orderData, this.status, this.updateCaller, {this.itemInId, this.grnId, this.preFillGrossWeight, this.preFillComment, this.dcGrossWeight, this.itemInGrossWeight, this.photoDone, this.grwtDone, this.commentDone}) {
    options.add({"name" : _EDIT, "image" : Icons.edit});
    options.add({"name" : _PHOTO, "image" : Icons.camera_alt_outlined});
    options.add({"name" : _GROSS_WEIGHT, "image" : "weight_balance.png"});
    options.add({"name" : _COMMENT, "image" : Icons.comment_outlined});
    options.add({"name" : _PRINT, "image" : Icons.print_outlined});
  }

  bool _canClick(String eventName) {
    switch (eventName) {
      case _EDIT: return orderData.status.statusText == "New" || (status == "GRN" && AppUtils.isPendingAt(orderData.tid.status, "GRN"));
      // case _GROSS_WEIGHT: return status != "PO";
      // case _COMMENT: return (status == "GRN" && AppUtils.isPendingAt(orderData.tid.status, "GRN"));
      case _PRINT: return false;
    }
    return true;
  }

  bool _isDone(String eventName) {
    switch (eventName) {
      case _PHOTO: return photoDone;
      case _GROSS_WEIGHT: return grwtDone;
      case _COMMENT: return commentDone;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.grey.shade300,
        border: Border.all(color: ColorTheme.FF999999, width: .5),
        borderRadius: BorderRadius.all(Radius.circular(10))
      ),
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: _getOptionsView(context),
      ),
    );
  }

  List<Widget> _getOptionsView(BuildContext context) {
    List<Widget> views = [];
    double dimen = (MediaQuery.of(context).size.width - 120)/options.length;
    views.add(SizedBox(width: 5,));
    options.forEach((element) {
      views.add(Column(
        children: [
          Icon(Icons.circle, size: 5, color: _isDone(element["name"]) ? Colors.green : Colors.black,),
          SizedBox(height: 5,),
          InkWell(
            onTap: _canClick(element["name"]) ? () {
              _handleClick(context, element["name"]);
            } : () {
              if (element["name"] != _PRINT) {
                AppUtils.showToast("Not allowed at this stage");
              }
            },
            child: Column(
              children: [
                Container(
                  width: dimen,
                  height: dimen,
                  decoration: BoxDecoration(
                      color: ColorTheme.white,
                      border: Border.all(color: ColorTheme.FF171F2B, width: .5),
                      borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                  margin: EdgeInsets.only(bottom: 4),
                  padding: EdgeInsets.all(5),
                  child: element["image"] is String ?  Container(alignment: Alignment.center, child: Image.asset("assets/images/${element["image"]}", height: dimen * .5, width: dimen * .5,)) : Icon(element["image"], size: dimen * .4,)/*Image.asset("assets/images/${element["image"]}", height: 20, width: 20,)*/
                ),
                Style.getPoppinsLightText(element["name"]?.toString(), dimen * .19, ColorTheme.FF333333, maxLines: 2, align: TextAlign.center)
              ],
            ),
          ),
        ],
      ));
      views.add(SizedBox(width: 5,));
    });
    return views;
  }

  _handleClick(BuildContext context, String name) async {
    switch(name) {
      case _EDIT:
        var result = await NavigationUtil.pushToNewScreen(context, EditOrderScreen(orderData.tid.id, status == "GRN" ? 2  : 1));
        if (result) {
          updateCaller?.call();
        }
        break;
      case _PHOTO:
        var result = await NavigationUtil.pushToNewScreen(context, PhotoScreen(status, orderData.tid.id, itemInId: itemInId, grnId: grnId, allowAddPhotos: (status == "ItemIn" && AppUtils.isPendingAt(orderData.tid.status, "ItemIn")) || (status == "GRN" && AppUtils.isPendingAt(orderData.tid.status, "GRN"))));
        if (result) {
          updateCaller?.call();
        }
        break;
      case _GROSS_WEIGHT:
        var result = await NavigationUtil.pushToNewScreen(context, GrossWeightScreen(status, orderData.tid.id, itemInId: itemInId, grnId: grnId, preFillGrossWeight: preFillGrossWeight, allowEdit: (status == "ItemIn"  && AppUtils.isPendingAt(orderData.tid.status, "ItemIn")) || (status == "GRN"  && AppUtils.isPendingAt(orderData.tid.status, "GRN")), showApiData: AppUtils.isCompletedAt(orderData.tid.status, "DC"),));
        if (result) {
          updateCaller?.call();
        }
        break;
      case _COMMENT:
        var result = await NavigationUtil.pushToNewScreen(context, CommentScreen(status, orderData.tid.id, grnId: grnId, preFillComment: preFillComment, allowEdit: (status == "GRN"  && AppUtils.isPendingAt(orderData.tid.status, "GRN")),));
        if (result) {
          updateCaller?.call();
        }
        break;
      default:
        // AppUtils.showToast("Operation not available");
    }
  }
}