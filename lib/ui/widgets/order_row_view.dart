import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/model/cart_response.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/date_time_util.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';
import 'package:intl/intl.dart';
import 'package:timeline_tile/timeline_tile.dart';

import 'expand_collapse_view.dart';
import 'netwrok_image_view.dart';

class OrderRowView extends StatefulWidget {
  final CartResponse response;
  OrderRowView(this.response);

  _OrderRowState createState() => _OrderRowState(this.response);
}

class _OrderRowState extends State<OrderRowView> {
  CartResponse response;
  bool isExpanded = false;
//  CartBloc _cartBloc;

  _OrderRowState(this.response);

  @override
  void initState() {
//    _cartBloc = CartBloc();
//    _cartBloc.add(GetOrderDetailEvent(response.id.toString()));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _buildView();
  }

  Widget _errorView() {
    return Container(
      alignment: Alignment.center,
      height: 100,
      child: Style.getPoppinsLightText("Unable to get data", 14, ColorTheme.FF333333),
    );
  }

  Widget _loaderView() {
    return Container(
      alignment: Alignment.center,
      height: 100,
      child: CircularProgressIndicator(),
    );
  }

  Widget _buildView() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListTile(
          leading: Container(alignment: Alignment.center,
              width: 30,
              child: NetworkImageView(PreferenceManager
                  .getLocationData()
                  ?.sellerurl ?? "", 30, 30, fit: BoxFit.scaleDown,)),
          trailing: AppUtils.getWidgetWithRupee(" ${response.finalTotal}", AppFonts.textSize14,
              ColorTheme.FF333333),
          title: Style.getPoppinsLightText(PreferenceManager
              .getLocationData()
              ?.groupName ?? "NA", 14, ColorTheme.FF333333),
          subtitle: Style.getPoppinsRegularText(
              "${response.lineItems} Products\nDelivery: ${DateTimeUtil.getFormatedDate(response.deliveryDate)} | ${DateTimeUtil.convertToHHMM(response.deliveryTime)}", 12, ColorTheme.FF999999, maxLines: 2),
          isThreeLine: true,
          onTap: () {
            setState(() {
              isExpanded = !isExpanded;
            });
          },
          contentPadding: EdgeInsets.only(left: 20, right: 20),
        ),
        ExpandedSection(
            expand: isExpanded,
            child: subView()),

        Divider(height: 5, color: ColorTheme.FFF4F4F4, thickness: 5,),
      ],
    );
  }

  double subItemWidth = 60;

  Widget subView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10,),

        SizedBox(
          height: 80,
          child: TimelineTile(
            isFirst: true,
            alignment: TimelineAlign.manual,
          lineXY: .08,
            afterLineStyle: LineStyle(
              color: ColorTheme.FFE3E3E3,
              thickness: 1
            ),
            indicatorStyle: IndicatorStyle(
              height: 8,
              width: 8,
              drawGap: false,
              indicatorXY: 0.27,
              indicator: Container(width: 4, height: 4, decoration: BoxDecoration(
                color: ColorTheme.FFF0674C,
                borderRadius: BorderRadius.all(Radius.circular(4))
              ),),
              color: ColorTheme.FFF0674C.withOpacity(.2)
            ),
//          startChild: SizedBox(width: 20),
            endChild: Container(
              height: 60,
              margin: EdgeInsets.only(left: 10, right: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Style.getPoppinsLightText("Order Placed", 14, ColorTheme.FFF0674C),
                  SizedBox(height: 3,),
                  Style.getPoppinsLightText("${DateFormat("dd MMM, yyyy").format(DateTime.now())} at ${DateFormat("hh:mm a").format(DateTime.now())?.toLowerCase()}", 12, ColorTheme.FF333333.withOpacity(.6)),
                ],
              ),
            ),
          ),
        ),
        SizedBox(
          height: 60,
          child: TimelineTile(
            alignment: TimelineAlign.manual,
            lineXY: .08,
            afterLineStyle: LineStyle(
                color: ColorTheme.FFE3E3E3,
                thickness: 1
            ),
            beforeLineStyle: LineStyle(
                color: ColorTheme.FFE3E3E3,
                thickness: 1
            ),
            indicatorStyle: IndicatorStyle(
                height: 8,
                width: 8,
                drawGap: false,
                indicatorXY: 0.37,
                color: ColorTheme.FFE3E3E3
            ),
//          startChild: SizedBox(width: 20),
            endChild: Container(
              height: 40,
              margin: EdgeInsets.only(left: 10, right: 20),
              child: Style.getPoppinsLightText("Packed", 14, ColorTheme.FF333333),
            ),
          ),
        ),
        SizedBox(
          height: 50,
          child: TimelineTile(
            alignment: TimelineAlign.manual,
            lineXY: .08,
            afterLineStyle: LineStyle(
                color: ColorTheme.FFE3E3E3,
                thickness: 1
            ),
            beforeLineStyle: LineStyle(
                color: ColorTheme.FFE3E3E3,
                thickness: 1
            ),
            indicatorStyle: IndicatorStyle(
                height: 8,
                width: 8,
                drawGap: false,
                indicatorXY: 0.3,
                color: ColorTheme.FFE3E3E3
            ),
//          startChild: SizedBox(width: 20),
            endChild: Container(
              height: 40,
              margin: EdgeInsets.only(left: 10, right: 20),
              child: Style.getPoppinsLightText("Item Out", 14, ColorTheme.FF333333),
            ),
          ),
        ),
        SizedBox(
          height: 30,
          child: TimelineTile(
            alignment: TimelineAlign.manual,
            lineXY: .08,
            isLast: true,
            afterLineStyle: LineStyle(
                color: ColorTheme.FFE3E3E3,
                thickness: 1
            ),
            beforeLineStyle: LineStyle(
                color: ColorTheme.FFE3E3E3,
                thickness: 1
            ),
            indicatorStyle: IndicatorStyle(
                height: 8,
                width: 8,
                drawGap: false,
                indicatorXY: 0.3,
                color: ColorTheme.FFE3E3E3
            ),
//          startChild: SizedBox(width: 20),
            endChild: Container(
              height: 40,
              margin: EdgeInsets.only(left: 10, right: 20),
              child: Style.getPoppinsLightText("Delivered", 14, ColorTheme.FF333333),
            ),
          ),
        ),
        SizedBox(height: 10,),
        _getDivider(),
        SizedBox(height: 15,),
        Padding(padding: EdgeInsets.only(left: 20, right: 20, bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Style.getPoppinsRegularText("ITEMS", AppFonts.textSize12,  ColorTheme.FF333333),
              SizedBox(width: 10,),
              Style.getPoppinsRegularText("TOTAL", AppFonts.textSize12,  ColorTheme.FF333333),
            ],
          ),),
        _getDivider(),
        for(int i=0;i<response.shoppingcartitems.length;i++)
          Padding(
            padding: EdgeInsets.only(left: 20, right: 20,),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(child: AppUtils.getProductName(response.shoppingcartitems[i].productName, response.shoppingcartitems[i].subText, AppFonts.textSize12, ColorTheme.FF333333,)),
                    SizedBox(width: 20,),
                    AppUtils.getWidgetWithRupee(" ${(double.parse(response.shoppingcartitems[i].totalBase)+double.parse(response.shoppingcartitems[i].totalTaxes)).toStringAsFixed(2)}", AppFonts.textSize12,  ColorTheme.FF333333),
                  ],
                ),
                SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(width: 60, child: Style.getPoppinsLightText("Price", AppFonts.textSize10,  ColorTheme.FF999999)),
                    SizedBox(width: 80, child: AppUtils.getWidgetWithRupee(" ${response.shoppingcartitems[i].price}", AppFonts.textSize10,  ColorTheme.FF999999, prefixText: ": ")),
                    Container(height: 15, color: ColorTheme.FFE3E3E3, width: .5, margin: EdgeInsets.only(left: 10, right: 15),),
                    SizedBox(width: 60, child: Style.getPoppinsLightText("QTY", AppFonts.textSize10,  ColorTheme.FF999999)),
                    SizedBox(width: 80, child: Style.getPoppinsLightText(": ${response.shoppingcartitems[i].quantity} ${response.shoppingcartitems[i].saleUnit}", AppFonts.textSize10,  ColorTheme.FF999999)),
                  ],
                ),
                SizedBox(height: 5,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(width: 60, child: Style.getPoppinsLightText("Pack Off", AppFonts.textSize10,  ColorTheme.FF999999)),
                    SizedBox(width: 80, child: Style.getPoppinsLightText(": ${response.shoppingcartitems[i].packOf} ${response.shoppingcartitems[i].packOfUnit}", AppFonts.textSize10,  ColorTheme.FF999999)),
                    Container(height: 15, color: ColorTheme.FFE3E3E3, width: .5, margin: EdgeInsets.only(left: 10, right: 15),),
                    SizedBox(width: 60, child: Style.getPoppinsLightText("Tax", AppFonts.textSize10,  ColorTheme.FF999999)),
                    SizedBox(width: 80, child: Style.getPoppinsLightText(": ${response.shoppingcartitems[i].totalTaxes}", AppFonts.textSize10,  ColorTheme.FF999999)),
                  ],
                ),
                SizedBox(height: 15,),
                Divider(height: .5, color: ColorTheme.FFE3E3E3,)
              ],
            ),
          ),
        SizedBox(height: 10,),
        _getBillRow("Item Total", response.totalBase),
        _getBillRow("GST", response.totalTaxes),
        _getBillRow("Convenience Fee", response.convenienceCharges),
        _getBillRow("Discount", response.discountAmount),
        SizedBox(height: 5,),
        _getDivider(),

//        Padding(padding: EdgeInsets.only(left: 20, right: 20, bottom: 3, top: 8),
//          child: Row(
//            mainAxisAlignment: MainAxisAlignment.end,
//            crossAxisAlignment: CrossAxisAlignment.center,
//            children: [
//              Style.getPoppinsLightText("Taxes", AppFonts.textSize12,  ColorTheme.FF666666),
//              Container(width: 120, alignment: Alignment.centerRight,child: AppUtils.getWidgetWithRupee(" ${response.totalTaxes}", AppFonts.textSize12, ColorTheme.FF666666)),
//            ],
//          ),),

        Padding(padding: EdgeInsets.only(left: 20, right: 20, bottom: 8, top: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Style.getPoppinsLightText("Total to Pay", AppFonts.textSize14,  ColorTheme.FF333333),
              Container(width: 120, alignment: Alignment.centerRight,child: AppUtils.getWidgetWithRupee(" ${response.finalTotal}", AppFonts.textSize14, ColorTheme.FF333333)),
            ],
          ),),
        _getDivider(),
        // Padding(
        //   padding: EdgeInsets.only(left: 20, right: 20, bottom: 5, top: 5),
        //   child: Style.getPoppinsLightText("Transaction ID : ${widget.transactionId}", 12, ColorTheme.FF333333.withOpacity(.6)),
        // ),
        // _getDivider(),
        SizedBox(height: 15,),
        Padding(padding: EdgeInsets.only(left: 20, right: 20,  bottom: 5),
          child: Style.getPoppinsLightText("Delivery Address", AppFonts.textSize14, ColorTheme.FF333333),),
        Padding(
          padding: EdgeInsets.only(left: 20, right: 20,  bottom: 0),
          child: Style.getPoppinsLightText(PreferenceManager.getName() ?? "Guest", AppFonts.textSize12, ColorTheme.FF333333),
        ),
        Padding(
          padding: EdgeInsets.only(left: 20, right: 20,  bottom: 3),
          child: Style.getPoppinsLightText(PreferenceManager.getUserSelectedAddress() != null ? AppUtils.addressFromObj(PreferenceManager.getUserSelectedAddress()) : "NA", AppFonts.textSize12, ColorTheme.FF666666, maxLines: 3),
        ),

        PreferenceManager.getMobile() != null ? Padding(
          padding: EdgeInsets.only(left: 20, right: 20,  bottom: 3),
          child: Style.getPoppinsLightText("Phone no.: ${PreferenceManager.getMobile() ?? "NA"}", AppFonts.textSize12, ColorTheme.FF666666,),
        ) : SizedBox(),

        SizedBox(height: 20,),
      ],
    );
  }

  Widget _getBillRow(String leftText, String rightText, {bool makeBold = false}) {
    return Padding(padding: EdgeInsets.only(left: 20, right: 20, bottom: 3),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Style.getPoppinsLightText(leftText, makeBold ? AppFonts.textSize14 : AppFonts.textSize12,  makeBold ? ColorTheme.FF333333 : ColorTheme.FF666666),
          SizedBox(width: 10,),
      AppUtils.getWidgetWithRupee(" $rightText", makeBold ? AppFonts.textSize14 : AppFonts.textSize12, makeBold ? ColorTheme.FF333333 : ColorTheme.FF666666),
        ],
      ),);
  }

  Widget _getDivider() => Padding(padding: EdgeInsets.only(left: 20, right: 20,), child: Divider(height: .5, color: ColorTheme.FFE3E3E3,),);
  Widget _getVerticalDivider() => Padding(padding: EdgeInsets.only(left: 10, right: 10,), child: Divider(height: 10, color: ColorTheme.FFE3E3E3, thickness: 2,),);
}