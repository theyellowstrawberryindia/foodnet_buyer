import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class GalleryPhotoViewWrapper extends StatefulWidget {
  GalleryPhotoViewWrapper({
    this.loadingBuilder,
    this.backgroundDecoration,
    this.minScale,
    this.maxScale,
    this.initialIndex,
    @required this.galleryItems,
    this.scrollDirection = Axis.horizontal,
  }) : pageController = PageController(initialPage: initialIndex);

  final LoadingBuilder loadingBuilder;
  final Decoration backgroundDecoration;
  final dynamic minScale;
  final dynamic maxScale;
  final int initialIndex;
  final PageController pageController;
  final List<String> galleryItems;
  final Axis scrollDirection;

  @override
  State<StatefulWidget> createState() {
    return _GalleryPhotoViewWrapperState();
  }
}

class _GalleryPhotoViewWrapperState extends State<GalleryPhotoViewWrapper> {
  int currentIndex;

  @override
  void initState() {
    currentIndex = widget.initialIndex;
    super.initState();
  }

  void onPageChanged(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: widget.backgroundDecoration,
        constraints: BoxConstraints.expand(
          height: MediaQuery.of(context).size.height,
        ),
        child: Stack(
          children: [
            PhotoViewGallery.builder(
//              scrollPhysics: const BouncingScrollPhysics(),
              builder: _buildItem,
              itemCount: widget.galleryItems.length,
              loadingBuilder: widget.loadingBuilder ?? (context, _progress) => Center(
                child: Container(
                  width: 40.0,
                  height: 40.0,
                  child: CircularProgressIndicator(
                    value: _progress == null
                        ? null
                        : _progress.cumulativeBytesLoaded /
                        _progress.expectedTotalBytes,
                  ),
                ),
              ),
              backgroundDecoration: widget.backgroundDecoration,
              pageController: widget.pageController,
              onPageChanged: onPageChanged,
              scrollDirection: widget.scrollDirection,
            ),
            Container(
              alignment: Alignment.topLeft,
              margin: EdgeInsets.only(left: 10, top: 35),
              child: InkWell(
                  onTap: () {
                    NavigationUtil.pop(context);
                  },
                  child: Padding(
                    padding: EdgeInsets.all(5),
                    child: Icon(Icons.arrow_back, color: ColorTheme.FFFDFDFD,),
                  ),
              ),
            ),
            widget.galleryItems.length > 1 ? Container(
              alignment: Alignment.bottomCenter,
              margin: EdgeInsets.only(left: 15, right: 15, bottom: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      if (currentIndex > 0) {
                        widget.pageController.animateToPage(currentIndex - 1,
                            duration: Duration(milliseconds: 200),
                            curve: Curves.linear);
                      }
                    },
                    child: Padding(
                      padding: EdgeInsets.all(5),
                      child: Icon(Icons.arrow_back_ios, color: ColorTheme.FFFDFDFD,),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      if (currentIndex < widget.galleryItems.length-1) {
                        widget.pageController.animateToPage(currentIndex + 1,
                            duration: Duration(milliseconds: 200),
                            curve: Curves.linear);
                      }
                    },
                    child: Padding(padding: EdgeInsets.all(5),child: Icon(Icons.arrow_forward_ios, color: ColorTheme.FFFDFDFD,)),
                  ),
                ],
              ),
            ) : SizedBox()
          ],
        ),
      ),
    );
  }

  PhotoViewGalleryPageOptions _buildItem(BuildContext context, int index) {

    return PhotoViewGalleryPageOptions(
      imageProvider: NetworkImage(widget.galleryItems[index]),
      initialScale: PhotoViewComputedScale.contained,
      minScale: PhotoViewComputedScale.contained * (0.5 + index / 10),
      maxScale: PhotoViewComputedScale.covered * 3,
      heroAttributes: PhotoViewHeroAttributes(tag: widget.galleryItems[index]),
    );
  }
}