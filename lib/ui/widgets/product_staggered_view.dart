import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:foodnet_buyer/data/model/product.dart';
import 'package:foodnet_buyer/ui/widgets/scroll_item_view.dart';

class ProductStaggeredView extends StatefulWidget {

  final List<Product> products;
  final GlobalKey rootKey;
  final GlobalKey cartKey;
  final String screen;
  ProductStaggeredView(this.products, this.rootKey, this.cartKey, this.screen, {Key key}): super(key: key);

  _ProductStaggeredViewState createState() => _ProductStaggeredViewState(products, rootKey, cartKey);

}
class _ProductStaggeredViewState extends State<ProductStaggeredView> {

  final List<Product> products;
  final GlobalKey rootKey;
  final GlobalKey cartKey;
  bool initalised = false;
  _ProductStaggeredViewState(this.products, this.rootKey, this.cartKey);

  @override
  void initState() {
//    AppUtils.delayTask((){
//      setState(() {
//        initalised = true;
//      });
//    }, delay: Duration(milliseconds: 300));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Builder(
          builder: (ctx) {
            return CustomScrollView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              slivers: [
                SliverStaggeredGrid.countBuilder(
                  key: UniqueKey(),
                  crossAxisCount: 2,
                  mainAxisSpacing: 15.0,
                  crossAxisSpacing: 15.0,
                  itemCount: products.length,
                  itemBuilder: (ctx, index) {
                    Product e = products[index];
                    return ScrollItemView(e, cartKey, rootKey, widget.screen, key: UniqueKey(),);
                  },
                  staggeredTileBuilder: (index) {
                    Product e = products[index];
                    return StaggeredTile.count(
                        1, e.isOffer() ? 0.95 : (267 / ((MediaQuery
                        .of(context)
                        .size
                        .width - 30) / 2)));
                  },
                )
              ],
            );
          }
      ),
    );
  }
}