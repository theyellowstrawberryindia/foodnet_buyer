import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_font.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/model/product.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/widgets/netwrok_image_view.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';

class ProductView extends StatelessWidget {
  final Key key;
  final Product product;
  final Function onClick;
  final Function onAddCartClick;
  final Key heroKey;

  ProductView(
      {this.key, this.product, this.onClick, this.onAddCartClick, this.heroKey})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(20),
      onTap: () {
        onClick(product);
      },
      child: Ink(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
          boxShadow: AppUtils.getBoxShadow(),
        ),
        child: Hero(
          tag: heroKey.toString(),
          child: Material(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            child: Container(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.only(left: 15),
                          child: NetworkImageView(
                            PreferenceManager.getLocationData()?.sellerurl ??
                                "",
                            30,
                            10,
                            fit: BoxFit.scaleDown,
                          ),
                        ),
                        Container(
                          width: 37,
                          height: 39,
                          alignment: Alignment.topRight,
                          decoration: BoxDecoration(
                              color: ColorTheme.FFF8F8F8,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(0),
                                topRight: Radius.circular(20),
                                bottomLeft: Radius.circular(10),
                                bottomRight: Radius.circular(0),
                              )),
                          child: IconButton(
                            onPressed: () => onAddCartClick(product),
                            icon: Icon(
                              Icons.add,
                              color: ColorTheme.darkGrey,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Center(
                      child: NetworkImageView(
                        product.documentUrl,
                        135,
                        135,
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Expanded(child: SizedBox()),
                    Padding(
                      padding: EdgeInsets.only(left: 15, right: 10),
                      child: AppUtils.getProductName(
                        product.productName,
                        product.subText,
                        AppFonts.textSize12,
                        ColorTheme.FF333333,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 15, right: 10),
                      child: Row(
                        children: <Widget>[
                          AppUtils.getWidgetWithRupee(' ${product.productMrp}',
                              AppFonts.textSize10, ColorTheme.FF333333,
                              isLight: false),
//                  const SizedBox(width: 6,),
//                  Style.getPoppinsRegularText('${AppUtils.getRupeeSymbol()}${product.productBaseRate}', AppFonts.textSize8, ColorTheme.FF333333),
//                  SizedBox(width: 6,),
//                  Style.getPoppinsRegularText('${AppUtils.getRupeeSymbol()}${product.productTaxRate}% OFF', AppFonts.textSize9, ColorTheme.FFF97062),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 15, right: 10),
                      child: Style.getPoppinsRegularText(
                          "per ${product.saleUnitName} [${product.size} ${product.sizeUnit}]",
                          AppFonts.textSize10,
                          ColorTheme.FF999999),
                    ),
                    SizedBox(
                      height: 5,
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
