import 'package:flutter/material.dart';
import 'package:foodnet_buyer/data/model/product.dart';
import 'package:foodnet_buyer/ui/product_detail/product_detail_screen.dart';
import 'package:foodnet_buyer/ui/widgets/offer_view.dart';
import 'package:foodnet_buyer/ui/widgets/product_view.dart';
import 'package:foodnet_buyer/ui/widgets/scale_route.dart';
import 'package:foodnet_buyer/util/add_to_cart_animation.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';

// ignore: must_be_immutable
class ScrollItemView extends StatelessWidget {
  final Product e;
  final GlobalKey cartKey, rootKey;
  final bool showAnimation;
  final Function(bool) onClick;
  final String screen;
  String catalogId;
  Widget offerView, productView;

  ScrollItemView(this.e, this.cartKey, this.rootKey, this.screen,
      {this.showAnimation = true, this.onClick, Key key})
      : super(key: key) {
    catalogId = PreferenceManager.getLocationData().productCatalogId;
  }

  @override
  Widget build(BuildContext context) {
    return _nonAnimatedView(context); //_animatedView(context);
  }

  Widget _nonAnimatedView(BuildContext context) {
    GlobalKey itemKey = GlobalKey();
    GlobalKey heroKey = GlobalKey();
    return e.isOffer()
        ? InkWell(
            onTap: () {
              // FBAnalytics.offerCardClicked(e.navigationType, e.adCampaignId, e.categoryId?.toString(), (e.productId ?? e.id)?.toString(), e.isParentCategory, screen);
              AppUtils.handleOfferNavigation(context, e);
            },
            child: OfferView(
              model: e,
            ))
        : ProductView(
            key: itemKey,
            product: e,
            heroKey: heroKey,
            onClick: (Product product) {
              onClick?.call(false);
              // FBAnalytics.productDetail((e.id ?? e.productId)?.toString(), e?.productName, screen);
              String catalogId = e.productcatalogId?.toString() ??
                  PreferenceManager.getLocationData().productCatalogId;
              Navigator.push(
                  context,
                  ScaleRoute(
                      page: ProductDetailScreen(
                    product: e,
                    productCatalogId: catalogId,
                    key: heroKey,
                  )));
              /*NavigationUtil.pushToNewScreen(
                  context,
                  ProductDetailScreen(
                    product: e,
                    productCatalogId: catalogId,
                    key: heroKey,
                  ));*/
            },
            onAddCartClick: (product) {
              onClick?.call(false);
              Offset start = AddToCartAnimation.getWidgetOffset(itemKey);
              Offset end = AddToCartAnimation.getWidgetOffset(cartKey);
              // FBAnalytics.productShortDetail((e.id ?? e.productId)?.toString(), e?.productName, screen);
              AppUtils.showAddToCartDialog(context, product, onComplete: () {
                Navigator.pop(context);
                AddToCartAnimation.show(rootKey, start, end, onFinish: () {});
              });
            });
  }
}
