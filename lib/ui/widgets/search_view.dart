import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/bloc/common_bloc.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:speech_to_text/speech_recognition_error.dart';
import 'package:speech_to_text/speech_recognition_result.dart';
import 'package:speech_to_text/speech_to_text.dart';

import 'image_view.dart';

class SearchView extends StatefulWidget {

  final Function(String) onTextChanged;
  final Function onCancel;
  final String preFilledText;
  final int thresholdLength;

  SearchView({Key key, this.onTextChanged, this.onCancel, this.preFilledText, this.thresholdLength = 1}): super(key: key);

  _SearchViewState createState() => _SearchViewState();

}
class _SearchViewState extends State<SearchView> {

  FocusNode _focusNode = FocusNode();
  Timer _debounce;
  final _controller = TextEditingController();
  bool _hasSpeech = false;
  double level = 0.0;
  double minSoundLevel = 50000;
  double maxSoundLevel = -50000;
  String lastWords = "";
  String lastError = "";
  String lastStatus = "";
  String _currentLocaleId = "";
  SpeechToText speech = SpeechToText();

  @override
  void initState() {
    speech = SpeechToText();
    if (widget.preFilledText != null && widget.preFilledText.isNotEmpty) {
      _controller.text = widget.preFilledText;
    }
    CommonBloc.get().addSearchListener(onClearFocus);
    super.initState();
  }

  onClearFocus() {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  @override
  void dispose() {
    CommonBloc.get().removeSearchListener(onClearFocus);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: double.maxFinite,
      height: 40,
      margin: EdgeInsets.symmetric(horizontal: 20,),
      padding: EdgeInsets.only(left: 10, right: 4,),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          border: Border.all(color: ColorTheme.FFE5E5E5, width: 1)
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Image.asset(
            AppAssets.search,
            width: 30,
            height: 30,
          ),
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(bottom: 6),
            height: 40,
            width: MediaQuery.of(context).size.width - 150,
            child: TextField(
              focusNode: _focusNode,
              textAlign: TextAlign.left,
              maxLines: 1,
              maxLength: 50,
              autofocus: false,
              style: Style.getTextStyleLight(14, ColorTheme.FF333333),
              textInputAction: TextInputAction.search,
              controller: _controller,
              onChanged: (value) {
                setState(() {
                  _setSearchText(value, setText: false);
                });
              },
              onSubmitted: (val){
                AppUtils.unfocusScope(context);
              },
              decoration: InputDecoration(
                  counter: null,
                  counterText: '',
                  contentPadding: const EdgeInsets.all(8),
                  hintStyle: Style.getTextStyleLight(14, ColorTheme.FF999999),
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  hintText: 'What are you looking for ?'),
            ),
          ),
          Expanded(child: SizedBox(width: 10,)),
          _controller.text.isNotEmpty
              ? InkWell(onTap: () {_onCancel(true);}, child: Container( width: 40, height: 40, child: Icon(Icons.cancel_outlined, size: 24, color: ColorTheme.darkGrey,)))
              : Container(
            width: 40,
            height: 40,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              boxShadow: level == 0 ? [] : [
                BoxShadow(
                    blurRadius: .26,
                    spreadRadius: level * 1.5,
                    color: ColorTheme.FFE3E3E3.withOpacity(.5))
              ],
              borderRadius: BorderRadius.all(Radius.circular(50)),
            ),
            child: IconButton(
              onPressed: () {
                AppUtils.unfocusScope(context);
                if (!_hasSpeech) {
                  initSpeechState();
                } else {
                  if (_hasSpeech && speech.isNotListening) {
                    startListening();
                  } else {
                    stopListening();
                  }
                }
              },
              iconSize: 24.0,
              icon: ImageView(
                asset: AppAssets.microphone,
                color: ColorTheme.FF333333,
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<void> initSpeechState() async {
    bool hasSpeech = await speech.initialize(
        onError: errorListener, onStatus: statusListener);
    if (hasSpeech) {
//      _localeNames = await speech.locales();

      var systemLocale = await speech.systemLocale();
      _currentLocaleId = systemLocale.localeId;
    }

    if (!mounted) return;

    setState(() {
      _hasSpeech = hasSpeech;
      if (_hasSpeech) {
        startListening();
      }
    });
  }

  _setSearchText(String text, {bool setText = true}) {
    if (setText) {
      _controller.text = text;
      _controller.selection = TextSelection(baseOffset: text.length, extentOffset: text.length);
    }
    _onSearchTextChanged(text);
  }

  _onSearchTextChanged(String value) {
    if (_debounce?.isActive ?? false) {
      _debounce.cancel();
    }
    _debounce = Timer(Duration(milliseconds: 300), () async {
      if (value.trim().length > widget.thresholdLength) {
        widget.onTextChanged?.call(value.trim());
      } else if (value.length == 0){
        _onCancel(false);
      }
    });
  }

  _onCancel(bool unFocus) {
    if (unFocus) {
      AppUtils.unfocusScope(context);
    }
    setState(() {});
    _controller.text = "";
    AppUtils.delayTask((){
      widget.onCancel?.call();
    }, delay: Duration(milliseconds: 200));
  }

  void startListening() {
    AppUtils.unfocusScope(context);
    lastWords = "";
    lastError = "";
    speech.listen(
        onResult: resultListener,
        listenFor: Duration(seconds: 5),
        localeId: _currentLocaleId,
        onSoundLevelChange: soundLevelListener,
        cancelOnError: true,
        partialResults: true,
        onDevice: false,
        listenMode: ListenMode.confirmation);
    setState(() {});
  }

  void stopListening() {
    print("stopListening");
    speech.stop();
    if (mounted) {
      setState(() {
        level = 0.0;
      });
    }
  }

  void cancelListening() {
    print("cancelListening");
    speech?.cancel();
    if (mounted) {
      setState(() {
        level = 0.0;
      });
    }
  }

  void resultListener(SpeechRecognitionResult result) {
    if (!mounted) {
      return;
    }
    setState(() {
      lastWords = "${result.recognizedWords}";
      print(lastWords);
      if(result.finalResult){
        level = 0.0;
        _setSearchText(lastWords);
      }
    });
  }

  void soundLevelListener(double level) {
    minSoundLevel = min(minSoundLevel, level);
    maxSoundLevel = max(maxSoundLevel, level);
    // print("sound level $level: $minSoundLevel - $maxSoundLevel ");
    if (mounted) {
      setState(() {
        this.level = level;
      });
    }
  }

  void errorListener(SpeechRecognitionError error) {
    print("Received error status: $error, listening: ${speech.isListening}");
    if (mounted) {
      setState(() {
        lastError = "${error.errorMsg} - ${error.permanent}";
      });
    }
  }

  void statusListener(String status) {
    print("Received listener status: $status, listening: ${speech.isListening}");
    if (!mounted) {
      return;
    }
    setState(() {
      lastStatus = "$status";
      if(!speech.isListening) {
        level = 0.0;
      }
    });
  }

}