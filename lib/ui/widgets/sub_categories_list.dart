import 'package:flutter/material.dart';
import 'package:foodnet_buyer/data/model/category_response.dart';
import 'package:foodnet_buyer/ui/widgets/cell/cell_category.dart';

class SubCategoriesList extends StatelessWidget {
  final int selectedIndex;
  final List<Child> items;
  final Function(int) onClick;
  final Color bgColor;

  SubCategoriesList(this.selectedIndex, this.items, {this.onClick, this.bgColor, Key key}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        color: bgColor,
        alignment: Alignment.center,
        height: 80,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: List.generate(items.length, (index) {
              return CellCategory(
                key: UniqueKey(),
                title: items[index].categoryName,
                image: items[index].categorydocuments != null && items[index].categorydocuments.length>0 ? items[index].categorydocuments[0].documentUrl : null,
                width: 40,
                height: 40,
                onClick: () {
                  onClick?.call(index);
                },
              );
            }
            ),
          ),
        )
    );
  }
}