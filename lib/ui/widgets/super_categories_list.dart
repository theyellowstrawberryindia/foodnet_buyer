import 'package:flutter/material.dart';
import 'package:foodnet_buyer/data/model/category_response.dart';
import 'package:foodnet_buyer/ui/widgets/cell/cell_category.dart';

class SuperCategoriesList extends StatelessWidget {
  final int selectedIndex;
  final List<SuperCategory> categories;
  final Function(int) onClick;
  final Color bgColor;

  SuperCategoriesList(this.selectedIndex, this.categories, {this.onClick, this.bgColor, Key key}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: bgColor,
      alignment: Alignment.center,
      height: 80,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: List.generate(categories.length, (index) {
            return CellCategory(
              key: UniqueKey(),
              title: categories[index].superCategoryName,
              image: categories[index].supercategorydocuments[0].documentUrl,
              onClick: () {
                onClick?.call(index);
              },
              isSelected: index == selectedIndex,
            );
          }
          ),
        ),
      ),
    );
  }
}