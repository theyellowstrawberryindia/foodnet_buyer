import 'package:flutter/material.dart';

class TextView extends StatelessWidget {
  final Key key;
  final String text;
  final int maxLines;
  final TextOverflow overflow;
  final TextAlign textAlign;
  final TextStyle style;

  TextView({
    this.key,
    @required this.text,
    this.maxLines ,
    this.overflow,
    this.textAlign,
    @required this.style,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(2.0),
      child: Text(
        text,
        textScaleFactor: 1.0,
        maxLines: maxLines,
        textAlign: textAlign,
        overflow: overflow,
        style: style,
      ),
    );
  }
}
