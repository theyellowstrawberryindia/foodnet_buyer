import 'package:add_cart_parabola/add_cart_parabola.dart';
import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/app_assets.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';

class AddToCartAnimation {
  static OverlayEntry currentLoader;
  static Function onComplete;

  static void show(GlobalKey rootKey, Offset startWidgetKey, Offset endWidgetKey, {Function onFinish}) {
    onComplete = onFinish;
    currentLoader = new OverlayEntry(
        builder: (context) => ParabolaAnimateWidget(rootKey, startWidgetKey, endWidgetKey,
            Image.asset(AppAssets.cart, color: ColorTheme.FFF0674C), animationListener, duration: 500,));

    Overlay.of(rootKey.currentContext).insert(currentLoader);
  }

  static Offset getWidgetOffset(GlobalKey key) {
    RenderBox renderBox = key.currentContext.findRenderObject();
    return renderBox.localToGlobal(Offset.zero);
  }

  static void animationListener(status) {
    if(status == AnimationStatus.completed){
      hide();
      onComplete?.call();
    }
  }

  static void hide() {
    try{
      if(currentLoader!=null) {
        currentLoader?.remove();
        currentLoader =null;
      }
    }catch(e){
      print(e);
    }

  }
}