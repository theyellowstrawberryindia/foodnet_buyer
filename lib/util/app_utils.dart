import 'package:connectivity/connectivity.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:foodnet_buyer/constant/color_theme.dart';
import 'package:foodnet_buyer/data/bloc/common_bloc.dart';
import 'package:foodnet_buyer/data/data_manager.dart';
import 'package:foodnet_buyer/data/model/location_data.dart';
import 'package:foodnet_buyer/data/model/order_details_model.dart';
import 'package:foodnet_buyer/data/model/product.dart';
import 'package:foodnet_buyer/data/netwroking/custom_exception.dart';
import 'package:foodnet_buyer/style/style.dart';
import 'package:foodnet_buyer/ui/SplashScreen.dart';
import 'package:foodnet_buyer/ui/cart/shopping_cart_screen.dart';
import 'package:foodnet_buyer/ui/coupon/coupon_detail.dart';
import 'package:foodnet_buyer/ui/dashboard/dashboard_screen.dart';
import 'package:foodnet_buyer/ui/login/StartScreen.dart';
import 'package:foodnet_buyer/ui/product_detail/product_detail_screen.dart';
import 'package:foodnet_buyer/ui/subcatgeory/subcategory_single_screen.dart';
import 'package:foodnet_buyer/ui/widgets/cart_icon.dart';
import 'package:foodnet_buyer/ui/widgets/dialog/add_to_cart.dart';
import 'package:foodnet_buyer/ui/widgets/dialog/list_option_dialog.dart';
import 'package:foodnet_buyer/util/navigation_util.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';
import 'package:intl/intl.dart' as Intl;

class AppUtils {
  AppUtils._();

  static getBoxShadow({double spreadRadius = 0}) {
    return [
      BoxShadow(
          color: ColorTheme.darkGrey.withOpacity(.4),
          blurRadius: 1.0,
          offset: Offset(1, 1),
          spreadRadius: spreadRadius),
    ];
  }

  static shadow() {
    return [
      BoxShadow(
        color: ColorTheme.darkGrey.withOpacity(.1),
        blurRadius: 2.0,
        offset: Offset(1, 1),
        spreadRadius: 3,
      ),
    ];
  }

  static bool checkAndShowLogin(BuildContext context, {bool fromCart = false}) {
    if (!PreferenceManager.isLoggedIn()) {
      DataManager.get().fromCart = fromCart;
      NavigationUtil.pushToNewScreen(context, StartScreen());
      return false;
    }
    return true;
  }

  static showAddToCartDialog(BuildContext context, Product product,
      {Function onComplete}) {
    showAnimatedDialog(
      context,
      AddToCartDialog(
        product,
        onComplete: onComplete,
      ),
      true,
    );
  }

  static showListOptionDialog(BuildContext context, List<dynamic> options,
      Function(int) onComplete, String Function(dynamic) displayValue) {
    showAnimatedDialog(
      context,
      ListOptionDialog(options, onComplete, displayValue),
      true,
    );
  }

  static showAnimatedDialog(
      BuildContext context, Widget dialogView, bool isDisMissible,
      {double cornerRadius = 20}) {
    showGeneralDialog(
        transitionBuilder: (context, a1, a2, widget) {
          return Transform.translate(
//          scale: a1.value,
            offset: Offset(0, 300 - (300 * a1.value)),
            child: Opacity(
                opacity: a1.value,
                child: StatefulBuilder(builder: (context, setState) {
                  return WillPopScope(
                    onWillPop: () async => isDisMissible,
                    child: Dialog(
                      elevation: 6,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(cornerRadius))),
                      child: dialogView,
                    ),
                  );
                })),
          );
        },
        transitionDuration: Duration(milliseconds: 250),
        context: context,
        barrierDismissible: isDisMissible,
        barrierLabel: '',
        barrierColor: Colors.black.withOpacity(.5),
        pageBuilder: (context, animation1, animation2) {
          return StatefulBuilder(builder: (context, setState) {
            return WillPopScope(
              onWillPop: () async => isDisMissible,
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.all(Radius.circular(cornerRadius))),
                elevation: 6,
                child: dialogView,
              ),
            );
          });
        });
  }

  static showBottomSheetView(BuildContext context, Widget view,
      {double cornerRadius = 20}) {
    showModalBottomSheet(
        context: context,
        builder: (ctx) {
          return view;
        },
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(cornerRadius))));
  }

  static log(String msg) {
    if (kDebugMode) {
      print(msg);
    }
  }

  static showToast(String msg) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 2,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 14.0);
  }

  static Future<bool> checkInternetAndShowMsg() async {
    bool result = await isConnectedToInternet();
    if (!result) {
      showToast(CustomException.ERROR_NO_INTERNETCONNECTION);
    }

    return result;
  }

  static Future<bool> isConnectedToInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    return connectivityResult != ConnectivityResult.none;
  }

  static String showCustomErrorMsg(dynamic e, {bool showToast = true}) {
    String msg = "";
    if (e is CustomException) {
      msg = e.getMsg();
    } else {
      msg = CustomException.ERROR_CRARSH_MSG;
    }
    if (showToast) {
      AppUtils.showToast(msg);
    }
    return msg;
  }

  static double getTextWidth(String text, double fontSize, String fontFamily,
      {double maxWidth = 800}) {
    final constraints = BoxConstraints(
      maxWidth: maxWidth, // maxwidth calculated
      minHeight: 0.0,
      minWidth: 0.0,
    );

    RenderParagraph renderParagraph = RenderParagraph(
      TextSpan(
        text: text,
        style: Style.getTextStyleLight(fontSize, ColorTheme.FF333333),
      ),
      textDirection: TextDirection.ltr,
      maxLines: 1,
    );
    renderParagraph.layout(constraints);
    double textlen = renderParagraph
        .getMaxIntrinsicWidth(renderParagraph.textSize.height)
        .ceilToDouble();
    return textlen;
  }

  static double getTextHeight(String text, TextStyle style,
      {double maxWidth = 800}) {
    final constraints = BoxConstraints(
      maxWidth: maxWidth, // maxwidth calculated
      minHeight: 0.0,
      minWidth: 0.0,
    );

    RenderParagraph renderParagraph = RenderParagraph(
        TextSpan(
          text: text,
          style: style,
        ),
        textDirection: TextDirection.ltr,
        maxLines: 1);
    renderParagraph.layout(constraints);
    double textlen =
        renderParagraph.getMaxIntrinsicHeight(style.fontSize).ceilToDouble();
    return textlen;
  }

  static bool isValidEmail(String email) {
    return RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);
  }

  static String padLeft(var value) {
    return value.toString().padLeft(2, "0");
  }

  static String showDoubleString(double value) {
    return value.toStringAsFixed(2);
  }

  static Widget getCartWidget(BuildContext context, {GlobalKey key}) {
    return CartIcon(
      key: key,
    );
  }

  static Widget getWidgetWithRupee(String text, double fontSize, Color color,
      {bool isLight = true, String prefixText}) {
    String decimalValue = '';
    String withoutDecimalText = text;
    if (text.indexOf(".") != -1) {
      decimalValue = text.substring(text.indexOf(".") + 1);
      withoutDecimalText = text.substring(0, text.indexOf("."));
    }
    if (decimalValue == "00" || decimalValue == "0") {
      decimalValue = '';
    }
    return RichText(
        text: TextSpan(
      text: prefixText ?? "",
      style: TextStyle(fontSize: fontSize, color: color),
      children: [
        TextSpan(
            text: "${getRupeeSymbol()}",
            style: TextStyle(fontSize: fontSize, color: color)),
        TextSpan(
            text: withoutDecimalText,
            style: isLight
                ? Style.getTextStyleLight(fontSize, color)
                : Style.getTextStyleRegular(fontSize, color)),
        WidgetSpan(
          child: Transform.translate(
            offset: Offset(0, fontSize >= 10 ? -4 : -2),
            child: Text(decimalValue,
                //superscript is usually smaller in size
                textScaleFactor: 0.7,
                style: isLight
                    ? Style.getTextStyleLight(fontSize, color)
                    : Style.getTextStyleRegular(fontSize, color)),
          ),
        ),
      ],
    ));
  }

  static Widget getProductName(
      String productName, String subText, double fontSize, Color color,
      {bool isLight = true,
      int maxLines = 2,
      int lines = 2,
      double subtextSize = 10,
      String suffix}) {
    return RichText(
        overflow: TextOverflow.ellipsis,
        maxLines: maxLines,
        text: TextSpan(
          children: [
            TextSpan(
              text: "$productName",
              style: isLight
                  ? Style.getTextStyleLight(fontSize, color)
                  : Style.getTextStyleMedium(fontSize, color),
            ),
            TextSpan(
                text: "${(subText?.length ?? 0) > 0 ? ' ($subText)' : ''}",
                style: Style.getTextStyleLight(subtextSize, color)),
            TextSpan(
              text: "${(suffix?.length ?? 0) > 0 ? ' $suffix' : ''}",
              style: isLight
                  ? Style.getTextStyleLight(fontSize, color)
                  : Style.getTextStyleMedium(fontSize, color)),
          ],
        ));
  }

  static String getRupeeSymbol() => "\u{20B9}";

  static void unfocusScope(BuildContext context) {
    clearFocus(context);
  }

  static void clearFocus(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
    CommonBloc.get().clearSearchFocus();
  }

  static void clearFocusForScreen(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  static int getIndex(List<dynamic> items, bool Function(dynamic) predicate) {
    for (int i = 0; i < items.length; i++) {
      if (predicate(items[i])) {
        return i;
      }
    }
    return 0;
  }

  static String addressFromObj(dynamic address) {
    String state = "";
    try {
      state = address["stateId"] != null
          ? DataManager.get().states.firstWhere((element) =>
              element["id"].toString() == address["stateId"]?.toString())
          : "";
    } catch (e) {}
    return "${address["flat_number"]}, ${address["building"]}, ${address["address_line1"]}, ${address["area"]}, ${address["city"]}, $state. ${address["pin_code"]}";
  }

  static Color getColorBasedOnStatus(String status) {
    switch (status.toLowerCase()) {
      case 'new':
        // return ColorTheme.white;
        return ColorTheme.darkGrey.withOpacity(0.5);
      case 'rejected':
        // return ColorTheme.FFF0674C;
        return ColorTheme.red;
      default:
        // return ColorTheme.green1;
        return ColorTheme.orderCardGreen;
    }
  }

  static Color getCardColor(String status) {
    switch (status.toLowerCase()) {
      case 'new':
        return ColorTheme.white;
      case 'rejected':
        return Colors.red[50];
      // return Colors.red[50].withOpacity(0.8);
      default:
        return Colors.green[50];
      // return Colors.green[50].withOpacity(0.8);
    }
  }

  static Color getStatusColor(String status) {
    switch (status) {
      case 'Pending':
        return ColorTheme.darkGrey;
      case 'Created':
        return ColorTheme.primaryColor;
      case 'Completed':
        return ColorTheme.primaryColor;
      case 'Accepted':
        return ColorTheme.primaryColor;
      case 'Updated':
        return ColorTheme.primaryColor;
      case 'Attached photos':
        return ColorTheme.primaryColor;
      default:
        return ColorTheme.darkGrey;
    }
  }

  static String formatDate(DateTime dateTime) {
    try {
      return Intl.DateFormat("dd MMM yy").format(dateTime);
    } catch (e) {
      print(e);
    }
  }

  static String formatCurrency(String amount) {
    final double price = double.parse(amount);
    try {
      final Intl.NumberFormat numberFormat =
          Intl.NumberFormat.currency(locale: 'en_IN', symbol: '₹');
      return numberFormat.format(price);
    } catch (e) {
      print(e);
    }
  }

  static onExit(BuildContext context) {
    NavigationUtil.clearAllAndAdd(context, SplashScreen());
  }

  static Future<void> delayTask(Function task,
      {Duration delay = const Duration(milliseconds: 500)}) async {
    return await Future.delayed(delay, task);
  }

  static void handleOfferNavigation(
    BuildContext context,
    dynamic data,
  ) {
    if (data != null && data is Product && !data.isOfferNoNavigation()) {
      Product e = data;
      String catalogId = e.productcatalogId?.toString() ??
          PreferenceManager.getLocationData().productCatalogId;
      NavigationUtil.pushToNewScreen(
        context,
        e.isOfferCategory()
            ? SubCategorySingleScreen(catalogId, e.categoryId?.toString(),
                e.categoryName ?? "", e.isParentCategory)
            : e.isOfferProduct()
                ? ProductDetailScreen(
                    product: e,
                    productCatalogId: catalogId,
                  )
                : CouponDetailScreen(
                    e.adCampaignId,
                    fromCoupon: false,
                  ),
      );
    }
  }

  static void openCartScreen(BuildContext context) {
    NavigationUtil.pushToNewScreen(context, ShoppingCartScreen());
  }

  static String getDisplayValue(dynamic value, {int decimalUpTo = 2}) {
    String stringQty = value.toString();
    if (stringQty == null || stringQty == "null") {
      return "";
    }
    int decimalIndex = stringQty.indexOf(".");
    if (decimalIndex != -1) {
      if (stringQty.length -1 > decimalIndex) {
        String decimal = stringQty.substring(decimalIndex+1);
        int decimalValue = int.parse(decimal);
        return decimalValue > 0 ? double.parse(stringQty).toStringAsFixed(decimalUpTo) : stringQty.substring(0, decimalIndex);
      } else {
        return stringQty.substring(0, decimalIndex);
      }
    }
    return stringQty;
  }

  static bool isPendingAt(List<StatusElement> status, String eventName) {
    return status.firstWhere((element) => element.key == eventName, orElse: ()=> null)?.value != 'Completed';
  }

  static String getPendingAtStatus(List<StatusElement> status) {
    return status.firstWhere((element) => element.value == "Pending", orElse: ()=> null)?.key ?? "Payment";
  }

  static bool isCompletedAt(List<StatusElement> status, String eventName) {
    return status.firstWhere((element) => element.key == eventName, orElse: ()=> null)?.value == 'Completed';
  }

  static bool isValid(String text) {
    return text != null && text.trim().length > 0;
  }

  static navigateAfterAddressSelection(BuildContext context, dynamic add) {
    PreferenceManager.setPartyId(add["partyId"]?.toString());
    PreferenceManager.setBuyerId(add["buyerId"]?.toString());
    PreferenceManager.setPartyLocationId(
        add["partylocationId"]?.toString());
    var zone = add["zones"] != null && add["zones"].length > 0
        ? add["zones"][0]
        : null;
    LocationData data = LocationData();
    data = LocationData.fromJson(add);
    data.zone = Zone.fromJson(zone);
    PreferenceManager.saveLocationData(data);
    PreferenceManager.saveUserSelectedAddress(add);
    PreferenceManager.setAddressPicked(true);
    NavigationUtil.clearAllAndAdd(context, DashboardScreen());
  }

  static Map<String, dynamic> getMapFromRemoteMessage(RemoteMessage message) {
    return {"notification" : {"title" : message.notification.title, "body" : message.notification.body}, "data" : message.data};
  }
}
