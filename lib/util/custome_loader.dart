import 'package:flutter/material.dart';
import 'package:foodnet_buyer/util/app_utils.dart';

class Loader extends StatelessWidget {
  static OverlayEntry currentLoader;

  static void show(BuildContext context, {bool showCenter = true}) {
    AppUtils.log("Loader: show at center = $showCenter");
    hide();
    currentLoader = new OverlayEntry(
        builder: (context) => Stack(
          alignment: showCenter ? Alignment.center : Alignment.bottomCenter,
          children: <Widget>[
            Container(
              color: Color(0x99ffffff),
            ),
            showCenter ? Center(
              child: Loader(),
            ) : Container(alignment: Alignment.bottomCenter,
              margin: const EdgeInsets.all(30),
              child: Loader(),),
          ],
        ));
    Overlay.of(context).insert(currentLoader);
  }
  static void hide() {
    try{
      if(currentLoader!=null) {
        currentLoader?.remove();
        currentLoader =null;
      }
    }catch(e){
      print(e);
    }

  }


  @override
  Widget build(BuildContext context) {
    return  WillPopScope(
      onWillPop: (){
        return;
      },
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );

  }
}
