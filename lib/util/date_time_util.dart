import 'package:flutter/material.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:intl/intl.dart';

import 'app_utils.dart';
import 'app_utils.dart';

class DateTimeUtil {

  static String DD_MMM = 'dd MMM';
  static String hh_mm_a = 'hh:mm a';
  static String DD_MM_YY = 'dd/MM/yy';
  static String DD_MM_YYYY = 'dd/MM/yyyy';

  static TimeOfDay getTimeFromString(String time) {
    try {
      List<String> times = time.split(":");
      return TimeOfDay(hour: int.parse(times[0]), minute: int.parse(times[1]));
    } catch (e) {
      AppUtils.log("getTimeFromString: error parsing time");
      return TimeOfDay.now();
    }
  }

  static String getDateFromDateTime(DateTime dateTime,String format){
    try{
      return DateFormat('$format').format(dateTime);
    }catch(e){
      AppUtils.log(e.toString());
      return '';
    }
  }

  static String formatDateTime(DateTime dateTime){
    try{
      return DateFormat.yMMMMd().format(dateTime);
    }catch(e){
      AppUtils.log(e.toString());
      return '';
    }
  }

  static DateTime getCurrentDayWithTime(String time, {DateTime dateTime}) {
    TimeOfDay timeOfDay = getTimeFromString(time);
    DateTime date = dateTime != null ? dateTime : DateTime.now();
    return DateTime(date.year, date.month, date.day, timeOfDay.hour, timeOfDay.minute);
  }

  static String getTimeFromIn24FormatDateTime(DateTime dateTime) {
    return "${AppUtils.padLeft(dateTime.hour)}:${AppUtils.padLeft(dateTime.minute)}";
  }

  static String getTimeIn12FormatFromDateTime(DateTime dateTime) {
    TimeOfDay timeOfDay = TimeOfDay(hour: dateTime.hour, minute: dateTime.minute);
    return "${AppUtils.padLeft(dateTime.hour)}:${AppUtils.padLeft(dateTime.minute)} ${timeOfDay.period == DayPeriod.am ? "am" : "pm"}";
  }

  static String getTimeIn12FormatFromTimeOfDay(TimeOfDay timeOfDay) {
    try{
      return DateFormat.jm().format(DateTime(DateTime.now().year,1,1,timeOfDay.hour,timeOfDay.minute,0));
    }catch(e){
      AppUtils.log(e.toString());
      return '';
    }
  }

  static String getFormatedDate(DateTime dateTime) {
    return DateFormat("EEEE, MMM dd").format(dateTime);
  }

  static String getFormatedDateFromString(String stringDate) {
    return getFormatedDate(DateFormat("dd/MM/yyyy").parse(stringDate));
  }

  static String getFormatedTime(String time) {
    time = converToProperTime(time);
    return DateFormat("hh:mm a").format(DateFormat("HH:mm:ss").parse(time));
  }

  static String convertToHHMM(String time) {
    return DateFormat("HH:mm").format(DateFormat("HH:mm:ss").parse(converToProperTime(time)));
  }

  static DateTime parseDate(String date) {
    if (date.contains("-")) {
      if (date.split("-")[0].length == 4) {
        return DateFormat("yyyy-MM-dd").parse(date);
      } else  {
        return DateFormat("dd-MM-yyyy").parse(date);
      }
    } else {
      return DateFormat("dd/MM/yyyy").parse(date);
    }
  }

  static String converToProperTime(String time) {
    if (time != null && time.split(":").length==2) {
      time = "$time:00";
    }
    return time;
  }
}