import 'package:flutter/material.dart';
import 'package:foodnet_buyer/constant/text_msgs.dart';
import 'package:foodnet_buyer/data/bloc/common_bloc.dart';
import 'package:foodnet_buyer/data/data_manager.dart';
import 'package:foodnet_buyer/data/model/cart_product.dart';
import 'package:foodnet_buyer/data/model/cart_response.dart';
import 'package:foodnet_buyer/data/model/location_data.dart';
import 'package:foodnet_buyer/data/model/product.dart';
import 'package:foodnet_buyer/data/netwroking/api_path.dart';
import 'package:foodnet_buyer/data/repository/cart_repository.dart';
import 'package:foodnet_buyer/ui/widgets/dialog/msg_dialog.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:foodnet_buyer/util/custome_loader.dart';
import 'package:foodnet_buyer/util/date_time_util.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';
import 'package:intl/intl.dart';

class EditPOShoppingCartUtil {
  static EditPOShoppingCartUtil _instance = EditPOShoppingCartUtil.internal();

  factory EditPOShoppingCartUtil() {
    return _instance;
  }

  EditPOShoppingCartUtil.internal();

  static EditPOShoppingCartUtil get() {
    return _instance;
  }

  updateCart(CartResponse response) {
    try {
      if (response != null && response.isNotEmpty()) {
        PreferenceManager.saveCartResponse(response);
        PreferenceManager.setCartId(response.id.toString());
      } else if (response != null && response.shoppingcartitems?.length == 0) {
        CartRepository.get().clearCart();
      }
      CommonBloc.get().updateBadge();
    } catch (e) {
      AppUtils.log("Save cart response error: ${e.toString()}");
    }
  }

  static DateTime getDeliveryDateTime(Product product) {
    LocationData locationData = PreferenceManager.getLocationData();
    bool zoneSameDayDelivery = locationData?.zone?.sameDayDelivery ?? false;
    String zoneCutOffTime = locationData?.zone?.cutoffTime ?? "18:00";
    String zoneDeliveryTime = locationData?.zone?.deliveryTime ?? "12:00";
    bool isSameDay = zoneSameDayDelivery && (product?.sameDayDelivery == true);
    DateTime dateTime = DateTime.now();
    DateTime cutOffTime = DateTimeUtil.getCurrentDayWithTime(zoneCutOffTime);
    if (isSameDay &&
        (dateTime.isBefore(cutOffTime) ||
            dateTime.isAtSameMomentAs(cutOffTime))) {
      return DateTime(dateTime.year, dateTime.month, dateTime.day,
          dateTime.hour + 2, dateTime.minute);
    } else {
      AppUtils.log("App Delivery time: $zoneDeliveryTime}");
      TimeOfDay timeOfDay = DateTimeUtil.getTimeFromString(zoneDeliveryTime);
      DateTime nextDay = DateTime.now().add(Duration(days: 1));
      DateTime nextDayWithTime = DateTime(nextDay.year, nextDay.month,
          nextDay.day, timeOfDay.hour, timeOfDay.minute);
      if (product?.sameDayDelivery == true) {
        return nextDayWithTime;
      } else {
        DateTime deliveryDateTime = nextDayWithTime;
        if (product?.attributes != null &&
            product?.attributes?.containsKey("deliveryTime") == true) {
          try {
            deliveryDateTime = DateFormat("dd/MM/yyyy HH:mm")
                .parse(product.attributes["deliveryTime"]);
          } catch (_) {
            AppUtils.log("Failed to parse product delivery time");
          }
        }

        return deliveryDateTime;
      }
    }
  }

  callUpdateToCart(BuildContext context, Map<String, dynamic> params,
      {Function onComplete, bool showLoader = true}) async {
    if (showLoader) {
      Loader.show(context);
    }
    try {
      var response = await CartRepository.get().updateOrder(params);
      if (response != null) {
        if (response["status"] == true) {
          if (response["data"] != null) {
            CartResponse cartResponse = CartResponse.fromJson(response["data"]);
            updateCart(cartResponse);
          }
          // FBAnalytics.cartUpdated(PreferenceManager.getCartId(), cartResponse.shoppingcartitems?.length?.toString() ?? "0");
        } else {
          // FBAnalytics.addToCartFailed(PreferenceManager.getCartId());
          if (showLoader) {
            AppUtils.showToast(response["message"] ?? TextMsgs.COMMON_ERROR);
          }
        }
      }
      if (showLoader) {
        Loader.hide();
      }
      onComplete?.call();
//      AppUtils.log("add to cart response: $response");
    } catch (e) {
      AppUtils.log("add to cart error: ${e.toString()}");
      if (showLoader) {
        Loader.hide();
        MsgDialog.showMsgDialog(context, TextMsgs.COMMON_ERROR, "OK", () {});
      }
    }
  }

  addProductToCart(BuildContext context, Product product, int qty,
      String packOff, String packUnit, String saleUnit,
      {double discount = 0, Function onComplete, String screen}) async {
    // if (!PreferenceManager.getLocationData().isInOperationalHours()) {
    //   AppUtils.showToast(TextMsgs.STORE_CLOSED);
    //   return;
    // }
    DateTime dateTime = getDeliveryDateTime(product);
    CartProduct cartProduct = CartProduct(
        productId: product.id ?? product.productId,
        quantity: qty.toDouble(),
        packets: qty,
        packOf: packOff,
        packOfUnit: packUnit,
        saleUnit: saleUnit,
        discountAmount: discount,
        deliveryDate: dateTime,
        deliveryTime:
            "${AppUtils.padLeft(dateTime.hour)}:${AppUtils.padLeft(dateTime.minute)}:00");

    // FBAnalytics.addToCart((product.id ?? product.productId).toString(), product.productName, screen, PreferenceManager.getCartId());
    await callUpdateToCart(context, _getCartParams(cartProduct),
        onComplete: onComplete);
  }

  updateCartToServer(BuildContext context, {Function onComplete}) async {
    await callUpdateToCart(context, _getCartParamsToUpdate(),
        onComplete: onComplete);
  }

  removeProduct(BuildContext context, CartProduct product,
      {Function onComplete}) async {
    await callUpdateToCart(context, _getCartParamsWithRemoveProduct(product),
        onComplete: onComplete);
  }

  updateProductQuantity(
      BuildContext context, CartProduct product, bool increment,
      {Function onComplete}) async {
    await callUpdateToCart(
        context, _getCartParamsWithChangedQty(product, increment),
        onComplete: onComplete);
  }

  updateProductDateTime(BuildContext context, CartProduct product,
      DateTime newDateTime, bool forAll,
      {Function onComplete}) async {
    await callUpdateToCart(context,
        _getCartParamsWithChangedDateTime(product, forAll, newDateTime),
        onComplete: onComplete);
  }

  updateAddress(BuildContext context,
      {Function onComplete, bool showLoader = true}) async {
    await callUpdateToCart(
        context,
        _getCartParamsFromList(
            PreferenceManager.getCartResponse().shoppingcartitems),
        onComplete: onComplete,
        showLoader: showLoader);
  }

  updateCouponCode(BuildContext context, String couponCode,
      {Function onComplete}) async {
    await callUpdateToCart(
        context,
        _getCartParamsFromList(
            PreferenceManager.getCartResponse().shoppingcartitems,
            newCouponCode: couponCode,
            clearCouponCode: couponCode == null),
        onComplete: onComplete);
  }

  Map<String, dynamic> _getCartParamsToUpdate() {
    List<CartProduct> cartItems =
        PreferenceManager.getCartResponse()?.shoppingcartitems ?? [];
    AppUtils.log("Intial cart items: ${cartItems.length}");
    _updateCartItemsSequence(cartItems);
    AppUtils.log("Final cart items: ${cartItems.length}");
    return _getCartParamsFromList(cartItems);
  }

  Map<String, dynamic> _getCartParams(CartProduct cartProduct) {
    List<CartProduct> cartItems =
        PreferenceManager.getCartResponse()?.shoppingcartitems ?? [];
    AppUtils.log("Intial cart items: ${cartItems.length}");
    _updateCartItemsWithProduct(cartItems, cartProduct);
    _updateCartItemsSequence(cartItems);
    AppUtils.log("Final cart items: ${cartItems.length}");

    return _getCartParamsFromList(cartItems);
  }

  Map<String, dynamic> _getCartParamsWithChangedQty(
      CartProduct cartProduct, bool increment) {
    List<CartProduct> cartItems =
        PreferenceManager.getCartResponse()?.shoppingcartitems ?? [];
    bool removeProduct = false;
    cartItems.forEach((element) {
      if (element.isSameProductForQtyChange(cartProduct)) {
        double qty = element.quantity;
        qty = increment ? qty + 1 : qty - 1;
        if (qty <= 0) {
          removeProduct = true;
        } else {
          element.quantity = qty;
          element.packets = qty.toInt();
        }
      }
    });
    if (removeProduct) {
      // FBAnalytics.removeFromCart(cartProduct.productId?.toString(), cartProduct.productName, PreferenceManager.getCartId());
      cartItems.removeWhere((element) => element.isSameProduct(cartProduct));
    }
    _updateCartItemsSequence(cartItems);

    return _getCartParamsFromList(cartItems);
  }

  Map<String, dynamic> _getCartParamsWithRemoveProduct(
      CartProduct cartProduct) {
    List<CartProduct> cartItems =
        PreferenceManager.getCartResponse()?.shoppingcartitems ?? [];
    // FBAnalytics.removeFromCart(cartProduct.productId?.toString(), cartProduct.productName, PreferenceManager.getCartId());
    cartItems.removeWhere((element) => element.isSameProduct(cartProduct));
    _updateCartItemsSequence(cartItems);

    return _getCartParamsFromList(cartItems);
  }

  Map<String, dynamic> _getCartParamsWithChangedDateTime(
      CartProduct cartProduct, bool forAll, DateTime newDateTime) {
    List<CartProduct> cartItems =
        PreferenceManager.getCartResponse()?.shoppingcartitems ?? [];
    cartItems.forEach((element) {
      if ((forAll && element.isSameDate(cartProduct.deliveryDate)) ||
          element.id == cartProduct.id) {
        element.updateDateTime(newDateTime);
      }
    });

    _updateCartItemsSequence(cartItems);

    return _getCartParamsFromList(cartItems);
  }

  Map<String, dynamic> _getCartParamsFromList(List<CartProduct> cartItems,
      {String newCouponCode, bool clearCouponCode = false}) {
    LocationData locationData = PreferenceManager.getLocationData();
    CartResponse cartResponse = PreferenceManager.getCartResponse();
    String couponCode = clearCouponCode ? null : newCouponCode;
    if (!clearCouponCode &&
        couponCode == null &&
        cartResponse != null &&
        cartResponse.couponCode != null) {
      couponCode = cartResponse.couponCode;
    }
    var params = {
      "tenantId": ApiPath.TENANT_ID,
      "id": DataManager.get().cartId,
      "addressId":
          "4", //PreferenceManager.isLoggedIn() ? (PreferenceManager.getUserSelectedAddress() != null? PreferenceManager.getUserSelectedAddress()["id"] : locationData.addressId) : locationData.addressId,
      "delivery_date": DataManager.get().delivery_date,
      "delivery_time": DataManager.get().delivery_time,
      "convenience_fee": cartResponse == null
          ? locationData?.zone?.convenienceFee ?? "80"
          : cartResponse.convenienceCharges,
      "products": cartItems.map((e) => e.toCartJson()).toList(),
    };
    /* if (PreferenceManager.getPartyId() != null) {
      params["partyId"] = PreferenceManager.getPartyId();
    }*/
    /*if (couponCode != null) {
      params["couponCode"] = couponCode;
    }*/
    return params;
  }

  _updateCartItemsWithProduct(List<CartProduct> items, CartProduct product) {
    bool alreadyAdded = false;
    items.forEach((element) {
      if (element.isSameProduct(product)) {
        element.update(product);
        alreadyAdded = true;
        return;
      }
    });

    if (!alreadyAdded) {
      items.add(product);
    }
  }

  _updateCartItemsSequence(List<CartProduct> items) {
    items.sort(
        (a, b) => a.getDateTimeToCompare().compareTo(b.getDateTimeToCompare()));
    for (int i = 0; i < items.length; i++) {
      items[i].sequence = i + 1;
    }
  }

  Future<void> associateCartIdWithPartyId(BuildContext context) async {
    if (PreferenceManager.getCartId() != null) {
      Loader.show(context);
      await CartRepository.get().associateCartWithUser(
          PreferenceManager.getCartId(), PreferenceManager.getPartyId());
      Loader.hide();
    }
  }

  bool isSelectedDateTimeDifferent(
      CartProduct cartProduct, DateTime newDateTime, bool forAll) {
    List<CartProduct> cartItems =
        PreferenceManager.getCartResponse()?.shoppingcartitems ?? [];
    bool result = false;
    cartItems.forEach((element) {
      if (element.id != cartProduct.id &&
          ((forAll && !element.isSameDate(cartProduct.deliveryDate)) ||
              !element.isSameDateTime(newDateTime))) {
        result = true;
      }
    });
    return result;
  }
}
