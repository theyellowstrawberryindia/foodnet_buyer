import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:foodnet_buyer/data/data_manager.dart';
import 'package:foodnet_buyer/data/model/LoginTokenResponse.dart';
import 'package:foodnet_buyer/data/repository/main_reporitory.dart';
import 'package:foodnet_buyer/ui/address/address_listing_screen.dart';
import 'package:foodnet_buyer/util/preference_manager.dart';

import 'app_utils.dart';
import 'navigation_util.dart';

class LoginUtil {
  static onLogin(BuildContext context, LoginTokenResponse otpResponse) async {
    PreferenceManager.setLoggedIn(true);
    PreferenceManager.setAccessToken(otpResponse.accessToken);
    PreferenceManager.setRefreshToken(otpResponse.refreshToken);
    PreferenceManager.setPartyCode(otpResponse.partyCode);
    PreferenceManager.setUserPartyId(otpResponse.userpartyId);
    PreferenceManager.setUserId(otpResponse.userId);
    PreferenceManager.setEmail(otpResponse.email);
    PreferenceManager.setMobile(otpResponse.mobile);
    PreferenceManager.setFirstName(otpResponse.firstName);
    PreferenceManager.setLastName(otpResponse.lastName);
    PreferenceManager.saveUserAddress(otpResponse.address);

    try {
      if (DataManager.get().fcmToken == null) {
        AppUtils.log("Saved FCM Token was null fetching token");
        DataManager.get().fcmToken = await FirebaseMessaging().getToken();
      }
      _sendTokenToServer(DataManager.get().fcmToken);

    } catch(e) {
      AppUtils.log("onLogin: Failed to send push details to server. reason ${e.toString()}");
    }

    AppUtils.delayTask(() {
      AppUtils.showToast(otpResponse.message.toString());
      if (otpResponse.address.length > 1) {
        NavigationUtil.clearAllAndAdd(context, AddressListingScreen());
      } else if (otpResponse.address.length == 1) {
        AppUtils.handleOfferNavigation(context, otpResponse.address[0]);
      } else {
        AppUtils.showToast("No addresses found");
      }
    }, delay: Duration(milliseconds: 100));
  }

  static Future _sendTokenToServer(String token) async {
    try {
      AppUtils.log("FCM Token: $token");
      MainRepository.get().saveUserDeviceForNotification(token);
    } catch (e) {
      AppUtils.log(
          "Failed to send push details to server. reason ${e.toString()}");
    }
  }
}
