import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class NavigationUtil {

  static Future<dynamic> clearAllAndAdd(BuildContext context, Widget widget) {
    return Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute( builder: (context) => widget), (route) => false);
  }

  static Future<dynamic> pushToNewScreen(BuildContext context, Widget widget) {
    return Navigator.of(context).push(MaterialPageRoute( builder: (context) => widget));
  }

  static Future<dynamic> pushAndReplaceToNewScreen(BuildContext context, Widget widget) {
    return Navigator.of(context).pushReplacement(MaterialPageRoute( builder: (context) => widget));
  }

  static void pop(BuildContext context, {dynamic result}) {
    Navigator.pop(context, result);
  }

  static bool canPop(BuildContext context) {
    return Navigator.canPop(context);
  }

  static systemPop({bool animated}) {
    SystemNavigator.pop(animated: animated);
  }
}
