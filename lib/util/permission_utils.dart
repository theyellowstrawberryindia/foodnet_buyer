import 'dart:developer';

import 'package:permission_handler/permission_handler.dart';

class PermissionUtil{

  PermissionUtil._internal();

  static PermissionUtil _instance = PermissionUtil._internal();

  static PermissionUtil get instance => _instance;

  Future<bool> requestSinglePermission(Permission permission)async{
    try{

      final result = await permission.request();

      if(result.isPermanentlyDenied){
      return await  openAppSettings();
      }else{
        return result.isGranted;
      }

    }catch(e){
      log(e.toString());
      return false;
    }
  }
}