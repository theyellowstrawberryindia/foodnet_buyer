import 'dart:convert';

import 'package:foodnet_buyer/constant/KeyName.dart';
import 'package:foodnet_buyer/data/data_manager.dart';
import 'package:foodnet_buyer/data/model/cart_response.dart';
import 'package:foodnet_buyer/data/model/location_data.dart';
import 'package:foodnet_buyer/util/app_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferenceManager {
  PreferenceManager._();
  static SharedPreferences _preferences;

  static init() async {
    _preferences = await SharedPreferences.getInstance();
  }

  static void setMobile(String data) {
    _preferences.setString(KeyName.MOBILE_NO, data);
  }

  static String getMobile() {
    return _preferences.getString(KeyName.MOBILE_NO) ?? '';
  }

  static void setEmail(String data) {
    _preferences.setString(KeyName.EMAIL, data);
  }

  static String getEmail() {
    return _preferences.getString(KeyName.EMAIL) ?? '';
  }

  static void setAccessToken(String data) {
    _preferences.setString(KeyName.ACCESS_TOKEN, data);
  }

  static String getAccessToken() {
    return _preferences.getString(KeyName.ACCESS_TOKEN);
  }

  static void setRefreshToken(String data) {
    _preferences.setString(KeyName.REFRESH_TOKEN, data);
  }

  static String getRefreshToken() {
    return _preferences.getString(KeyName.REFRESH_TOKEN);
  }

  static void setToken(String data) async {
    _preferences = await SharedPreferences.getInstance();
    _preferences.setString(KeyName.TOKEN, data);
  }

  static String getToken() {
    return _preferences.getString(KeyName.TOKEN);
  }

  static void setFirstName(String data) {
    _preferences.setString(KeyName.FIRST_NAME, data);
  }

  static String getFirstName() {
    return _preferences.getString(KeyName.FIRST_NAME);
  }

  static void setLastName(String data) {
    _preferences.setString(KeyName.LAST_NAME, data);
  }

  static String getLastName() {
    return _preferences.getString(KeyName.LAST_NAME);
  }

  static void setOtp(String data) {
    _preferences.setString(KeyName.OTP, data);
  }

  static String getOtp() {
    return _preferences.getString(KeyName.OTP);
  }

  static void isLogin(bool flag) {
    _preferences.setBool(KeyName.IS_LOGIN, flag);
  }

  static bool getLogin() {
    return _preferences.getBool(KeyName.IS_LOGIN) ?? false;
  }

  static void setAddressPicked(bool flag) {
    _preferences.setBool(KeyName.ADDRESS_PICKED, flag);
  }

  static bool isAddressPicked() {
    return _preferences.getBool(KeyName.ADDRESS_PICKED) ?? false;
  }

  static bool alreadyLoggedIn() => getAccessToken().isNotEmpty;

  static String getLatitude() => _preferences.getString(KeyName.LATITUDE);

  static setLatitude(String lat) =>
      _preferences.setString(KeyName.LATITUDE, lat);

  static String getLongitude() => _preferences.getString(KeyName.LONGITUDE);

  static setLongitude(String lng) =>
      _preferences.setString(KeyName.LONGITUDE, lng);

  static String getAddress() => _preferences.getString(KeyName.ADDRESS);

  static setAddress(String address) =>
      _preferences.setString(KeyName.ADDRESS, address);

  static String getCartId() => _preferences.getString(KeyName.CART_ID);

  static setCartId(String cartId) =>
      _preferences.setString(KeyName.CART_ID, cartId);

  static String getPartyId() => _preferences.getString(KeyName.PARTY_ID);

  static setPartyId(String partyId) =>
      _preferences.setString(KeyName.PARTY_ID, partyId);

  static String getBuyerId() => _preferences.getString(KeyName.BUYER_ID);

  static setBuyerId(String buyerId) =>
      _preferences.setString(KeyName.BUYER_ID, buyerId);

  static String getPartyLocationId() =>
      _preferences.getString(KeyName.PARTY_LOCATION_ID);

  static setPartyLocationId(String partyLocationId) =>
      _preferences.setString(KeyName.PARTY_LOCATION_ID, partyLocationId);

  static String getPartyCode() => _preferences.getString(KeyName.PARTY_CODE);

  static setPartyCode(String partyCode) =>
      _preferences.setString(KeyName.PARTY_CODE, partyCode);

  static String getUserId() => _preferences.getString(KeyName.USER_ID);

  static setUserId(String userId) =>
      _preferences.setString(KeyName.USER_ID, userId);

  static int getUserPartyId() => _preferences.getInt(KeyName.USER_PARTY_ID);

  static setUserPartyId(int userPartyId) =>
      _preferences.setInt(KeyName.USER_PARTY_ID, userPartyId);

//  static bool shouldAssociateCart() => _preferences.getBool(KeyName.ASSOCIATE_CART);
//
//  static setAssociateCart(bool value) => _preferences.setBool(KeyName.ASSOCIATE_CART, value);

  static getName() {
    return getFirstName() != null
        ? (getFirstName() + " " + getLastName())
        : null;
  }

  static LocationData getLocationData() {
    if (DataManager.get().getLocationData() == null) {
      String data = _preferences.getString(KeyName.LOCATION_DATA);
      if (data != null) {
        LocationData locationData = LocationData.fromJson(json.decode(data));
        DataManager.get().setLocationData(locationData);
      }
    }
    return DataManager.get().getLocationData();
  }

  static saveLocationData(LocationData locationData) {
    DataManager.get().setLocationData(locationData);
    String data = json.encode(locationData.toJson());
    _preferences.setString(KeyName.LOCATION_DATA, data);
//    PreferenceManager.locationData = locationData;
  }

  static CartResponse getCartResponse() {
    String data = _preferences.getString(KeyName.CART_RESPONSE);
    if (data != null) {
      CartResponse cartResponse = CartResponse.fromJson(json.decode(data));
      return cartResponse;
    }
    return null;
  }

  static saveCartResponse(CartResponse cartResponse) {
    String data =
        cartResponse != null ? json.encode(cartResponse.toJson()) : null;
    _preferences.setString(KeyName.CART_RESPONSE, data);
  }

  static List<dynamic> getUserAddress() {
    String data = _preferences.getString(KeyName.USER_ADDRESS);
    if (data != null) {
      List<dynamic> addresses = json.decode(data);
      return addresses;
    }
    return [];
  }

  static saveUserAddress(List<dynamic> address) {
    String data = address != null ? json.encode(address) : [];
    _preferences.setString(KeyName.USER_ADDRESS, data);
  }

  static dynamic getUserSelectedAddress() {
    String data = _preferences.getString(KeyName.USER_SELECTED_ADDRESS);
    if (data != null) {
      return json.decode(data);
    }
    return null;
  }

  static saveUserSelectedAddress(dynamic address) {
    String data = address != null ? json.encode(address) : null;
    _preferences.setString(KeyName.USER_SELECTED_ADDRESS, data);
  }

  static String getDisplayAddress() {
    dynamic address = getUserSelectedAddress();
    if (address != null) {
      return AppUtils.addressFromObj(address);
    } else {
      return getAddress();
    }
  }

  static bool isLoggedIn() =>
      _preferences.getBool(KeyName.IS_LOGGED_IN) ?? false;

  static setLoggedIn(bool value) =>
      _preferences.setBool(KeyName.IS_LOGGED_IN, value);

  static bool showNotificationBadge() =>
      _preferences.getBool(KeyName.SHOW_NOTIFICATION_BADGE) ?? false;

  static setShowNotificationBadge(bool value) =>
      _preferences.setBool(KeyName.SHOW_NOTIFICATION_BADGE, value);

  static bool hasSavedLocation() {
    return getLatitude() != null &&
        getLongitude() != null &&
        getAddress() != null;
  }

  static void clearAll() async {
    // LocationData data = getLocationData();
    // String lat = PreferenceManager.getLatitude();
    // String lng = PreferenceManager.getLongitude();
    _preferences.clear();
    // saveLocationData(data);
    // setLatitude(lat);
    // setLongitude(lng);
    // setAddress(await MapsUtil.addressFromLocation(double.parse(lat), double.parse(lng)));
  }

  static setAddressSelectionID(int addressId) =>
      _preferences.setInt(KeyName.ADDRESS_ID, addressId);

  static int getAddressSelectionID() => _preferences.getInt(KeyName.ADDRESS_ID);

  static setStoreCode(String storeCode) =>
      _preferences.setString(KeyName.STORE_CODE, storeCode);

  static String getStoreCode() => _preferences.getString(KeyName.STORE_CODE);

  static setStoreID(int storeID) =>
      _preferences.setInt(KeyName.STORE_ID, storeID);

  static int getStoreID() => _preferences.getInt(KeyName.STORE_ID);
}
