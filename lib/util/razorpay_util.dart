

import 'package:foodnet_buyer/util/preference_manager.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

class RazorPayUtil{
  RazorPayUtil._internal();

  static RazorPayUtil _instance = RazorPayUtil._internal();

  static RazorPayUtil get instance => _instance;

  Razorpay _razorPay;
  Function(String transactionId) onSuccess;
  Function(String) onFailure;

  init() {
    _razorPay = Razorpay();
    _razorPay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorPay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorPay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  open(double amount, String name, String desc,
      {Function(String) success, Function(String) failure}) {
    this.onSuccess = success;
    this.onFailure = failure;
    _razorPay.open(_getOptions(amount, name, desc));
  }

  dynamic _getOptions(double amount, String name, String desc) {
//    kDebugMode ? 'rzp_test_bxHTff2ATMg1Zc' : 'rzp_live_hTCoRgT8zsNsjP',
    var options = {
      'key': 'rzp_test_bxHTff2ATMg1Zc',
      'amount': (amount * 100).round(),
      'name': name,
      'description': desc,
      'prefill': {
        'contact': PreferenceManager.getMobile(),
        'email': PreferenceManager.getEmail()??''
      },
      'theme':{
        'color':'#F0674C'
      }
    };
    return options;
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    onSuccess?.call(response.paymentId);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    // Do something when payment fails
    onFailure?.call(response.message);
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    // Do something when an external wallet was selected
    onFailure?.call("Error processing payment");
  }

  clear() {
    _razorPay.clear();
  }
}