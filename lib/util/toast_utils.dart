
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:foodnet_buyer/style/style.dart';

class ToastUtils{
 static FToast _ftost;
 static ToastUtils _instance;


 ToastUtils();

  static ToastUtils init(BuildContext context) {
    if(_instance==null)
      _instance = ToastUtils();

   _ftost = FToast();
   _ftost.init(context);

   return _instance;
 }


 showSuccessToast(String message){
   Widget toast = Container(
     padding: const EdgeInsets.all(14),
     decoration: BoxDecoration(
       borderRadius: BorderRadius.circular(10),
       color: Colors.white,
       border: Border.all(color: Colors.green),
     ),
     child: Row(
       mainAxisSize: MainAxisSize.min,
       children: [
         Icon(Icons.check,color: Colors.green,),
         const  SizedBox( width: 12.0,),
         Style.getPoppinsRegularText(message, 16, Colors.green,maxLines: 2),
         Expanded(child: Container())
       ],
     ),
   );


   _ftost.showToast(
     child: toast,
     gravity: ToastGravity.TOP,
     toastDuration: Duration(seconds: 3),
   );
 }



}